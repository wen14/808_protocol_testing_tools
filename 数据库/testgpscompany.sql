/*
SQLyog Ultimate v11.3 (32 bit)
MySQL - 5.1.56-community : Database - testgpscompany
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`testgpscompany` /*!40100 DEFAULT CHARACTER SET gbk */;

USE `testgpscompany`;

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `ConfigName` char(64) COLLATE gbk_bin NOT NULL COMMENT '置配名',
  `ConfigValue` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '置配值',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ConfigName`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='系统参数表';

/*Data for the table `config` */

insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsA1Value','20000000','加密参数2');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsC1Value','30000000','加密参数1');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsM1Value','10000000','加密参数3');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('检测机构代码','bjhsjc','检测机构代码/简称');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('终端鉴权码','jtpassword','默认的终端鉴权码,暂定测试系统所有终端都使用此鉴权码进行鉴权');

/*Table structure for table `environment` */

DROP TABLE IF EXISTS `environment`;

CREATE TABLE `environment` (
  `EnvironmentID` int(11) NOT NULL,
  `EnvironmentName` varchar(64) COLLATE gbk_bin NOT NULL,
  `EnvironmentDesc` varchar(500) COLLATE gbk_bin DEFAULT NULL,
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `TestFlag` int(11) DEFAULT '1',
  PRIMARY KEY (`EnvironmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试环境表';

/*Data for the table `environment` */

insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (1,'下级平台协议','',1,1);
insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (2,'终端环境','',2,1);
insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (3,'下级平台流程','',1,2);

/*Table structure for table `eventlog` */

DROP TABLE IF EXISTS `eventlog`;

CREATE TABLE `eventlog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `Flag` int(11) NOT NULL COMMENT '0=系统错误日志 1=上行 2=下行',
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标',
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL DEFAULT '0' COMMENT '测试编号',
  `EventDate` datetime NOT NULL COMMENT '发生时间',
  `ExampleID` int(11) DEFAULT NULL COMMENT '用例编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `SerialNumber` int(11) NOT NULL COMMENT '消息流水号',
  `ResultFlag` int(11) DEFAULT NULL COMMENT '0=正常 1=错误 2=不能识别',
  `OraginMsg` longblob COMMENT '解析前的原始数据',
  `OutMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '日志描叙',
  `MediaType` int(11) DEFAULT NULL COMMENT '0=无附加数据 1=图片 2=多媒体（声音/视频）',
  `MediaData` longblob COMMENT '存储多媒体图片数据',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `IsAlarm` int(11) DEFAULT NULL,
  `AlarmDescription` varchar(200) COLLATE gbk_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Index1_EventLog` (`TestID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试日志表';

/*Data for the table `eventlog` */

insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (1,2,'bjhsjc220111111000101','bjhsjc22011111100010101','2011-11-11 10:26:14',215,'',1,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (2,2,'bjhsjc220111111000101','bjhsjc22011111100010102','2011-11-11 10:36:27',215,'',2,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (3,2,'bjhsjc220111111000301','bjhsjc22011111100030102','2011-11-11 17:04:49',215,'',1,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (4,2,'bjhsjc220111111000301','bjhsjc22011111100030102','2011-11-11 17:07:36',18,'',2,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (5,2,'bjhsjc220111111000301','bjhsjc22011111100030102','2011-11-11 17:07:51',29,'',3,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (6,2,'bjhsjc220111111000301','bjhsjc22011111100030103','2011-11-11 17:08:14',215,'',4,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (7,2,'bjhsjc220111111000301','bjhsjc22011111100030105','2011-11-11 17:45:03',215,'',1,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (8,2,'bjhsjc220140430000201','bjhsjc22014043000020101','2014-04-30 15:59:55',215,'',1,0,NULL,'',NULL,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (9,1,'0','0','2014-05-05 09:29:15',0,'0100',9,1,'~\0\0-5�RQ	\0	\0{\0{C0291123\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01585953��A12345�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (10,1,'0','0','2014-05-06 10:48:31',0,'0002',99,1,'~\0\0\03\0\0\0A\0c~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (11,1,'0','0','2014-05-06 14:43:52',0,'0100',21,1,'~\0\0-3\0\0\0\0\0Mf}9PAB123456\0RPR\0\0\0h1330008��K9005\0G~','终端注册：省城ID=11, 市县城ID=1101,制造商ID=f,终端型号=AB123456,终端ID=1330008,车辆颜色=2,车牌= 闽K9005',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (12,1,'0','0','2014-05-06 17:03:14',0,'0002',917,1,'~\0\0\03\0\0\0A��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (13,1,'0','0','2014-05-06 18:11:11',0,'0002',1089,1,'~\0\0\03\0\0\0AA4~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (14,1,'0','0','2014-05-06 18:21:57',0,'0002',462,1,'~\0\0\03\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (15,1,'0','0','2014-05-06 18:24:23',0,'0002',821,1,'~\0\0\05�RQ	5�~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (16,1,'0','0','2014-05-07 09:11:05',0,'0002',4,1,'~\0\0\05�RQ	\0�~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (17,1,'0','0','2014-05-07 16:18:16',0,'0002',194,1,'~\0\0\03\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (18,1,'0','0','2014-05-08 10:13:40',0,'0002',176,1,'~\0\0\03\0\0\0A\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (19,1,'0','0','2014-05-08 10:36:59',0,'0002',191,1,'~\0\0\03\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (20,1,'0','0','2014-05-08 10:49:47',0,'0002',240,1,'~\0\0\03\0\0\0A\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (21,1,'0','0','2014-05-08 16:17:35',0,'0002',942,1,'~\0\0\03\0\0\0A��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (22,1,'0','0','2014-05-09 11:23:44',0,'0002',47,1,'~\0\0\05�RQ	\0/�~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (23,1,'0','0','2014-05-09 11:24:19',0,'0102',49,1,'~\05�RQ	\01mEmeV8vB�~','终端上行鉴权指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (24,1,'0','0','2014-05-13 15:42:53',0,'0002',59,1,'~\0\0\03\0\0\0\0;~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (25,1,'0','0','2014-05-13 15:57:49',0,'0002',23,1,'~\0\0\03\0\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (26,1,'0','0','2014-05-13 17:03:57',0,'0002',329,1,'~\0\0\03\0\0\0Ip~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (27,1,'0','0','2014-05-13 17:12:15',0,'0002',22,1,'~\0\0\03\0\0\0\0.~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);

/*Table structure for table `eventlog_copy` */

DROP TABLE IF EXISTS `eventlog_copy`;

CREATE TABLE `eventlog_copy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `Flag` int(11) NOT NULL COMMENT '0=系统错误日志 1=上行 2=下行',
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标',
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL DEFAULT '0' COMMENT '测试编号',
  `EventDate` datetime NOT NULL COMMENT '发生时间',
  `ExampleID` int(11) DEFAULT NULL COMMENT '用例编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `SerialNumber` int(11) NOT NULL COMMENT '消息流水号',
  `ResultFlag` int(11) DEFAULT NULL COMMENT '0=正常 1=错误 2=不能识别',
  `OraginMsg` longblob COMMENT '解析前的原始数据',
  `OutMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '日志描叙',
  `MediaType` int(11) DEFAULT NULL COMMENT '0=无附加数据 1=图片 2=多媒体（声音/视频）',
  `MediaData` longblob COMMENT '存储多媒体图片数据',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `IsAlarm` int(11) DEFAULT NULL,
  `AlarmDescription` varchar(200) COLLATE gbk_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Index1_EventLog` (`TestID`)
) ENGINE=InnoDB AUTO_INCREMENT=92594 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试日志表';

/*Data for the table `eventlog_copy` */

insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91394,1,'0','0','2011-09-26 15:21:54',0,'0100',8,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\06~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91395,1,'0','0','2011-09-26 15:36:17',0,'0100',8,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\06~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91396,1,'0','0','2011-09-27 16:55:04',0,'0100',1,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0?~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91397,1,'0','0','2011-09-27 16:55:34',0,'0100',2,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0<~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91398,1,'0','0','2011-09-27 16:56:04',0,'0100',3,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0=~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91399,1,'0','0','2011-09-27 16:56:34',0,'0100',4,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0:~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91400,1,'0','0','2011-10-10 15:57:03',0,'0100',9,1,'~\0\06��\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\07~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91401,0,'0','0','2011-10-10 15:57:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91402,1,'0','0','2011-10-10 15:58:03',0,'0100',11,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\05~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91403,0,'0','0','2011-10-10 15:58:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91404,1,'0','0','2011-10-10 15:59:03',0,'0100',13,0,'~\0\06��\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\03~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91405,1,'0','0','2011-10-10 15:59:33',0,'0100',14,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\00~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91406,0,'0','0','2011-10-10 16:00:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91407,1,'0','0','2011-10-10 16:00:33',0,'0100',16,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0.~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91408,1,'0','0','2011-10-10 16:01:03',0,'0100',17,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0/~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91409,1,'0','0','2011-10-10 16:01:33',0,'0100',18,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0,~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91410,0,'0','0','2011-10-10 16:02:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91411,1,'0','0','2011-10-10 16:02:33',0,'0100',20,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0*~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91412,1,'0','0','2011-10-10 16:03:03',0,'0100',21,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0+~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91413,0,'0','0','2011-10-10 16:03:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91414,1,'0','0','2011-10-10 16:04:03',0,'0100',23,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0)~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91415,1,'0','0','2011-10-10 16:04:33',0,'0100',23,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0)~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91416,1,'0','0','2011-10-10 16:05:03',0,'0100',25,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\'~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91417,1,'0','0','2011-10-10 16:05:33',0,'0100',25,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\'~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91418,1,'0','0','2011-10-10 16:06:03',0,'0100',27,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0%~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91419,0,'0','0','2011-10-10 16:06:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91420,1,'0','0','2011-10-10 16:07:03',0,'0100',29,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0#~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91421,1,'0','0','2011-10-10 16:07:33',0,'0100',29,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0#~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91422,1,'0','0','2011-10-10 16:08:03',0,'0100',31,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0!~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91423,0,'0','0','2011-10-10 16:08:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91424,1,'0','0','2011-10-10 16:09:03',0,'0100',33,0,'~\0\06��\0!\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91425,1,'0','0','2011-10-10 16:09:33',0,'0100',34,1,'~\0\06��\0\"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91426,0,'0','0','2011-10-10 16:10:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91427,1,'0','0','2011-10-10 16:10:33',0,'0100',36,0,'~\0\06��\0$\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91428,1,'0','0','2011-10-10 16:11:03',0,'0100',37,1,'~\0\06��\0%\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91429,0,'0','0','2011-10-10 16:11:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91430,1,'0','0','2011-10-10 16:12:03',0,'0100',39,1,'~\0\06��\0\'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91431,1,'0','0','2011-10-10 16:12:33',0,'0100',39,0,'~\0\06��\0\'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91432,1,'0','0','2011-10-10 16:13:03',0,'0100',41,0,'~\0\06��\0)\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91433,1,'0','0','2011-10-10 16:13:33',0,'0100',42,1,'~\0\06��\0*\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91434,0,'0','0','2011-10-10 16:14:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91435,1,'0','0','2011-10-10 16:14:33',0,'0100',44,1,'~\0\06��\0,\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91436,1,'0','0','2011-10-10 16:15:03',0,'0100',44,0,'~\0\06��\0,\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91437,1,'0','0','2011-10-10 16:15:33',0,'0100',46,1,'~\0\06��\0.\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91438,0,'0','0','2011-10-10 16:16:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91439,1,'0','0','2011-10-10 16:16:33',0,'0100',48,1,'~\0\06��\00\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91440,0,'0','0','2011-10-10 16:17:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91441,1,'0','0','2011-10-10 16:17:33',0,'0100',50,0,'~\0\06��\02\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91442,1,'0','0','2011-10-10 16:18:03',0,'0100',51,1,'~\0\06��\03\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91443,1,'0','0','2011-10-10 16:18:33',0,'0100',51,0,'~\0\06��\03\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91444,1,'0','0','2011-10-10 16:19:03',0,'0100',53,1,'~\0\06��\05\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91445,1,'0','0','2011-10-10 16:19:33',0,'0100',53,0,'~\0\06��\05\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91446,1,'0','0','2011-10-10 16:20:03',0,'0100',55,1,'~\0\06��\07\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91447,0,'0','0','2011-10-10 16:20:33',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91448,1,'0','0','2011-10-10 16:21:03',0,'0100',57,0,'~\0\06��\09\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91449,1,'0','0','2011-10-10 16:21:33',0,'0100',58,1,'~\0\06��\0:\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91450,0,'0','0','2011-10-10 16:22:03',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91451,1,'0','0','2011-10-10 16:22:33',0,'0100',60,1,'~\0\06��\0<\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91452,1,'0','0','2011-10-10 16:32:52',0,'0100',14,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\00~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91453,0,'0','0','2011-10-10 16:33:22',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91454,1,'0','0','2011-10-10 16:33:52',0,'0100',16,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0.~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91455,1,'0','0','2011-10-10 16:34:22',0,'0100',17,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0/~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91456,1,'0','0','2011-10-10 16:34:52',0,'0100',17,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0/~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91457,1,'0','0','2011-10-10 16:35:22',0,'0100',19,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0-~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91458,1,'0','0','2011-10-10 16:35:52',0,'0100',19,0,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0-~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91459,1,'0','0','2011-10-10 16:36:22',0,'0100',21,1,'~\0\06��\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0+~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91460,1,'123456','','2011-10-12 20:53:25',-1,'0x1001',16,1,'[\0\0\0H\0\0\0\0\0�v1.\0\0��\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o��]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91461,1,'123456','','2011-10-12 20:53:27',-1,'0x1005',17,1,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0����]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91462,1,'123456','','2011-10-12 20:53:41',-1,'0x9005',18,1,'[\0\0\0\Z\0\0\0�\0\0�v1.\0\0��5�]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91463,1,'123456','','2011-10-13 09:42:42',-1,'0x1001',1,1,'[\0\0\0H\0\0\0\0\0�v1.\0\0k�\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o�]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91464,1,'123456','','2011-10-13 09:42:44',-1,'0x1005',2,1,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0k�]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91465,1,'123456','','2011-10-13 09:43:14',-1,'0x9005',3,1,'[\0\0\0\Z\0\0\0�\0\0�v1.\0\0k���]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91466,1,'123456','','2011-10-13 13:53:31',-1,'0xABAB',-257049941,1,'0','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91467,1,'123456','','2011-10-13 14:27:26',-1,'0x1001',9,0,'[\0\0\0H\0\0\0	\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o£]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91468,1,'123456','','2011-10-13 14:27:44',-1,'0x1005',10,0,'[\0\0\0\Z\0\0\0\n\0\0�v1.\0\0\0\0\0\n�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91469,1,'123456','','2011-10-13 15:35:24',-1,'0x1001',1,0,'[\0\0\0H\0\0\0\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o��]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91470,1,'123456','','2011-10-13 15:41:35',-1,'0x1001',4,0,'[\0\0\0H\0\0\0\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o��]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91471,2,'123456','','2011-10-13 15:42:09',-1,'0x1002',4,0,'[\0\0\0\0\0\0\0\0�\0\0\0\0\0*\0\0\0ZO]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91472,1,'123456','','2011-10-13 15:42:09',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91473,2,'123456','','2011-10-13 15:42:09',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91474,1,'123456','','2011-10-13 15:42:40',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91475,2,'123456','','2011-10-13 15:42:40',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91476,1,'123456','','2011-10-13 15:43:11',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91477,2,'123456','','2011-10-13 15:43:11',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91478,1,'123456','','2011-10-13 15:43:41',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91479,2,'123456','','2011-10-13 15:43:42',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91480,1,'123456','','2011-10-13 15:44:13',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91481,2,'123456','','2011-10-13 15:44:13',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91482,1,'123456','','2011-10-13 15:44:44',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91483,2,'123456','','2011-10-13 15:44:44',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91484,1,'123456','','2011-10-13 15:45:15',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91485,2,'123456','','2011-10-13 15:45:15',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91486,1,'123456','','2011-10-13 15:45:46',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91487,2,'123456','','2011-10-13 15:45:46',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91488,1,'123456','','2011-10-13 15:46:17',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91489,2,'123456','','2011-10-13 15:46:23',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91490,1,'123456','','2011-10-13 15:46:47',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91491,2,'123456','','2011-10-13 15:46:48',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91492,1,'123456','','2011-10-13 15:47:19',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91493,2,'123456','','2011-10-13 15:47:19',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91494,1,'123456','','2011-10-13 15:47:50',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91495,2,'123456','','2011-10-13 15:47:50',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91496,1,'123456','','2011-10-13 15:48:20',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0GU]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91497,2,'123456','','2011-10-13 15:48:21',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91498,1,'123456','','2011-10-13 16:37:27',-1,'0x1001',1,0,'[\0\0\0H\0\0\0\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o�]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91499,2,'123456','','2011-10-13 16:37:28',-1,'0x1002',1,0,'[\0\0\0\0\0\0\0\0�\0\0\0\0\0*\0\0\0�0]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91500,1,'123456','','2011-10-13 16:37:52',-1,'0x1005',2,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0sH]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91501,2,'123456','','2011-10-13 16:37:52',-1,'0x1006',2,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91502,1,'123456','','2011-10-13 16:45:11',-1,'0x1001',4,0,'[\0\0\0H\0\0\0\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o6�]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91503,2,'123456','','2011-10-13 16:45:12',-1,'0x1002',4,0,'[\0\0\0\0\0\0\0\0�\0\0\0\0\0*\0\0\0ZO]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91504,1,'123456','','2011-10-13 16:45:42',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91505,2,'123456','','2011-10-13 16:45:42',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91506,1,'123456','','2011-10-13 16:46:12',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91507,2,'123456','','2011-10-13 16:46:12',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91508,1,'123456','','2011-10-13 16:46:43',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91509,2,'123456','','2011-10-13 16:46:44',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91510,1,'123456','','2011-10-13 16:47:13',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91511,2,'123456','','2011-10-13 16:47:14',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91512,1,'123456','','2011-10-13 16:47:44',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91513,2,'123456','','2011-10-13 16:47:44',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91514,1,'123456','','2011-10-13 16:47:52',-1,'0x1200',7,0,'[\0\0\0g\0\0\0\0\0\0�v1.\0\0\0\0\0��A25307\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0\0\0\0#Eg�\0\0\0\0\0\0#Eg�\0\0\0\0#Eg\0\0\0Vww13812345323\0��]','收到车辆注册信息，车牌号:粤A25307，车辆颜色编号:1， 子业务标识:1201，平台唯一编码:，车载终端厂商唯一编码:，车载终端型号:，车载终端编号:，车载终端SIM卡号:13812345323',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91515,1,'123456','','2011-10-13 16:48:07',-1,'0x0812',33554432,1,'[\0\0\0Z\0\0\0\0\0\0�v1.\0\0)��A25007\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0$\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0+�]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91516,1,'123456','','2011-10-13 16:48:14',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91517,2,'123456','','2011-10-13 16:48:15',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91518,1,'123456','','2011-10-13 16:48:45',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91519,2,'123456','','2011-10-13 16:48:45',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91520,1,'123456','','2011-10-13 16:49:15',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91521,2,'123456','','2011-10-13 16:49:16',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91522,1,'123456','','2011-10-13 16:49:34',-1,'0x0912',16777216,1,'[\0\0\0Z\0\0\0	\0\0\0�v1.\0\0)��A35201\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0%\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0.S]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91523,1,'123456','','2011-10-13 16:49:46',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91524,2,'123456','','2011-10-13 16:49:49',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91525,1,'123456','','2011-10-13 16:49:51',-1,'0x1200',10,0,'[\0\0\0F\0\0\0\n\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N�\r\0\0\0\0NFm��]','收到申请交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1207',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91526,1,'123456','','2011-10-13 16:50:16',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91527,2,'123456','','2011-10-13 16:50:17',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91528,1,'123456','','2011-10-13 16:50:21',-1,'0x1200',11,0,'[\0\0\06\0\0\0\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�]','收到取消交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1208，数据为：0',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91529,1,'123456','','2011-10-13 16:50:48',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91530,2,'123456','','2011-10-13 16:50:49',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91531,1,'123456','','2011-10-13 16:50:51',-1,'0x1200',12,0,'[\0\0\0F\0\0\0\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0\0N�\r\0\0\0\0NFmEo]','收到补发车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1209',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91532,1,'123456','','2011-10-13 16:51:19',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91533,2,'123456','','2011-10-13 16:51:19',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91534,1,'123456','','2011-10-13 16:51:27',-1,'0x9200',13,1,'[\0\0\06\0\0\0\r�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�%]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91535,1,'123456','','2011-10-13 16:51:50',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91536,2,'123456','','2011-10-13 16:51:50',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91537,1,'123456','','2011-10-13 16:53:41',-1,'0x1001',14,0,'[\0\0\0H\0\0\0\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"oh]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91538,2,'123456','','2011-10-13 16:53:41',-1,'0x1002',14,0,'[\0\0\0\0\0\0\0\0�\0\0\0\0\0*\0\0\0��]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91539,2,'123456','','2011-10-13 16:53:41',-1,'0x9001',0,0,'[\0\0\0\0\0\0\0�\0\0�\0\0\0\0H$\0\0��]','从链路连接请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91540,1,'123456','','2011-10-13 16:53:42',-1,'0x9002',19,0,'[\0\0\0\0\0\0�\0\0�\0\0\0\0\0\0\0\0\00_]','验证结果：0。target:123456',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91541,1,'123456','','2011-10-13 16:54:02',-1,'0x1005',5,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0�]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91542,2,'123456','','2011-10-13 16:54:02',-1,'0x1006',5,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0p]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91543,1,'123456','','2011-10-13 16:54:11',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91544,2,'123456','','2011-10-13 16:54:12',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91545,1,'123456','','2011-10-13 16:54:32',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91546,2,'123456','','2011-10-13 16:54:32',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91547,2,'123456','','2011-10-13 16:54:41',-1,'0x9005',1,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0sg]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91548,1,'123456','','2011-10-13 16:54:52',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91549,2,'123456','','2011-10-13 16:54:52',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91550,1,'123456','','2011-10-13 16:55:13',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91551,2,'123456','','2011-10-13 16:55:13',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91552,1,'123456','','2011-10-13 16:55:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91553,2,'123456','','2011-10-13 16:55:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91554,2,'123456','','2011-10-13 16:55:41',-1,'0x9005',2,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�B]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91555,1,'123456','','2011-10-13 16:55:53',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91556,2,'123456','','2011-10-13 16:55:54',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91557,1,'123456','','2011-10-13 16:56:18',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91558,2,'123456','','2011-10-13 16:56:18',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91559,1,'123456','','2011-10-13 16:56:18',-1,'0x1200',17,0,'[\0\0\0g\0\0\0\0\0\0�v1.\0\0\0\0\0��A25307\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0\0\0\0#Eg�\0\0\0\0\0\0#Eg�\0\0\0\0#Eg\0\0\0Vww13812345323\0�]','收到车辆注册信息，车牌号:粤A25307，车辆颜色编号:1， 子业务标识:1201，平台唯一编码:，车载终端厂商唯一编码:，车载终端型号:，车载终端编号:，车载终端SIM卡号:13812345323',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91560,1,'123456','','2011-10-13 16:56:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91561,2,'123456','','2011-10-13 16:56:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91562,2,'123456','','2011-10-13 16:56:41',-1,'0x9005',3,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0>�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91563,1,'123456','','2011-10-13 16:56:54',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91564,2,'123456','','2011-10-13 16:56:55',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91565,1,'123456','','2011-10-13 16:57:16',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91566,2,'123456','','2011-10-13 16:57:17',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91567,1,'123456','','2011-10-13 16:57:37',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91568,2,'123456','','2011-10-13 16:57:39',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91569,2,'123456','','2011-10-13 16:57:41',-1,'0x9005',4,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0F)]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91570,1,'123456','','2011-10-13 16:57:57',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91571,2,'123456','','2011-10-13 16:57:57',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91572,1,'123456','','2011-10-13 16:58:18',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91573,2,'123456','','2011-10-13 16:58:18',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91574,1,'123456','','2011-10-13 16:58:38',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91575,2,'123456','','2011-10-13 16:58:41',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91576,2,'123456','','2011-10-13 16:58:41',-1,'0x9005',5,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91577,1,'123456','','2011-10-13 16:58:59',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91578,2,'123456','','2011-10-13 16:58:59',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91579,1,'123456','','2011-10-13 16:59:19',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91580,2,'123456','','2011-10-13 16:59:19',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91581,1,'123456','','2011-10-13 16:59:39',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91582,2,'123456','','2011-10-13 16:59:41',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91583,2,'123456','','2011-10-13 16:59:41',-1,'0x9005',6,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91584,1,'123456','','2011-10-13 16:59:59',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91585,2,'123456','','2011-10-13 17:00:00',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91586,1,'123456','','2011-10-13 17:00:20',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91587,2,'123456','','2011-10-13 17:00:20',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91588,2,'123456','','2011-10-13 17:00:41',-1,'0x9005',7,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91589,1,'123456','','2011-10-13 17:00:41',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91590,2,'123456','','2011-10-13 17:00:42',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91591,1,'123456','','2011-10-13 17:01:02',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91592,2,'123456','','2011-10-13 17:01:02',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91593,1,'123456','','2011-10-13 17:01:22',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91594,2,'123456','','2011-10-13 17:01:23',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91595,2,'123456','','2011-10-13 17:01:41',-1,'0x9005',8,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91596,1,'123456','','2011-10-13 17:01:43',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91597,2,'123456','','2011-10-13 17:01:43',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91598,1,'123456','','2011-10-13 17:02:03',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91599,2,'123456','','2011-10-13 17:02:04',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91600,1,'123456','','2011-10-13 17:02:23',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91601,2,'123456','','2011-10-13 17:02:24',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91602,2,'123456','','2011-10-13 17:02:41',-1,'0x9005',9,0,'[\0\0\0\Z\0\0\0	�\0\0�\0\0\0\0\0\0\0T\"]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91603,1,'123456','','2011-10-13 17:02:44',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91604,2,'123456','','2011-10-13 17:02:44',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91605,1,'123456','','2011-10-13 17:03:06',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91606,2,'123456','','2011-10-13 17:03:07',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91607,1,'123456','','2011-10-13 17:03:24',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91608,2,'123456','','2011-10-13 17:03:24',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91609,2,'123456','','2011-10-13 17:03:41',-1,'0x9005',10,0,'[\0\0\0\Z\0\0\0\n�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91610,1,'123456','','2011-10-13 17:03:45',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91611,2,'123456','','2011-10-13 17:03:45',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91612,1,'123456','','2011-10-13 17:04:05',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91613,2,'123456','','2011-10-13 17:04:05',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91614,1,'123456','','2011-10-13 17:04:25',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91615,2,'123456','','2011-10-13 17:04:27',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91616,2,'123456','','2011-10-13 17:04:41',-1,'0x9005',11,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91617,1,'123456','','2011-10-13 17:04:46',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91618,2,'123456','','2011-10-13 17:04:47',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91619,1,'123456','','2011-10-13 17:05:07',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91620,2,'123456','','2011-10-13 17:05:07',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91621,1,'123456','','2011-10-13 17:05:36',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91622,2,'123456','','2011-10-13 17:05:36',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91623,2,'123456','','2011-10-13 17:05:41',-1,'0x9005',12,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0al]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91624,1,'123456','','2011-10-13 17:05:56',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91625,2,'123456','','2011-10-13 17:05:56',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91626,1,'123456','','2011-10-13 17:06:16',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91627,2,'123456','','2011-10-13 17:06:17',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91628,1,'123456','','2011-10-13 17:06:37',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91629,2,'123456','','2011-10-13 17:06:37',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91630,2,'123456','','2011-10-13 17:06:41',-1,'0x9005',13,0,'[\0\0\0\Z\0\0\0\r�\0\0�\0\0\0\0\0\0\0ϐ]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91631,1,'123456','','2011-10-13 17:06:57',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91632,2,'123456','','2011-10-13 17:06:57',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91633,1,'123456','','2011-10-13 17:07:19',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91634,2,'123456','','2011-10-13 17:07:19',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91635,1,'123456','','2011-10-13 17:07:39',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91636,2,'123456','','2011-10-13 17:07:39',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91637,2,'123456','','2011-10-13 17:07:41',-1,'0x9005',14,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0,�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91638,1,'123456','','2011-10-13 17:07:59',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91639,2,'123456','','2011-10-13 17:07:59',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91640,1,'123456','','2011-10-13 17:08:20',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91641,2,'123456','','2011-10-13 17:08:20',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91642,1,'123456','','2011-10-13 17:08:40',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91643,2,'123456','','2011-10-13 17:08:42',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91644,2,'123456','','2011-10-13 17:08:42',-1,'0x9005',15,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�I]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91645,1,'123456','','2011-10-13 17:09:01',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91646,2,'123456','','2011-10-13 17:09:01',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91647,1,'123456','','2011-10-13 17:09:21',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91648,2,'123456','','2011-10-13 17:09:22',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91649,2,'123456','','2011-10-13 17:09:41',-1,'0x9005',16,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91650,1,'123456','','2011-10-13 17:09:42',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91651,2,'123456','','2011-10-13 17:09:42',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91652,1,'123456','','2011-10-13 17:10:05',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91653,2,'123456','','2011-10-13 17:10:05',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91654,1,'123456','','2011-10-13 17:10:25',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91655,2,'123456','','2011-10-13 17:10:25',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91656,2,'123456','','2011-10-13 17:10:41',-1,'0x9005',17,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0=�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91657,1,'123456','','2011-10-13 17:10:45',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91658,2,'123456','','2011-10-13 17:10:46',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91659,1,'123456','','2011-10-13 17:11:06',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91660,2,'123456','','2011-10-13 17:11:06',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91661,1,'123456','','2011-10-13 17:11:26',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91662,2,'123456','','2011-10-13 17:11:26',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91663,2,'123456','','2011-10-13 17:11:41',-1,'0x9005',18,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91664,1,'123456','','2011-10-13 17:11:47',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91665,2,'123456','','2011-10-13 17:11:47',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91666,1,'123456','','2011-10-13 17:12:07',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91667,2,'123456','','2011-10-13 17:12:07',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91668,1,'123456','','2011-10-13 17:12:28',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91669,2,'123456','','2011-10-13 17:12:28',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91670,2,'123456','','2011-10-13 17:12:41',-1,'0x9005',19,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0p4]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91671,1,'123456','','2011-10-13 17:12:48',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91672,2,'123456','','2011-10-13 17:12:48',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91673,1,'123456','','2011-10-13 17:13:09',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91674,2,'123456','','2011-10-13 17:13:09',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91675,1,'123456','','2011-10-13 17:13:29',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91676,2,'123456','','2011-10-13 17:13:30',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91677,2,'123456','','2011-10-13 17:13:41',-1,'0x9005',20,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91678,1,'123456','','2011-10-13 17:13:50',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91679,2,'123456','','2011-10-13 17:13:50',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91680,1,'123456','','2011-10-13 17:14:10',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91681,2,'123456','','2011-10-13 17:14:10',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91682,1,'123456','','2011-10-13 17:14:30',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91683,2,'123456','','2011-10-13 17:14:30',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91684,2,'123456','','2011-10-13 17:14:41',-1,'0x9005',21,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�_]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91685,1,'123456','','2011-10-13 17:14:50',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91686,2,'123456','','2011-10-13 17:14:51',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91687,1,'123456','','2011-10-13 17:15:11',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91688,2,'123456','','2011-10-13 17:15:11',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91689,1,'123456','','2011-10-13 17:15:31',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91690,2,'123456','','2011-10-13 17:15:31',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91691,2,'123456','','2011-10-13 17:15:41',-1,'0x9005',22,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0Ez]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91692,1,'123456','','2011-10-13 17:15:52',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91693,2,'123456','','2011-10-13 17:15:52',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91694,1,'123456','','2011-10-13 17:16:12',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91695,2,'123456','','2011-10-13 17:16:12',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91696,1,'123456','','2011-10-13 17:16:32',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91697,2,'123456','','2011-10-13 17:16:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91698,2,'123456','','2011-10-13 17:16:41',-1,'0x9005',23,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91699,1,'123456','','2011-10-13 17:16:53',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91700,2,'123456','','2011-10-13 17:16:53',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91701,1,'123456','','2011-10-13 17:17:00',-1,'0x1212',33554432,1,'[\0\0\0Z\0\0\0\0\0\0�v1.\0\0)��A25007\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0$\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0Ln]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91702,1,'123456','','2011-10-13 17:17:13',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91703,2,'123456','','2011-10-13 17:17:13',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91704,1,'123456','','2011-10-13 17:17:17',-1,'0x1312',16777216,1,'[\0\0\0Z\0\0\0\0\0\0�v1.\0\0)��A35201\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0%\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0�]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91705,1,'123456','','2011-10-13 17:17:33',-1,'0x1200',20,0,'[\0\0\0F\0\0\0\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N�\r\0\0\0\0NFmؐ]','收到申请交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1207',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91706,2,'123456','','2011-10-13 17:17:33',-1,'0x9200',20,0,'[\0\0\07\0\0\0�\0\0\0�v1.�g\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0Y�]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91707,1,'123456','','2011-10-13 17:17:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91708,2,'123456','','2011-10-13 17:17:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91709,1,'123456','','2011-10-13 17:17:54',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91710,2,'123456','','2011-10-13 17:17:54',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91711,1,'123456','','2011-10-13 17:18:03',-1,'0x1200',21,0,'[\0\0\06\0\0\0\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0nO]','收到取消交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1208，数据为：0',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91712,2,'123456','','2011-10-13 17:18:03',-1,'0x9200',21,0,'[\0\0\07\0\0\0�\0\0\0�v1.�J\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0�]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91713,1,'123456','','2011-10-13 17:18:15',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91714,2,'123456','','2011-10-13 17:18:15',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91715,1,'123456','','2011-10-13 17:19:34',-1,'0x1200',22,0,'[\0\0\0F\0\0\0\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0\0N�\r\0\0\0\0NFm�\r]','收到补发车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1209',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91716,2,'123456','','2011-10-13 17:19:34',-1,'0x9005',24,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�T]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91717,1,'123456','','2011-10-13 17:19:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91718,2,'123456','','2011-10-13 17:19:35',-1,'0x9200',22,0,'[\0\0\07\0\0\0�\0\0\0�v1.m=\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�	\0\0\0\0�v]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91719,2,'123456','','2011-10-13 17:19:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91720,1,'123456','','2011-10-13 17:19:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91721,2,'123456','','2011-10-13 17:19:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91722,1,'123456','','2011-10-13 17:19:35',-1,'0x9200',23,1,'[\0\0\06\0\0\0�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�d]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91723,1,'123456','','2011-10-13 17:19:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91724,2,'123456','','2011-10-13 17:19:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91725,1,'123456','','2011-10-13 17:19:55',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91726,2,'123456','','2011-10-13 17:19:55',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91727,1,'123456','','2011-10-13 17:20:15',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91728,2,'123456','','2011-10-13 17:20:15',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91729,2,'123456','','2011-10-13 17:20:34',-1,'0x9005',25,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0\Z�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91730,1,'123456','','2011-10-13 17:20:36',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91731,2,'123456','','2011-10-13 17:20:36',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91732,1,'123456','','2011-10-13 17:20:41',-1,'0x9200',24,1,'[\0\0\0\Z\0\0\0�\0\0\0�v1.\0\0\0\0\0�,]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91733,1,'123456','','2011-10-13 17:20:56',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91734,2,'123456','','2011-10-13 17:20:56',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91735,1,'123456','','2011-10-13 17:21:15',-1,'0x9200',25,1,'[\0\0\06\0\0\0�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�0]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91736,1,'123456','','2011-10-13 17:21:17',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91737,2,'123456','','2011-10-13 17:21:17',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91738,2,'123456','','2011-10-13 17:21:34',-1,'0x9005',26,0,'[\0\0\0\Z\0\0\0\Z�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91739,1,'123456','','2011-10-13 17:21:37',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91740,2,'123456','','2011-10-13 17:21:37',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91741,1,'123456','','2011-10-13 17:21:57',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91742,2,'123456','','2011-10-13 17:21:57',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91743,1,'123456','','2011-10-13 17:22:18',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91744,2,'123456','','2011-10-13 17:22:18',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91745,2,'123456','','2011-10-13 17:22:34',-1,'0x9005',27,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0Wq]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91746,1,'123456','','2011-10-13 17:22:38',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91747,2,'123456','','2011-10-13 17:22:38',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91748,1,'123456','','2011-10-13 17:22:59',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91749,2,'123456','','2011-10-13 17:22:59',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91750,1,'123456','','2011-10-13 17:23:19',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91751,2,'123456','','2011-10-13 17:23:20',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91752,2,'123456','','2011-10-13 17:23:34',-1,'0x9005',28,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0/�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91753,1,'123456','','2011-10-13 17:23:39',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91754,2,'123456','','2011-10-13 17:23:39',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91755,1,'123456','','2011-10-13 17:24:00',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91756,2,'123456','','2011-10-13 17:24:00',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91757,1,'123456','','2011-10-13 17:24:10',-1,'0x9200',26,1,'[\0\0\06\0\0\0\Z�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0:�]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91758,1,'123456','','2011-10-13 17:24:20',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91759,2,'123456','','2011-10-13 17:24:20',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91760,2,'123456','','2011-10-13 17:24:34',-1,'0x9005',29,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�\Z]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91761,1,'123456','','2011-10-13 17:24:41',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91762,2,'123456','','2011-10-13 17:24:41',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91763,1,'123456','','2011-10-13 17:24:43',-1,'0x9300',27,1,'[\0\0\0\Z\0\0\0�\0\0\0�v1.\0\0\0\0\0.h]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91764,1,'123456','','2011-10-13 17:25:01',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91765,2,'123456','','2011-10-13 17:25:01',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91766,2,'123456','','2011-10-13 17:29:15',-1,'0x9005',30,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0b?]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91767,1,'123456','','2011-10-13 17:29:20',-1,'0x9300',28,1,'[\0\0\0\Z\0\0\0�\0\0\0�v1.\0\0\0\0\0V�]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91768,1,'123456','','2011-10-13 17:29:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91769,2,'123456','','2011-10-13 17:29:33',-1,'0x9005',31,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91770,2,'123456','','2011-10-13 17:29:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91771,1,'123456','','2011-10-13 17:29:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91772,2,'123456','','2011-10-13 17:29:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91773,1,'123456','','2011-10-13 17:29:34',-1,'0x1400',29,1,'[\0\0\02\0\0\0\0\0\0�v1.\0\0\0\0\0��A25307\0\0\0\0\0\0\0\0\0\0\0\0\0w�]','数据包长度错误',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91774,1,'123456','','2011-10-13 17:29:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91775,1,'123456','','2011-10-13 17:29:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91776,2,'123456','','2011-10-13 17:29:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91777,2,'123456','','2011-10-13 17:29:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91778,1,'123456','','2011-10-13 17:29:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91779,2,'123456','','2011-10-13 17:29:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91780,1,'123456','','2011-10-13 17:29:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91781,2,'123456','','2011-10-13 17:29:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91782,1,'123456','','2011-10-13 17:29:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91783,2,'123456','','2011-10-13 17:29:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91784,1,'123456','','2011-10-13 17:29:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91785,2,'123456','','2011-10-13 17:29:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91786,1,'123456','','2011-10-13 17:29:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91787,2,'123456','','2011-10-13 17:29:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91788,1,'123456','','2011-10-13 17:29:35',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91789,2,'123456','','2011-10-13 17:29:35',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91790,1,'123456','','2011-10-13 17:29:35',-1,'0x1E14',16777216,1,'[\0\0\0^\0\0\0\0\0\0�v1.\0\0)��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N�\0\0\0\0\0\0����ĳ��ԭ�򱨾�\0\0\0\0��]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91791,1,'123456','','2011-10-13 17:29:36',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91792,2,'123456','','2011-10-13 17:29:36',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91793,1,'123456','','2011-10-13 17:29:36',-1,'0x9500',31,1,'[\0\0\0J\0\0\0�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\001088888888\0\0\0\0\0\0\0\0\0�K]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91794,1,'123456','','2011-10-13 17:29:36',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91795,2,'123456','','2011-10-13 17:29:36',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91796,1,'123456','','2011-10-13 17:29:53',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91797,2,'123456','','2011-10-13 17:29:53',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91798,1,'123456','','2011-10-13 17:30:14',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91799,2,'123456','','2011-10-13 17:30:14',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91800,2,'123456','','2011-10-13 17:30:15',-1,'0x9005',32,0,'[\0\0\0\Z\0\0\0 �\0\0�\0\0\0\0\0\0\0@�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91801,1,'123456','','2011-10-13 17:30:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91802,2,'123456','','2011-10-13 17:30:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91803,2,'123456','','2011-10-13 17:31:56',-1,'0x9005',33,0,'[\0\0\0\Z\0\0\0!�\0\0�\0\0\0\0\0\0\0�s]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91804,1,'123456','','2011-10-13 17:32:01',-1,'0x9500',32,1,'[\0\0\08\0\0\0 �\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0��]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91805,1,'123456','','2011-10-13 17:32:14',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91806,1,'123456','','2011-10-13 17:32:14',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91807,2,'123456','','2011-10-13 17:32:14',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91808,2,'123456','','2011-10-13 17:32:14',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91809,1,'123456','','2011-10-13 17:32:14',-1,'0x9500',33,1,'[\0\0\0\Z\0\0\0!�\0\0\0�v1.\0\0\0\0\0�\r]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91810,1,'123456','','2011-10-13 17:32:14',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91811,2,'123456','','2011-10-13 17:32:14',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91812,1,'123456','','2011-10-13 17:32:14',-1,'0x9500',34,1,'[\0\0\0F\0\0\0\"�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0\0N�\r\0\0\0\0NFm��]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91813,1,'123456','','2011-10-13 17:32:15',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91814,2,'123456','','2011-10-13 17:32:15',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91815,2,'123456','','2011-10-13 17:32:34',-1,'0x9005',34,0,'[\0\0\0\Z\0\0\0\"�\0\0�\0\0\0\0\0\0\0\rV]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91816,1,'123456','','2011-10-13 17:32:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91817,2,'123456','','2011-10-13 17:32:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91818,1,'123456','','2011-10-13 17:32:55',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91819,2,'123456','','2011-10-13 17:32:55',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91820,1,'123456','','2011-10-13 17:33:15',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91821,2,'123456','','2011-10-13 17:33:15',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91822,1,'123456','','2011-10-13 17:35:32',-1,'0x9600',35,1,'[\0\0\06\0\0\0#�\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0��]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91823,2,'123456','','2011-10-13 17:35:33',-1,'0x9005',35,0,'[\0\0\0\Z\0\0\0#�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91824,1,'123456','','2011-10-13 17:35:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91825,2,'123456','','2011-10-13 17:35:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91826,1,'123456','','2011-10-13 17:35:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91827,2,'123456','','2011-10-13 17:35:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91828,1,'123456','','2011-10-13 17:35:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91829,2,'123456','','2011-10-13 17:35:33',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91830,1,'123456','','2011-10-13 17:35:33',-1,'0x9500',36,1,'[\0\0\0\Z\0\0\0$�\0\0\0�v1.\0\0\0\0\0�C]','未知消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91831,1,'123456','','2011-10-13 17:35:33',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91832,2,'123456','','2011-10-13 17:35:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91833,1,'123456','','2011-10-13 17:35:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91834,2,'123456','','2011-10-13 17:35:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91835,1,'123456','','2011-10-13 17:35:34',-1,'0x1005',15,0,'[\0\0\0\Z\0\0\0\0\0�v1.\0\0\0\0\0aC]','[主链路连接保持请求消息]',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91836,2,'123456','','2011-10-13 17:35:34',-1,'0x1006',15,0,'[\0\0\0\Z\0\0\0\0\0�\0\0\0\0\0\0\0\Z�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91837,1,'0','0','2011-10-13 18:00:47',0,'0100',1,0,'~\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A1~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91838,1,'123456','','2011-10-14 11:06:53',-1,'0x1001',38,0,'[\0\0\0H\0\0\0&\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o�]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91839,2,'123456','','2011-10-14 11:06:53',-1,'0x1002',38,0,'[\0\0\0\0\0\0&\0\0�\0\0\0\0\0*\0\0\0�\'�]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91840,2,'123456','','2011-10-14 11:06:54',-1,'0x9001',0,0,'[\0\0\0\0\0\0\0�\0\0�\0\0\0\0H$\0\0��]','从链路连接请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91841,2,'123456','','2011-10-14 11:06:54',-1,'0x9005',1,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0sg]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91842,1,'123456','','2011-10-14 11:06:54',-1,'0x9002',3232,0,'[\0\0\0\0\0��\0\0�\0\0\0\0\0\0\0\0\0q�]','验证结果：0。target:123456',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91843,1,'123456','','2011-10-14 11:07:24',-1,'0x1005',39,0,'[\0\0\0\Z\0\0\0\'\0\0�v1.\0\0\0\0\0�]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91844,2,'123456','','2011-10-14 11:07:24',-1,'0x1006',39,0,'[\0\0\0\Z\0\0\0\'\0\0�\0\0\0\0\0\0\0��]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91845,2,'123456','','2011-10-14 11:07:53',-1,'0x9005',2,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�B]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91846,2,'123456','','2011-10-14 11:08:53',-1,'0x9005',3,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0>�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91847,1,'123456','','2011-10-14 11:11:59',-1,'0x1200',41,0,'[\0\0\0g\0\0\0)\0\0\0�v1.\0\0\0\0\0��A25307\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0\0\0\0#Eg�\0\0\0\0\0\0#Eg�\0\0\0\0#Eg\0\0\0Vww13812345323\0�(]','收到车辆注册信息，车牌号:粤A25307，车辆颜色编号:1， 子业务标识:1201，平台唯一编码:，车载终端厂商唯一编码:，车载终端型号:，车载终端编号:，车载终端SIM卡号:13812345323',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91848,2,'123456','','2011-10-14 11:11:59',-1,'0x9005',4,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0F)]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91849,1,'123456','','2011-10-14 11:12:11',-1,'0x2A12',33554432,1,'[\0\0\0Z\0\0\0*\0\0\0�v1.\0\0)��A25007\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0$\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0j]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91850,1,'123456','','2011-10-14 11:12:28',-1,'0x2B12',16777216,1,'[\0\0\0Z\0\0\0+\0\0\0�v1.\0\0)��A35201\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0%\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0Ƕ]','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91851,1,'123456','','2011-10-14 11:13:32',-1,'0x1200',44,0,'[\0\0\0F\0\0\0,\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N�\r\0\0\0\0NFm�\Z]','收到申请交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1207',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91852,2,'123456','','2011-10-14 11:13:32',-1,'0x9005',5,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91853,1,'123456','','2011-10-14 11:13:33',-1,'0x1200',45,0,'[\0\0\06\0\0\0-\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0B�]','收到取消交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1208，数据为：0',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91854,2,'123456','','2011-10-14 11:13:33',-1,'0x9200',44,0,'[\0\0\07\0\0\0,�\0\0\0�v1.�g\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0X]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91855,2,'123456','','2011-10-14 11:13:33',-1,'0x9200',45,0,'[\0\0\07\0\0\0-�\0\0\0�v1.�J\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0��]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91856,1,'123456','','2011-10-14 11:14:08',-1,'0x1200',46,0,'[\0\0\0F\0\0\0.\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0\0N�\r\0\0\0\0NFm�]','收到补发车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1209',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91857,2,'123456','','2011-10-14 11:14:08',-1,'0x9200',46,0,'[\0\0\07\0\0\0.�\0\0\0�v1.m=\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�	\0\0\0\0��]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91858,2,'123456','','2011-10-14 11:15:07',-1,'0x9005',6,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91859,2,'123456','','2011-10-14 11:16:07',-1,'0x9005',7,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91860,2,'123456','','2011-10-14 11:17:07',-1,'0x9005',8,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91861,2,'123456','','2011-10-14 11:18:07',-1,'0x9005',9,0,'[\0\0\0\Z\0\0\0	�\0\0�\0\0\0\0\0\0\0T\"]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91862,2,'123456','','2011-10-14 11:19:07',-1,'0x9005',10,0,'[\0\0\0\Z\0\0\0\n�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91863,1,'0','0','2011-10-14 11:19:35',0,'0102',2,0,'~\0\0\0\0\0\0~','终端上行鉴权指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91864,1,'0','0','2011-10-14 11:20:05',0,'0102',3,0,'~\0\0\0\0\0\0~','终端上行鉴权指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91865,2,'123456','','2011-10-14 11:20:07',-1,'0x9005',11,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91866,2,'123456','','2011-10-14 11:21:07',-1,'0x9005',12,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0al]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91867,2,'123456','','2011-10-14 11:22:07',-1,'0x9005',13,0,'[\0\0\0\Z\0\0\0\r�\0\0�\0\0\0\0\0\0\0ϐ]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91868,2,'123456','','2011-10-14 11:23:07',-1,'0x9005',14,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0,�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91869,2,'123456','','2011-10-14 11:24:07',-1,'0x9005',15,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�I]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91870,2,'123456','','2011-10-14 11:25:07',-1,'0x9005',16,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91871,2,'123456','','2011-10-14 11:26:47',-1,'0x9005',17,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0=�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91872,1,'0','0','2011-10-14 13:08:48',0,'0100',1,1,'~\0\0\"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91873,1,'0','0','2011-10-14 13:17:28',0,'0100',1,1,'~\0\0\"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91874,1,'123456','','2011-10-14 15:05:15',-1,'0x1001',49,0,'[\0\0\0H\0\0\01\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"oU6]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91875,1,'123456','','2011-10-14 15:05:28',-1,'0x1005',50,0,'[\0\0\0\Z\0\0\02\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91876,1,'123456','','2011-10-14 15:11:11',-1,'0x1001',53,0,'[\0\0\0H\0\0\05\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o#�]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91877,2,'123456','','2011-10-14 15:11:11',-1,'0x1002',53,0,'[\0\0\0\0\0\05\0\0�\0\0\0\0\0*\0\0\0�	]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91878,2,'123456','','2011-10-14 15:11:11',-1,'0x9001',0,0,'[\0\0\0\0\0\0\0�\0\0�\0\0\0\0H$\0\0��]','从链路连接请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91879,1,'123456','','2011-10-14 15:11:11',-1,'0x9002',3236,0,'[\0\0\0\0\0��\0\0�\0\0\0\0\0\0\0\0\0�7]','验证结果：0。target:123456',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91880,1,'123456','','2011-10-14 15:11:20',-1,'0x1005',50,0,'[\0\0\0\Z\0\0\02\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91881,2,'123456','','2011-10-14 15:11:20',-1,'0x1006',50,0,'[\0\0\0\Z\0\0\02\0\0�\0\0\0\0\0\0\0�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91882,1,'123456','','2011-10-14 15:11:29',-1,'0x1005',50,0,'[\0\0\0\Z\0\0\02\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91883,2,'123456','','2011-10-14 15:11:29',-1,'0x1006',50,0,'[\0\0\0\Z\0\0\02\0\0�\0\0\0\0\0\0\0�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91884,1,'123456','','2011-10-14 15:11:36',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91885,2,'123456','','2011-10-14 15:11:37',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91886,1,'123456','','2011-10-14 15:11:45',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91887,2,'123456','','2011-10-14 15:11:46',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91888,1,'123456','','2011-10-14 15:11:54',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91889,2,'123456','','2011-10-14 15:11:54',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91890,1,'123456','','2011-10-14 15:12:04',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91891,2,'123456','','2011-10-14 15:12:04',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91892,2,'123456','','2011-10-14 15:12:10',-1,'0x9005',1,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0sg]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91893,1,'123456','','2011-10-14 15:12:13',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91894,2,'123456','','2011-10-14 15:12:13',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91895,1,'123456','','2011-10-14 15:12:22',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91896,2,'123456','','2011-10-14 15:12:22',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91897,1,'123456','','2011-10-14 15:12:31',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91898,2,'123456','','2011-10-14 15:12:31',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91899,1,'123456','','2011-10-14 15:12:40',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91900,2,'123456','','2011-10-14 15:12:40',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91901,1,'123456','','2011-10-14 15:12:49',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91902,2,'123456','','2011-10-14 15:12:49',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91903,1,'123456','','2011-10-14 15:12:57',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91904,2,'123456','','2011-10-14 15:12:58',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91905,1,'123456','','2011-10-14 15:13:06',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91906,2,'123456','','2011-10-14 15:13:06',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91907,2,'123456','','2011-10-14 15:13:10',-1,'0x9005',2,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�B]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91908,1,'123456','','2011-10-14 15:13:15',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91909,2,'123456','','2011-10-14 15:13:15',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91910,1,'123456','','2011-10-14 15:13:24',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91911,2,'123456','','2011-10-14 15:13:24',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91912,1,'123456','','2011-10-14 15:13:34',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91913,2,'123456','','2011-10-14 15:13:34',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91914,1,'123456','','2011-10-14 15:13:51',-1,'0x1200',56,0,'[\0\0\0g\0\0\08\0\0\0�v1.\0\0\0\0\0��A25307\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0\0\0\0#Eg�\0\0\0\0\0\0#Eg�\0\0\0\0#Eg\0\0\0Vww13812345323\0#�]','收到车辆注册信息，车牌号:粤A25307，车辆颜色编号:1， 子业务标识:1201，平台唯一编码:，车载终端厂商唯一编码:，车载终端型号:，车载终端编号:，车载终端SIM卡号:13812345323',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91915,1,'123456','','2011-10-14 15:13:51',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91916,2,'123456','','2011-10-14 15:13:51',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91917,1,'123456','','2011-10-14 15:13:51',-1,'0x1200',57,0,'[\0\0\0Z\0\0\09\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\01\0\0\0$\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0f�]','收到车辆实时定位信息，车牌号:粤A25007，车辆颜色编号:49，子业务类型标识:1202，定位信息:',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91918,1,'123456','','2011-10-14 15:13:59',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91919,2,'123456','','2011-10-14 15:13:59',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91920,1,'123456','','2011-10-14 15:14:08',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91921,2,'123456','','2011-10-14 15:14:08',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91922,1,'123456','','2011-10-14 15:14:08',-1,'0x1200',58,0,'[\0\0\0[\0\0\0:\0\0\0�v1.\0\0\0\0\0��A35201\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0%\0% DE`�h��\0-\08\0\0:�\0\0\"\0\0\0\0\0\0;T]','收到车辆定位信息自动补报请求消息，车牌号:粤A35201，车辆颜色编号:1，子业务类型标识:1203，卫星定位数据个数为:1，卫星定位数据1:',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91923,2,'123456','','2011-10-14 15:14:10',-1,'0x9005',3,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0>�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91924,1,'123456','','2011-10-14 15:14:17',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91925,2,'123456','','2011-10-14 15:14:17',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91926,1,'123456','','2011-10-14 15:14:26',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91927,2,'123456','','2011-10-14 15:14:27',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91928,1,'123456','','2011-10-14 15:14:35',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91929,2,'123456','','2011-10-14 15:14:35',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91930,1,'123456','','2011-10-14 15:14:45',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91931,2,'123456','','2011-10-14 15:14:45',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91932,1,'123456','','2011-10-14 15:14:53',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91933,2,'123456','','2011-10-14 15:14:53',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91934,1,'123456','','2011-10-14 15:15:02',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91935,2,'123456','','2011-10-14 15:15:02',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91936,2,'123456','','2011-10-14 15:15:10',-1,'0x9005',4,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0F)]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91937,1,'123456','','2011-10-14 15:15:11',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91938,2,'123456','','2011-10-14 15:15:11',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91939,1,'123456','','2011-10-14 15:15:20',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91940,2,'123456','','2011-10-14 15:15:21',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91941,1,'123456','','2011-10-14 15:15:29',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91942,2,'123456','','2011-10-14 15:15:30',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91943,1,'123456','','2011-10-14 15:15:38',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91944,2,'123456','','2011-10-14 15:15:38',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91945,1,'123456','','2011-10-14 15:15:48',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91946,2,'123456','','2011-10-14 15:15:48',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91947,1,'123456','','2011-10-14 15:15:57',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91948,2,'123456','','2011-10-14 15:15:57',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91949,1,'123456','','2011-10-14 15:16:07',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91950,2,'123456','','2011-10-14 15:16:07',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91951,2,'123456','','2011-10-14 15:16:10',-1,'0x9005',5,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0��]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91952,1,'123456','','2011-10-14 15:16:16',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91953,2,'123456','','2011-10-14 15:16:16',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91954,1,'123456','','2011-10-14 15:16:24',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91955,2,'123456','','2011-10-14 15:16:25',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91956,1,'123456','','2011-10-14 15:16:34',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91957,2,'123456','','2011-10-14 15:16:35',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91958,1,'123456','','2011-10-14 15:16:43',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91959,2,'123456','','2011-10-14 15:16:43',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91960,1,'123456','','2011-10-14 15:16:52',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91961,2,'123456','','2011-10-14 15:16:52',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91962,1,'123456','','2011-10-14 15:16:59',-1,'0x1200',59,0,'[\0\0\0F\0\0\0;\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0N�\r\0\0\0\0NFmI]','收到申请交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1207',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91963,2,'123456','','2011-10-14 15:16:59',-1,'0x9200',59,0,'[\0\0\07\0\0\0;�\0\0\0�v1.m=\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0p]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91964,1,'123456','','2011-10-14 15:17:01',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91965,2,'123456','','2011-10-14 15:17:01',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91966,1,'123456','','2011-10-14 15:17:10',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91967,2,'123456','','2011-10-14 15:17:10',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91968,1,'123456','','2011-10-14 15:17:20',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91969,2,'123456','','2011-10-14 15:17:20',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91970,1,'123456','','2011-10-14 15:17:29',-1,'0x1200',60,0,'[\0\0\06\0\0\0<\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0�]','收到取消交换指定车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1208，数据为：0',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91971,2,'123456','','2011-10-14 15:17:29',-1,'0x9200',60,0,'[\0\0\07\0\0\0<�\0\0\0�v1.�,\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�\0\0\0\0�]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91972,1,'123456','','2011-10-14 15:17:29',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91973,2,'123456','','2011-10-14 15:17:29',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91974,1,'123456','','2011-10-14 15:17:39',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91975,2,'123456','','2011-10-14 15:17:39',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91976,1,'123456','','2011-10-14 15:17:48',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91977,2,'123456','','2011-10-14 15:17:48',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91978,1,'123456','','2011-10-14 15:17:58',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91979,2,'123456','','2011-10-14 15:17:58',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91980,1,'123456','','2011-10-14 15:17:59',-1,'0x1200',61,0,'[\0\0\0F\0\0\0=\0\0\0�v1.\0\0\0\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0\0N�\r\0\0\0\0NFm��]','收到补发车辆定位信息请求消息，车牌号:粤A25007，车辆颜色编号:1，子业务类型标识:1209',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91981,2,'123456','','2011-10-14 15:17:59',-1,'0x9200',61,0,'[\0\0\07\0\0\0=�\0\0\0�v1.�r\0\0��A25007\0\0\0\0\0\0\0\0\0\0\0\0\0�	\0\0\0\0��]','',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91982,1,'123456','','2011-10-14 15:50:41',-1,'0x1001',66,0,'[\0\0\0H\0\0\0B\0\0�v1.\0\0\0\0\0\0\0\0{12345678192.168.1.114\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\"o��]','用户名：123；密码：12345678；下级平台提供对应的从链路服务IP地址：192.168.1.114；下级平台提供对应的从链路服务端口号：8815。',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91983,2,'123456','','2011-10-14 15:50:42',-1,'0x1002',66,0,'[\0\0\0\0\0\0B\0\0�\0\0\0\0\0*\0\0\0Z�]','主链路登录应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91984,2,'123456','','2011-10-14 15:50:42',-1,'0x9001',0,0,'[\0\0\0\0\0\0\0�\0\0�\0\0\0\0H$\0\0��]','从链路连接请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91985,1,'123456','','2011-10-14 15:50:42',-1,'0x9002',3534,0,'[\0\0\0\0\0\rΐ\0\0�\0\0\0\0\0\0\0\0\0z]','验证结果：0。target:123456',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91986,1,'123456','','2011-10-14 15:50:51',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91987,2,'123456','','2011-10-14 15:50:51',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91988,1,'123456','','2011-10-14 15:51:03',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91989,2,'123456','','2011-10-14 15:51:03',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91990,1,'123456','','2011-10-14 15:51:12',-1,'0x1005',54,0,'[\0\0\0\Z\0\0\06\0\0�v1.\0\0\0\0\0;d]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91991,2,'123456','','2011-10-14 15:51:12',-1,'0x1006',54,0,'[\0\0\0\Z\0\0\06\0\0�\0\0\0\0\0\0\0@�]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91992,1,'123456','','2011-10-14 15:51:13',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91993,2,'123456','','2011-10-14 15:51:13',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91994,1,'123456','','2011-10-14 15:51:20',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91995,2,'123456','','2011-10-14 15:51:21',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91996,1,'123456','','2011-10-14 15:51:28',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91997,2,'123456','','2011-10-14 15:51:28',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91998,1,'123456','','2011-10-14 15:51:36',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (91999,2,'123456','','2011-10-14 15:51:36',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92000,2,'123456','','2011-10-14 15:51:41',-1,'0x9005',1,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0sg]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92001,1,'123456','','2011-10-14 15:51:44',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92002,2,'123456','','2011-10-14 15:51:44',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92003,1,'123456','','2011-10-14 15:51:53',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92004,2,'123456','','2011-10-14 15:51:53',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92005,1,'123456','','2011-10-14 15:52:04',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92006,2,'123456','','2011-10-14 15:52:04',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92007,1,'123456','','2011-10-14 15:52:14',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92008,2,'123456','','2011-10-14 15:52:14',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92009,1,'123456','','2011-10-14 15:52:29',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92010,2,'123456','','2011-10-14 15:52:29',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92011,1,'123456','','2011-10-14 15:52:29',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92012,2,'123456','','2011-10-14 15:52:30',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92013,1,'123456','','2011-10-14 15:52:39',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92014,2,'123456','','2011-10-14 15:52:39',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92015,2,'123456','','2011-10-14 15:52:41',-1,'0x9005',2,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0�B]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92016,1,'123456','','2011-10-14 15:52:47',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92017,2,'123456','','2011-10-14 15:52:48',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92018,1,'123456','','2011-10-14 15:52:58',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92019,2,'123456','','2011-10-14 15:52:58',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92020,1,'123456','','2011-10-14 15:53:11',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92021,2,'123456','','2011-10-14 15:53:11',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92022,1,'123456','','2011-10-14 15:53:19',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92023,2,'123456','','2011-10-14 15:53:19',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92024,1,'123456','','2011-10-14 15:53:30',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92025,2,'123456','','2011-10-14 15:53:30',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92026,2,'123456','','2011-10-14 15:53:41',-1,'0x9005',3,0,'[\0\0\0\Z\0\0\0�\0\0�\0\0\0\0\0\0\0>�]','从链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92027,1,'123456','','2011-10-14 15:53:41',-1,'0x1005',67,0,'[\0\0\0\Z\0\0\0C\0\0�v1.\0\0\0\0\0��]','主链路连接保持请求消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92028,2,'123456','','2011-10-14 15:53:41',-1,'0x1006',67,0,'[\0\0\0\Z\0\0\0C\0\0�\0\0\0\0\0\0\0�l]','主链路连接保持应答消息',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92029,1,'0','0','2011-10-15 12:32:45',0,'0002',20,0,'~\0\0\01\0\0\0\0\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92030,1,'0','0','2011-10-15 14:41:17',0,'0002',21,0,'~\0\0\01\0\0\0\0&~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92031,1,'0','0','2011-10-15 14:41:17',0,'0100',22,0,'~\0\0������\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92032,1,'0','0','2011-10-15 14:41:47',0,'0002',23,0,'~\0\0\01\0\0\0\0$~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92033,1,'0','0','2011-10-15 14:42:17',0,'0002',24,0,'~\0\0\01\0\0\0\0+~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92034,1,'0','0','2011-10-15 14:42:17',0,'0100',25,0,'~\0\0������\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92035,1,'0','0','2011-10-15 14:42:47',0,'0002',26,0,'~\0\0\01\0\0\0\0\Z)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92036,1,'0','0','2011-10-15 14:56:40',0,'0002',24,0,'~\0\0\01\0\0\0\0+~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92037,2,'0','0','2011-10-15 14:56:40',0,'8001',22,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:22,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92038,1,'0','0','2011-10-15 14:56:40',0,'0002',25,0,'~\0\0\01\0\0\0\0*~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92039,1,'0','0','2011-10-15 14:56:41',0,'0002',26,1,'~\0\0\01\0\0\0\0\Z)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92040,0,'0','0','2011-10-15 14:56:57',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92041,2,'0','0','2011-10-15 14:56:57',0,'8001',25,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:25,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92042,1,'0','0','2011-10-15 14:57:27',0,'0002',28,0,'~\0\0\01\0\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92043,1,'0','0','2011-10-15 15:07:55',0,'0002',22,1,'~\0\0\01\0\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92044,2,'0','0','2011-10-15 15:07:55',0,'8001',20,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:20,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92045,1,'0','0','2011-10-15 15:08:25',0,'0002',22,0,'~\0\0\01\0\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92046,2,'0','0','2011-10-15 15:08:25',0,'8001',21,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:21,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92047,2,'0','0','2011-10-15 15:20:50',0,'8001',44,0,'�\01\0\0\0\0,\0,\0�','平台通用应答:对应终端流水号:44,对应消息ID:0x0102,鉴权成功',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92048,1,'0','0','2011-10-15 15:35:54',0,'0002',22,1,'~\0\0\01\0\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92049,2,'0','0','2011-10-15 15:35:54',0,'8001',20,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:20,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92050,1,'0','0','2011-10-15 15:36:24',0,'0002',22,0,'~\0\0\01\0\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92051,2,'0','0','2011-10-15 15:36:24',0,'8001',21,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:21,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92052,1,'999999','','2011-10-15 15:53:31',-1,'0x6473',1717854566,1,'afdsafdafdsafds','断包错包',0,'','',NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92053,1,'0','0','2011-10-15 17:55:12',0,'0002',58,0,'~\0\0\01\0\0\0\0:\n~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92054,1,'0','0','2011-10-15 17:55:12',0,'0002',59,0,'~\0\0\01\0\0\0\0;\n~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92055,1,'0','0','2011-10-15 17:55:41',0,'0002',60,0,'~\0\0\01\0\0\0\0<~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92056,1,'0','0','2011-10-15 17:56:11',0,'0002',61,0,'~\0\0\01\0\0\0\0=\r~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92057,1,'0','0','2011-10-15 17:56:41',0,'0002',62,0,'~\0\0\01\0\0\0\0>~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92058,1,'0','0','2011-10-15 17:57:11',0,'0002',63,0,'~\0\0\01\0\0\0\0?~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92059,1,'0','0','2011-10-15 17:57:41',0,'0002',64,0,'~\0\0\01\0\0\0\0@p~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92060,1,'0','0','2011-10-15 17:58:11',0,'0002',65,0,'~\0\0\01\0\0\0\0Ap~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92061,1,'0','0','2011-10-15 17:58:41',0,'0002',66,0,'~\0\0\01\0\0\0\0Bq~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92062,1,'0','0','2011-10-15 17:59:11',0,'0002',67,0,'~\0\0\01\0\0\0\0Cs~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92063,1,'0','0','2011-10-15 17:59:41',0,'0002',68,0,'~\0\0\01\0\0\0\0Du~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92064,1,'0','0','2011-10-15 18:00:11',0,'0002',69,0,'~\0\0\01\0\0\0\0Ev~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92065,1,'0','0','2011-10-15 18:00:41',0,'0002',70,0,'~\0\0\01\0\0\0\0Fv~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92066,1,'0','0','2011-10-15 18:01:11',0,'0002',71,0,'~\0\0\01\0\0\0\0Gv~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92067,1,'0','0','2011-10-15 18:05:43',0,'0100',10,0,'~\0\0\"1\0\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A00029~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92068,1,'0','0','2011-10-15 18:26:04',0,'0002',25,0,'~\0\0\01\0\0\0\0)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92069,2,'0','0','2011-10-15 18:26:04',0,'8001',22,0,'�\01\0\0\0\0\0 \0\0�','平台通用应答:对应终端流水号:22,对应消息ID:0x2000,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92070,2,'0','0','2011-10-15 18:26:04',0,'8001',23,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:23,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92071,1,'0','0','2011-10-15 18:26:34',0,'0002',26,0,'~\0\0\01\0\0\0\0\Z+~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92072,1,'0','0','2011-10-15 18:27:04',0,'0002',27,0,'~\0\0\01\0\0\0\0(~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92073,1,'0','0','2011-10-15 18:27:34',0,'0002',28,0,'~\0\0\01\0\0\0\0,~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92074,2,'0','0','2011-10-15 18:27:34',0,'8001',26,0,'�\01\0\0\0\0\Z\0\Z\0\0�','平台通用应答:对应终端流水号:26,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92075,1,'0','0','2011-10-15 18:28:04',0,'0002',29,0,'~\0\0\01\0\0\0\0,~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92076,1,'0','0','2011-10-15 18:28:34',0,'0002',30,0,'~\0\0\01\0\0\0\0-~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92077,1,'0','0','2011-10-15 18:29:04',0,'0002',31,0,'~\0\0\01\0\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92078,2,'0','0','2011-10-15 18:29:04',0,'8001',29,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:29,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92079,1,'0','0','2011-10-17 11:26:46',0,'0002',5,0,'~\0\0\01\0\0\0\05~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92080,1,'0','0','2011-10-17 11:26:47',0,'0002',6,0,'~\0\0\01\0\0\0\05~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92081,1,'0','0','2011-10-17 11:36:44',0,'0002',26,0,'~\0\0\01\0\0\0\0\Z-~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92082,1,'0','0','2011-10-17 11:37:14',0,'0100',27,0,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006(~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92083,2,'0','0','2011-10-17 11:37:14',0,'8100',27,0,'�\0\01\0\0\0\0\0\01610678280\0�','消息无分包,注册应答:注册成功,鉴权码:1610678280',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92084,1,'0','0','2011-10-17 11:37:39',0,'0102',28,0,'~\0\n1\0\0\0\01610678280&~','终端上行鉴权指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92085,2,'0','0','2011-10-17 11:37:39',0,'8001',28,0,'�\01\0\0\0\0\0\0�','平台通用应答:对应终端流水号:28,对应消息ID:0x0102,鉴权成功',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92086,1,'0','0','2011-10-17 11:37:39',0,'0002',30,0,'~\0\0\01\0\0\0\0*~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92087,1,'0','0','2011-10-17 11:38:09',0,'0100',31,0,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0007,~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92088,2,'0','0','2011-10-17 11:38:09',0,'8100',31,0,'�\0\01\0\0\0\0\0\01610678281\0�','消息无分包,注册应答:注册成功,鉴权码:1610678281',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92089,1,'0','0','2011-10-17 11:41:29',0,'0002',46,0,'~\0\0\01\0\0\0\0.~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92090,1,'0','0','2011-10-17 11:41:30',0,'0002',47,0,'~\0\0\01\0\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92091,1,'0','0','2011-10-17 11:42:00',0,'0002',48,0,'~\0\0\01\0\0\0\00\0~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92092,1,'0','0','2011-10-17 11:42:30',0,'0002',49,0,'~\0\0\01\0\0\0\01\0~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92093,2,'0','0','2011-10-17 11:42:30',0,'8001',21,0,'�\01\0\0\0\0\0 \0\0�','平台通用应答:对应终端流水号:21,对应消息ID:0x2000,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92094,2,'0','0','2011-10-17 11:42:30',0,'8001',22,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:22,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92095,1,'0','0','2011-10-17 11:42:31',0,'0002',50,0,'~\0\0\01\0\0\0\02~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92096,1,'0','0','2011-10-17 11:43:01',0,'0002',51,0,'~\0\0\01\0\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92097,1,'0','0','2011-10-17 11:43:31',0,'0002',52,0,'~\0\0\01\0\0\0\04\0~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92098,2,'0','0','2011-10-17 11:43:31',0,'8001',33,0,'�\01\0\0\0\0!\0! \0\0�','平台通用应答:对应终端流水号:33,对应消息ID:0x2000,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92099,2,'0','0','2011-10-17 11:43:31',0,'8001',34,0,'�\01\0\0\0\0\"\0\"\0\0�','平台通用应答:对应终端流水号:34,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92100,1,'0','0','2011-10-17 11:43:32',0,'0002',53,0,'~\0\0\01\0\0\0\05\0~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92101,1,'0','0','2011-10-17 11:44:02',0,'0002',54,0,'~\0\0\01\0\0\0\06~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92102,1,'0','0','2011-10-17 11:44:32',0,'0002',55,0,'~\0\0\01\0\0\0	\07~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92103,2,'0','0','2011-10-17 11:44:32',0,'8001',45,0,'�\01\0\0\0\0-\0- \0\0�','平台通用应答:对应终端流水号:45,对应消息ID:0x2000,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92104,2,'0','0','2011-10-17 11:44:32',0,'8001',46,0,'�\01\0\0\0\0.\0.\0\0�','平台通用应答:对应终端流水号:46,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92105,1,'0','0','2011-10-17 11:44:33',0,'0002',56,0,'~\0\0\01\0\0\0\08\Z~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92106,1,'0','0','2011-10-17 11:45:03',0,'0002',57,0,'~\0\0\01\0\0\0\09\n~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92107,1,'0','0','2011-10-17 11:45:33',0,'0002',58,0,'~\0\0\01\0\0\0\0:\n~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92108,1,'0','0','2011-10-17 11:46:03',0,'0002',59,0,'~\0\0\01\0\0\0\0;\n~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92109,1,'0','0','2011-10-17 11:48:34',0,'0002',65,0,'~\0\0\01\0\0\0	\0Az~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92110,2,'0','0','2011-10-17 11:48:34',0,'8001',56,0,'�\01\0\0\0\08\08\0\0�','平台通用应答:对应终端流水号:56,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92111,1,'0','0','2011-10-17 11:48:35',0,'0002',66,0,'~\0\0\01\0\0\0\0B`~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92112,1,'0','0','2011-10-17 11:49:05',0,'0002',67,0,'~\0\0\01\0\0\0\0Cp~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92113,1,'0','0','2011-10-17 11:49:35',0,'0002',68,0,'~\0\0\01\0\0\0\0Dt~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92114,1,'0','0','2011-10-17 11:50:35',0,'0002',70,0,'~\0\0\01\0\0\0\0Fp~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92115,1,'0','0','2011-10-17 11:50:36',0,'0002',71,0,'~\0\0\01\0\0\0\0Gp~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92116,1,'0','0','2011-10-17 11:51:06',0,'0002',72,0,'~\0\0\01\0\0\0\0H|~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92117,1,'0','0','2011-10-17 11:52:36',0,'0002',75,0,'~\0\0\01\0\0\0	\0Kp~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92118,1,'0','0','2011-10-17 11:54:37',0,'0002',80,0,'~\0\0\01\0\0\0\0Pf~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92119,1,'0','0','2011-10-17 11:54:38',0,'0002',81,0,'~\0\0\01\0\0\0\0Qf~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92120,1,'0','0','2011-10-17 11:55:08',0,'0002',82,0,'~\0\0\01\0\0\0\0Rf~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92121,1,'0','0','2011-10-17 11:55:38',0,'0002',83,0,'~\0\0\01\0\0\0\0Sf~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92122,1,'0','0','2011-10-17 11:56:08',0,'0002',84,0,'~\0\0\01\0\0\0\0Tn~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92123,1,'0','0','2011-10-17 11:56:38',0,'0002',85,0,'~\0\0\01\0\0\0	\0Un~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92124,1,'0','0','2011-10-17 11:58:39',0,'0002',90,0,'~\0\0\01\0\0\0\0Zl~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92125,1,'0','0','2011-10-17 11:58:40',0,'0002',91,0,'~\0\0\01\0\0\0\0[l~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92126,1,'0','0','2011-10-17 11:59:10',0,'0002',92,0,'~\0\0\01\0\0\0\0\\h~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92127,1,'0','0','2011-10-17 11:59:40',0,'0002',93,0,'~\0\0\01\0\0\0\0]h~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92128,1,'0','0','2011-10-17 12:00:10',0,'0002',94,0,'~\0\0\01\0\0\0\0^d~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92129,1,'0','0','2011-10-17 12:00:40',0,'0002',95,0,'~\0\0\01\0\0\0	\0_d~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92130,1,'0','0','2011-10-17 12:00:41',0,'0002',96,0,'~\0\0\01\0\0\0\0`B~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92131,1,'0','0','2011-10-17 12:01:11',0,'0002',97,0,'~\0\0\01\0\0\0\0aR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92132,1,'0','0','2011-10-17 12:01:41',0,'0002',98,0,'~\0\0\01\0\0\0\0bR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92133,1,'0','0','2011-10-17 12:02:11',0,'0002',99,0,'~\0\0\01\0\0\0\0cR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92134,1,'0','0','2011-10-17 12:02:41',0,'0002',100,0,'~\0\0\01\0\0\0\0dR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92135,1,'0','0','2011-10-17 12:02:42',0,'0002',101,0,'~\0\0\01\0\0\0\0eR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92136,1,'0','0','2011-10-17 12:03:12',0,'0002',102,0,'~\0\0\01\0\0\0\0fR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92137,1,'0','0','2011-10-17 12:03:42',0,'0002',103,0,'~\0\0\01\0\0\0\0gR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92138,1,'0','0','2011-10-17 12:04:12',0,'0002',104,0,'~\0\0\01\0\0\0\0hR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92139,1,'0','0','2011-10-17 12:04:42',0,'0002',105,0,'~\0\0\01\0\0\0	\0iR~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92140,1,'0','0','2011-10-17 12:04:43',0,'0002',106,0,'~\0\0\01\0\0\0\0jH~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92141,1,'0','0','2011-10-17 12:05:13',0,'0002',107,0,'~\0\0\01\0\0\0\0kX~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92142,1,'0','0','2011-10-17 12:05:43',0,'0002',108,0,'~\0\0\01\0\0\0\0l\\~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92143,1,'0','0','2011-10-17 12:06:13',0,'0002',109,0,'~\0\0\01\0\0\0\0m\\~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92144,1,'0','0','2011-10-17 12:06:43',0,'0002',110,0,'~\0\0\01\0\0\0\0nX~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92145,1,'0','0','2011-10-17 12:06:44',0,'0002',111,0,'~\0\0\01\0\0\0\0oX~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92146,1,'0','0','2011-10-17 12:07:14',0,'0002',112,0,'~\0\0\01\0\0\0\0pD~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92147,1,'0','0','2011-10-17 12:07:44',0,'0002',113,0,'~\0\0\01\0\0\0\0qD~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92148,1,'0','0','2011-10-17 12:08:14',0,'0002',114,0,'~\0\0\01\0\0\0\0rH~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92149,1,'0','0','2011-10-17 12:08:44',0,'0002',115,0,'~\0\0\01\0\0\0	\0sH~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92150,1,'0','0','2011-10-17 12:08:45',0,'0002',116,0,'~\0\0\01\0\0\0\0tV~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92151,1,'0','0','2011-10-17 12:09:15',0,'0002',117,0,'~\0\0\01\0\0\0\0uF~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92152,1,'0','0','2011-10-17 12:09:45',0,'0002',118,0,'~\0\0\01\0\0\0\0vF~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92153,1,'0','0','2011-10-17 12:10:15',0,'0002',119,0,'~\0\0\01\0\0\0\0wF~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92154,1,'0','0','2011-10-17 12:10:45',0,'0002',120,0,'~\0\0\01\0\0\0\0xN~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92155,1,'0','0','2011-10-17 12:10:46',0,'0002',121,0,'~\0\0\01\0\0\0\0yN~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92156,1,'0','0','2011-10-17 12:11:16',0,'0002',122,0,'~\0\0\01\0\0\0\0zN~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92157,1,'0','0','2011-10-17 12:11:46',0,'0002',123,0,'~\0\0\01\0\0\0\0{N~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92158,1,'0','0','2011-10-17 12:12:16',0,'0002',124,0,'~\0\0\01\0\0\0\0|F~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92159,1,'0','0','2011-10-17 12:12:46',0,'0002',125,0,'~\0\0\01\0\0\0	\0}F~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92160,1,'0','0','2011-10-17 12:12:47',0,'0002',126,0,'~\0\0\01\0\0\0\0}\\~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92161,1,'0','0','2011-10-17 12:13:17',0,'0002',127,0,'~\0\0\01\0\0\0\0L~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92162,1,'0','0','2011-10-17 12:13:47',0,'0002',128,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92163,1,'0','0','2011-10-17 12:14:17',0,'0002',129,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92164,1,'0','0','2011-10-17 12:14:47',0,'0002',130,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92165,1,'0','0','2011-10-17 12:14:48',0,'0002',131,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92166,1,'0','0','2011-10-17 12:15:18',0,'0002',132,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92167,1,'0','0','2011-10-17 12:15:48',0,'0002',133,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92168,1,'0','0','2011-10-17 12:16:18',0,'0002',134,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92169,1,'0','0','2011-10-17 12:16:48',0,'0002',135,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92170,1,'0','0','2011-10-17 12:16:49',0,'0002',136,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92171,1,'0','0','2011-10-17 12:17:19',0,'0002',137,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92172,1,'0','0','2011-10-17 12:17:49',0,'0002',138,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92173,1,'0','0','2011-10-17 12:18:19',0,'0002',139,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92174,1,'0','0','2011-10-17 12:18:49',0,'0002',140,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92175,1,'0','0','2011-10-17 12:18:50',0,'0002',141,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92176,1,'0','0','2011-10-17 12:19:20',0,'0002',142,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92177,1,'0','0','2011-10-17 12:19:50',0,'0002',143,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92178,1,'0','0','2011-10-17 12:20:20',0,'0002',144,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92179,1,'0','0','2011-10-17 12:20:50',0,'0002',145,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92180,1,'0','0','2011-10-17 12:20:51',0,'0002',146,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92181,1,'0','0','2011-10-17 12:21:21',0,'0002',147,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92182,1,'0','0','2011-10-17 12:21:51',0,'0002',148,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92183,1,'0','0','2011-10-17 12:22:21',0,'0002',149,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92184,1,'0','0','2011-10-17 12:22:51',0,'0002',150,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92185,1,'0','0','2011-10-17 12:22:52',0,'0002',151,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92186,1,'0','0','2011-10-17 12:23:22',0,'0002',152,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92187,1,'0','0','2011-10-17 12:23:52',0,'0002',153,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92188,1,'0','0','2011-10-17 12:24:22',0,'0002',154,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92189,1,'0','0','2011-10-17 12:24:52',0,'0002',155,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92190,1,'0','0','2011-10-17 12:24:53',0,'0002',156,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92191,1,'0','0','2011-10-17 12:25:23',0,'0002',157,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92192,1,'0','0','2011-10-17 12:25:53',0,'0002',158,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92193,1,'0','0','2011-10-17 12:26:23',0,'0002',159,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92194,1,'0','0','2011-10-17 12:26:53',0,'0002',160,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92195,1,'0','0','2011-10-17 12:26:54',0,'0002',161,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92196,1,'0','0','2011-10-17 12:27:24',0,'0002',162,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92197,1,'0','0','2011-10-17 12:27:54',0,'0002',163,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92198,1,'0','0','2011-10-17 12:28:24',0,'0002',164,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92199,1,'0','0','2011-10-17 12:28:54',0,'0002',165,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92200,1,'0','0','2011-10-17 12:28:55',0,'0002',166,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92201,1,'0','0','2011-10-17 12:29:25',0,'0002',167,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92202,1,'0','0','2011-10-17 12:29:55',0,'0002',168,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92203,1,'0','0','2011-10-17 12:30:25',0,'0002',169,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92204,1,'0','0','2011-10-17 12:30:55',0,'0002',170,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92205,1,'0','0','2011-10-17 12:30:56',0,'0002',171,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92206,1,'0','0','2011-10-17 12:31:26',0,'0002',172,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92207,1,'0','0','2011-10-17 12:31:56',0,'0002',173,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92208,1,'0','0','2011-10-17 12:32:26',0,'0002',174,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92209,1,'0','0','2011-10-17 12:32:56',0,'0002',175,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92210,1,'0','0','2011-10-17 12:32:57',0,'0002',176,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92211,1,'0','0','2011-10-17 12:33:27',0,'0002',177,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92212,1,'0','0','2011-10-17 12:33:57',0,'0002',178,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92213,1,'0','0','2011-10-17 12:34:27',0,'0002',179,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92214,1,'0','0','2011-10-17 12:34:57',0,'0002',180,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92215,1,'0','0','2011-10-17 12:34:58',0,'0002',181,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92216,1,'0','0','2011-10-17 12:35:28',0,'0002',182,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92217,1,'0','0','2011-10-17 12:35:58',0,'0002',183,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92218,1,'0','0','2011-10-17 12:36:28',0,'0002',184,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92219,1,'0','0','2011-10-17 12:36:58',0,'0002',185,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92220,1,'0','0','2011-10-17 12:36:59',0,'0002',186,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92221,1,'0','0','2011-10-17 12:37:29',0,'0002',187,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92222,1,'0','0','2011-10-17 12:37:59',0,'0002',188,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92223,1,'0','0','2011-10-17 12:38:29',0,'0002',189,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92224,1,'0','0','2011-10-17 12:38:59',0,'0002',190,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92225,1,'0','0','2011-10-17 12:39:00',0,'0002',191,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92226,1,'0','0','2011-10-17 12:39:30',0,'0002',192,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92227,1,'0','0','2011-10-17 12:40:00',0,'0002',193,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92228,1,'0','0','2011-10-17 12:40:30',0,'0002',194,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92229,1,'0','0','2011-10-17 12:41:00',0,'0002',195,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92230,1,'0','0','2011-10-17 12:41:01',0,'0002',196,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92231,1,'0','0','2011-10-17 12:41:31',0,'0002',197,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92232,1,'0','0','2011-10-17 12:42:01',0,'0002',198,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92233,1,'0','0','2011-10-17 12:42:31',0,'0002',199,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92234,1,'0','0','2011-10-17 12:43:01',0,'0002',200,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92235,1,'0','0','2011-10-17 12:43:02',0,'0002',201,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92236,1,'0','0','2011-10-17 12:43:32',0,'0002',202,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92237,1,'0','0','2011-10-17 12:44:02',0,'0002',203,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92238,1,'0','0','2011-10-17 12:44:32',0,'0002',204,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92239,1,'0','0','2011-10-17 12:45:02',0,'0002',205,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92240,1,'0','0','2011-10-17 12:45:03',0,'0002',206,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92241,1,'0','0','2011-10-17 12:45:33',0,'0002',207,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92242,1,'0','0','2011-10-17 12:46:03',0,'0002',208,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92243,1,'0','0','2011-10-17 12:46:33',0,'0002',209,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92244,1,'0','0','2011-10-17 12:47:03',0,'0002',210,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92245,1,'0','0','2011-10-17 12:47:04',0,'0002',211,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92246,1,'0','0','2011-10-17 12:47:34',0,'0002',212,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92247,1,'0','0','2011-10-17 12:48:04',0,'0002',213,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92248,1,'0','0','2011-10-17 12:48:34',0,'0002',214,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92249,1,'0','0','2011-10-17 12:49:04',0,'0002',215,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92250,1,'0','0','2011-10-17 12:49:05',0,'0002',216,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92251,1,'0','0','2011-10-17 12:49:35',0,'0002',217,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92252,1,'0','0','2011-10-17 12:50:05',0,'0002',218,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92253,1,'0','0','2011-10-17 12:50:35',0,'0002',219,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92254,1,'0','0','2011-10-17 12:51:05',0,'0002',220,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92255,1,'0','0','2011-10-17 12:51:06',0,'0002',221,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92256,1,'0','0','2011-10-17 12:51:36',0,'0002',222,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92257,1,'0','0','2011-10-17 12:52:06',0,'0002',223,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92258,1,'0','0','2011-10-17 12:52:36',0,'0002',224,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92259,1,'0','0','2011-10-17 12:53:06',0,'0002',225,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92260,1,'0','0','2011-10-17 12:53:07',0,'0002',226,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92261,1,'0','0','2011-10-17 12:53:37',0,'0002',227,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92262,1,'0','0','2011-10-17 12:54:07',0,'0002',228,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92263,1,'0','0','2011-10-17 12:54:37',0,'0002',229,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92264,1,'0','0','2011-10-17 12:55:07',0,'0002',230,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92265,1,'0','0','2011-10-17 12:55:08',0,'0002',231,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92266,1,'0','0','2011-10-17 12:55:38',0,'0002',232,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92267,1,'0','0','2011-10-17 12:56:08',0,'0002',233,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92268,1,'0','0','2011-10-17 12:56:38',0,'0002',234,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92269,1,'0','0','2011-10-17 12:57:08',0,'0002',235,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92270,1,'0','0','2011-10-17 12:57:09',0,'0002',236,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92271,1,'0','0','2011-10-17 12:57:39',0,'0002',237,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92272,1,'0','0','2011-10-17 12:58:09',0,'0002',238,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92273,1,'0','0','2011-10-17 12:58:39',0,'0002',239,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92274,1,'0','0','2011-10-17 12:59:09',0,'0002',240,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92275,1,'0','0','2011-10-17 12:59:10',0,'0002',241,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92276,1,'0','0','2011-10-17 12:59:40',0,'0002',242,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92277,1,'0','0','2011-10-17 13:00:10',0,'0002',243,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92278,1,'0','0','2011-10-17 13:00:40',0,'0002',244,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92279,1,'0','0','2011-10-17 13:01:10',0,'0002',245,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92280,1,'0','0','2011-10-17 13:01:11',0,'0002',246,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92281,1,'0','0','2011-10-17 13:01:41',0,'0002',247,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92282,1,'0','0','2011-10-17 13:02:11',0,'0002',248,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92283,1,'0','0','2011-10-17 13:02:41',0,'0002',249,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92284,1,'0','0','2011-10-17 13:03:11',0,'0002',250,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92285,1,'0','0','2011-10-17 13:03:12',0,'0002',251,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92286,1,'0','0','2011-10-17 13:03:42',0,'0002',252,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92287,1,'0','0','2011-10-17 13:04:12',0,'0002',253,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92288,1,'0','0','2011-10-17 13:04:42',0,'0002',254,0,'~\0\0\01\0\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92289,1,'0','0','2011-10-17 13:05:12',0,'0002',255,0,'~\0\0\01\0\0\0	\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92290,1,'0','0','2011-10-17 13:05:13',0,'0002',256,0,'~\0\0\01\0\0\0\0#~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92291,1,'0','0','2011-10-17 13:05:43',0,'0002',257,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92292,1,'0','0','2011-10-17 13:06:13',0,'0002',258,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92293,1,'0','0','2011-10-17 13:06:43',0,'0002',259,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92294,1,'0','0','2011-10-17 13:07:13',0,'0002',260,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92295,1,'0','0','2011-10-17 13:07:14',0,'0002',261,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92296,1,'0','0','2011-10-17 13:07:44',0,'0002',262,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92297,1,'0','0','2011-10-17 13:08:14',0,'0002',263,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92298,1,'0','0','2011-10-17 13:08:44',0,'0002',264,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92299,1,'0','0','2011-10-17 13:09:14',0,'0002',265,0,'~\0\0\01\0\0\0		3~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92300,1,'0','0','2011-10-17 13:09:15',0,'0002',266,0,'~\0\0\01\0\0\0\n)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92301,1,'0','0','2011-10-17 13:09:45',0,'0002',267,0,'~\0\0\01\0\0\09~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92302,1,'0','0','2011-10-17 13:10:15',0,'0002',268,0,'~\0\0\01\0\0\0=~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92303,1,'0','0','2011-10-17 13:10:45',0,'0002',269,0,'~\0\0\01\0\0\0\r=~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92304,1,'0','0','2011-10-17 13:11:15',0,'0002',270,0,'~\0\0\01\0\0\09~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92305,1,'0','0','2011-10-17 13:11:16',0,'0002',271,0,'~\0\0\01\0\0\09~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92306,1,'0','0','2011-10-17 13:11:46',0,'0002',272,0,'~\0\0\01\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92307,1,'0','0','2011-10-17 13:12:16',0,'0002',273,0,'~\0\0\01\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92308,1,'0','0','2011-10-17 13:12:46',0,'0002',274,0,'~\0\0\01\0\0\0)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92309,1,'0','0','2011-10-17 13:13:16',0,'0002',275,0,'~\0\0\01\0\0\0	)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92310,1,'0','0','2011-10-17 13:13:17',0,'0002',276,0,'~\0\0\01\0\0\07~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92311,1,'0','0','2011-10-17 13:13:47',0,'0002',277,0,'~\0\0\01\0\0\0\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92312,1,'0','0','2011-10-17 13:14:17',0,'0002',278,0,'~\0\0\01\0\0\0\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92313,1,'0','0','2011-10-17 13:14:47',0,'0002',279,0,'~\0\0\01\0\0\0\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92314,1,'0','0','2011-10-17 13:15:17',0,'0002',280,0,'~\0\0\01\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92315,1,'0','0','2011-10-17 13:15:18',0,'0002',281,0,'~\0\0\01\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92316,1,'0','0','2011-10-17 13:15:48',0,'0002',282,0,'~\0\0\01\0\0\0\Z/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92317,1,'0','0','2011-10-17 13:16:18',0,'0002',283,0,'~\0\0\01\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92318,1,'0','0','2011-10-17 13:16:48',0,'0002',284,0,'~\0\0\01\0\0\0\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92319,1,'0','0','2011-10-17 13:17:18',0,'0002',285,0,'~\0\0\01\0\0\0	\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92320,1,'0','0','2011-10-17 13:17:19',0,'0002',286,0,'~\0\0\01\0\0\0=~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92321,1,'0','0','2011-10-17 13:17:49',0,'0002',287,0,'~\0\0\01\0\0\0-~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92322,1,'0','0','2011-10-17 13:18:19',0,'0002',288,0,'~\0\0\01\0\0\0 ~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92323,1,'0','0','2011-10-17 13:18:49',0,'0002',289,0,'~\0\0\01\0\0\0!~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92324,1,'0','0','2011-10-17 13:19:19',0,'0002',290,0,'~\0\0\01\0\0\0\"~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92325,1,'0','0','2011-10-17 13:19:20',0,'0002',291,0,'~\0\0\01\0\0\0#~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92326,1,'0','0','2011-10-17 13:19:50',0,'0002',292,0,'~\0\0\01\0\0\0$~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92327,1,'0','0','2011-10-17 13:20:20',0,'0002',293,0,'~\0\0\01\0\0\0%~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92328,1,'0','0','2011-10-17 13:20:50',0,'0002',294,0,'~\0\0\01\0\0\0&~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92329,1,'0','0','2011-10-17 13:21:20',0,'0002',295,0,'~\0\0\01\0\0\0	\'~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92330,1,'0','0','2011-10-17 13:21:21',0,'0002',296,0,'~\0\0\01\0\0\0(~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92331,1,'0','0','2011-10-17 13:21:51',0,'0002',297,0,'~\0\0\01\0\0\0)~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92332,1,'0','0','2011-10-17 13:22:21',0,'0002',298,0,'~\0\0\01\0\0\0*~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92333,1,'0','0','2011-10-17 13:22:51',0,'0002',299,0,'~\0\0\01\0\0\0+~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92334,1,'0','0','2011-10-17 13:23:21',0,'0002',300,0,'~\0\0\01\0\0\0,~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92335,1,'0','0','2011-10-17 13:23:22',0,'0002',301,0,'~\0\0\01\0\0\0-~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92336,1,'0','0','2011-10-17 13:23:52',0,'0002',302,0,'~\0\0\01\0\0\0.~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92337,1,'0','0','2011-10-17 13:24:22',0,'0002',303,0,'~\0\0\01\0\0\0/~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92338,1,'0','0','2011-10-17 13:24:52',0,'0002',304,0,'~\0\0\01\0\0\00~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92339,1,'0','0','2011-10-17 13:25:22',0,'0002',305,0,'~\0\0\01\0\0\0	1~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92340,1,'0','0','2011-10-17 13:25:23',0,'0002',306,0,'~\0\0\01\0\0\02~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92341,1,'0','0','2011-10-17 13:25:53',0,'0002',307,0,'~\0\0\01\0\0\03~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92342,1,'0','0','2011-10-17 13:26:23',0,'0002',308,0,'~\0\0\01\0\0\04~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92343,1,'0','0','2011-10-17 13:26:53',0,'0002',309,0,'~\0\0\01\0\0\05~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92344,1,'0','0','2011-10-17 13:27:23',0,'0002',310,0,'~\0\0\01\0\0\06~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92345,1,'0','0','2011-10-17 13:27:24',0,'0002',311,0,'~\0\0\01\0\0\07~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92346,1,'0','0','2011-10-17 13:27:54',0,'0002',312,0,'~\0\0\01\0\0\08\r~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92347,1,'0','0','2011-10-17 13:28:24',0,'0002',313,0,'~\0\0\01\0\0\09\r~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92348,1,'0','0','2011-10-17 13:28:54',0,'0002',314,0,'~\0\0\01\0\0\0:~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92349,1,'0','0','2011-10-17 13:29:24',0,'0002',315,0,'~\0\0\01\0\0\0	;~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92350,1,'0','0','2011-10-17 13:29:25',0,'0002',316,0,'~\0\0\01\0\0\0<~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92351,1,'0','0','2011-10-17 13:29:55',0,'0002',317,0,'~\0\0\01\0\0\0=~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92352,1,'0','0','2011-10-17 13:30:25',0,'0002',318,0,'~\0\0\01\0\0\0>~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92353,1,'0','0','2011-10-17 13:30:55',0,'0002',319,0,'~\0\0\01\0\0\0?~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92354,1,'0','0','2011-10-17 13:31:25',0,'0002',320,0,'~\0\0\01\0\0\0@w~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92355,1,'0','0','2011-10-17 13:31:26',0,'0002',321,0,'~\0\0\01\0\0\0Aw~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92356,1,'0','0','2011-10-17 13:31:56',0,'0002',322,0,'~\0\0\01\0\0\0Bw~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92357,1,'0','0','2011-10-17 13:32:26',0,'0002',323,0,'~\0\0\01\0\0\0Cw~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92358,1,'0','0','2011-10-17 13:32:56',0,'0002',324,0,'~\0\0\01\0\0\0D~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92359,1,'0','0','2011-10-17 13:33:26',0,'0002',325,0,'~\0\0\01\0\0\0	E~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92360,1,'0','0','2011-10-17 13:33:27',0,'0002',326,0,'~\0\0\01\0\0\0Fe~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92361,1,'0','0','2011-10-17 13:33:57',0,'0002',327,0,'~\0\0\01\0\0\0Gu~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92362,1,'0','0','2011-10-17 13:34:27',0,'0002',328,0,'~\0\0\01\0\0\0Hy~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92363,1,'0','0','2011-10-17 13:34:57',0,'0002',329,0,'~\0\0\01\0\0\0Iy~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92364,1,'0','0','2011-10-17 13:35:27',0,'0002',330,0,'~\0\0\01\0\0\0J}~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92365,1,'0','0','2011-10-17 13:35:28',0,'0002',331,0,'~\0\0\01\0\0\0K}~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92366,1,'0','0','2011-10-17 13:35:58',0,'0002',332,0,'~\0\0\01\0\0\0Ly~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92367,1,'0','0','2011-10-17 13:36:28',0,'0002',333,0,'~\0\0\01\0\0\0My~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92368,1,'0','0','2011-10-17 13:36:58',0,'0002',334,0,'~\0\0\01\0\0\0Nu~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92369,1,'0','0','2011-10-17 13:37:28',0,'0002',335,0,'~\0\0\01\0\0\0	Ou~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92370,1,'0','0','2011-10-17 13:37:29',0,'0002',336,0,'~\0\0\01\0\0\0Ps~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92371,1,'0','0','2011-10-17 13:37:59',0,'0002',337,0,'~\0\0\01\0\0\0Qc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92372,1,'0','0','2011-10-17 13:38:29',0,'0002',338,0,'~\0\0\01\0\0\0Rc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92373,1,'0','0','2011-10-17 13:38:59',0,'0002',339,0,'~\0\0\01\0\0\0Sc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92374,1,'0','0','2011-10-17 13:39:29',0,'0002',340,0,'~\0\0\01\0\0\0Tc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92375,1,'0','0','2011-10-17 13:39:30',0,'0002',341,0,'~\0\0\01\0\0\0Uc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92376,1,'0','0','2011-10-17 13:40:00',0,'0002',342,0,'~\0\0\01\0\0\0Vc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92377,1,'0','0','2011-10-17 13:40:30',0,'0002',343,0,'~\0\0\01\0\0\0Wc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92378,1,'0','0','2011-10-17 13:41:00',0,'0002',344,0,'~\0\0\01\0\0\0Xc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92379,1,'0','0','2011-10-17 13:41:30',0,'0002',345,0,'~\0\0\01\0\0\0	Yc~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92380,1,'0','0','2011-10-17 13:41:31',0,'0002',346,0,'~\0\0\01\0\0\0Zy~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92381,1,'0','0','2011-10-17 13:42:01',0,'0002',347,0,'~\0\0\01\0\0\0[i~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92382,1,'0','0','2011-10-17 13:42:31',0,'0002',348,0,'~\0\0\01\0\0\0\\m~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92383,1,'0','0','2011-10-17 13:43:01',0,'0002',349,0,'~\0\0\01\0\0\0]m~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92384,1,'0','0','2011-10-17 13:43:31',0,'0002',350,0,'~\0\0\01\0\0\0^i~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92385,1,'0','0','2011-10-17 13:43:32',0,'0002',351,0,'~\0\0\01\0\0\0_i~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92386,1,'0','0','2011-10-17 13:44:02',0,'0002',352,0,'~\0\0\01\0\0\0`U~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92387,1,'0','0','2011-10-17 13:44:32',0,'0002',353,0,'~\0\0\01\0\0\0aU~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92388,1,'0','0','2011-10-17 13:45:02',0,'0002',354,0,'~\0\0\01\0\0\0bY~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92389,1,'0','0','2011-10-17 13:45:32',0,'0002',355,0,'~\0\0\01\0\0\0	cY~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92390,1,'0','0','2011-10-17 13:45:33',0,'0002',356,0,'~\0\0\01\0\0\0dG~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92391,1,'0','0','2011-10-17 13:46:03',0,'0002',357,0,'~\0\0\01\0\0\0eW~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92392,1,'0','0','2011-10-17 13:46:33',0,'0002',358,0,'~\0\0\01\0\0\0fW~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92393,1,'0','0','2011-10-17 13:47:03',0,'0002',359,0,'~\0\0\01\0\0\0gW~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92394,1,'0','0','2011-10-17 13:47:33',0,'0002',360,0,'~\0\0\01\0\0\0h_~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92395,1,'0','0','2011-10-17 13:47:34',0,'0002',361,0,'~\0\0\01\0\0\0i_~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92396,1,'0','0','2011-10-17 13:48:04',0,'0002',362,0,'~\0\0\01\0\0\0j_~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92397,1,'0','0','2011-10-17 13:48:34',0,'0002',363,0,'~\0\0\01\0\0\0k_~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92398,1,'0','0','2011-10-17 13:49:04',0,'0002',364,0,'~\0\0\01\0\0\0lW~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92399,1,'0','0','2011-10-17 13:49:34',0,'0002',365,0,'~\0\0\01\0\0\0	mW~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92400,1,'0','0','2011-10-17 13:49:35',0,'0002',366,0,'~\0\0\01\0\0\0nM~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92401,1,'0','0','2011-10-17 13:50:05',0,'0002',367,0,'~\0\0\01\0\0\0o]~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92402,1,'0','0','2011-10-17 13:50:35',0,'0002',368,0,'~\0\0\01\0\0\0pA~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92403,1,'0','0','2011-10-17 13:51:05',0,'0002',369,0,'~\0\0\01\0\0\0qA~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92404,1,'0','0','2011-10-17 13:51:35',0,'0002',370,0,'~\0\0\01\0\0\0rE~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92405,1,'0','0','2011-10-17 13:51:36',0,'0002',371,0,'~\0\0\01\0\0\0sE~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92406,1,'0','0','2011-10-17 13:52:06',0,'0002',372,0,'~\0\0\01\0\0\0tA~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92407,1,'0','0','2011-10-17 13:52:36',0,'0002',373,0,'~\0\0\01\0\0\0uA~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92408,1,'0','0','2011-10-17 13:53:06',0,'0002',374,0,'~\0\0\01\0\0\0vM~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92409,1,'0','0','2011-10-17 13:53:36',0,'0002',375,0,'~\0\0\01\0\0\0	wM~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92410,1,'0','0','2011-10-17 13:53:37',0,'0002',376,0,'~\0\0\01\0\0\0x[~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92411,1,'0','0','2011-10-17 13:54:07',0,'0002',377,0,'~\0\0\01\0\0\0yK~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92412,1,'0','0','2011-10-17 13:54:37',0,'0002',378,0,'~\0\0\01\0\0\0zK~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92413,1,'0','0','2011-10-17 13:55:07',0,'0002',379,0,'~\0\0\01\0\0\0{K~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92414,1,'0','0','2011-10-17 13:55:37',0,'0002',380,0,'~\0\0\01\0\0\0|K~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92415,1,'0','0','2011-10-17 13:55:38',0,'0002',381,0,'~\0\0\01\0\0\0}K~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92416,1,'0','0','2011-10-17 13:56:08',0,'0002',382,0,'~\0\0\01\0\0\0}K~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92417,1,'0','0','2011-10-17 13:56:38',0,'0002',383,0,'~\0\0\01\0\0\0K~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92418,1,'0','0','2011-10-17 13:57:08',0,'0002',384,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92419,1,'0','0','2011-10-17 13:57:38',0,'0002',385,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92420,1,'0','0','2011-10-17 13:57:39',0,'0002',386,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92421,1,'0','0','2011-10-17 13:58:09',0,'0002',387,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92422,1,'0','0','2011-10-17 13:58:39',0,'0002',388,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92423,1,'0','0','2011-10-17 13:59:09',0,'0002',389,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92424,1,'0','0','2011-10-17 13:59:39',0,'0002',390,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92425,1,'0','0','2011-10-17 13:59:40',0,'0002',391,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92426,1,'0','0','2011-10-17 14:00:10',0,'0002',392,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92427,1,'0','0','2011-10-17 14:00:40',0,'0002',393,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92428,1,'0','0','2011-10-17 14:01:10',0,'0002',394,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92429,1,'0','0','2011-10-17 14:01:40',0,'0002',395,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92430,1,'0','0','2011-10-17 14:01:41',0,'0002',396,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92431,1,'0','0','2011-10-17 14:02:11',0,'0002',397,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92432,1,'0','0','2011-10-17 14:02:41',0,'0002',398,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92433,1,'0','0','2011-10-17 14:03:11',0,'0002',399,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92434,1,'0','0','2011-10-17 14:03:41',0,'0002',400,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92435,1,'0','0','2011-10-17 14:03:42',0,'0002',401,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92436,1,'0','0','2011-10-17 14:04:12',0,'0002',402,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92437,1,'0','0','2011-10-17 14:04:42',0,'0002',403,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92438,1,'0','0','2011-10-17 14:05:12',0,'0002',404,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92439,1,'0','0','2011-10-17 14:05:42',0,'0002',405,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92440,1,'0','0','2011-10-17 14:05:43',0,'0002',406,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92441,1,'0','0','2011-10-17 14:06:13',0,'0002',407,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92442,1,'0','0','2011-10-17 14:06:43',0,'0002',408,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92443,1,'0','0','2011-10-17 14:07:13',0,'0002',409,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92444,1,'0','0','2011-10-17 14:07:43',0,'0002',410,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92445,1,'0','0','2011-10-17 14:07:44',0,'0002',411,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92446,1,'0','0','2011-10-17 14:08:14',0,'0002',412,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92447,1,'0','0','2011-10-17 14:08:44',0,'0002',413,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92448,1,'0','0','2011-10-17 14:09:14',0,'0002',414,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92449,1,'0','0','2011-10-17 14:09:44',0,'0002',415,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92450,1,'0','0','2011-10-17 14:09:45',0,'0002',416,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92451,1,'0','0','2011-10-17 14:10:15',0,'0002',417,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92452,1,'0','0','2011-10-17 14:10:45',0,'0002',418,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92453,1,'0','0','2011-10-17 14:11:15',0,'0002',419,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92454,1,'0','0','2011-10-17 14:11:45',0,'0002',420,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92455,1,'0','0','2011-10-17 14:11:46',0,'0002',421,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92456,1,'0','0','2011-10-17 14:12:16',0,'0002',422,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92457,1,'0','0','2011-10-17 14:12:46',0,'0002',423,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92458,1,'0','0','2011-10-17 14:13:16',0,'0002',424,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92459,1,'0','0','2011-10-17 14:13:46',0,'0002',425,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92460,1,'0','0','2011-10-17 14:13:47',0,'0002',426,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92461,1,'0','0','2011-10-17 14:14:17',0,'0002',427,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92462,1,'0','0','2011-10-17 14:14:47',0,'0002',428,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92463,1,'0','0','2011-10-17 14:15:17',0,'0002',429,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92464,1,'0','0','2011-10-17 14:15:47',0,'0002',430,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92465,1,'0','0','2011-10-17 14:15:48',0,'0002',431,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92466,1,'0','0','2011-10-17 14:16:18',0,'0002',432,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92467,1,'0','0','2011-10-17 14:16:48',0,'0002',433,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92468,1,'0','0','2011-10-17 14:17:18',0,'0002',434,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92469,1,'0','0','2011-10-17 14:17:48',0,'0002',435,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92470,1,'0','0','2011-10-17 14:17:49',0,'0002',436,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92471,1,'0','0','2011-10-17 14:18:19',0,'0002',437,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92472,1,'0','0','2011-10-17 14:18:49',0,'0002',438,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92473,1,'0','0','2011-10-17 14:19:19',0,'0002',439,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92474,1,'0','0','2011-10-17 14:19:49',0,'0002',440,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92475,1,'0','0','2011-10-17 14:19:50',0,'0002',441,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92476,1,'0','0','2011-10-17 14:20:20',0,'0002',442,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92477,1,'0','0','2011-10-17 14:20:50',0,'0002',443,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92478,1,'0','0','2011-10-17 14:21:20',0,'0002',444,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92479,1,'0','0','2011-10-17 14:21:50',0,'0002',445,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92480,1,'0','0','2011-10-17 14:21:51',0,'0002',446,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92481,1,'0','0','2011-10-17 14:22:21',0,'0002',447,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92482,1,'0','0','2011-10-17 14:22:51',0,'0002',448,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92483,1,'0','0','2011-10-17 14:23:21',0,'0002',449,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92484,1,'0','0','2011-10-17 14:23:51',0,'0002',450,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92485,1,'0','0','2011-10-17 14:23:52',0,'0002',451,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92486,1,'0','0','2011-10-17 14:24:22',0,'0002',452,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92487,1,'0','0','2011-10-17 14:24:52',0,'0002',453,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92488,1,'0','0','2011-10-17 14:25:22',0,'0002',454,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92489,1,'0','0','2011-10-17 14:25:52',0,'0002',455,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92490,1,'0','0','2011-10-17 14:25:53',0,'0002',456,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92491,1,'0','0','2011-10-17 14:26:23',0,'0002',457,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92492,1,'0','0','2011-10-17 14:26:53',0,'0002',458,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92493,1,'0','0','2011-10-17 14:27:23',0,'0002',459,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92494,1,'0','0','2011-10-17 14:27:53',0,'0002',460,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92495,1,'0','0','2011-10-17 14:27:54',0,'0002',461,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92496,1,'0','0','2011-10-17 14:28:24',0,'0002',462,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92497,1,'0','0','2011-10-17 14:28:54',0,'0002',463,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92498,1,'0','0','2011-10-17 14:29:24',0,'0002',464,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92499,1,'0','0','2011-10-17 14:29:54',0,'0002',465,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92500,1,'0','0','2011-10-17 14:29:55',0,'0002',466,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92501,1,'0','0','2011-10-17 14:30:25',0,'0002',467,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92502,1,'0','0','2011-10-17 14:30:55',0,'0002',468,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92503,1,'0','0','2011-10-17 14:31:25',0,'0002',469,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92504,1,'0','0','2011-10-17 14:31:55',0,'0002',470,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92505,1,'0','0','2011-10-17 14:31:56',0,'0002',471,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92506,1,'0','0','2011-10-17 14:32:26',0,'0002',472,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92507,1,'0','0','2011-10-17 14:32:56',0,'0002',473,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92508,1,'0','0','2011-10-17 14:33:27',0,'0002',474,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92509,1,'0','0','2011-10-17 14:33:56',0,'0002',475,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92510,1,'0','0','2011-10-17 14:33:57',0,'0002',476,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92511,1,'0','0','2011-10-17 14:34:27',0,'0002',477,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92512,1,'0','0','2011-10-17 14:34:57',0,'0002',478,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92513,1,'0','0','2011-10-17 14:35:27',0,'0002',479,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92514,1,'0','0','2011-10-17 14:35:57',0,'0002',480,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92515,1,'0','0','2011-10-17 14:35:58',0,'0002',481,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92516,1,'0','0','2011-10-17 14:36:28',0,'0002',482,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92517,1,'0','0','2011-10-17 14:36:58',0,'0002',483,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92518,1,'0','0','2011-10-17 14:37:28',0,'0002',484,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92519,1,'0','0','2011-10-17 14:37:58',0,'0002',485,0,'~\0\0\01\0\0\0	��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92520,1,'0','0','2011-10-17 14:37:59',0,'0002',486,0,'~\0\0\01\0\0\0��~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92521,1,'0','0','2011-10-18 16:20:41',0,'0002',12,1,'~\0\0\01\0\0\0\0?~','终端上行心跳指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92522,2,'0','0','2011-10-18 16:20:41',0,'8001',11,0,'�\01\0\0\0\0\0\0\0�','平台通用应答:对应终端流水号:11,对应消息ID:0x0002,成功/确认',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92523,1,'0','0','2011-10-20 11:13:00',0,'0100',8,1,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006;~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92524,2,'0','0','2011-10-20 11:13:30',0,'8100',6,0,'�\0\01\0\0\0\0\0�','消息无分包,注册应答:车辆已被注册',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92525,1,'0','0','2011-10-20 11:13:30',0,'0100',8,0,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006;~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92526,0,'0','0','2011-10-20 11:19:48',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92527,1,'0','0','2011-10-20 11:21:08',0,'0100',26,1,'~\0\0\"1\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006)~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92528,1,'0','0','2011-10-20 11:21:45',0,'0100',28,1,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002/~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92529,0,'0','0','2011-10-20 11:22:15',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92530,1,'0','0','2011-10-20 11:26:00',0,'0100',36,1,'~\0\0\"1\0\0\0\0$\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92531,1,'0','0','2011-10-20 11:29:25',0,'0100',47,1,'~\0\0\"1\0\0\0\0/\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92532,1,'0','0','2011-10-20 11:33:40',0,'0100',67,0,'~\0\0\"1\0\0\0\0C\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0005p~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92533,1,'0','0','2011-10-20 11:34:15',0,'0100',70,0,'~\0\0\"1\0\0\0\0F\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002u~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92534,1,'0','0','2011-10-20 11:34:15',0,'0100',71,1,'~\0\0\"1\0\0\0\0G\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002t~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92535,1,'0','0','2011-10-20 11:34:15',0,'0100',71,0,'~\0\0\"1\0\0\0\0G\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002t~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92536,1,'0','0','2011-10-20 11:34:20',0,'0100',73,1,'~\0\0\"1\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0005z~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92537,1,'0','0','2011-10-20 11:35:02',0,'0100',75,1,'~\0\0\"1\0\0\0\0K\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001x~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92538,1,'0','0','2011-10-20 11:35:32',0,'0100',75,0,'~\0\0\"1\0\0\0\0K\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001x~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92539,2,'0','0','2011-10-20 11:35:32',0,'8100',75,0,'�\0\01\0\0\0\0K\0K�','消息无分包,注册应答:数据库中无该车辆',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92540,0,'0','0','2011-10-20 11:36:12',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92541,1,'0','0','2011-10-20 11:39:03',0,'0100',96,1,'~\0\0\"1\0\0\0\0`\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004S~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92542,1,'0','0','2011-10-20 11:40:58',0,'0100',107,1,'~\0\0\"1\0\0\0\0k\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003X~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92543,1,'0','0','2011-10-20 11:40:58',0,'0100',107,0,'~\0\0\"1\0\0\0\0k\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003X~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92544,1,'0','0','2011-10-20 11:42:18',0,'0100',113,1,'~\0\0\"1\0\0\0\0q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003B~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92545,1,'0','0','2011-10-20 11:42:48',0,'0100',113,0,'~\0\0\"1\0\0\0\0q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003B~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92546,1,'0','0','2011-10-20 11:42:53',0,'0100',115,1,'~\0\0\"1\0\0\0\0s\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0005@~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92547,1,'0','0','2011-10-20 11:44:13',0,'0100',120,1,'~\0\0\"1\0\0\0\0x\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004K~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92548,1,'0','0','2011-10-20 11:45:58',0,'0100',125,1,'~\0\0\"1\0\0\0\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003N~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92549,1,'0','0','2011-10-20 11:46:28',0,'0100',125,0,'~\0\0\"1\0\0\0\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003N~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92550,1,'0','0','2011-10-20 11:48:43',0,'0100',131,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92551,1,'0','0','2011-10-20 11:49:13',0,'0100',131,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92552,1,'0','0','2011-10-20 11:49:18',0,'0100',134,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92553,1,'0','0','2011-10-20 11:50:58',0,'0100',138,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92554,1,'0','0','2011-10-20 11:50:58',0,'0100',138,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92555,2,'0','0','2011-10-20 11:50:58',0,'8100',138,0,'�\0\01\0\0\0\0�\0��','消息无分包,注册应答:数据库中无该车辆',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92556,1,'0','0','2011-10-20 11:57:04',0,'0100',159,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92557,1,'0','0','2011-10-20 11:57:19',0,'0100',162,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92558,1,'0','0','2011-10-20 11:57:49',0,'0100',162,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92559,1,'0','0','2011-10-20 12:02:04',0,'0100',179,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92560,1,'0','0','2011-10-20 12:02:05',0,'0100',179,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92561,1,'0','0','2011-10-20 12:03:14',0,'0100',184,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92562,1,'0','0','2011-10-20 12:03:15',0,'0100',184,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92563,1,'0','0','2011-10-20 12:03:20',0,'0100',187,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92564,1,'0','0','2011-10-20 12:04:00',0,'0100',190,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92565,1,'0','0','2011-10-20 12:04:50',0,'0100',195,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92566,1,'0','0','2011-10-20 12:07:39',0,'0100',208,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0002�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92567,1,'0','0','2011-10-20 12:07:54',0,'0100',209,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92568,1,'0','0','2011-10-20 12:07:55',0,'0100',210,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92569,1,'0','0','2011-10-20 12:08:24',0,'0100',210,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92570,2,'0','0','2011-10-20 12:08:25',0,'8100',210,0,'�\0\01\0\0\0\0�\0��','消息无分包,注册应答:数据库中无该车辆',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92571,1,'0','0','2011-10-20 12:08:29',0,'0100',212,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92572,1,'0','0','2011-10-20 12:09:14',0,'0100',218,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92573,1,'0','0','2011-10-20 12:10:29',0,'0100',225,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92574,1,'0','0','2011-10-20 12:10:39',0,'0100',227,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92575,1,'0','0','2011-10-20 12:10:45',0,'0100',229,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92576,1,'0','0','2011-10-20 12:11:49',0,'0100',233,0,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92577,1,'0','0','2011-10-20 12:17:49',0,'0100',249,1,'~\0\0\"1\0\0\0\0�\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001�~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92578,2,'0','0','2011-10-20 12:17:49',0,'8100',248,0,'�\0\01\0\0\0\0�\0��','消息无分包,注册应答:数据库中无该车辆',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92579,1,'0','0','2011-10-20 12:20:34',0,'0100',257,1,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A00033~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92580,1,'0','0','2011-10-20 12:25:54',0,'0100',270,1,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004<~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92581,1,'0','0','2011-10-20 12:27:04',0,'0100',273,1,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001#~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92582,1,'0','0','2011-10-20 12:27:34',0,'0100',273,0,'~\0\0\"1\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001#~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92583,1,'0','0','2011-10-20 12:33:24',0,'0100',294,1,'~\0\0\"1\0\0\0&\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0004~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92584,1,'0','0','2011-10-20 12:37:39',0,'0100',303,1,'~\0\0\"1\0\0\0/\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92585,1,'0','0','2011-10-20 12:42:44',0,'0100',317,1,'~\0\0\"1\0\0\0=\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92586,1,'0','0','2011-10-20 12:43:14',0,'0100',317,0,'~\0\0\"1\0\0\0=\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0003~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92587,1,'0','0','2011-10-20 12:43:19',0,'0100',319,1,'~\0\0\"1\0\0\0?\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0005\r~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92588,1,'0','0','2011-10-20 12:43:59',0,'0100',321,0,'~\0\0\"1\0\0\0A\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001s~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92589,1,'0','0','2011-10-20 12:44:44',0,'0100',326,1,'~\0\0\"1\0\0\0F\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0006t~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92590,0,'0','0','2011-10-20 12:44:49',0,'0000',0,1,'','',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92591,1,'0','0','2011-10-20 12:46:44',0,'0100',333,1,'~\0\0\"1\0\0\0M\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92592,1,'0','0','2011-10-20 12:47:14',0,'0100',333,0,'~\0\0\"1\0\0\0M\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001~','终端上行注册指令',0,NULL,NULL,NULL,NULL);
insert  into `eventlog_copy`(`ID`,`Flag`,`TargetID`,`TestID`,`EventDate`,`ExampleID`,`MessageID`,`SerialNumber`,`ResultFlag`,`OraginMsg`,`OutMsg`,`MediaType`,`MediaData`,`Note`,`IsAlarm`,`AlarmDescription`) values (92593,1,'0','0','2011-10-20 13:37:28',0,'0100',9,1,'~\0\0\"1\0\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0BJD-A0001:~','终端上行注册指令',0,NULL,NULL,NULL,NULL);

/*Table structure for table `example` */

DROP TABLE IF EXISTS `example`;

CREATE TABLE `example` (
  `ExampleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否只读，默认为1：只读',
  `FunctionID` int(11) NOT NULL COMMENT '功能ID',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定 ',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '答应结果',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='统系用例表';

/*Data for the table `example` */

insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (18,'从链路连接请求消息用例',1,82,1,0,10,'','','','从链路返回连接结果为以下信息中的一个：\r\n成功；\r\n校验码错误；\r\n资源紧张，稍后再连接（已经占用）；\r\n其他。','0x9002','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (19,'从链路连接保持请求消息用例',1,94,1,0,60,'','空','','返回数据体为空，消息ID为0x9006。','0x9006','下发从链路连接保持请求消息。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (20,'从链路注销请求消息用例',1,100,0,0,5,'\0\0?','','[DWORD]4231\r\n','','0x9003','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (21,'主链路注销应答消息',1,1011,0,1,2,'\0\0A','主链路注销','[DWORD]321\r\n','','主链路注销?','确定主链路注销','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (28,'主链路登录用例',1,85,0,1,30,'','下级平台主动上传链路登录请求','','下级平台上传以下信息：\r\n1.用户名；\r\n2.密码；\r\n3.对应从链路服务端IP地址；\r\n4.对应从链路服务端口号。','0x1001','请上传用户名和密码等登录信息。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (29,'主链路连接保持用例',1,87,1,1,60,'','主链路连接保持','','下级平台向上级平台发送主链路连接保持请求消息。','0x1005','执行此用例前请先提示下级平台登录上级平台，此用例检测过程中企业平台不作任何操作','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (30,'主链路动态信息交换数据体用例',1,137,1,1,20,'','','','','0x1200','请上传车辆动态信息交换业务数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (31,'上传车辆注册信息用例',1,244,1,1,120,'','上传车辆注册信息','','下级平台主动上传车辆注册信息：\r\n车牌号\r\n车牌颜色\r\n子业务类型=0x1201\r\n平台唯一编码\r\n车载终端厂商唯一编码\r\n车载终端型号\r\n车载终端编号\r\n车载终端SIM卡号','0x1201','提示送检平台“上传车辆注册信息（0x1201）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (32,'实时上传车辆定位信息用例',1,245,1,1,60,'','下级平台实时上传车辆定位信息','','返回实时上传车辆定位信息请求指令\r\n包括车牌号、车牌颜色、定位数据、子业务数据类型=0x1202','0x1202','提示送检平台“实时上传车辆定位信息（0x1202）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (33,'车辆定位信息自动补报请求用例',1,247,1,1,120,'','','','返回车辆定位信息：\r\n车牌号\r\n车牌颜色\r\n子业务数据类型=0x1203\r\n定位数据','0x1203','提示送检平台上传“车辆定位信息自动补报请求（0x1203）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (34,'申请交换指定车辆定位信息请求',1,248,1,1,60,'','请上传申请交换指定车辆定位信息','','返回申请交换指定车辆定位信息指令，\r\n包括：车牌号、车牌颜色、开始时间、结束时间、子业务数据类型=0x1207','0x1207','提示送检平台上传“申请交换指定车辆定位信息（0x1207）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (35,'取消交换指定车辆定位信息请求用例',1,250,1,1,60,'','取消交换指定车辆定位信息请求','','返回取消交换指定车辆定位信息请求指令\r\n包括：车牌号、车牌颜色、子业务数据类型=0x1208','0x1208','提示送检平台上传“取消交换指定车辆定位信息请求（0x1208）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (36,'补发车辆定位信息请求',1,249,1,1,120,'','下级平台补发车辆定位信息','','返回补发车辆定位信息请求指令，\r\n包括车牌号、车牌颜色、开始时间、结束时间、子业务数据类型=0x1209，并应答0x9209','0x1209','提示送检平台上传“补发车辆定位信息请求（0x1209）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (37,'从链路车辆动态信息数据体用例',1,109,0,2,2,'vqlRMTIzNDUAAAAAAAAAAAAAAAAAAZIBAAAABAAAEjQ=','车牌号=“京Q12345”；\r\n后续数据长度=4；\r\n数据=1234','[STRING|21]京Q12345\r\n[BYTE|1]1\r\n[WORD]37377\r\n[DWORD]0004\r\n[BYTE|4]1234\r\n','根据具体消息不同返回不同的内容','0x9200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (38,'交换车辆定位信息用例',1,112,0,1,10,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEeCgfbETIJBvvfUAHg9QAAZABkAAAnEAEsAfQAAAAAAAAAAA==','车牌号=测A12345;\r\n车辆颜色=2(黄色);\r\n加密标识=1（加密）;\r\n子标识=0x9202;\r\n4.5.8.1---共上传1条定位数据;\r\r\n时间=2011-10-30 17:50:09;\r\r\n经度=117.17度;\r\n\r纬度=31.52度;\r\n\r速度=100km/h;\r\n\r行驶记录速度=100km/h;\r\n\r车辆当前里程=10000km;\r\n\r方向=300°;\r\n\r海拔高度=500m;\r\n\r车辆状态=00000000;\r\r\n报警状态=00000000(无报警)。','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|36]011E0A07DB11320906FBDF5001E0F5000064006400002710012C01F40000000000000000\r\n','','','交换车辆定位信息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (39,'车辆定位信息交换补发消息用例',1,113,0,1,10,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAASQIBHgoH2xEyCQb731AB4PUAAGQAZAAAJxABLAH0AAAAAAAAAAABHgoH2xEyEwb731AB4PUAAGQAZAAAJxABLAH0AAAAAAAAAAE=','车牌号=测A12345;\r\n车辆颜色=2(黄色);\r\n加密标识=1（加密）;\r\n子业务标识=0x9203;\r\n4.5.8.1---共上传2条定位数据;\r\r\n时间=(1)2011-10-30 17:50:09，(2)2011-10-30 17:50:19;\r\n\r经度=117.17度;\r\r\n纬度=31.52度;\r\r\n速度=100km/h;\r\n\r行驶记录速度=100km/h;\r\r\n车辆当前里程=10000km;\r\n\r方向=300°;\r\n\r海拔高度=500m;\r\r\n车辆状态=00000000;\r\r\n报警状态=（1）00000000，（2）00000001。','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]73\r\n[BYTE|1]2\r\n[BYTE|36]011E0A07DB11320906FBDF5001E0F5000064006400002710012C01F40000000000000000\r\n\r[BYTE|36]011E0A07DB11321306FBDF5001E0F5000064006400002710012C01F40000000000000001\r\n','','','车辆定位信息交换补发消息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (40,'启动车辆定位信息交换请求用例',1,110,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIFAAAAAQA=','车牌号=测A12345；\r\n车牌颜色编码=2（黄色）；\r\n子业务类型标识=0x9205；\r\n后续数据长度=1；\r\n启动定位信息交换原因=00（车辆进入指定区域）','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37381\r\n[DWORD]1\r\n[BYTE|1]00\r\n','上级平台下发0x9205，并应答下级平台0x1205','0x1205','启动车辆定位信息交换请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (41,'结束车辆定位信息交换请求',1,114,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIGAAAAAQE=','车牌号=测A12345；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=01（人工停止交换）\r\n','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]01\r\n','上级平台下发0x9206,下级平台应答0x1206','0x1206','结束车辆定位信息交换：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (42,'上报驾驶员身份识别信息请求用例',1,118,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIKAAAAAA==','车牌号=测A12345\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37386\r\n[DWORD]0\r\n','上级平台下发0x920A，并应答下级平台0x120A，收到内容如下：车牌号，车牌颜色，驾驶员姓名，身份证编号，从业资格证号，发证机构名称等','0x120A','上报驾驶员身份识别信息请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (43,'上报车辆电子运单请求用例',1,119,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApILAAAAAA==','车牌号=测A12345\r\n车牌颜色编码=2（黄色）\r\n子业务类型标示=0x920B\r\n数据长度=0','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37387\r\n[DWORD]0\r\n','上级平台下发0x920B，下级平台应答0x120B。收到消息内容如下：车牌号，车牌颜色，电子运单数据体长度，电子运单数据内容','0x120B','上报车辆电子运单请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (44,'主链路数据体用例',1,147,1,1,20,'','','','返回上传平台间交互信息数据体指令','0x1300','请上传平台间交互信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (46,'从链路平台信息交互数据用例',1,120,1,0,20,'?\0\0\0\0\0\054','子业务类型标识=0x9300\r\n数据=0','[WORD]37632\r\n[DWORD]5\r\n[BYTE|5]3534\r\n','','0x9300','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (59,'平台查岗请求用例',1,121,1,1,60,'kwEAAAApATExMTExMTExMTEAAAAAAAEAAAAUxOO589DVo78AAAAAAAAAAAAAAAA=','子业务类型=0x9301\r\n查岗对象类型=0x01\r\n查岗对象ID=平台ID（1111111111）\r\n信息ID=1\r\n信息长度=20\r\n信息内容=“你贵姓？”','[WORD]37633\r\n[DWORD]41\r\n[BYTE]1\r\n[STRING|12]1111111111\r\n[DWORD]1\r\n[DWORD]20\r\n[STRING|20]你贵姓？\r\n','上级平台下发0x9301，下级平台应答0x1301。查岗对象类型为1,查岗对象ID与中心分配的一致,消息ID为1。','0x1301','平台查岗请求（查岗对象类型=1）：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (61,'下发平台间报文请求用例',1,122,1,0,30,'kwIAAAA9ATExMTExMTExMTEAAAAAAAEAAAAUxr3MqLzksajOxDoxKzE9PwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==','子业务数据类型=0x9302\r\n信息ID=0x9302\r\n对象类型=0x01\r\n查岗对象ID=平台ID(1111111111)\r\n信息ID=1\r\n信息长度=17\r\n信息内容=“平台间报文:1+1=?”','[WORD]37634\r\n[DWORD]61\r\n[BYTE]1\r\n[STRING|12]1111111111\r\n[DWORD]1\r\n[DWORD]20\r\n[STRING|40]平台间报文:1+1=?\r\n','上级平台下发0x9302，并应答下级平台0x1302。对象类型为1,对象ID与中心分配的一致,消息ID为1','0x1302','下发平台间报文请求（对象类型<0X02）：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (62,'主链路车辆报警数据用例',1,149,1,1,20,'','主链路车辆报警数据','','返回主链路车辆报警信息业务数据体指令。','0x1400','请上传主链路车辆报警数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (91,'1.1.1终端注册成功',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=“10001”\r\n终端型号=“12345678”\r\n终端ID=“7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=京A12345','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答 注册成功；鉴权码=XXX','8100','1、确认企业平台中以下资料未被注册：\r\n终端ID=”7654321”\r\n车牌=”京A12345”\r\n2、在模拟终端发送终端注册指令\r\n3、观察模拟终端是否返回“成功；鉴权码=XXX”\r\n4、保存“鉴权码=XXX”的信息，以备“终端鉴权”功能测试。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (95,'上报报警信息用例',1,150,1,1,40,'','上报报警信息用例','','返回上报报警信息指令，\r\n包括车牌号、车牌颜色、报警信息来源、报警类型、报警时间、信息ID、报警信息内容','0x1402','请上传报警信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (98,'3.1.1文本信息下发',1,196,0,3,30,'Hb27zaiyv9Ct0unOxLG+0MXPos/Ct6LWuMHusuLK1KO6MTIzo6xBQkNEo6yjoUAjo6QlLi4uLi4=','1、从平台逐个下发所有类型的文本信息指令，下发内容譬如：“交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....”；\r\n2、观察模拟终端显示的标志和文本信息；\r\n3、观察平台接收到的终端应答指令。\r\n','[BYTE|1]1D\r\n[STRING|55]交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....\r\n','通用应答','0001','1、模拟终端显示的标志和文本信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (99,'1.1.2终端注册失败',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=”10001”\r\n终端型号=”12345678”\r\n终端ID=”7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=”京A12345”','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答：注册失败','8100','1、在模拟终端发送终端注册指令\r\n2、观察模拟终端是否返回“注册失败”\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (100,'从链路报警数据用例',1,124,1,0,20,'vqlRNTQzMjEAAAAAAAAAAAAAAAAAA5QAAAAACgAAAAAAAAADRlQ=','车牌号=“京Q54321”\r\n车牌颜色编码=3（黑色）\r\n子业务数据类型=0x9400\r\n数据=0x9400','[STRING|21]京Q54321\r\n[BYTE|1]3\r\n[WORD]37888\r\n[DWORD]10\r\n[BYTE|10]34654\r\n','','0x9400','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (104,'报警督办请求用例',1,125,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQBAAAAXAIACQAAAABOBVmyAACUAQAAAABOpjCyAFN1cGVydmlzb3IAAAAAAAAwMTA4ODQ1Njc4OQAAAAAAAAAAAHN1cGVydmlzb3JfZUBzdXBlci5jb20AAAAAAAAAAAAA','车牌号=“测A12345”\r\n车牌颜色编号=2（黄色）\r\n报警信息来源=0x02（企业监控平台）\r\n报警类型=0x0009（盗警）\r\n报警时间=2011-06-25 11:44:50\r\n报警督办ID=0x9401\r\n督办截止时间=2011-10-25 11:44:50\r\n督办级别=0x00（紧急）\r\n督办人=“Supervisor”\r\n督办人联系电话=“01088456789”\r\n督办联系电子邮件=“supervisor_e@super.com”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37889\r\n[DWORD]92\r\n[BYTE|1]02\r\n[WORD]9\r\n[TIME_T|8]2011-06-25 11:44:50\r\n[DWORD]37889\r\n[TIME_T|8]2011-10-25 11:44:50\r\n[BYTE|1]00\r\n[STRING|16]Supervisor\r\n[STRING|20]01088456789\r\n[STRING|32]supervisor_e@super.com\r\n','上级平台下发0x9401，下级平台应答0x1401\r\n返回消息参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色编号=2\r\n报警督办ID=37889\r\n报警处理结果=0x00（处理中）\r\n/报警处理结果=0x01（已处理完毕）\r\n/报警处理结果=0x02（不作处理）\r\n/报警处理结果=0x02（将来处理）','0x1401','报警督办请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (105,'1.2.1终端注销成功',1,187,0,3,30,'','终端注销','','通用应答','0001','1、在模拟终端发送终端注销指令\r\n2、观察平台是否下发通用应答：成功\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (110,'1.3.1终端鉴权成功',1,188,0,3,30,'MTIzNDU2Nzg5MEE=','1、前提：终端注册成功；\r\n2、使用注册时平台下发的鉴权码进行鉴权；\r\n鉴权码：\"1234567890A\"','[STRING|11]1234567890A\r\n','通用应答：鉴权成功','0001','1、在模拟终端发送终端鉴权指令\r\n2、观察平台是否下发通用应答：成功','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (112,'1.4.1终端心跳',1,189,0,3,30,'','1、前提：终端鉴权成功；\r\n2、终端开始按照默认时间间隔发送心跳，平台应答心跳；','','通用应答：平台的心跳应答','8001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (117,'3.2.1追加事件',1,197,0,3,30,'AgIBCLO1wb65ytXPAgSxqdPq','追加事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：暴雨','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]2\r\n[BYTE|1]4\r\n[STRING|4]暴雨\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (118,'3.2.2删除特定事件',1,197,0,3,30,'BAEBAA==','删除特定事件：\r\n设置总数：1\r\n事件项列表：\r\n事件ID：1\r\n事件内容长度：0','[BYTE|1]4\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台设置后结果相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (119,'3.3.1事件报告',1,198,0,3,30,'AQ==','事件报告：\r\n事件ID：1','[BYTE|1]1\r\n','平台通用应答','8001','模拟终端上传的事件报告，平台能正确显示。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (120,'6.1.1更新圆形区域',1,206,0,0,30,'AAEAAAABAD4CV6ugBu6Y4AAAA+gAZAo=','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n中心点纬度=39.3度\r\n中心点经度=116.3度\r\n半径=1000米\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]00\r\n[BYTE|1]01\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]39300000\r\n[DWORD]116300000\r\n[DWORD]1000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (121,'6.2.1删除圆形区域',1,207,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (122,'1.5.1设置终端参数',1,190,0,3,30,'BgAAAAEEAAAAPAAAACAEAAAAAAAAAEALMDEwODc2NTQzMjEAAABBETEzNjAxMDEyMzQ1AAAARQQAAAAAAAAARgQAAAA8','平台下发如下参数\r\n心跳间隔=60s，\r\n位置汇报策略=位置汇报，\r\n监控平台电话号码=01087654321，\r\n复位电话号码=13601012345，\r\n终端电话接听策略=自动接听，\r\n每次最长通话时间=60s','[BYTE|1]6\r\n[DWORD]1\r\n[BYTE|1]4\r\n[DWORD]60\r\n[DWORD]32\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]64\r\n[BYTE|1]B\r\n[STRING|11]01087654321\r\n[DWORD]65\r\n[BYTE|1]11\r\n[STRING|11]13601012345\r\n[DWORD]69\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]70\r\n[BYTE|1]4\r\n[DWORD]60','通用应答：成功','8001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (123,'3.4.1提问下发',1,199,0,3,30,'GBS1scews7XBvsrHt/HT0LnK1c+jvwEAAsrHAgACt/E=','提问下发：\r\n标志：0x14：终端TTS播读，广告屏显示；\r\n问题内容长度：20\r\n问题：当前车辆是否有故障？\r\n候选答案列表：\r\n答案ID：1\r\n答案内容长度：2\r\n答案：是\r\n答案ID：2\r\n答案内容长度：2\r\n答案：否','[BYTE|1]18\r\n[BYTE|1]14\r\n[STRING|20]当前车辆是否有故障？\r\n[BYTE|1]1\r\n[WORD]2\r\n[STRING|2]是\r\n[BYTE|1]2\r\n[WORD]2\r\n[STRING|2]否\r\n','终端通用应答','0001','1、模拟终端显示的提问内容与平台下发相同；\r\n2、平台正确接收模拟终端的通用应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (124,'6.3.1更新矩形区域',1,208,0,0,30,'AAEAAAABAD4CYruoBurizAJb8WAG9Yo4AGQK','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n左上点纬度=40.025度\r\n左上点经度=116.05678度\r\n右下点纬度=39.58度\r\n右下点经度=116.755度\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]0\r\n[BYTE|1]1\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (126,'3.8.1提问应答',1,231,0,3,30,'','提问应答：\r\n应答流水号：对应平台下发的提问的流水号；\r\n答案ID：1\r\n','','','','平台能解析出终端的的提问应答，答案ID是1；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (127,'从链路车辆静态信息交换业务',1,134,1,2,20,'y9VBMDAwMDEAAAAAAAAAAAAAAAAAMZYAAQ==','车牌号=“苏A00001”\r\n车牌颜色=1(蓝色)','[STRING|21]苏A00001\r\n[BYTE|1]31\r\n[WORD]38400\r\n[BYTE|1]1\r\n','','0x9600','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (128,'主链路车辆静态信息数据用例',1,154,1,1,20,'','主链路车辆静态信息数据','','返回主链路车辆静态信息数据指令','0x1600','请上传车辆静态信息数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (129,'主链路监管数据用例',1,152,1,1,20,'','主链路监管数据','','返回主链路监管业务数据指令','0x1500','请在送检企业平台向监管平台发送车辆监管业务','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (130,'补报车辆静态信息请求消息用例',1,135,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApYBAAAAAA==','车牌号=测A12345\r\n车牌颜色=2(黄色)','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38401\r\n[DWORD]00000000\r\n\r\n','上级平台下发0x9601，并收到下级平台的应答0x1601\r\n车牌号=;\r\n车牌颜色=;\r\n车辆类型=;\r\n运输行业编码=;\r\n车籍地=;\r\n业户ID=;\r\n业户名称=;\r\n业户联系电话=;\r\n……','0x1601','补报车辆静态信息请求消息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (131,'6.4.1删除矩形区域',1,209,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (132,'3.5.1信息点播菜单追加',1,200,0,3,30,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻\r\n','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (133,'从链路监管数据用例',1,128,1,2,20,'tKhBNTQzMjEAAAAAAAAAAAAAAAAAOZUAAAAABQAAASNF','车牌号=“川A54321”\r\n车牌颜色编号=9（其他）\r\n监管数据=0x12345','[STRING|21]川A54321\r\n[BYTE|1]39\r\n[WORD]38144\r\n[DWORD]5\r\n[BYTE|5]12345\r\n','','0x9500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (134,'3.6.1信息点播',1,201,0,3,30,'','信息点播\r\n信息类型：1\r\n点播/取消标志：1，点播；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、平台能定期根据终端点播的信息，下发点播的相关信息。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (135,'车辆单向监听请求用例',1,129,1,1,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUBAAAAFDAxMDg4Nzg5NjUtNjc1NAAAAAAA','车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n回拨电话号码=“0108878965-6754”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38145\r\n[DWORD]20\r\n[STRING|20]0108878965-6754\r\n','上级平台下发0x9501，下级平台应答0x1501。返回消息内容参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n应答结果=0x00（监听成功）\r\n/应答结果=0x01（监听失败）','0x1501','车辆单向监听请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (136,'6.5.1更新多边形区域',1,210,0,0,60,'AAAAAQA+AGQKAAQCW/FgBurizAJiu6gG6uLMAmK7qAb1ijgCW/FgBvWKOA==','区域ID=1\r\n区域属性=0x003E\r\n最高速度=100公里/小时\r\n超速持续时间=10秒\r\n区域总顶点数=4\r\n顶点纬度=39.58度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.755度\r\n顶点纬度=39.58度\r\n顶点经度=116.755度','[DWORD]1\r\n[WORD]62\r\n[WORD]100\r\n[BYTE|1]0A\r\n[WORD]4\r\n[DWORD]39580000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116755000\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n','成功','0001','1、平台下发设置多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (137,'3.7.1信息服务',1,202,0,3,30,'AQAqz8LO57Gxvqm12Mf4vavT0MDX0+rM7Mb4o6zH68zhx7DX9rrD17yxuKGj','信息类型：1\r\n信息长度：42\r\n信息内容：下午北京地区将有雷雨天气，请提前做好准备。','[BYTE|1]1\r\n[WORD]42\r\n[STRING|42]下午北京地区将有雷雨天气，请提前做好准备。\r\n','终端通用应答','0001','1、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (139,'6.6.1删除多边形区域',1,211,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (140,'车辆拍照请求用例',1,130,0,0,60,'vqlBNTQzMjEAAAAAAAAAAAAAAAAAMZUCAAAAAgkI','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n镜头ID=0x09\r\n照片大小=0x08（704*576）','[STRING|21]京A54321\r\n[BYTE|1]31\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]09\r\n[BYTE|1]08\r\n','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n拍照应答标识=0x00（不支持拍照相）\r\n/拍照应答标识=0x01（完成拍照）\r\n/拍照应答标识=0x02（完成拍照、照片数据稍后传送）\r\n/拍照应答标识=0x03（未拍照不在线）\r\n/拍照应答标识=0x04（未拍照无法使用指定镜头）\r\n/拍照应答标识=0x05（未拍照其他原因）\r\n/拍照应答标识=0x09（车牌号码错误）\r\n拍照位置点\r\n镜头ID=0x09\r\n图片长度\r\n图片大小==0x08（704*576）\r\n图片格式=0x01（jpg）\r\n/图片格式=0x02（gif）\r\n/图片格式=0x03（tiff）\r\n/图片格式=0x04（png）\r\n图片内容','0x9502','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (141,'下发车辆报文请求用例',1,131,1,1,60,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUDAAAADwAAAAEAAAAABr30vLEAAA==','车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n报文优先级=0x00\r\n报文内容=紧急','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38147\r\n[DWORD]15\r\n[DWORD]1\r\n[BYTE|1]00\r\n[DWORD]6\r\n[STRING|6]紧急\r\n','上级平台下发0x9503，下级平台应答0x1503。\r\n返回信息参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n消息ID=1\r\n应答结果=0x00（下发成功）\r\n/应答结果=0x01（下发失败）','0x1503','下发车辆报文请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (142,'下发车辆行驶记录请求用例',1,132,1,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUEAAAAAQE=','车牌号=测A12345\r\n车牌颜色=2（黄色）\r\n子业务类型标示=0x9504\r\n ','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38148\r\n[DWORD]1\r\n[BYTE|1]1\r\n','上级平台下发0x9504，下级平台应答0x1504。返回信息参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色=“黄色”\r\n车辆行驶记录信息=','0x1504','下发车辆行驶记录请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (143,'1.6.1查询终端参数',1,191,0,3,90,'','查询终端参数','','查询终端参数应答：59个参数\r\n参数总数=59\r\n参数ID=0x0001,参数长度=4,参数值=60;\r\n参数ID=0x0002,参数长度=4,参数值=20;\r\n参数ID=0x0003,参数长度=4,参数值=3;\r\n参数ID=0x0004,参数长度=4,参数值=25;\r\n参数ID=0x0005,参数长度=4,参数值=2;\r\n参数ID=0x0006,参数长度=4,参数值=86400;\r\n参数ID=0x0007,参数长度=4,参数值=1;\r\n参数ID=0x0010,参数长度=5,参数值=\"CMNET\";\r\n参数ID=0x0011,参数长度=4,参数值=\"card\";\r\n参数ID=0x0012,参数长度=4,参数值=\"card\";\r\n参数ID=0x0013,参数长度=9,参数值=\"127.0.0.1\";\r\n参数ID=0x0014,参数长度=7,参数值=\"CSYL.BJ\";\r\n参数ID=0x0015,参数长度=4,参数值=\"CSBJ\";\r\n参数ID=0x0016,参数长度=4,参数值=\"BJCS\";\r\n参数ID=0x0017,参数长度=13,参数值=\"192.168.1.106\";\r\n参数ID=0x0018,参数长度=4,参数值=6500;\r\n参数ID=0x0019,参数长度=4,参数值=6580;\r\n参数ID=0x0020,参数长度=4,参数值=0;\r\n参数ID=0x0021,参数长度=4,参数值=0;\r\n参数ID=0x0022,参数长度=4,参数值=300;\r\n参数ID=0x0027,参数长度=4,参数值=600;\r\n参数ID=0x0028,参数长度=4,参数值=10;\r\n参数ID=0x0029,参数长度=4,参数值=60;\r\n参数ID=0x002C,参数长度=4,参数值=1000;\r\n参数ID=0x002D,参数长度=4,参数值=2000;\r\n参数ID=0x002E,参数长度=4,参数值=500;\r\n参数ID=0x002F,参数长度=4,参数值=500;\r\n参数ID=0x0030,参数长度=4,参数值=130;\r\n参数ID=0x0040,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0041,参数长度=11,参数值=\"13601012345\";\r\n参数ID=0x0042,参数长度=11,参数值=\"18876543210\";\r\n参数ID=0x0043,参数长度=12,参数值=\"106590202345\";\r\n参数ID=0x0044,参数长度=11,参数值=\"13901011000\";\r\n参数ID=0x0045,参数长度=4,参数值=0;\r\n参数ID=0x0046,参数长度=4,参数值=60;\r\n参数ID=0x0047,参数长度=4,参数值=3600;\r\n参数ID=0x0048,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0049,参数长度=12,参数值=\"106590202347\";\r\n参数ID=0x0050,参数长度=4,参数值=0;\r\n参数ID=0x0051,参数长度=4,参数值=0x0000000F;\r\n参数ID=0x0052,参数长度=4,参数值=0;\r\n参数ID=0x0053,参数长度=4,参数值=0;\r\n参数ID=0x0054,参数长度=4,参数值=0x000000FF;\r\n参数ID=0x0055,参数长度=4,参数值=120;\r\n参数ID=0x0056,参数长度=4,参数值=30;\r\n参数ID=0x0057,参数长度=4,参数值=14400;\r\n参数ID=0x0058,参数长度=4,参数值=36000;\r\n参数ID=0x0059,参数长度=4,参数值=900;\r\n参数ID=0x005A,参数长度=4,参数值=600;\r\n参数ID=0x0070,参数长度=4,参数值=6;\r\n参数ID=0x0071,参数长度=4,参数值=128;\r\n参数ID=0x0072,参数长度=4,参数值=70;\r\n参数ID=0x0073,参数长度=4,参数值=60;\r\n参数ID=0x0074,参数长度=4,参数值=255;\r\n参数ID=0x0080,参数长度=4,参数值=983490;\r\n参数ID=0x0081,参数长度=4,参数值=17;\r\n参数ID=0x0082,参数长度=4,参数值=0;\r\n参数ID=0x0083,参数长度=8,参数值=\"京A12345\";\r\n参数ID=0x0084,参数长度=4,参数值=1;','0104','平台对照“返回消息”逐一检查读取的59个参数是否与终端上报参数相同','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (144,'车辆应急接入监管平台请求用例',1,133,1,0,40,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUFAAAAkQAAAAAAAACyEN+3/s7xxvdBUE4AAAAAAAAAAAAAAEFkbWluaVVzZXIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyM3JzZDMyNDUAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjE4NAAAAAAAAAAAAAAAAAAAAAAAAAAiYB+QAAAAAbG18DM=','车牌号=“测A12345”\r\n车牌颜色=“2”\r\n鉴权码=“B210DF”\r\n拨号点名称=“服务器APN”\r\n拨号用户名=“AdminiUser”\r\n拨号密码=23rsd3245\r\n服务器IP=“192.168.1.184”\r\nTCP端口=“8800”\r\nUDP端口=“8080”\r\n结束时间=“2012-08-01 14:20:35”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38149\r\n[DWORD]145\r\n[BYTE|10]B210DF\r\n[STRING|20]服务器APN\r\n[STRING|49]AdminiUser\r\n[STRING|22]23rsd3245\r\n[STRING|32]192.168.1.184\r\n[WORD]8800\r\n[WORD]8080\r\n[TIME_T|8]2200-08-01 14:20:35\r\n','上级平台下发0x9505，下级平台应答0x1505。\r\n返回信息参考如下：\r\n车牌号=“测A12345”\r\n应答结果=0x00（车载终端成功收到该命令）\r\n/应答结果=0x01（无该车辆）\r\n/应答结果=0x02（其它原因失败）','0x1505','车辆应急接入监管平台请求：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (146,'报警预警消息用例',1,126,0,1,15,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAHgMACwAAAABOBZKzAAAAD8arwOvCt8/fsai+rwAAAA==','车牌号=“测A12345”\r\n车牌颜色编码=4（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=11\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“偏离路线报警”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]11\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]偏离路线报警\r\n','无','','报警预警消息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (147,'实时交换报警用例',1,127,0,1,15,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQDAAAAHgMACAAAAABOBZKzAAAAD9S9veexqL6vAAAAAAAAAA==','车牌号=“测A12345”\r\n车牌颜色编码=2（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=08\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“越界报警”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37891\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]08\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]越界报警\r\n','无','','实时交换报警：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (148,'4.1.1电话回拨',1,203,0,3,60,'ADAxMDg4ODg4ODg4','电话回拨(普通通话):\r\n标志 = 0 \r\n电话号码 = \"01088888888\"','[BYTE|1]0\r\n[STRING|11]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (149,'4.1.2电话回拨_监听',1,203,0,3,60,'ATAxMDg4ODg4ODg4AAAAAAAAAAAA','电话回拨(监听):\r\n标志 : 1\r\n电话号码 : 01088888888','[BYTE|1]1\r\n[STRING|20]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (150,'4.2.1设置电话本_删除',1,204,0,0,60,'AA==','删除终端上存储的所有联系人:\r\n设置类型 = 0','[BYTE|1]0','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (151,'1.7.1终端控制',1,192,0,3,90,'ATtDTU5FVDs7OzE5Mi4xNjguMS4xMDM7NjAwMDs7MTAwMDE7MS4wMzsxLjAwOzM2MDA=','无限升级指令：\r\nURL地址=;(空)\r\n拨号点名称=\"CMNET\"\r\n拨号用户名=;\r\n拨号密码=;\r\n地址=\"192.168.1.103\"\r\nTCP端口=6000\r\nUDP端口=;\r\n硬件版本=\"1.03\"\r\n固件版本=\"1.00\"\r\n连接到服务器的时限=3600','[BYTE|1]1\r\n[STRING|49];CMNET;;;192.168.1.103;6000;;10001;1.03;1.00;3600\r\n','通用应答：成功','0001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (152,'4.2.2设置电话本_更新电话本',1,204,0,3,60,'AQUBCzEzODEyMzQ1Njc4BNXFyP0CCzEzODEyMzQ1Njc5BMDuy8QDCzEzODEyMzQ1NjgwBM31zuUDDDA3NTUxMjM0NTY3OAbVxcj9y8QDDDA3NjkzMzMzNTU1NQbVxcj9zuU=','更新电话本(删除终端中已有全部联系人并追加消息中的联系人):\r\n设置类型 = 1\r\n联系人总数= 5\r\n联系人项 :\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 11\r\n电话号码 = \"13812345679\"\r\n联系人长度 = 4\r\n联系人 = \"李四\"\r\n\r\n标志 = 3\r\n号码长度 = 11\r\n电话号码 = \"13812345680\"\r\n联系人长度 = 4\r\n联系人 = \"王五\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"张三四\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"076933335555\"\r\n联系人长度 = 6\r\n联系人 = \"张三五\"\r\n\r\n','[BYTE|1]1\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0B\r\n[STRING|11]13812345679\r\n[BYTE|1]4\r\n[STRING|4]李四\r\n[BYTE|1]3\r\n[BYTE|1]0B\r\n[STRING|11]13812345680\r\n[BYTE|1]4\r\n[STRING|4]王五\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]张三四\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]076933335555\r\n[BYTE|1]6\r\n[STRING|6]张三五\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (153,'6.7.1更新路线',1,212,0,0,30,'AAAAAQA+AAYAAAABAAAAAQJgU0AG71wwZAIAZAoAAAACAAAAAgJgVmAG74NAZAMCWAAKAGQKAAAAAwAAAAICYF1oBu/RYGQDAlgACgBkCgAAAAQAAAACAmBiGAbwPsBkAwJYAAoAZAoAAAAFAAAAAgJgXWgG8E5gZAMCWAAKAGQKAAAABgAAAAICYFw8BvCUsGQDAlgACgBkCg==','路线ID=1\r\n路线属性=62\r\n路线总拐点数=6\r\n\r\n路段ID=1\r\n拐点ID=1，39.867200，116.350000，路宽=100米，属性=2，限速=100公里/小时，超速持续时间=10秒\r\n\r\n路段ID=2\r\n拐点ID=2，39.868000，116.360000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=3，39.869800，116.380000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=4，39.871000，116.408000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=5，39.869800，116.412000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=6，39.869500，116.430000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒','[DWORD]1\r\n[WORD]62\r\n[WORD]6\r\n[DWORD]1\r\n[DWORD]1\r\n[DWORD]39867200\r\n[DWORD]116350000\r\n[BYTE|1]64\r\n[BYTE|1]2\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]2\r\n[DWORD]2\r\n[DWORD]39868000\r\n[DWORD]116360000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]3\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116380000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]4\r\n[DWORD]2\r\n[DWORD]39871000\r\n[DWORD]116408000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]5\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116412000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]6\r\n[DWORD]2\r\n[DWORD]39869500\r\n[DWORD]116430000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A','成功','0001','1、平台下发设置路线指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (154,'6.8.1删除所有路线',1,213,0,0,30,'AA==','路线数=0','[BYTE|1]0\r\n','成功','0001','1、平台下发删除线路指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (155,'4.2.3设置电话本_追加电话本',1,204,0,3,60,'AgIBCzEzODEyMzQ1Njc4BNXFyP0CDDA3NTUxMjM0NTY3OAbA7sj9y8Q=','设置电话本（追加电话本）\r\n设置类型 = 2\r\n联系人总数 = 2\r\n联系人项\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"李三四\"\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]李三四\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (156,'4.2.4设置电话本_修改电话本',1,204,0,3,60,'13812345678张三','设置电话本（修改电话本，以联系人为索引）\r\n设置类型 = 3\r\n联系人总数 = 1\r\n联系人项\r\n','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (157,'2.1.1手动报警',1,193,0,3,90,'','使用模拟终端中的“4、手动位置上报”,选中紧急告警项。然后点击执行命令按钮','','通用应答：成功','0001','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (158,'2.2.1位置信息查询',1,194,0,3,90,'','查询位置信息','','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。至少一种以上的附件信息测试。','0201','检测平台收到的信息和模拟终端发送的位置信息内容一致','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (159,'2.3.1临时位置跟踪控制',1,195,0,3,180,'ABQAAAB4','时间间隔=10秒\r\n位置跟踪有效期=120秒','[WORD]20\r\n[DWORD]120\r\n','使用模拟终端的“5、定时位置上报”，按照设置的时间间隔及持续时间上报位置信息12次','0001','1、平台下发临时位置跟踪控制指令；\r\n2、模拟终端按“时间间隔”和“跟踪有效期”定时上传位置信息；\r\n3、观察平台显示的信息内容是否与模拟终端上传一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (160,'5.1.1车辆控制_车门解锁',1,205,0,3,60,'AA==','控制标志=0（车门解锁）','[BYTE|1]0\r\n','模拟终端自动回应，平台上收到回应，显示“车门解锁”状态：\r\n应答流水号= \r\n位置信息  =\r\n','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门解锁”状态。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (161,'5.1.2车辆控制_车门加锁',1,205,0,3,60,'AQ==','控制标志=1（车门加锁）','[BYTE|1]1\r\n','模拟终端自动回应，平台上收到回应，显示“车门加锁”状态：\r\n应答流水号= \r\n位置信息  =','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门加锁”状态。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (162,'9.2.1数据上行透传',1,225,0,3,30,'','使用模拟终端中手动用力中的“7、数据上行透传”，设置透传类型已经透传内容进行上传。','','平台通用应答','8001','1、观察平台收到的透传信息是否与模拟终端中输入的透传信息一致。\r\n2、观察平台是否正确返回应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (164,'3.5.2信息点播菜单修改',1,200,0,3,30,'AwIBAAjCt7/20MXPogIACLn6xNrQws7F','追加信息点播菜单\r\n设置类型：修改\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：路况信息\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国内新闻\r\n','[BYTE|1]3\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]路况信息\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国内新闻','终端通用应答','0001','1、平台下发信息点播菜单修改指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (165,'3.5.3信息点播菜单更新',1,200,0,3,30,'AQMBAAixsb6pzOzG+AIACLn6vMrQws7FAgAIwsPTzr3pydw=','追加信息点播菜单\r\n设置类型：更新\r\n信息项总数：3\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：北京天气\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国际新闻\r\n信息类型：3\r\n信息名称长度：8\r\n信息名称：旅游介绍\r\n','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]北京天气\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国际新闻\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]旅游介绍','终端通用应答','0001','1、平台下发信息点播菜单更新指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (166,'3.5.4信息点播菜单删除',1,200,0,3,30,'AA==','删除终端全部信息菜单','[BYTE|1]0\r\n','终端通用应答','0001','1、平台下发信息点播菜单删除指令；\r\n2、观察模拟终端是否全部删除信息点播菜单；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (167,'3.6.2信息取消',1,201,0,3,30,'','信息取消\r\n信息类型：1\r\n点播/取消标志：0，取消；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、终端不再接收到已取消的信息服务。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (168,'3.2.3更新事件',1,197,0,3,30,'AQMBCMDX0+rM7Mb4AgiztcG+ucrVzwMIwrfD5syuy/o=','更新事件\r\n设置总数：3\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：雷雨天气\r\n事件ID：2\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：3\r\n事件内容长度：8\r\n事件内容：路面坍塌','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]雷雨天气\r\n[BYTE|1]2\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]3\r\n[BYTE|1]8\r\n[STRING|8]路面坍塌','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (169,'3.2.4修改事件',1,197,0,3,30,'AwEBCLW9tO+3tMCh','修改事件：\r\n设置总数：1\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：到达反馈','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]到达反馈\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (170,'3.2.5删除全部事件',1,197,0,3,30,'AA==','删除终端现有所有事件','[BYTE|1]0\r\n','终端通用应答','0001','1、观察终端是否删除全部事件。\r\n2、平台是否正确接收终端应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (171,'8.1.1手动上发多媒体事件',1,218,0,1,30,'','','','多媒体事件上传通用应答','0x0800','请确认终端已发送多媒体事件信息上传报文','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (174,'7.2.1电子运单上报',1,216,0,3,90,'','使用模拟终端的“手工用例”中的“9、电子运单上报”发送数据\r\n运单数据“ID：20110628 货物名称：打印机20台 目的地：中国深圳”','','','8001','平台收到与终端发送信息一致的运单信息','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (175,'9.1.1数据下行透传',1,224,0,0,30,'123456789','透传消息类型=1\r\n透传消息内容=“123456789”','[BYTE|1]1\r\n[STRING|9]123456789\r\n','成功','0001','1、平台下发数据下行透传指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (177,'8.2.1抓拍摄像头通道1立即上传',1,220,0,2,60,'\0\0\0\0','拍摄照片：1\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：640×480\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回图片应当为标注通道号码的粉红色测试图片','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (182,'7.3.1驾驶员身份信息采集上报',1,217,0,3,90,'','使用模拟终端的“手工用例”中的“6、驾驶员身份信息采集上报”上报信息\r\n1、驾驶员姓名：张老三\r\n2、驾驶员身份证：010456198009080234\r\n3、从业资格证编号：0106785869869807\r\n4、发证机构名称：北京市道路运输管理局\r\n','','','8001','平台收到与上报数据相同的驾驶员信息','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (186,'8.2.2录像通道1上传',1,220,0,2,300,'Af//AAAAAQEBAQEBAQ==','终端录像\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：320×240\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]65535\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回数据为AVI（RAFF）容器H264视频，内容为蓝色GPS图样文字与JT/T808-2011字样3维动画','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (187,'8.3.1检索图片列表',1,221,0,3,60,'AAAAEQYBAAABEQYwI1lZ','多媒体类型：图片\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]0\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959\r\n','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (188,'8.3.2检索音频列表',1,221,0,3,60,'AQAAEQYBAAABEQYwI1lZ','多媒体类型：音频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]1\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (189,'8.3.3检索视频列表',1,221,0,3,60,'AgAAEQYBAAABEQYwI1lZ','多媒体类型：视频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]2\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (191,'8.4.1上传图像',1,222,0,2,60,'AAEAEQYlAAABEQYlI1lZAA==','上传存储图像\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0\r\n','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (192,'8.4.2上传音频',1,222,0,2,100,'AQEAEQYlAAABEQYlI1lZAA==','上传存储音频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (193,'8.4.3上传视频',1,222,0,2,300,'AgEAEQYlAAABEQYlI1lZAA==','上传存储视频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (194,'8.5.1录音32Kbps立即上传',1,223,0,2,100,'AQAAAAM=','开始录音\r\n持续录音\r\n实时上传\r\n采样率：32Kbps','[BYTE|1]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]3\r\n','开始录音通用应答','0x8804','同时上传多媒体数据\r\n音频内容为440Hz断续正弦波测试信号','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (195,'1.5.2超速设置',1,190,0,3,30,'','平台下发如下参数\r\n限速值=80公里/小时；\r\n持续时间=10秒','','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (196,'1.5.3疲劳驾驶设置',1,190,0,3,600,'AgAAAFcEAAACWAAAAFkEAAAAtA==','平台下发如下参数\r\n连续驾驶时间=600秒；\r\n最小休息时间=180秒。','[BYTE|1]2\r\n[DWORD]87\r\n[BYTE|1]4\r\n[DWORD]600\r\n[DWORD]89\r\n[BYTE|1]4\r\n[DWORD]180\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (197,'1.5.4超时停车',1,190,0,3,300,'AQAAAFoEAAABLA==','平台下发如下参数\r\n最长停车时间=300s','[BYTE|1]1\r\n[DWORD]90\r\n[BYTE|1]4\r\n[DWORD]300\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (198,'2.1.2电瓶欠压',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“终端主电源欠压”项\r\n然后点击执行命令按钮','','通用应答成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (199,'主链路注销用例',1,233,1,1,60,'','','','返回消息ID为1003','0x1003','通知被测下级平台发送主从链路断开消息（1003协议消息）','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (200,'2.1.3断电提醒',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“电路断开”项\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (201,'2.1.4终端故障',1,193,0,3,30,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。随便选择设备故障中的一个。\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (202,'2.1.5休眠',1,193,0,3,300,'AgAAACEEAAAAAAAAACcEAAABLA==','先通过平台进行参数设置：\r\n位置汇报方案=0（根据ACC状态）\r\n休眠时汇报时间间隔=300秒\r\n使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须选择ACC关。\r\n然后点击执行命令按钮','[BYTE|1]2\r\n[DWORD]33\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]39\r\n[BYTE|1]4\r\n[DWORD]300\r\n','平台通用应答','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (203,'3.5.5信息点播菜单追加',1,200,1,0,10,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (206,'3.2.6追加新事件',1,197,1,3,30,'','追加新事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：阴雨天气\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：路滑','','终端通用应答','0001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (207,'7.1.1行车记录仪数据采集',1,214,0,1,30,'','下行行车记录仪设置指令','','返回行车记录仪数据上传，其中命令字与下发命令字相同','0700','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (208,'下级平台实时上传车辆定位信息',1,234,0,1,60,'','','','返回消息的业务类型为1200，业务子类型为1202','','请提示下级平台实时上传不少于3条车牌号为“测A12345”，车牌颜色为黄色的车辆的定位信息(1202消息)','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (209,'下级平台车辆定位信息补报',1,235,0,1,60,'','','','最少收到3条定位信息补报，消息ID为1200，业务子类型为1203','','请提示下级平台补报不少于3条车牌号为“测A12345”，车牌颜色为黄色的车辆的自动补报信息（1203号协议）','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (211,'平台查岗请求用例2',1,121,1,1,60,'kwEAAAApAjExMTExMTExMTEAAAAAAAMAAAAU1tC5+srXtrzKx6O/AAAAAAAAAAA=','子业务类型=0x9301\r\n数据长度=41\r\n查岗对象类型=0x02\r\n查岗对象ID=业户经营许可证（1111111111）\r\n信息ID=3\r\n信息长度=20\r\n信息内容=“中国首都是？”','[WORD]37633\r\n[DWORD]41\r\n[BYTE]2\r\n[STRING|12]1111111111\r\n[DWORD]3\r\n[DWORD]20\r\n[STRING|20]中国首都是？\r\n','上级平台下发0x9301，下级平台应答0x1301。查岗对象类型为2,查岗对象ID为业户经营许可证,消息ID为3','0x1301','平台查岗请求（查岗对象类型=2）：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (212,'主动上报车辆电子运单信息消息用例',1,252,1,1,60,'','','','子业务数据类型=0x120D','0x120D','提示送检平台上传“主动上报车辆电子运单信息消息（0x120D）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (213,'主动上报驾驶员身份信息消息用例',1,251,1,1,60,'','','','子业务类型标识=0x120C','0x120C','提示送检平台上传“主动上报驾驶员身份信息消息（0x120C）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (214,'下发平台间报文请求用例2',1,122,1,1,30,'kwIAAAA9AjExMTExMTExMTEAAAAAAAIAAAAoxr3MqLzksajOxDo1KzU9PwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==','子业务数据类型=0x9302\r\n对象类型=0x02\r\n查岗对象ID=业户经营许可证（1111111111）\r\n信息ID=2\r\n报文内容=平台间报文:5+5=?','[WORD]37634\r\n[DWORD]61\r\n[BYTE]2\r\n[STRING|12]1111111111\r\n[DWORD]2\r\n[DWORD]40\r\n[STRING|40]平台间报文:5+5=?\r\n','上级平台下发0x9302，下级平台应答0x1302,查岗对象类型为2,查岗对象ID为业户经营许可证,消息ID为2','0x1302','下发平台间报文请求（对象类型=0X02）：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (215,'主链路登录请求用例',1,81,1,1,120,'','','','登录消息ID为0x1001','0x1001','请提示企业平台登录','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (216,'交换车辆静态信息用例',1,111,0,0,10,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIEAAAAJFRSQU5TX1RZUEU6PTAzMDtWSU46PbLiQTEyMzQ1AAAAAAAAAA==','车牌号=测A12345;\r\n车辆颜色=2（黄色）;\r\n子业务类型=0x9204;\r\n静态消息数据体=TRANS_TYPE:=030;VIN:=测A12345\r\n','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37380\r\n[DWORD]36\r\n[STRING|36]TRANS_TYPE:=030;VIN:=测A12345','','','交换车辆静态信息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (217,'主动上报报警处理结果信息消息用例',1,258,1,1,60,'','','','车牌号码=\r\n车牌颜色=\r\n子业务类型标识=0x1403\r\n报警处理结果，定义如下：\r\n0x00：处理中；\r\n0x01：已处理完毕；\r\n0x02：不作处理；\r\n0x03：将来处理。\r\n','0x1403','提示送检平台“主动上报报警处理结果信息消息（0x1403）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (218,'车辆拍照请求消息用例',1,242,1,1,60,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUCAAAAAgEB','车牌号=测A12345\r\n颜色=2\r\n子业务类型标识=38146\r\n后续长度=2\r\n镜头ID=1\r\nsize=1','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n\r\n','上级平台下发0x9502，下级平台应答0x1502','0x1502','车辆拍照请求消息：送检平台确认收到内容，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (219,'上报报警信息消息用例',1,256,1,1,60,'','','','车牌号=\r\n颜色=\r\n子业务类型标识=0x1402\r\n\r\n\r\n','0x1402','提示送检平台“上报报警信息（0x1402）”，通过判断数据收发一致性验证通过','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (220,'从链路注销用例',1,260,1,1,15,'','','','消息ID-0x9004','0x9004','','',1,0);

/*Table structure for table `example_copy` */

DROP TABLE IF EXISTS `example_copy`;

CREATE TABLE `example_copy` (
  `ExampleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否只读，默认为1：只读',
  `FunctionID` int(11) NOT NULL COMMENT '功能ID',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定 ',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '答应结果',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='统系用例表';

/*Data for the table `example_copy` */

insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (18,'从链路连接请求消息用例',1,93,0,0,10,'AAAABQ==','校验码，正常时与主链路登录应答消息中的校验码相同。','[DWORD]5\r\n','从链路返回连接结果为以下信息中的一个：\r\n成功；\r\n校验码错误；\r\n资源紧张，稍后再连接（已经占用）；\r\n其他。','0x9002','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (19,'从链路连接保持请求消息用例',1,94,1,0,120,'','空','','返回数据体为空，消息ID为0x9006。','0x9006','下发从链路连接保持请求消息。','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (20,'从链路注销请求消息用例',1,100,0,0,5,'\0\0?','','[DWORD]4231\r\n','','0x9003','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (21,'主链路注销应答消息',1,101,0,1,2,'\0\0A','主链路注销','[DWORD]321\r\n','','主链路注销?','确定主链路注销','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (28,'主链路登录用例',1,85,0,1,30,'','下级平台主动上传链路登录请求','','下级平台上传以下信息：\r\n1.用户名；\r\n2.密码；\r\n3.对应从链路服务端IP地址；\r\n4.对应从链路服务端口号。','0x1001','请上传用户名和密码等登录信息。','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (29,'主链路连接保持用例',1,87,1,1,120,'','主链路连接保持','','下级平台向上级平台发送主链路连接保持请求消息。','0x1005','执行此用例前请先提示下级平台登录上级平台，此用例检测过程中企业平台不作任何操作','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (30,'主链路动态信息交换数据体用例',1,137,1,1,20,'','','','','0x1200','请上传车辆动态信息交换业务数据','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (31,'上传车辆注册信息用例',1,138,1,1,20,'','上传车辆注册信息','','下级平台主动上传车辆注册信息：\r\n车牌号\r\n车牌颜色\r\n平台唯一编码\r\n车载终端厂商唯一编码\r\n车载终端型号\r\n车载终端编号\r\n车载终端SIM卡号','0x1201','请上传车辆注册信息','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (32,'实时上传车辆定位信息用例',1,155,1,1,60,'','实时上传车辆定位信息','','返回实时上传车辆定位信息请求指令\r\n包括车牌号、车牌颜色、定位数据','0x1202','请确保有送检平台有车辆在线，且有位置信息汇报','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (33,'车辆定位信息自动补报请求用例',1,139,1,1,120,'','','','返回车辆定位信息：\r\n车牌号\r\n车牌颜色\r\n定位数据（大于等于1条，小于等于5条）','0x1203','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络\r\n','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (34,'申请交换指定车辆定位信息请求',1,140,1,1,30,'','请上传申请交换指定车辆定位信息','','返回申请交换指定车辆定位信息指令，\r\n包括：车牌号、车牌颜色、开始时间、结束时间','0x1207','请上传申请交换指定车辆定位信息','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (35,'取消交换指定车辆定位信息请求用例',1,143,1,1,40,'','取消交换指定车辆定位信息请求','','返回取消交换指定车辆定位信息请求指令\r\n包括：车牌号、车牌颜色','0x1208','请取消之前申请监控的特殊车辆','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (36,'补发车辆定位信息请求',1,142,1,1,30,'','补发车辆定位信息','','返回补发车辆定位信息请求指令，\r\n包括车牌号、车牌颜色、开始时间、结束时间','0x1209','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (37,'从链路车辆动态信息数据体用例',1,109,0,2,2,'vqlRMTIzNDUAAAAAAAAAAAAAAAAAAZIBAAAABAAAEjQ=','车牌号=“京Q12345”；\r\n后续数据长度=4；\r\n数据=1234','[STRING|21]京Q12345\r\n[BYTE|1]1\r\n[WORD]37377\r\n[DWORD]0004\r\n[BYTE|4]1234\r\n','根据具体消息不同返回不同的内容','0x9200','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (38,'交换车辆定位信息用例',1,110,0,1,10,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApICAAAAJAAHBwfZAAAAAAAAEAAAABAAEAAQABAAEAAQABAAAAAAAAAAAA==','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n车辆当前总里程数（4）\r\n方向（2）\r\n海拔高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000000\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (39,'车辆定位信息交换补发消息用例',1,111,0,1,10,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIDAAAAtQUABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAEABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAIABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAMABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAQABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAU=','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n当前总里程数（4）\r\n方向（2）\r\n高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000001\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000002\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000003\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000004\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000005\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (40,'启动车辆定位信息交换请求用例',1,113,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIFAAAAAQA=','车牌号=测0002；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=00（车辆进入指定区域）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37381\r\n[DWORD]1\r\n[BYTE|1]00\r\n','返回的消息业务子类型为1205','0x1200','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (41,'结束车辆定位信息交换请求',1,114,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIGAAAAAQE=','车牌号=测0002；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=01（人工停止交换）\r\n','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]01\r\n','返回的消息消息ID为1200，业务子类型为1206','0x1200','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (42,'上报驾驶员身份识别信息请求用例',1,118,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIKAAAAAA==','车牌号=测0002\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37386\r\n[DWORD]0\r\n','消息业务子类型为120A\r\n内容如下：\r\n车牌号\r\n车牌颜色\r\n驾驶员姓名\r\n身份证编号\r\n从业资格证号\r\n发证机构名称','0x1200','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (43,'上报车辆电子运单请求用例',1,119,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApILAAAAAA==','车牌号=测0002\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37387\r\n[DWORD]0\r\n','返回消息业务子类型为120B\r\n消息内容：\r\n车牌号\r\n车牌颜色\r\n电子运单数据体长度\r\n电子运单数据内容','0x1200','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (44,'主链路数据体用例',1,147,1,1,20,'','','','返回上传平台间交互信息数据体指令','0x1300','请上传平台间交互信息','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (46,'从链路平台信息交互数据用例',1,120,1,0,20,'?\0\0\0\0\0\054','子业务类型标识=0x9300\r\n数据=0','[WORD]37632\r\n[DWORD]5\r\n[BYTE|5]3534\r\n','','0x9300','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (59,'平台查岗请求用例',1,121,0,0,30,'kwEAAAAQAACTAQAAAAiy6bjaAAAAAA==','信息ID=0x9301\r\n信息长度=8\r\n信息内容=“查岗”','[WORD]37633\r\n[DWORD]16\r\n[DWORD]37633\r\n[DWORD]8\r\n[STRING|8]查岗\r\n','返回平台查岗应答，消息的业务子类型为1301','0x1300','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (61,'下发平台间报文请求用例',1,122,0,0,30,'kwIAAAAZAACTAgAAABHPwreixr3MqLzksajOxAAAAA==','子业务数据类型=0x9302\r\n信息ID=0x9302\r\n信息长度=17\r\n信息内容=“下发平台间报文”','[WORD]37634\r\n[DWORD]25\r\n[DWORD]37634\r\n[DWORD]17\r\n[STRING|17]下发平台间报文\r\n','返回下发平台间报文应答，业务子类型为1302','0x1300','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (62,'主链路车辆报警数据用例',1,149,1,1,20,'','主链路车辆报警数据','','返回主链路车辆报警信息业务数据体指令。','0x1400','请上传主链路车辆报警数据','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (91,'1.1.1终端注册成功',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=“10001”\r\n终端型号=“12345678”\r\n终端ID=“7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=京A12345','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答 注册成功；鉴权码=XXX','8100','1、确认企业平台中以下资料未被注册：\r\n终端ID=”7654321”\r\n车牌=”京A12345”\r\n2、在模拟终端发送终端注册指令\r\n3、观察模拟终端是否返回“成功；鉴权码=XXX”\r\n4、保存“鉴权码=XXX”的信息，以备“终端鉴权”功能测试。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (95,'上报报警信息用例',1,150,1,1,40,'','上报报警信息用例','','返回上报报警信息指令，\r\n包括车牌号、车牌颜色、报警信息来源、报警类型、报警时间、信息ID、报警信息内容','0x1402','请上传报警信息','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (98,'3.1.1文本信息下发',1,196,0,3,30,'Hb27zaiyv9Ct0unOxLG+0MXPos/Ct6LWuMHusuLK1KO6MTIzo6xBQkNEo6yjoUAjo6QlLi4uLi4=','1、从平台逐个下发所有类型的文本信息指令，下发内容譬如：“交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....”；\r\n2、观察模拟终端显示的标志和文本信息；\r\n3、观察平台接收到的终端应答指令。\r\n','[BYTE|1]1D\r\n[STRING|55]交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....\r\n','通用应答','0001','1、模拟终端显示的标志和文本信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (99,'1.1.2终端注册失败',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=”10001”\r\n终端型号=”12345678”\r\n终端ID=”7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=”京A12345”','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答：注册失败','8100','1、在模拟终端发送终端注册指令\r\n2、观察模拟终端是否返回“注册失败”\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (100,'从链路报警数据用例',1,124,1,0,20,'vqlRNTQzMjEAAAAAAAAAAAAAAAAAA5QAAAAACgAAAAAAAAADRlQ=','车牌号=“京Q54321”\r\n车牌颜色编码=3（黑色）\r\n子业务数据类型=0x9400\r\n数据=0x9400','[STRING|21]京Q54321\r\n[BYTE|1]3\r\n[WORD]37888\r\n[DWORD]10\r\n[BYTE|10]34654\r\n','','0x9400','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (104,'报警督办请求用例',1,125,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQBAAAAXAIACQAAAABOBVmyAAAOqgAAAABOpjCyAFN1cGVydmlzb3IAAAAAAAAwMTA4ODQ1Njc4OQAAAAAAAAAAAHN1cGVydmlzb3JfZUBzdXBlci5jb20AAAAAAAAAAAAA','车牌号=“测0002”\r\n车牌颜色编号=2（黄色）\r\n报警信息来源=0x02（企业监控平台）\r\n报警类型=0x0009（盗警）\r\n报警时间=2011-06-25 11:44:50\r\n报警督办ID=0x9401\r\n督办截止时间=2011-10-25 11:44:50\r\n督办级别=0x00（紧急）\r\n督办人=“Supervisor”\r\n督办人联系电话=“01088456789”\r\n督办联系电子邮件=“supervisor_e@super.com”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37889\r\n[DWORD]92\r\n[BYTE|1]02\r\n[WORD]9\r\n[TIME_T|8]2011-06-25 11:44:50\r\n[DWORD]003754\r\n[TIME_T|8]2011-10-25 11:44:50\r\n[BYTE|1]00\r\n[STRING|16]Supervisor\r\n[STRING|20]01088456789\r\n[STRING|32]supervisor_e@super.com\r\n','返回报警督办应答，业务子类型为1401\r\n\r\n返回消息参考如下：\r\n车牌号=“测0002”\r\n车牌颜色编号\r\n报警督办ID\r\n报警处理结果=0x00（处理中）\r\n/报警处理结果=0x01（已处理完毕）\r\n/报警处理结果=0x02（不作处理）\r\n/报警处理结果=0x02（将来处理）','0x1400','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (105,'1.2.1终端注销成功',1,187,0,3,30,'','终端注销','','通用应答','0001','1、在模拟终端发送终端注销指令\r\n2、观察平台是否下发通用应答：成功\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (110,'1.3.1终端鉴权成功',1,188,0,3,30,'MTIzNDU2Nzg5MEE=','1、前提：终端注册成功；\r\n2、使用注册时平台下发的鉴权码进行鉴权；\r\n鉴权码：\"1234567890A\"','[STRING|11]1234567890A\r\n','通用应答：鉴权成功','0001','1、在模拟终端发送终端鉴权指令\r\n2、观察平台是否下发通用应答：成功','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (112,'1.4.1终端心跳',1,189,0,3,30,'','1、前提：终端鉴权成功；\r\n2、终端开始按照默认时间间隔发送心跳，平台应答心跳；','','通用应答：平台的心跳应答','8001','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (117,'3.2.1追加事件',1,197,0,3,30,'AgIBCLO1wb65ytXPAgSxqdPq','追加事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：暴雨','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]2\r\n[BYTE|1]4\r\n[STRING|4]暴雨\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (118,'3.2.2删除特定事件',1,197,0,3,30,'BAEBAA==','删除特定事件：\r\n设置总数：1\r\n事件项列表：\r\n事件ID：1\r\n事件内容长度：0','[BYTE|1]4\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台设置后结果相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (119,'3.3.1事件报告',1,198,0,3,30,'AQ==','事件报告：\r\n事件ID：1','[BYTE|1]1\r\n','平台通用应答','8001','模拟终端上传的事件报告，平台能正确显示。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (120,'6.1.1更新圆形区域',1,206,0,0,30,'AAEAAAABAD4CV6ugBu6Y4AAAA+gAZAo=','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n中心点纬度=39.3度\r\n中心点经度=116.3度\r\n半径=1000米\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]00\r\n[BYTE|1]01\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]39300000\r\n[DWORD]116300000\r\n[DWORD]1000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (121,'6.2.1删除圆形区域',1,207,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (122,'1.5.1设置终端参数',1,190,0,3,30,'BgAAAAEEAAAAPAAAACAEAAAAAAAAAEALMDEwODc2NTQzMjEAAABBETEzNjAxMDEyMzQ1AAAARQQAAAAAAAAARgQAAAA8','平台下发如下参数\r\n心跳间隔=60s，\r\n位置汇报策略=位置汇报，\r\n监控平台电话号码=01087654321，\r\n复位电话号码=13601012345，\r\n终端电话接听策略=自动接听，\r\n每次最长通话时间=60s','[BYTE|1]6\r\n[DWORD]1\r\n[BYTE|1]4\r\n[DWORD]60\r\n[DWORD]32\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]64\r\n[BYTE|1]B\r\n[STRING|11]01087654321\r\n[DWORD]65\r\n[BYTE|1]11\r\n[STRING|11]13601012345\r\n[DWORD]69\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]70\r\n[BYTE|1]4\r\n[DWORD]60','通用应答：成功','8001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (123,'3.4.1提问下发',1,199,0,3,30,'GBS1scews7XBvsrHt/HT0LnK1c+jvwEAAsrHAgACt/E=','提问下发：\r\n标志：0x14：终端TTS播读，广告屏显示；\r\n问题内容长度：20\r\n问题：当前车辆是否有故障？\r\n候选答案列表：\r\n答案ID：1\r\n答案内容长度：2\r\n答案：是\r\n答案ID：2\r\n答案内容长度：2\r\n答案：否','[BYTE|1]18\r\n[BYTE|1]14\r\n[STRING|20]当前车辆是否有故障？\r\n[BYTE|1]1\r\n[WORD]2\r\n[STRING|2]是\r\n[BYTE|1]2\r\n[WORD]2\r\n[STRING|2]否\r\n','终端通用应答','0001','1、模拟终端显示的提问内容与平台下发相同；\r\n2、平台正确接收模拟终端的通用应答。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (124,'6.3.1更新矩形区域',1,208,0,0,30,'AAEAAAABAD4CYruoBurizAJb8WAG9Yo4AGQK','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n左上点纬度=40.025度\r\n左上点经度=116.05678度\r\n右下点纬度=39.58度\r\n右下点经度=116.755度\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]0\r\n[BYTE|1]1\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (126,'3.8.1提问应答',1,231,0,3,30,'','提问应答：\r\n应答流水号：对应平台下发的提问的流水号；\r\n答案ID：1\r\n','','','','平台能解析出终端的的提问应答，答案ID是1；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (127,'从链路车辆静态信息交换业务',1,134,1,2,20,'y9VBMDAwMDEAAAAAAAAAAAAAAAAAMZYAAQ==','车牌号=“苏A00001”\r\n车牌颜色=1(蓝色)','[STRING|21]苏A00001\r\n[BYTE|1]31\r\n[WORD]38400\r\n[BYTE|1]1\r\n','','0x9600','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (128,'主链路车辆静态信息数据用例',1,154,1,1,20,'','主链路车辆静态信息数据','','返回主链路车辆静态信息数据指令','0x1600','请上传车辆静态信息数据','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (129,'主链路监管数据用例',1,152,1,1,20,'','主链路监管数据','','返回主链路监管业务数据指令','0x1500','请在送检企业平台向监管平台发送车辆监管业务','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (130,'补报车辆静态信息请求消息用例',1,135,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApYBAAAAAA==','车牌号=“测0002”\r\n车牌颜色=2(黄色)','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38401\r\n[DWORD]00000000\r\n\r\n','返回补报车辆静态信息应答，业务子类型为0x1601\r\n\r\n车辆静态信息：\r\n车牌号\r\n车牌颜色\r\n车辆类型\r\n运输行业编码\r\n车籍地\r\n业户ID\r\n业户名称\r\n业户联系电话','0x1600','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (131,'6.4.1删除矩形区域',1,209,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (132,'3.5.1信息点播菜单追加',1,200,0,3,30,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻\r\n','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (133,'从链路监管数据用例',1,128,1,2,20,'tKhBNTQzMjEAAAAAAAAAAAAAAAAAOZUAAAAABQAAASNF','车牌号=“川A54321”\r\n车牌颜色编号=9（其他）\r\n监管数据=0x12345','[STRING|21]川A54321\r\n[BYTE|1]39\r\n[WORD]38144\r\n[DWORD]5\r\n[BYTE|5]12345\r\n','','0x9500','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (134,'3.6.1信息点播',1,201,0,3,30,'','信息点播\r\n信息类型：1\r\n点播/取消标志：1，点播；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、平台能定期根据终端点播的信息，下发点播的相关信息。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (135,'车辆单向监听请求用例',1,129,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUBAAAAFDAxMDg4Nzg5NjUtNjc1NAAAAAAA','车牌号=“测0002”\r\n车牌颜色=2（黄色）\r\n回拨电话号码=“0108878965-6754”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38145\r\n[DWORD]20\r\n[STRING|20]0108878965-6754\r\n','返回车辆单向监听应答，业务子类型为1501\r\n返回消息内容参考如下：\r\n车牌号=“测0002”\r\n车牌颜色=4（黄色）\r\n应答结果=0x00（监听成功）\r\n/应答结果=0x01（监听失败）','0x1500','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (136,'6.5.1更新多边形区域',1,210,0,0,60,'AAAAAQA+AGQKAAQCW/FgBurizAJiu6gG6uLMAmK7qAb1ijgCW/FgBvWKOA==','区域ID=1\r\n区域属性=0x003E\r\n最高速度=100公里/小时\r\n超速持续时间=10秒\r\n区域总顶点数=4\r\n顶点纬度=39.58度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.755度\r\n顶点纬度=39.58度\r\n顶点经度=116.755度','[DWORD]1\r\n[WORD]62\r\n[WORD]100\r\n[BYTE|1]0A\r\n[WORD]4\r\n[DWORD]39580000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116755000\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n','成功','0001','1、平台下发设置多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (137,'3.7.1信息服务',1,202,0,3,30,'AQAqz8LO57Gxvqm12Mf4vavT0MDX0+rM7Mb4o6zH68zhx7DX9rrD17yxuKGj','信息类型：1\r\n信息长度：42\r\n信息内容：下午北京地区将有雷雨天气，请提前做好准备。','[BYTE|1]1\r\n[WORD]42\r\n[STRING|42]下午北京地区将有雷雨天气，请提前做好准备。\r\n','终端通用应答','0001','1、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (139,'6.6.1删除多边形区域',1,211,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (140,'车辆拍照请求用例',1,130,0,0,60,'vqlBNTQzMjEAAAAAAAAAAAAAAAAAMZUCAAAAAgkI','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n镜头ID=0x09\r\n照片大小=0x08（704*576）','[STRING|21]京A54321\r\n[BYTE|1]31\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]09\r\n[BYTE|1]08\r\n','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n拍照应答标识=0x00（不支持拍照相）\r\n/拍照应答标识=0x01（完成拍照）\r\n/拍照应答标识=0x02（完成拍照、照片数据稍后传送）\r\n/拍照应答标识=0x03（未拍照不在线）\r\n/拍照应答标识=0x04（未拍照无法使用指定镜头）\r\n/拍照应答标识=0x05（未拍照其他原因）\r\n/拍照应答标识=0x09（车牌号码错误）\r\n拍照位置点\r\n镜头ID=0x09\r\n图片长度\r\n图片大小==0x08（704*576）\r\n图片格式=0x01（jpg）\r\n/图片格式=0x02（gif）\r\n/图片格式=0x03（tiff）\r\n/图片格式=0x04（png）\r\n图片内容','0x9502','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (141,'下发车辆报文请求用例',1,131,0,0,60,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUDAAAADwAAAAEAAAAABr30vLEAAA==','车牌号=“测0002”\r\n车牌颜色=2（黄色）\r\n报文优先级=0x00\r\n报文内容=6','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38147\r\n[DWORD]15\r\n[DWORD]1\r\n[BYTE|1]00\r\n[DWORD]6\r\n[STRING|6]紧急\r\n','返回下发车辆报文应答，业务子类型为0x1503\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n消息ID=1500\r\n应答结果=0x00（下发成功）\r\n/应答结果=0x01（下发失败）','0x1500','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (142,'下发车辆行驶记录请求用例',1,132,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUEAAAAEAAAAABOBY/CAAAAAE/oFMI=','车牌号=“测0002”\r\n开始时间=2011-06-25 15:35:30\r\n结束时间=2012-06-25 15:35:30','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38148\r\n[DWORD]16\r\n[TIME_T|8]2011-06-25 15:35:30\r\n[TIME_T|8]2012-06-25 15:35:30\r\n','返回上报车辆行驶记录应答，业务子类型为1504\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n车辆行驶记录信息','0x1500','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (143,'1.6.1查询终端参数',1,191,0,3,90,'','查询终端参数','','查询终端参数应答：59个参数\r\n参数总数=59\r\n参数ID=0x0001,参数长度=4,参数值=60;\r\n参数ID=0x0002,参数长度=4,参数值=20;\r\n参数ID=0x0003,参数长度=4,参数值=3;\r\n参数ID=0x0004,参数长度=4,参数值=25;\r\n参数ID=0x0005,参数长度=4,参数值=2;\r\n参数ID=0x0006,参数长度=4,参数值=86400;\r\n参数ID=0x0007,参数长度=4,参数值=1;\r\n参数ID=0x0010,参数长度=5,参数值=\"CMNET\";\r\n参数ID=0x0011,参数长度=4,参数值=\"card\";\r\n参数ID=0x0012,参数长度=4,参数值=\"card\";\r\n参数ID=0x0013,参数长度=9,参数值=\"127.0.0.1\";\r\n参数ID=0x0014,参数长度=7,参数值=\"CSYL.BJ\";\r\n参数ID=0x0015,参数长度=4,参数值=\"CSBJ\";\r\n参数ID=0x0016,参数长度=4,参数值=\"BJCS\";\r\n参数ID=0x0017,参数长度=13,参数值=\"192.168.1.106\";\r\n参数ID=0x0018,参数长度=4,参数值=6500;\r\n参数ID=0x0019,参数长度=4,参数值=6580;\r\n参数ID=0x0020,参数长度=4,参数值=0;\r\n参数ID=0x0021,参数长度=4,参数值=0;\r\n参数ID=0x0022,参数长度=4,参数值=300;\r\n参数ID=0x0027,参数长度=4,参数值=600;\r\n参数ID=0x0028,参数长度=4,参数值=10;\r\n参数ID=0x0029,参数长度=4,参数值=60;\r\n参数ID=0x002C,参数长度=4,参数值=1000;\r\n参数ID=0x002D,参数长度=4,参数值=2000;\r\n参数ID=0x002E,参数长度=4,参数值=500;\r\n参数ID=0x002F,参数长度=4,参数值=500;\r\n参数ID=0x0030,参数长度=4,参数值=130;\r\n参数ID=0x0040,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0041,参数长度=11,参数值=\"13601012345\";\r\n参数ID=0x0042,参数长度=11,参数值=\"18876543210\";\r\n参数ID=0x0043,参数长度=12,参数值=\"106590202345\";\r\n参数ID=0x0044,参数长度=11,参数值=\"13901011000\";\r\n参数ID=0x0045,参数长度=4,参数值=0;\r\n参数ID=0x0046,参数长度=4,参数值=60;\r\n参数ID=0x0047,参数长度=4,参数值=3600;\r\n参数ID=0x0048,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0049,参数长度=12,参数值=\"106590202347\";\r\n参数ID=0x0050,参数长度=4,参数值=0;\r\n参数ID=0x0051,参数长度=4,参数值=0x0000000F;\r\n参数ID=0x0052,参数长度=4,参数值=0;\r\n参数ID=0x0053,参数长度=4,参数值=0;\r\n参数ID=0x0054,参数长度=4,参数值=0x000000FF;\r\n参数ID=0x0055,参数长度=4,参数值=120;\r\n参数ID=0x0056,参数长度=4,参数值=30;\r\n参数ID=0x0057,参数长度=4,参数值=14400;\r\n参数ID=0x0058,参数长度=4,参数值=36000;\r\n参数ID=0x0059,参数长度=4,参数值=900;\r\n参数ID=0x005A,参数长度=4,参数值=600;\r\n参数ID=0x0070,参数长度=4,参数值=6;\r\n参数ID=0x0071,参数长度=4,参数值=128;\r\n参数ID=0x0072,参数长度=4,参数值=70;\r\n参数ID=0x0073,参数长度=4,参数值=60;\r\n参数ID=0x0074,参数长度=4,参数值=255;\r\n参数ID=0x0080,参数长度=4,参数值=983490;\r\n参数ID=0x0081,参数长度=4,参数值=17;\r\n参数ID=0x0082,参数长度=4,参数值=0;\r\n参数ID=0x0083,参数长度=8,参数值=\"京A12345\";\r\n参数ID=0x0084,参数长度=4,参数值=1;','0104','平台对照“返回消息”逐一检查读取的59个参数是否与终端上报参数相同','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (144,'车辆应急接入监管平台请求用例',1,133,0,0,40,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUFAAAAkQAAAAAAAACyEN+3/s7xxvdBUE4AAAAAAAAAAAAAAEFkbWluaVVzZXIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyM3JzZDMyNDUAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjE4NAAAAAAAAAAAAAAAAAAAAAAAAAAiYB+QAAAAAbG18DM=','车牌号=“测0002”\r\n鉴权码=“B210DF”\r\n拨号点名称=“服务器APN”\r\n拨号用户名=“AdminiUser”\r\n拨号密码=23rsd3245\r\n服务器IP=“192.168.1.184”\r\nTCP端口=“8800”\r\nUDP端口=“8080”\r\n结束时间=“2012-08-01 14:20:35”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38149\r\n[DWORD]145\r\n[BYTE|10]B210DF\r\n[STRING|20]服务器APN\r\n[STRING|49]AdminiUser\r\n[STRING|22]23rsd3245\r\n[STRING|32]192.168.1.184\r\n[WORD]8800\r\n[WORD]8080\r\n[TIME_T|8]2200-08-01 14:20:35\r\n','返回车辆应急接入监管平台应答，消息业务子类型为0x1505\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n应答结果=0x00（车载终端成功收到该命令）\r\n/应答结果=0x01（无该车辆）\r\n/应答结果=0x02（其它原因失败）','0x1500','','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (146,'报警预警消息用例',1,126,0,1,15,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQCAAAAHgMACwAAAABOBZKzAAAAD8arwOvCt8/fsai+rwAAAA==','车牌号=“测0002”\r\n车牌颜色编码=4（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=11\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“偏离路线报警”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]11\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]偏离路线报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (147,'实时交换报警用例',1,127,0,1,15,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQDAAAAHgMACAAAAABOBZKzAAAAD9S9veexqL6vAAAAAAAAAA==','车牌号=“测0002”\r\n车牌颜色编码=2（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=08\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“越界报警”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37891\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]08\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]越界报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (148,'4.1.1电话回拨',1,203,0,3,60,'ADAxMDg4ODg4ODg4','电话回拨(普通通话):\r\n标志 = 0 \r\n电话号码 = \"01088888888\"','[BYTE|1]0\r\n[STRING|11]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (149,'4.1.2电话回拨_监听',1,203,0,3,60,'ATAxMDg4ODg4ODg4AAAAAAAAAAAA','电话回拨(监听):\r\n标志 : 1\r\n电话号码 : 01088888888','[BYTE|1]1\r\n[STRING|20]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (150,'4.2.1设置电话本_删除',1,204,0,0,60,'AA==','删除终端上存储的所有联系人:\r\n设置类型 = 0','[BYTE|1]0','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (151,'1.7.1终端控制',1,192,0,3,90,'ATtDTU5FVDs7OzE5Mi4xNjguMS4xMDM7NjAwMDs7MTAwMDE7MS4wMzsxLjAwOzM2MDA=','无限升级指令：\r\nURL地址=;(空)\r\n拨号点名称=\"CMNET\"\r\n拨号用户名=;\r\n拨号密码=;\r\n地址=\"192.168.1.103\"\r\nTCP端口=6000\r\nUDP端口=;\r\n硬件版本=\"1.03\"\r\n固件版本=\"1.00\"\r\n连接到服务器的时限=3600','[BYTE|1]1\r\n[STRING|49];CMNET;;;192.168.1.103;6000;;10001;1.03;1.00;3600\r\n','通用应答：成功','0001','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (152,'4.2.2设置电话本_更新电话本',1,204,0,3,60,'AQUBCzEzODEyMzQ1Njc4BNXFyP0CCzEzODEyMzQ1Njc5BMDuy8QDCzEzODEyMzQ1NjgwBM31zuUDDDA3NTUxMjM0NTY3OAbVxcj9y8QDDDA3NjkzMzMzNTU1NQbVxcj9zuU=','更新电话本(删除终端中已有全部联系人并追加消息中的联系人):\r\n设置类型 = 1\r\n联系人总数= 5\r\n联系人项 :\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 11\r\n电话号码 = \"13812345679\"\r\n联系人长度 = 4\r\n联系人 = \"李四\"\r\n\r\n标志 = 3\r\n号码长度 = 11\r\n电话号码 = \"13812345680\"\r\n联系人长度 = 4\r\n联系人 = \"王五\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"张三四\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"076933335555\"\r\n联系人长度 = 6\r\n联系人 = \"张三五\"\r\n\r\n','[BYTE|1]1\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0B\r\n[STRING|11]13812345679\r\n[BYTE|1]4\r\n[STRING|4]李四\r\n[BYTE|1]3\r\n[BYTE|1]0B\r\n[STRING|11]13812345680\r\n[BYTE|1]4\r\n[STRING|4]王五\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]张三四\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]076933335555\r\n[BYTE|1]6\r\n[STRING|6]张三五\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (153,'6.7.1更新路线',1,212,0,0,30,'AAAAAQA+AAYAAAABAAAAAQJgU0AG71wwZAIAZAoAAAACAAAAAgJgVmAG74NAZAMCWAAKAGQKAAAAAwAAAAICYF1oBu/RYGQDAlgACgBkCgAAAAQAAAACAmBiGAbwPsBkAwJYAAoAZAoAAAAFAAAAAgJgXWgG8E5gZAMCWAAKAGQKAAAABgAAAAICYFw8BvCUsGQDAlgACgBkCg==','路线ID=1\r\n路线属性=62\r\n路线总拐点数=6\r\n\r\n路段ID=1\r\n拐点ID=1，39.867200，116.350000，路宽=100米，属性=2，限速=100公里/小时，超速持续时间=10秒\r\n\r\n路段ID=2\r\n拐点ID=2，39.868000，116.360000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=3，39.869800，116.380000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=4，39.871000，116.408000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=5，39.869800，116.412000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=6，39.869500，116.430000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒','[DWORD]1\r\n[WORD]62\r\n[WORD]6\r\n[DWORD]1\r\n[DWORD]1\r\n[DWORD]39867200\r\n[DWORD]116350000\r\n[BYTE|1]64\r\n[BYTE|1]2\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]2\r\n[DWORD]2\r\n[DWORD]39868000\r\n[DWORD]116360000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]3\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116380000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]4\r\n[DWORD]2\r\n[DWORD]39871000\r\n[DWORD]116408000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]5\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116412000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]6\r\n[DWORD]2\r\n[DWORD]39869500\r\n[DWORD]116430000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A','成功','0001','1、平台下发设置路线指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (154,'6.8.1删除所有路线',1,213,0,0,30,'AA==','路线数=0','[BYTE|1]0\r\n','成功','0001','1、平台下发删除线路指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (155,'4.2.3设置电话本_追加电话本',1,204,0,3,60,'AgIBCzEzODEyMzQ1Njc4BNXFyP0CDDA3NTUxMjM0NTY3OAbA7sj9y8Q=','设置电话本（追加电话本）\r\n设置类型 = 2\r\n联系人总数 = 2\r\n联系人项\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"李三四\"\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]李三四\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (156,'4.2.4设置电话本_修改电话本',1,204,0,3,60,'13812345678张三','设置电话本（修改电话本，以联系人为索引）\r\n设置类型 = 3\r\n联系人总数 = 1\r\n联系人项\r\n','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (157,'2.1.1手动报警',1,193,0,3,90,'','使用模拟终端中的“4、手动位置上报”,选中紧急告警项。然后点击执行命令按钮','','通用应答：成功','0001','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (158,'2.2.1位置信息查询',1,194,0,3,90,'','查询位置信息','','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。至少一种以上的附件信息测试。','0201','检测平台收到的信息和模拟终端发送的位置信息内容一致','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (159,'2.3.1临时位置跟踪控制',1,195,0,3,180,'ABQAAAB4','时间间隔=10秒\r\n位置跟踪有效期=120秒','[WORD]20\r\n[DWORD]120\r\n','使用模拟终端的“5、定时位置上报”，按照设置的时间间隔及持续时间上报位置信息12次','0001','1、平台下发临时位置跟踪控制指令；\r\n2、模拟终端按“时间间隔”和“跟踪有效期”定时上传位置信息；\r\n3、观察平台显示的信息内容是否与模拟终端上传一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (160,'5.1.1车辆控制_车门解锁',1,205,0,3,60,'AA==','控制标志=0（车门解锁）','[BYTE|1]0\r\n','模拟终端自动回应，平台上收到回应，显示“车门解锁”状态：\r\n应答流水号= \r\n位置信息  =\r\n','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门解锁”状态。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (161,'5.1.2车辆控制_车门加锁',1,205,0,3,60,'AQ==','控制标志=1（车门加锁）','[BYTE|1]1\r\n','模拟终端自动回应，平台上收到回应，显示“车门加锁”状态：\r\n应答流水号= \r\n位置信息  =','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门加锁”状态。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (162,'9.2.1数据上行透传',1,225,0,3,30,'','使用模拟终端中手动用力中的“7、数据上行透传”，设置透传类型已经透传内容进行上传。','','平台通用应答','8001','1、观察平台收到的透传信息是否与模拟终端中输入的透传信息一致。\r\n2、观察平台是否正确返回应答。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (164,'3.5.2信息点播菜单修改',1,200,0,3,30,'AwIBAAjCt7/20MXPogIACLn6xNrQws7F','追加信息点播菜单\r\n设置类型：修改\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：路况信息\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国内新闻\r\n','[BYTE|1]3\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]路况信息\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国内新闻','终端通用应答','0001','1、平台下发信息点播菜单修改指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (165,'3.5.3信息点播菜单更新',1,200,0,3,30,'AQMBAAixsb6pzOzG+AIACLn6vMrQws7FAgAIwsPTzr3pydw=','追加信息点播菜单\r\n设置类型：更新\r\n信息项总数：3\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：北京天气\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国际新闻\r\n信息类型：3\r\n信息名称长度：8\r\n信息名称：旅游介绍\r\n','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]北京天气\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国际新闻\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]旅游介绍','终端通用应答','0001','1、平台下发信息点播菜单更新指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (166,'3.5.4信息点播菜单删除',1,200,0,3,30,'AA==','删除终端全部信息菜单','[BYTE|1]0\r\n','终端通用应答','0001','1、平台下发信息点播菜单删除指令；\r\n2、观察模拟终端是否全部删除信息点播菜单；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (167,'3.6.2信息取消',1,201,0,3,30,'','信息取消\r\n信息类型：1\r\n点播/取消标志：0，取消；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、终端不再接收到已取消的信息服务。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (168,'3.2.3更新事件',1,197,0,3,30,'AQMBCMDX0+rM7Mb4AgiztcG+ucrVzwMIwrfD5syuy/o=','更新事件\r\n设置总数：3\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：雷雨天气\r\n事件ID：2\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：3\r\n事件内容长度：8\r\n事件内容：路面坍塌','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]雷雨天气\r\n[BYTE|1]2\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]3\r\n[BYTE|1]8\r\n[STRING|8]路面坍塌','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (169,'3.2.4修改事件',1,197,0,3,30,'AwEBCLW9tO+3tMCh','修改事件：\r\n设置总数：1\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：到达反馈','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]到达反馈\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (170,'3.2.5删除全部事件',1,197,0,3,30,'AA==','删除终端现有所有事件','[BYTE|1]0\r\n','终端通用应答','0001','1、观察终端是否删除全部事件。\r\n2、平台是否正确接收终端应答。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (171,'8.1.1手动上发多媒体事件',1,218,0,1,30,'','','','多媒体事件上传通用应答','0x0800','请确认终端已发送多媒体事件信息上传报文','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (174,'7.2.1电子运单上报',1,216,0,3,90,'','使用模拟终端的“手工用例”中的“9、电子运单上报”发送数据\r\n运单数据“ID：20110628 货物名称：打印机20台 目的地：中国深圳”','','','8001','平台收到与终端发送信息一致的运单信息','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (175,'9.1.1数据下行透传',1,224,0,0,30,'123456789','透传消息类型=1\r\n透传消息内容=“123456789”','[BYTE|1]1\r\n[STRING|9]123456789\r\n','成功','0001','1、平台下发数据下行透传指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (177,'8.2.1抓拍摄像头通道1立即上传',1,220,0,2,60,'\0\0\0\0','拍摄照片：1\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：640×480\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回图片应当为标注通道号码的粉红色测试图片','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (182,'7.3.1驾驶员身份信息采集上报',1,217,0,3,90,'','使用模拟终端的“手工用例”中的“6、驾驶员身份信息采集上报”上报信息\r\n1、驾驶员姓名：张老三\r\n2、驾驶员身份证：010456198009080234\r\n3、从业资格证编号：0106785869869807\r\n4、发证机构名称：北京市道路运输管理局\r\n','','','8001','平台收到与上报数据相同的驾驶员信息','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (186,'8.2.2录像通道1上传',1,220,0,2,300,'Af//AAAAAQEBAQEBAQ==','终端录像\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：320×240\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]65535\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回数据为AVI（RAFF）容器H264视频，内容为蓝色GPS图样文字与JT/T808-2011字样3维动画','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (187,'8.3.1检索图片列表',1,221,0,3,60,'AAAAEQYBAAABEQYwI1lZ','多媒体类型：图片\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]0\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959\r\n','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (188,'8.3.2检索音频列表',1,221,0,3,60,'AQAAEQYBAAABEQYwI1lZ','多媒体类型：音频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]1\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (189,'8.3.3检索视频列表',1,221,0,3,60,'AgAAEQYBAAABEQYwI1lZ','多媒体类型：视频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]2\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (191,'8.4.1上传图像',1,222,0,2,60,'AAEAEQYlAAABEQYlI1lZAA==','上传存储图像\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0\r\n','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (192,'8.4.2上传音频',1,222,0,2,100,'AQEAEQYlAAABEQYlI1lZAA==','上传存储音频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (193,'8.4.3上传视频',1,222,0,2,300,'AgEAEQYlAAABEQYlI1lZAA==','上传存储视频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (194,'8.5.1录音32Kbps立即上传',1,223,0,2,100,'AQAAAAM=','开始录音\r\n持续录音\r\n实时上传\r\n采样率：32Kbps','[BYTE|1]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]3\r\n','开始录音通用应答','0x8804','同时上传多媒体数据\r\n音频内容为440Hz断续正弦波测试信号','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (195,'1.5.2超速设置',1,190,0,3,30,'','平台下发如下参数\r\n限速值=80公里/小时；\r\n持续时间=10秒','','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (196,'1.5.3疲劳驾驶设置',1,190,0,3,600,'AgAAAFcEAAACWAAAAFkEAAAAtA==','平台下发如下参数\r\n连续驾驶时间=600秒；\r\n最小休息时间=180秒。','[BYTE|1]2\r\n[DWORD]87\r\n[BYTE|1]4\r\n[DWORD]600\r\n[DWORD]89\r\n[BYTE|1]4\r\n[DWORD]180\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (197,'1.5.4超时停车',1,190,0,3,300,'AQAAAFoEAAABLA==','平台下发如下参数\r\n最长停车时间=300s','[BYTE|1]1\r\n[DWORD]90\r\n[BYTE|1]4\r\n[DWORD]300\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (198,'2.1.2电瓶欠压',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“终端主电源欠压”项\r\n然后点击执行命令按钮','','通用应答成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (199,'注销主从链路消息',1,233,1,1,30,'','','','返回消息ID为1003','0x1003','通知被测下级平台发送主从链路断开消息（1003协议消息）','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (200,'2.1.3断电提醒',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“电路断开”项\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (201,'2.1.4终端故障',1,193,0,3,30,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。随便选择设备故障中的一个。\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (202,'2.1.5休眠',1,193,0,3,300,'AgAAACEEAAAAAAAAACcEAAABLA==','先通过平台进行参数设置：\r\n位置汇报方案=0（根据ACC状态）\r\n休眠时汇报时间间隔=300秒\r\n使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须选择ACC关。\r\n然后点击执行命令按钮','[BYTE|1]2\r\n[DWORD]33\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]39\r\n[BYTE|1]4\r\n[DWORD]300\r\n','平台通用应答','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (203,'3.5.5信息点播菜单追加',1,200,1,0,10,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (206,'3.2.6追加新事件',1,197,1,3,30,'','追加新事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：阴雨天气\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：路滑','','终端通用应答','0001','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (207,'7.1.1行车记录仪数据采集',1,214,0,1,30,'','下行行车记录仪设置指令','','返回行车记录仪数据上传，其中命令字与下发命令字相同','0700','','',2,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (208,'下级平台实时上传车辆定位信息',1,234,0,1,60,'','','','返回消息的业务类型为1200，业务子类型为1202','','请提示下级平台实时上传不少于3条车牌号为“川A12345”的定位信息(1202消息)','',1,0);
insert  into `example_copy`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (209,'下级平台车辆定位信息补报',1,235,0,1,60,'','','','最少收到3条定位信息补报，消息ID为1200，业务子类型为1203','','请提示下级平台补报不少于3条车牌号为“川A12345”自动补报信息（1203号协议）','',1,0);

/*Table structure for table `example_copy1` */

DROP TABLE IF EXISTS `example_copy1`;

CREATE TABLE `example_copy1` (
  `ExampleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否只读，默认为1：只读',
  `FunctionID` int(11) NOT NULL COMMENT '功能ID',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定 ',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '答应结果',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='统系用例表';

/*Data for the table `example_copy1` */

insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (18,'从链路连接请求消息用例',1,82,1,0,120,'','','','从链路返回连接结果为以下信息中的一个：\r\n成功；\r\n校验码错误；\r\n资源紧张，稍后再连接（已经占用）；\r\n其他。','0x9002','请提示企业平台关闭连接，重新登录','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (19,'从链路连接保持请求消息用例',1,94,1,0,120,'','空','','返回数据体为空，消息ID为0x9006。','0x9006','下发从链路连接保持请求消息。','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (20,'从链路注销请求消息用例',1,100,0,0,5,'\0\0?','','[DWORD]4231\r\n','','0x9003','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (21,'主链路注销应答消息',1,1011,0,1,2,'\0\0A','主链路注销','[DWORD]321\r\n','','主链路注销?','确定主链路注销','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (28,'主链路登录用例',1,85,0,1,30,'','下级平台主动上传链路登录请求','','下级平台上传以下信息：\r\n1.用户名；\r\n2.密码；\r\n3.对应从链路服务端IP地址；\r\n4.对应从链路服务端口号。','0x1001','请上传用户名和密码等登录信息。','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (29,'主链路连接保持用例',1,87,1,1,120,'','主链路连接保持','','下级平台向上级平台发送主链路连接保持请求消息。','0x1005','执行此用例前请先提示下级平台登录上级平台，此用例检测过程中企业平台不作任何操作','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (30,'主链路动态信息交换数据体用例',1,137,1,1,20,'','','','','0x1200','请上传车辆动态信息交换业务数据','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (31,'上传车辆注册信息用例',1,244,0,1,60,'','上传车辆注册信息','','下级平台主动上传车辆注册信息：\r\n车牌号\r\n车牌颜色\r\n子业务类型=0x1201\r\n平台唯一编码\r\n车载终端厂商唯一编码\r\n车载终端型号\r\n车载终端编号\r\n车载终端SIM卡号','0x1200','请上传车辆注册信息','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (32,'实时上传车辆定位信息用例',1,245,0,1,60,'','实时上传车辆定位信息','','返回实时上传车辆定位信息请求指令\r\n包括车牌号、车牌颜色、定位数据、子业务数据类型=0x1202','0x1200','请确保有送检平台有车辆在线，且有位置信息汇报','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (33,'车辆定位信息自动补报请求用例',1,247,0,1,120,'','','','返回车辆定位信息：\r\n车牌号\r\n车牌颜色\r\n子业务数据类型=0x1203\r\n定位数据（大于等于1条，小于等于5条）','0x1200','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络\r\n','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (34,'申请交换指定车辆定位信息请求',1,248,0,1,30,'','请上传申请交换指定车辆定位信息','','返回申请交换指定车辆定位信息指令，\r\n包括：车牌号、车牌颜色、开始时间、结束时间、子业务数据类型=0x1207','0x1200','请上传申请交换指定车辆定位信息','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (35,'取消交换指定车辆定位信息请求用例',1,250,0,1,40,'','取消交换指定车辆定位信息请求','','返回取消交换指定车辆定位信息请求指令\r\n包括：车牌号、车牌颜色、子业务数据类型=0x1208','0x1200','请取消之前申请监控的特殊车辆','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (36,'补发车辆定位信息请求',1,249,0,1,30,'','补发车辆定位信息','','返回补发车辆定位信息请求指令，\r\n包括车牌号、车牌颜色、开始时间、结束时间、子业务数据类型=0x1209','0x1200','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (37,'从链路车辆动态信息数据体用例',1,109,0,2,2,'vqlRMTIzNDUAAAAAAAAAAAAAAAAAAZIBAAAABAAAEjQ=','车牌号=“京Q12345”；\r\n后续数据长度=4；\r\n数据=1234','[STRING|21]京Q12345\r\n[BYTE|1]1\r\n[WORD]37377\r\n[DWORD]0004\r\n[BYTE|4]1234\r\n','根据具体消息不同返回不同的内容','0x9200','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (38,'交换车辆定位信息用例',1,112,0,1,10,'suIxMjM0NQAAAAAAAAAAAAAAAAAAApICAAAAJAAHBwfZAAAAAAAAEAAAABAAEAAQABAAEAAQABAAAAAAAAAAAA==','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n车辆当前总里程数（4）\r\n方向（2）\r\n海拔高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000000\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (39,'车辆定位信息交换补发消息用例',1,113,0,1,10,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAEABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAIABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAMABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAQABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAU=','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n当前总里程数（4）\r\n方向（2）\r\n高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000001\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000002\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000003\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000004\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000005\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (40,'启动车辆定位信息交换请求用例',1,110,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIFAAAAAQA=','车牌号=测A12345；\r\n车牌颜色编码=2（黄色）；\r\n子业务类型标识；\r\n后续数据长度；\r\n启动定位信息交换原因=00（车辆进入指定区域）','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37381\r\n[DWORD]1\r\n[BYTE|1]00\r\n','返回的消息业务子类型为1205','0x1200','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (41,'结束车辆定位信息交换请求',1,114,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIGAAAAAQE=','车牌号=测A12345；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=01（人工停止交换）\r\n','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]01\r\n','返回的消息消息ID为1200，业务子类型为1206','0x1200','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (42,'上报驾驶员身份识别信息请求用例',1,118,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApIKAAAAAA==','车牌号=测A12345\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37386\r\n[DWORD]0\r\n','消息业务子类型为120A\r\n内容如下：\r\n车牌号\r\n车牌颜色\r\n驾驶员姓名\r\n身份证编号\r\n从业资格证号\r\n发证机构名称','0x1200','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (43,'上报车辆电子运单请求用例',1,119,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApILAAAAAA==','车牌号=测A12345\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37387\r\n[DWORD]0\r\n','返回消息业务子类型为120B\r\n消息内容：\r\n车牌号\r\n车牌颜色\r\n电子运单数据体长度\r\n电子运单数据内容','0x1200','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (44,'主链路数据体用例',1,147,1,1,20,'','','','返回上传平台间交互信息数据体指令','0x1300','请上传平台间交互信息','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (46,'从链路平台信息交互数据用例',1,120,1,0,20,'?\0\0\0\0\0\054','子业务类型标识=0x9300\r\n数据=0','[WORD]37632\r\n[DWORD]5\r\n[BYTE|5]3534\r\n','','0x9300','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (59,'平台查岗请求用例',1,121,0,1,60,'kwEAAAAdATExMTExMTExMTEAAAAAAAEAAAAIsum42gAAAAA=','子业务类型=0x9301\r\n查岗对象类型=0x01\r\n查岗对象ID=平台ID，由中心提供\r\n信息ID=1\r\n信息长度=8\r\n信息内容=“查岗”','[WORD]37633\r\n[DWORD]29\r\n[BYTE]1\r\n[STRING|12]1111111111\r\n[DWORD]1\r\n[DWORD]8\r\n[STRING|8]查岗\r\n','返回平台查岗应答，消息的业务子类型为1301,查岗对象类型为1,查岗对象ID与中心分配的一致,消息ID为1','0x1300','平台查岗','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (61,'下发平台间报文请求用例',1,122,0,0,30,'kwIAAAAmATExMTExMTExMTEAAAAAAAEAAAARz8K3osa9zKi85LGozsQAAAA=','子业务数据类型=0x9302\r\n信息ID=0x9302\r\n查岗对象类型=0x01\r\n查岗对象ID=平台ID，由中心提供\r\n信息ID=1\r\n信息长度=17\r\n信息内容=“下发平台间报文”','[WORD]37634\r\n[DWORD]38\r\n[BYTE]1\r\n[STRING|12]1111111111\r\n[DWORD]1\r\n[DWORD]17\r\n[STRING|17]下发平台间报文\r\n','返回下发平台间报文应答，业务子类型为1302,查岗对象类型为1,查岗对象ID与中心分配的一致,消息ID为1','0x1300','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (62,'主链路车辆报警数据用例',1,149,1,1,20,'','主链路车辆报警数据','','返回主链路车辆报警信息业务数据体指令。','0x1400','请上传主链路车辆报警数据','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (91,'1.1.1终端注册成功',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=“10001”\r\n终端型号=“12345678”\r\n终端ID=“7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=京A12345','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答 注册成功；鉴权码=XXX','8100','1、确认企业平台中以下资料未被注册：\r\n终端ID=”7654321”\r\n车牌=”京A12345”\r\n2、在模拟终端发送终端注册指令\r\n3、观察模拟终端是否返回“成功；鉴权码=XXX”\r\n4、保存“鉴权码=XXX”的信息，以备“终端鉴权”功能测试。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (95,'上报报警信息用例',1,150,1,1,40,'','上报报警信息用例','','返回上报报警信息指令，\r\n包括车牌号、车牌颜色、报警信息来源、报警类型、报警时间、信息ID、报警信息内容','0x1402','请上传报警信息','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (98,'3.1.1文本信息下发',1,196,0,3,30,'Hb27zaiyv9Ct0unOxLG+0MXPos/Ct6LWuMHusuLK1KO6MTIzo6xBQkNEo6yjoUAjo6QlLi4uLi4=','1、从平台逐个下发所有类型的文本信息指令，下发内容譬如：“交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....”；\r\n2、观察模拟终端显示的标志和文本信息；\r\n3、观察平台接收到的终端应答指令。\r\n','[BYTE|1]1D\r\n[STRING|55]交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....\r\n','通用应答','0001','1、模拟终端显示的标志和文本信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (99,'1.1.2终端注册失败',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=”10001”\r\n终端型号=”12345678”\r\n终端ID=”7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=”京A12345”','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答：注册失败','8100','1、在模拟终端发送终端注册指令\r\n2、观察模拟终端是否返回“注册失败”\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (100,'从链路报警数据用例',1,124,1,0,20,'vqlRNTQzMjEAAAAAAAAAAAAAAAAAA5QAAAAACgAAAAAAAAADRlQ=','车牌号=“京Q54321”\r\n车牌颜色编码=3（黑色）\r\n子业务数据类型=0x9400\r\n数据=0x9400','[STRING|21]京Q54321\r\n[BYTE|1]3\r\n[WORD]37888\r\n[DWORD]10\r\n[BYTE|10]34654\r\n','','0x9400','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (104,'报警督办请求用例',1,125,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQBAAAAXAIACQAAAABOBVmyAACUAQAAAABOpjCyAFN1cGVydmlzb3IAAAAAAAAwMTA4ODQ1Njc4OQAAAAAAAAAAAHN1cGVydmlzb3JfZUBzdXBlci5jb20AAAAAAAAAAAAA','车牌号=“测A12345”\r\n车牌颜色编号=2（黄色）\r\n报警信息来源=0x02（企业监控平台）\r\n报警类型=0x0009（盗警）\r\n报警时间=2011-06-25 11:44:50\r\n报警督办ID=0x9401\r\n督办截止时间=2011-10-25 11:44:50\r\n督办级别=0x00（紧急）\r\n督办人=“Supervisor”\r\n督办人联系电话=“01088456789”\r\n督办联系电子邮件=“supervisor_e@super.com”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37889\r\n[DWORD]92\r\n[BYTE|1]02\r\n[WORD]9\r\n[TIME_T|8]2011-06-25 11:44:50\r\n[DWORD]37889\r\n[TIME_T|8]2011-10-25 11:44:50\r\n[BYTE|1]00\r\n[STRING|16]Supervisor\r\n[STRING|20]01088456789\r\n[STRING|32]supervisor_e@super.com\r\n','返回报警督办应答，业务子类型为1401\r\n\r\n返回消息参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色编号=2\r\n报警督办ID=37889\r\n报警处理结果=0x00（处理中）\r\n/报警处理结果=0x01（已处理完毕）\r\n/报警处理结果=0x02（不作处理）\r\n/报警处理结果=0x02（将来处理）','0x1400','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (105,'1.2.1终端注销成功',1,187,0,3,30,'','终端注销','','通用应答','0001','1、在模拟终端发送终端注销指令\r\n2、观察平台是否下发通用应答：成功\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (110,'1.3.1终端鉴权成功',1,188,0,3,30,'MTIzNDU2Nzg5MEE=','1、前提：终端注册成功；\r\n2、使用注册时平台下发的鉴权码进行鉴权；\r\n鉴权码：\"1234567890A\"','[STRING|11]1234567890A\r\n','通用应答：鉴权成功','0001','1、在模拟终端发送终端鉴权指令\r\n2、观察平台是否下发通用应答：成功','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (112,'1.4.1终端心跳',1,189,0,3,30,'','1、前提：终端鉴权成功；\r\n2、终端开始按照默认时间间隔发送心跳，平台应答心跳；','','通用应答：平台的心跳应答','8001','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (117,'3.2.1追加事件',1,197,0,3,30,'AgIBCLO1wb65ytXPAgSxqdPq','追加事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：暴雨','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]2\r\n[BYTE|1]4\r\n[STRING|4]暴雨\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (118,'3.2.2删除特定事件',1,197,0,3,30,'BAEBAA==','删除特定事件：\r\n设置总数：1\r\n事件项列表：\r\n事件ID：1\r\n事件内容长度：0','[BYTE|1]4\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台设置后结果相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (119,'3.3.1事件报告',1,198,0,3,30,'AQ==','事件报告：\r\n事件ID：1','[BYTE|1]1\r\n','平台通用应答','8001','模拟终端上传的事件报告，平台能正确显示。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (120,'6.1.1更新圆形区域',1,206,0,0,30,'AAEAAAABAD4CV6ugBu6Y4AAAA+gAZAo=','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n中心点纬度=39.3度\r\n中心点经度=116.3度\r\n半径=1000米\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]00\r\n[BYTE|1]01\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]39300000\r\n[DWORD]116300000\r\n[DWORD]1000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (121,'6.2.1删除圆形区域',1,207,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (122,'1.5.1设置终端参数',1,190,0,3,30,'BgAAAAEEAAAAPAAAACAEAAAAAAAAAEALMDEwODc2NTQzMjEAAABBETEzNjAxMDEyMzQ1AAAARQQAAAAAAAAARgQAAAA8','平台下发如下参数\r\n心跳间隔=60s，\r\n位置汇报策略=位置汇报，\r\n监控平台电话号码=01087654321，\r\n复位电话号码=13601012345，\r\n终端电话接听策略=自动接听，\r\n每次最长通话时间=60s','[BYTE|1]6\r\n[DWORD]1\r\n[BYTE|1]4\r\n[DWORD]60\r\n[DWORD]32\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]64\r\n[BYTE|1]B\r\n[STRING|11]01087654321\r\n[DWORD]65\r\n[BYTE|1]11\r\n[STRING|11]13601012345\r\n[DWORD]69\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]70\r\n[BYTE|1]4\r\n[DWORD]60','通用应答：成功','8001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (123,'3.4.1提问下发',1,199,0,3,30,'GBS1scews7XBvsrHt/HT0LnK1c+jvwEAAsrHAgACt/E=','提问下发：\r\n标志：0x14：终端TTS播读，广告屏显示；\r\n问题内容长度：20\r\n问题：当前车辆是否有故障？\r\n候选答案列表：\r\n答案ID：1\r\n答案内容长度：2\r\n答案：是\r\n答案ID：2\r\n答案内容长度：2\r\n答案：否','[BYTE|1]18\r\n[BYTE|1]14\r\n[STRING|20]当前车辆是否有故障？\r\n[BYTE|1]1\r\n[WORD]2\r\n[STRING|2]是\r\n[BYTE|1]2\r\n[WORD]2\r\n[STRING|2]否\r\n','终端通用应答','0001','1、模拟终端显示的提问内容与平台下发相同；\r\n2、平台正确接收模拟终端的通用应答。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (124,'6.3.1更新矩形区域',1,208,0,0,30,'AAEAAAABAD4CYruoBurizAJb8WAG9Yo4AGQK','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n左上点纬度=40.025度\r\n左上点经度=116.05678度\r\n右下点纬度=39.58度\r\n右下点经度=116.755度\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]0\r\n[BYTE|1]1\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (126,'3.8.1提问应答',1,231,0,3,30,'','提问应答：\r\n应答流水号：对应平台下发的提问的流水号；\r\n答案ID：1\r\n','','','','平台能解析出终端的的提问应答，答案ID是1；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (127,'从链路车辆静态信息交换业务',1,134,1,2,20,'y9VBMDAwMDEAAAAAAAAAAAAAAAAAMZYAAQ==','车牌号=“苏A00001”\r\n车牌颜色=1(蓝色)','[STRING|21]苏A00001\r\n[BYTE|1]31\r\n[WORD]38400\r\n[BYTE|1]1\r\n','','0x9600','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (128,'主链路车辆静态信息数据用例',1,154,1,1,20,'','主链路车辆静态信息数据','','返回主链路车辆静态信息数据指令','0x1600','请上传车辆静态信息数据','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (129,'主链路监管数据用例',1,152,1,1,20,'','主链路监管数据','','返回主链路监管业务数据指令','0x1500','请在送检企业平台向监管平台发送车辆监管业务','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (130,'补报车辆静态信息请求消息用例',1,135,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApYBAAAAAA==','车牌号=“测A12345”\r\n车牌颜色=2(黄色)','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38401\r\n[DWORD]00000000\r\n\r\n','返回补报车辆静态信息应答，业务子类型为0x1601\r\n\r\n车辆静态信息：\r\n车牌号\r\n车牌颜色\r\n车辆类型\r\n运输行业编码\r\n车籍地\r\n业户ID\r\n业户名称\r\n业户联系电话','0x1600','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (131,'6.4.1删除矩形区域',1,209,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (132,'3.5.1信息点播菜单追加',1,200,0,3,30,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻\r\n','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (133,'从链路监管数据用例',1,128,1,2,20,'tKhBNTQzMjEAAAAAAAAAAAAAAAAAOZUAAAAABQAAASNF','车牌号=“川A54321”\r\n车牌颜色编号=9（其他）\r\n监管数据=0x12345','[STRING|21]川A54321\r\n[BYTE|1]39\r\n[WORD]38144\r\n[DWORD]5\r\n[BYTE|5]12345\r\n','','0x9500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (134,'3.6.1信息点播',1,201,0,3,30,'','信息点播\r\n信息类型：1\r\n点播/取消标志：1，点播；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、平台能定期根据终端点播的信息，下发点播的相关信息。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (135,'车辆单向监听请求用例',1,129,0,1,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUBAAAAFDAxMDg4Nzg5NjUtNjc1NAAAAAAA','车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n回拨电话号码=“0108878965-6754”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38145\r\n[DWORD]20\r\n[STRING|20]0108878965-6754\r\n','返回车辆单向监听应答，业务子类型为1501\r\n返回消息内容参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n应答结果=0x00（监听成功）\r\n/应答结果=0x01（监听失败）','0x1500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (136,'6.5.1更新多边形区域',1,210,0,0,60,'AAAAAQA+AGQKAAQCW/FgBurizAJiu6gG6uLMAmK7qAb1ijgCW/FgBvWKOA==','区域ID=1\r\n区域属性=0x003E\r\n最高速度=100公里/小时\r\n超速持续时间=10秒\r\n区域总顶点数=4\r\n顶点纬度=39.58度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.755度\r\n顶点纬度=39.58度\r\n顶点经度=116.755度','[DWORD]1\r\n[WORD]62\r\n[WORD]100\r\n[BYTE|1]0A\r\n[WORD]4\r\n[DWORD]39580000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116755000\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n','成功','0001','1、平台下发设置多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (137,'3.7.1信息服务',1,202,0,3,30,'AQAqz8LO57Gxvqm12Mf4vavT0MDX0+rM7Mb4o6zH68zhx7DX9rrD17yxuKGj','信息类型：1\r\n信息长度：42\r\n信息内容：下午北京地区将有雷雨天气，请提前做好准备。','[BYTE|1]1\r\n[WORD]42\r\n[STRING|42]下午北京地区将有雷雨天气，请提前做好准备。\r\n','终端通用应答','0001','1、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (139,'6.6.1删除多边形区域',1,211,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (140,'车辆拍照请求用例',1,130,0,0,60,'vqlBNTQzMjEAAAAAAAAAAAAAAAAAMZUCAAAAAgkI','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n镜头ID=0x09\r\n照片大小=0x08（704*576）','[STRING|21]京A54321\r\n[BYTE|1]31\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]09\r\n[BYTE|1]08\r\n','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n拍照应答标识=0x00（不支持拍照相）\r\n/拍照应答标识=0x01（完成拍照）\r\n/拍照应答标识=0x02（完成拍照、照片数据稍后传送）\r\n/拍照应答标识=0x03（未拍照不在线）\r\n/拍照应答标识=0x04（未拍照无法使用指定镜头）\r\n/拍照应答标识=0x05（未拍照其他原因）\r\n/拍照应答标识=0x09（车牌号码错误）\r\n拍照位置点\r\n镜头ID=0x09\r\n图片长度\r\n图片大小==0x08（704*576）\r\n图片格式=0x01（jpg）\r\n/图片格式=0x02（gif）\r\n/图片格式=0x03（tiff）\r\n/图片格式=0x04（png）\r\n图片内容','0x9502','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (141,'下发车辆报文请求用例',1,131,0,1,60,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUDAAAADwAAAAEAAAAABr30vLEAAA==','车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n报文优先级=0x00\r\n报文内容=6','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38147\r\n[DWORD]15\r\n[DWORD]1\r\n[BYTE|1]00\r\n[DWORD]6\r\n[STRING|6]紧急\r\n','返回下发车辆报文应答，业务子类型为0x1503\r\n\r\n返回信息参考如下：\r\n车牌号=“测A12345”\r\n车牌颜色=2（黄色）\r\n消息ID=1\r\n应答结果=0x00（下发成功）\r\n/应答结果=0x01（下发失败）','0x1500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (142,'下发车辆行驶记录请求用例',1,132,0,0,30,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUEAAAAEAE=','车牌号=“测A12345”\r\n ','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38148\r\n[DWORD]16\r\n[BYTE|1]1\r\n','返回上报车辆行驶记录应答，业务子类型为1504\r\n\r\n返回信息参考如下：\r\n车牌号=“测A12345”\r\n车辆行驶记录信息','0x1500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (143,'1.6.1查询终端参数',1,191,0,3,90,'','查询终端参数','','查询终端参数应答：59个参数\r\n参数总数=59\r\n参数ID=0x0001,参数长度=4,参数值=60;\r\n参数ID=0x0002,参数长度=4,参数值=20;\r\n参数ID=0x0003,参数长度=4,参数值=3;\r\n参数ID=0x0004,参数长度=4,参数值=25;\r\n参数ID=0x0005,参数长度=4,参数值=2;\r\n参数ID=0x0006,参数长度=4,参数值=86400;\r\n参数ID=0x0007,参数长度=4,参数值=1;\r\n参数ID=0x0010,参数长度=5,参数值=\"CMNET\";\r\n参数ID=0x0011,参数长度=4,参数值=\"card\";\r\n参数ID=0x0012,参数长度=4,参数值=\"card\";\r\n参数ID=0x0013,参数长度=9,参数值=\"127.0.0.1\";\r\n参数ID=0x0014,参数长度=7,参数值=\"CSYL.BJ\";\r\n参数ID=0x0015,参数长度=4,参数值=\"CSBJ\";\r\n参数ID=0x0016,参数长度=4,参数值=\"BJCS\";\r\n参数ID=0x0017,参数长度=13,参数值=\"192.168.1.106\";\r\n参数ID=0x0018,参数长度=4,参数值=6500;\r\n参数ID=0x0019,参数长度=4,参数值=6580;\r\n参数ID=0x0020,参数长度=4,参数值=0;\r\n参数ID=0x0021,参数长度=4,参数值=0;\r\n参数ID=0x0022,参数长度=4,参数值=300;\r\n参数ID=0x0027,参数长度=4,参数值=600;\r\n参数ID=0x0028,参数长度=4,参数值=10;\r\n参数ID=0x0029,参数长度=4,参数值=60;\r\n参数ID=0x002C,参数长度=4,参数值=1000;\r\n参数ID=0x002D,参数长度=4,参数值=2000;\r\n参数ID=0x002E,参数长度=4,参数值=500;\r\n参数ID=0x002F,参数长度=4,参数值=500;\r\n参数ID=0x0030,参数长度=4,参数值=130;\r\n参数ID=0x0040,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0041,参数长度=11,参数值=\"13601012345\";\r\n参数ID=0x0042,参数长度=11,参数值=\"18876543210\";\r\n参数ID=0x0043,参数长度=12,参数值=\"106590202345\";\r\n参数ID=0x0044,参数长度=11,参数值=\"13901011000\";\r\n参数ID=0x0045,参数长度=4,参数值=0;\r\n参数ID=0x0046,参数长度=4,参数值=60;\r\n参数ID=0x0047,参数长度=4,参数值=3600;\r\n参数ID=0x0048,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0049,参数长度=12,参数值=\"106590202347\";\r\n参数ID=0x0050,参数长度=4,参数值=0;\r\n参数ID=0x0051,参数长度=4,参数值=0x0000000F;\r\n参数ID=0x0052,参数长度=4,参数值=0;\r\n参数ID=0x0053,参数长度=4,参数值=0;\r\n参数ID=0x0054,参数长度=4,参数值=0x000000FF;\r\n参数ID=0x0055,参数长度=4,参数值=120;\r\n参数ID=0x0056,参数长度=4,参数值=30;\r\n参数ID=0x0057,参数长度=4,参数值=14400;\r\n参数ID=0x0058,参数长度=4,参数值=36000;\r\n参数ID=0x0059,参数长度=4,参数值=900;\r\n参数ID=0x005A,参数长度=4,参数值=600;\r\n参数ID=0x0070,参数长度=4,参数值=6;\r\n参数ID=0x0071,参数长度=4,参数值=128;\r\n参数ID=0x0072,参数长度=4,参数值=70;\r\n参数ID=0x0073,参数长度=4,参数值=60;\r\n参数ID=0x0074,参数长度=4,参数值=255;\r\n参数ID=0x0080,参数长度=4,参数值=983490;\r\n参数ID=0x0081,参数长度=4,参数值=17;\r\n参数ID=0x0082,参数长度=4,参数值=0;\r\n参数ID=0x0083,参数长度=8,参数值=\"京A12345\";\r\n参数ID=0x0084,参数长度=4,参数值=1;','0104','平台对照“返回消息”逐一检查读取的59个参数是否与终端上报参数相同','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (144,'车辆应急接入监管平台请求用例',1,133,0,0,40,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUFAAAAkQAAAAAAAACyEN+3/s7xxvdBUE4AAAAAAAAAAAAAAEFkbWluaVVzZXIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyM3JzZDMyNDUAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjE4NAAAAAAAAAAAAAAAAAAAAAAAAAAiYB+QAAAAAbG18DM=','车牌号=“测A12345”\r\n鉴权码=“B210DF”\r\n拨号点名称=“服务器APN”\r\n拨号用户名=“AdminiUser”\r\n拨号密码=23rsd3245\r\n服务器IP=“192.168.1.184”\r\nTCP端口=“8800”\r\nUDP端口=“8080”\r\n结束时间=“2012-08-01 14:20:35”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38149\r\n[DWORD]145\r\n[BYTE|10]B210DF\r\n[STRING|20]服务器APN\r\n[STRING|49]AdminiUser\r\n[STRING|22]23rsd3245\r\n[STRING|32]192.168.1.184\r\n[WORD]8800\r\n[WORD]8080\r\n[TIME_T|8]2200-08-01 14:20:35\r\n','返回车辆应急接入监管平台应答，消息业务子类型为0x1505\r\n\r\n返回信息参考如下：\r\n车牌号=“测A12345”\r\n应答结果=0x00（车载终端成功收到该命令）\r\n/应答结果=0x01（无该车辆）\r\n/应答结果=0x02（其它原因失败）','0x1500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (146,'报警预警消息用例',1,126,0,1,15,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAHgMACwAAAABOBZKzAAAAD8arwOvCt8/fsai+rwAAAA==','车牌号=“测A12345”\r\n车牌颜色编码=4（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=11\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“偏离路线报警”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]11\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]偏离路线报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (147,'实时交换报警用例',1,127,0,1,15,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApQDAAAAHgMACAAAAABOBZKzAAAAD9S9veexqL6vAAAAAAAAAA==','车牌号=“测A2345”\r\n车牌颜色编码=2（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=08\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“越界报警”','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37891\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]08\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]越界报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (148,'4.1.1电话回拨',1,203,0,3,60,'ADAxMDg4ODg4ODg4','电话回拨(普通通话):\r\n标志 = 0 \r\n电话号码 = \"01088888888\"','[BYTE|1]0\r\n[STRING|11]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (149,'4.1.2电话回拨_监听',1,203,0,3,60,'ATAxMDg4ODg4ODg4AAAAAAAAAAAA','电话回拨(监听):\r\n标志 : 1\r\n电话号码 : 01088888888','[BYTE|1]1\r\n[STRING|20]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (150,'4.2.1设置电话本_删除',1,204,0,0,60,'AA==','删除终端上存储的所有联系人:\r\n设置类型 = 0','[BYTE|1]0','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (151,'1.7.1终端控制',1,192,0,3,90,'ATtDTU5FVDs7OzE5Mi4xNjguMS4xMDM7NjAwMDs7MTAwMDE7MS4wMzsxLjAwOzM2MDA=','无限升级指令：\r\nURL地址=;(空)\r\n拨号点名称=\"CMNET\"\r\n拨号用户名=;\r\n拨号密码=;\r\n地址=\"192.168.1.103\"\r\nTCP端口=6000\r\nUDP端口=;\r\n硬件版本=\"1.03\"\r\n固件版本=\"1.00\"\r\n连接到服务器的时限=3600','[BYTE|1]1\r\n[STRING|49];CMNET;;;192.168.1.103;6000;;10001;1.03;1.00;3600\r\n','通用应答：成功','0001','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (152,'4.2.2设置电话本_更新电话本',1,204,0,3,60,'AQUBCzEzODEyMzQ1Njc4BNXFyP0CCzEzODEyMzQ1Njc5BMDuy8QDCzEzODEyMzQ1NjgwBM31zuUDDDA3NTUxMjM0NTY3OAbVxcj9y8QDDDA3NjkzMzMzNTU1NQbVxcj9zuU=','更新电话本(删除终端中已有全部联系人并追加消息中的联系人):\r\n设置类型 = 1\r\n联系人总数= 5\r\n联系人项 :\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 11\r\n电话号码 = \"13812345679\"\r\n联系人长度 = 4\r\n联系人 = \"李四\"\r\n\r\n标志 = 3\r\n号码长度 = 11\r\n电话号码 = \"13812345680\"\r\n联系人长度 = 4\r\n联系人 = \"王五\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"张三四\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"076933335555\"\r\n联系人长度 = 6\r\n联系人 = \"张三五\"\r\n\r\n','[BYTE|1]1\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0B\r\n[STRING|11]13812345679\r\n[BYTE|1]4\r\n[STRING|4]李四\r\n[BYTE|1]3\r\n[BYTE|1]0B\r\n[STRING|11]13812345680\r\n[BYTE|1]4\r\n[STRING|4]王五\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]张三四\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]076933335555\r\n[BYTE|1]6\r\n[STRING|6]张三五\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (153,'6.7.1更新路线',1,212,0,0,30,'AAAAAQA+AAYAAAABAAAAAQJgU0AG71wwZAIAZAoAAAACAAAAAgJgVmAG74NAZAMCWAAKAGQKAAAAAwAAAAICYF1oBu/RYGQDAlgACgBkCgAAAAQAAAACAmBiGAbwPsBkAwJYAAoAZAoAAAAFAAAAAgJgXWgG8E5gZAMCWAAKAGQKAAAABgAAAAICYFw8BvCUsGQDAlgACgBkCg==','路线ID=1\r\n路线属性=62\r\n路线总拐点数=6\r\n\r\n路段ID=1\r\n拐点ID=1，39.867200，116.350000，路宽=100米，属性=2，限速=100公里/小时，超速持续时间=10秒\r\n\r\n路段ID=2\r\n拐点ID=2，39.868000，116.360000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=3，39.869800，116.380000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=4，39.871000，116.408000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=5，39.869800，116.412000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=6，39.869500，116.430000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒','[DWORD]1\r\n[WORD]62\r\n[WORD]6\r\n[DWORD]1\r\n[DWORD]1\r\n[DWORD]39867200\r\n[DWORD]116350000\r\n[BYTE|1]64\r\n[BYTE|1]2\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]2\r\n[DWORD]2\r\n[DWORD]39868000\r\n[DWORD]116360000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]3\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116380000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]4\r\n[DWORD]2\r\n[DWORD]39871000\r\n[DWORD]116408000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]5\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116412000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]6\r\n[DWORD]2\r\n[DWORD]39869500\r\n[DWORD]116430000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A','成功','0001','1、平台下发设置路线指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (154,'6.8.1删除所有路线',1,213,0,0,30,'AA==','路线数=0','[BYTE|1]0\r\n','成功','0001','1、平台下发删除线路指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (155,'4.2.3设置电话本_追加电话本',1,204,0,3,60,'AgIBCzEzODEyMzQ1Njc4BNXFyP0CDDA3NTUxMjM0NTY3OAbA7sj9y8Q=','设置电话本（追加电话本）\r\n设置类型 = 2\r\n联系人总数 = 2\r\n联系人项\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"李三四\"\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]李三四\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (156,'4.2.4设置电话本_修改电话本',1,204,0,3,60,'13812345678张三','设置电话本（修改电话本，以联系人为索引）\r\n设置类型 = 3\r\n联系人总数 = 1\r\n联系人项\r\n','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (157,'2.1.1手动报警',1,193,0,3,90,'','使用模拟终端中的“4、手动位置上报”,选中紧急告警项。然后点击执行命令按钮','','通用应答：成功','0001','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (158,'2.2.1位置信息查询',1,194,0,3,90,'','查询位置信息','','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。至少一种以上的附件信息测试。','0201','检测平台收到的信息和模拟终端发送的位置信息内容一致','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (159,'2.3.1临时位置跟踪控制',1,195,0,3,180,'ABQAAAB4','时间间隔=10秒\r\n位置跟踪有效期=120秒','[WORD]20\r\n[DWORD]120\r\n','使用模拟终端的“5、定时位置上报”，按照设置的时间间隔及持续时间上报位置信息12次','0001','1、平台下发临时位置跟踪控制指令；\r\n2、模拟终端按“时间间隔”和“跟踪有效期”定时上传位置信息；\r\n3、观察平台显示的信息内容是否与模拟终端上传一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (160,'5.1.1车辆控制_车门解锁',1,205,0,3,60,'AA==','控制标志=0（车门解锁）','[BYTE|1]0\r\n','模拟终端自动回应，平台上收到回应，显示“车门解锁”状态：\r\n应答流水号= \r\n位置信息  =\r\n','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门解锁”状态。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (161,'5.1.2车辆控制_车门加锁',1,205,0,3,60,'AQ==','控制标志=1（车门加锁）','[BYTE|1]1\r\n','模拟终端自动回应，平台上收到回应，显示“车门加锁”状态：\r\n应答流水号= \r\n位置信息  =','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门加锁”状态。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (162,'9.2.1数据上行透传',1,225,0,3,30,'','使用模拟终端中手动用力中的“7、数据上行透传”，设置透传类型已经透传内容进行上传。','','平台通用应答','8001','1、观察平台收到的透传信息是否与模拟终端中输入的透传信息一致。\r\n2、观察平台是否正确返回应答。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (164,'3.5.2信息点播菜单修改',1,200,0,3,30,'AwIBAAjCt7/20MXPogIACLn6xNrQws7F','追加信息点播菜单\r\n设置类型：修改\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：路况信息\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国内新闻\r\n','[BYTE|1]3\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]路况信息\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国内新闻','终端通用应答','0001','1、平台下发信息点播菜单修改指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (165,'3.5.3信息点播菜单更新',1,200,0,3,30,'AQMBAAixsb6pzOzG+AIACLn6vMrQws7FAgAIwsPTzr3pydw=','追加信息点播菜单\r\n设置类型：更新\r\n信息项总数：3\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：北京天气\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国际新闻\r\n信息类型：3\r\n信息名称长度：8\r\n信息名称：旅游介绍\r\n','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]北京天气\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国际新闻\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]旅游介绍','终端通用应答','0001','1、平台下发信息点播菜单更新指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (166,'3.5.4信息点播菜单删除',1,200,0,3,30,'AA==','删除终端全部信息菜单','[BYTE|1]0\r\n','终端通用应答','0001','1、平台下发信息点播菜单删除指令；\r\n2、观察模拟终端是否全部删除信息点播菜单；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (167,'3.6.2信息取消',1,201,0,3,30,'','信息取消\r\n信息类型：1\r\n点播/取消标志：0，取消；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、终端不再接收到已取消的信息服务。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (168,'3.2.3更新事件',1,197,0,3,30,'AQMBCMDX0+rM7Mb4AgiztcG+ucrVzwMIwrfD5syuy/o=','更新事件\r\n设置总数：3\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：雷雨天气\r\n事件ID：2\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：3\r\n事件内容长度：8\r\n事件内容：路面坍塌','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]雷雨天气\r\n[BYTE|1]2\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]3\r\n[BYTE|1]8\r\n[STRING|8]路面坍塌','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (169,'3.2.4修改事件',1,197,0,3,30,'AwEBCLW9tO+3tMCh','修改事件：\r\n设置总数：1\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：到达反馈','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]到达反馈\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (170,'3.2.5删除全部事件',1,197,0,3,30,'AA==','删除终端现有所有事件','[BYTE|1]0\r\n','终端通用应答','0001','1、观察终端是否删除全部事件。\r\n2、平台是否正确接收终端应答。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (171,'8.1.1手动上发多媒体事件',1,218,0,1,30,'','','','多媒体事件上传通用应答','0x0800','请确认终端已发送多媒体事件信息上传报文','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (174,'7.2.1电子运单上报',1,216,0,3,90,'','使用模拟终端的“手工用例”中的“9、电子运单上报”发送数据\r\n运单数据“ID：20110628 货物名称：打印机20台 目的地：中国深圳”','','','8001','平台收到与终端发送信息一致的运单信息','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (175,'9.1.1数据下行透传',1,224,0,0,30,'123456789','透传消息类型=1\r\n透传消息内容=“123456789”','[BYTE|1]1\r\n[STRING|9]123456789\r\n','成功','0001','1、平台下发数据下行透传指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (177,'8.2.1抓拍摄像头通道1立即上传',1,220,0,2,60,'\0\0\0\0','拍摄照片：1\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：640×480\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回图片应当为标注通道号码的粉红色测试图片','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (182,'7.3.1驾驶员身份信息采集上报',1,217,0,3,90,'','使用模拟终端的“手工用例”中的“6、驾驶员身份信息采集上报”上报信息\r\n1、驾驶员姓名：张老三\r\n2、驾驶员身份证：010456198009080234\r\n3、从业资格证编号：0106785869869807\r\n4、发证机构名称：北京市道路运输管理局\r\n','','','8001','平台收到与上报数据相同的驾驶员信息','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (186,'8.2.2录像通道1上传',1,220,0,2,300,'Af//AAAAAQEBAQEBAQ==','终端录像\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：320×240\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]65535\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回数据为AVI（RAFF）容器H264视频，内容为蓝色GPS图样文字与JT/T808-2011字样3维动画','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (187,'8.3.1检索图片列表',1,221,0,3,60,'AAAAEQYBAAABEQYwI1lZ','多媒体类型：图片\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]0\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959\r\n','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (188,'8.3.2检索音频列表',1,221,0,3,60,'AQAAEQYBAAABEQYwI1lZ','多媒体类型：音频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]1\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (189,'8.3.3检索视频列表',1,221,0,3,60,'AgAAEQYBAAABEQYwI1lZ','多媒体类型：视频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]2\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (191,'8.4.1上传图像',1,222,0,2,60,'AAEAEQYlAAABEQYlI1lZAA==','上传存储图像\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0\r\n','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (192,'8.4.2上传音频',1,222,0,2,100,'AQEAEQYlAAABEQYlI1lZAA==','上传存储音频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (193,'8.4.3上传视频',1,222,0,2,300,'AgEAEQYlAAABEQYlI1lZAA==','上传存储视频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (194,'8.5.1录音32Kbps立即上传',1,223,0,2,100,'AQAAAAM=','开始录音\r\n持续录音\r\n实时上传\r\n采样率：32Kbps','[BYTE|1]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]3\r\n','开始录音通用应答','0x8804','同时上传多媒体数据\r\n音频内容为440Hz断续正弦波测试信号','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (195,'1.5.2超速设置',1,190,0,3,30,'','平台下发如下参数\r\n限速值=80公里/小时；\r\n持续时间=10秒','','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (196,'1.5.3疲劳驾驶设置',1,190,0,3,600,'AgAAAFcEAAACWAAAAFkEAAAAtA==','平台下发如下参数\r\n连续驾驶时间=600秒；\r\n最小休息时间=180秒。','[BYTE|1]2\r\n[DWORD]87\r\n[BYTE|1]4\r\n[DWORD]600\r\n[DWORD]89\r\n[BYTE|1]4\r\n[DWORD]180\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (197,'1.5.4超时停车',1,190,0,3,300,'AQAAAFoEAAABLA==','平台下发如下参数\r\n最长停车时间=300s','[BYTE|1]1\r\n[DWORD]90\r\n[BYTE|1]4\r\n[DWORD]300\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (198,'2.1.2电瓶欠压',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“终端主电源欠压”项\r\n然后点击执行命令按钮','','通用应答成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (199,'注销主从链路消息',1,233,1,1,30,'','','','返回消息ID为1003','0x1003','通知被测下级平台发送主从链路断开消息（1003协议消息）','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (200,'2.1.3断电提醒',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“电路断开”项\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (201,'2.1.4终端故障',1,193,0,3,30,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。随便选择设备故障中的一个。\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (202,'2.1.5休眠',1,193,0,3,300,'AgAAACEEAAAAAAAAACcEAAABLA==','先通过平台进行参数设置：\r\n位置汇报方案=0（根据ACC状态）\r\n休眠时汇报时间间隔=300秒\r\n使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须选择ACC关。\r\n然后点击执行命令按钮','[BYTE|1]2\r\n[DWORD]33\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]39\r\n[BYTE|1]4\r\n[DWORD]300\r\n','平台通用应答','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (203,'3.5.5信息点播菜单追加',1,200,1,0,10,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (206,'3.2.6追加新事件',1,197,1,3,30,'','追加新事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：阴雨天气\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：路滑','','终端通用应答','0001','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (207,'7.1.1行车记录仪数据采集',1,214,0,1,30,'','下行行车记录仪设置指令','','返回行车记录仪数据上传，其中命令字与下发命令字相同','0700','','',2,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (208,'下级平台实时上传车辆定位信息',1,234,0,1,60,'','','','返回消息的业务类型为1200，业务子类型为1202','','请提示下级平台实时上传不少于3条车牌号为“测A12345”，车牌颜色为黄色的车辆的定位信息(1202消息)','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (209,'下级平台车辆定位信息补报',1,235,0,1,60,'','','','最少收到3条定位信息补报，消息ID为1200，业务子类型为1203','','请提示下级平台补报不少于3条车牌号为“测A12345”，车牌颜色为黄色的车辆的自动补报信息（1203号协议）','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (211,'平台查岗请求用例2',1,121,0,1,60,'kwEAAAAdAjExMTExMTExMTEAAAAAAAEAAAAIsum42gAAAAA=','子业务类型=0x9301\r\n查岗对象类型=0x02\r\n查岗对象ID=业户经营许可证\r\n信息ID=2\r\n信息长度=8\r\n信息内容=“查岗”','[WORD]37633\r\n[DWORD]29\r\n[BYTE]2\r\n[STRING|12]1111111111\r\n[DWORD]1\r\n[DWORD]8\r\n[STRING|8]查岗\r\n','返回平台查岗应答，消息的业务子类型为1301,查岗对象类型为2,查岗对象ID为业户经营许可证,消息ID为2','0x1300','业户查岗','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (212,'主动上报车辆电子运单信息消息用例',1,252,0,1,60,'','','','子业务数据类型=0x120D','0x1200','下级平台主动上报车辆电子运单信息，向上级平台上报车辆当前电子运单\r\n车牌号=测A12345\r\n车牌颜色编码=2（黄色）\r\n子业务数据类型=0x120D','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (213,'主动上报驾驶员身份信息消息用例',1,251,0,1,60,'','','','子业务类型标识=0x120C','0x1200','下级平台在接收到车载终端上传的驾驶员身份信息后，进行合法性验证并将结果主动向上级平台上报。\r\n车牌号=测A12345\r\n车牌颜色=2黄色\r\n子业务类型标识=0x120C','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (214,'下发平台间报文请求用例2',1,122,0,1,30,'kwIAAAAmAjExMTExMTExMTEAAAAAAAIAAAARz8K3osa9zKi85LGozsQAAAA=','子业务数据类型=0x9302\r\n信息ID=0x9302\r\n查岗对象类型=0x02\r\n查岗对象ID=业户经营许可证\r\n信息ID=2','[WORD]37634\r\n[DWORD]38\r\n[BYTE]2\r\n[STRING|12]1111111111\r\n[DWORD]2\r\n[DWORD]17\r\n[STRING|17]下发平台间报文\r\n','返回下发平台间报文应答，业务子类型为1302,查岗对象类型为2,查岗对象ID为业户经营许可证,消息ID为2','0x1300','业户报文','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (215,'主链路登录请求用例',1,81,1,1,120,'','','','登录消息ID为0x1001','0x1001','请提示企业平台登录','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (216,'交换车辆静态信息用例',1,111,0,0,10,'suIxMjM0NQAAAAAAAAAAAAAAAAAAApIEAAAAJFRSQU5TX1RZUEU6PTAzMDtWSU46PbLiQTEyMzQ1AAAAAAAAAA==','\r\n车牌号=测A12345\r\n车辆颜色=2（黄色）\r\n子业务类型=0x9204\r\n','[STRING|21]测12345\r\n[BYTE|1]2\r\n[WORD]37380\r\n[DWORD]36\r\n[STRING|36]TRANS_TYPE:=030;VIN:=测A12345','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (217,'主动上报报警处理结果信息消息用例',1,258,0,1,60,'','','','车牌号码=测A12345\r\n车牌颜色=2\r\n子业务类型标识=0x1403\r\n报警处理结果，定义如下：\r\n0x00：处理中；\r\n0x01：已处理完毕；\r\n0x02：不作处理；\r\n0x03：将来处理。\r\n','0x1403','描述：下级平台向主动向上级平台上报报警处理结果，其数据体规定见表53。本条消息上级平台无需应答。','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (218,'车辆拍照请求消息用例',1,242,0,1,60,'suJBMTIzNDUAAAAAAAAAAAAAAAAAApUCAAAAAgEB','车牌号=测A12345\r\n颜色=2\r\n子业务类型标识=38146\r\n后续长度=2\r\n镜头ID=1\r\nsize=1','[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n\r\n','子业务类型=0x1502','0x1500','','',1,0);
insert  into `example_copy1`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (219,'上报报警信息消息用例',1,256,0,1,60,'','','','车牌号=测A12345\r\n颜色=]2\r\n子业务类型标识=0x1402\r\n\r\n\r\n','0x1402','请提示被测平台上传报警信息消息，本消息上级平台无需应答','',1,0);

/*Table structure for table `examplebak` */

DROP TABLE IF EXISTS `examplebak`;

CREATE TABLE `examplebak` (
  `ExampleID` int(11) NOT NULL COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '功能ID 1=只读 0=可编辑',
  `FunctionID` int(11) NOT NULL COMMENT '功能编号',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示（提示内容为备注字段内容）',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间（秒）',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙字符',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙字符',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='用例管理';

/*Data for the table `examplebak` */

/*Table structure for table `factory` */

DROP TABLE IF EXISTS `factory`;

CREATE TABLE `factory` (
  `FactoryID` int(11) NOT NULL AUTO_INCREMENT COMMENT '商厂编号',
  `FactoryName` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '厂商名称',
  `Linker` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '联系人',
  `Telephone` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '联系电话',
  `Address` varchar(64) COLLATE gbk_bin DEFAULT NULL COMMENT '联系地址',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `provinceID` varchar(10) COLLATE gbk_bin NOT NULL COMMENT '省编号',
  PRIMARY KEY (`FactoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='厂商信息';

/*Data for the table `factory` */

insert  into `factory`(`FactoryID`,`FactoryName`,`Linker`,`Telephone`,`Address`,`Note`,`provinceID`) values (1,'中交兴路','刘','13812345678','北京市海淀区','测试','010');
insert  into `factory`(`FactoryID`,`FactoryName`,`Linker`,`Telephone`,`Address`,`Note`,`provinceID`) values (2,'chinamobel','11','11','11','11','010');
insert  into `factory`(`FactoryID`,`FactoryName`,`Linker`,`Telephone`,`Address`,`Note`,`provinceID`) values (3,'厦门雅迅','1111','13100001234','22222','12345678','0591');

/*Table structure for table `functiongroupref` */

DROP TABLE IF EXISTS `functiongroupref`;

CREATE TABLE `functiongroupref` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '号编',
  `GroupID` int(11) NOT NULL COMMENT '分类编号',
  `FunctionID` int(11) NOT NULL COMMENT '功能编号',
  `SerialNumber` int(11) NOT NULL DEFAULT '0' COMMENT '号序',
  `Flag` int(11) NOT NULL COMMENT '1=应具有；2=可选；3=不必具有',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=843 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标分类和功能关系';

/*Data for the table `functiongroupref` */

insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (312,3,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (313,3,85,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (314,3,87,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (315,3,93,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (316,3,94,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (317,3,80,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (318,3,100,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (319,3,101,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (320,3,102,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (321,3,103,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (322,3,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (323,3,109,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (324,3,110,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (325,3,111,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (326,3,112,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (327,3,113,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (328,3,114,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (329,3,115,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (330,3,116,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (331,3,117,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (332,3,118,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (333,3,119,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (334,3,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (335,3,120,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (336,3,121,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (337,3,122,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (338,3,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (339,3,107,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (340,3,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (341,3,124,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (573,5,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (574,5,81,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (575,5,82,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (576,5,87,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (577,5,94,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (578,5,101,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (579,5,244,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (580,5,245,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (581,5,247,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (582,5,248,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (583,5,249,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (584,5,250,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (585,5,251,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (586,5,252,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (587,5,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (588,5,110,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (589,5,111,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (590,5,112,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (591,5,113,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (592,5,114,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (593,5,118,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (594,5,119,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (595,5,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (596,5,121,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (597,5,122,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (598,5,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (599,5,256,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (600,5,258,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (601,5,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (602,5,125,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (603,5,126,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (604,5,127,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (605,5,175,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (606,5,186,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (607,5,187,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (608,5,188,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (609,5,189,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (610,5,190,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (611,5,191,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (612,5,192,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (613,5,176,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (614,5,193,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (615,5,194,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (616,5,195,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (617,5,177,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (618,5,196,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (619,5,197,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (620,5,198,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (621,5,199,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (622,5,200,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (623,5,201,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (624,5,202,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (625,5,231,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (626,5,178,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (627,5,203,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (628,5,204,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (629,5,179,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (630,5,205,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (631,5,180,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (632,5,206,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (633,5,207,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (634,5,208,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (635,5,209,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (636,5,210,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (637,5,211,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (638,5,212,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (639,5,213,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (640,5,181,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (641,5,214,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (642,5,216,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (643,5,217,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (644,5,182,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (645,5,218,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (646,5,220,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (647,5,221,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (648,5,222,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (649,5,223,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (650,5,183,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (651,5,224,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (652,5,225,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (653,5,1061,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (654,5,129,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (655,5,131,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (656,5,132,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (657,5,133,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (658,5,242,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (659,5,1071,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (660,5,135,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (661,5,2321,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (662,5,233,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (753,6,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (754,6,81,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (755,6,82,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (756,6,87,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (757,6,94,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (758,6,101,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (759,6,244,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (760,6,245,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (761,6,247,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (762,6,248,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (763,6,249,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (764,6,250,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (765,6,251,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (766,6,252,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (767,6,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (768,6,110,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (769,6,111,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (770,6,112,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (771,6,113,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (772,6,114,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (773,6,118,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (774,6,119,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (775,6,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (776,6,121,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (777,6,122,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (778,6,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (779,6,256,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (780,6,258,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (781,6,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (782,6,125,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (783,6,126,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (784,6,127,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (785,6,175,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (786,6,186,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (787,6,187,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (788,6,188,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (789,6,189,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (790,6,190,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (791,6,191,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (792,6,192,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (793,6,176,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (794,6,193,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (795,6,194,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (796,6,195,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (797,6,177,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (798,6,196,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (799,6,197,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (800,6,198,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (801,6,199,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (802,6,200,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (803,6,201,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (804,6,202,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (805,6,231,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (806,6,178,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (807,6,203,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (808,6,204,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (809,6,179,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (810,6,205,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (811,6,180,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (812,6,206,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (813,6,207,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (814,6,208,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (815,6,209,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (816,6,210,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (817,6,211,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (818,6,212,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (819,6,213,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (820,6,181,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (821,6,214,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (822,6,216,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (823,6,217,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (824,6,182,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (825,6,218,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (826,6,220,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (827,6,221,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (828,6,222,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (829,6,223,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (830,6,183,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (831,6,224,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (832,6,225,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (833,6,1061,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (834,6,129,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (835,6,131,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (836,6,132,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (837,6,133,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (838,6,242,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (839,6,1071,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (840,6,135,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (841,6,2321,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (842,6,233,0,1);

/*Table structure for table `functiongroupref_copy` */

DROP TABLE IF EXISTS `functiongroupref_copy`;

CREATE TABLE `functiongroupref_copy` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '号编',
  `GroupID` int(11) NOT NULL COMMENT '分类编号',
  `FunctionID` int(11) NOT NULL COMMENT '功能编号',
  `SerialNumber` int(11) NOT NULL DEFAULT '0' COMMENT '号序',
  `Flag` int(11) NOT NULL COMMENT '1=应具有；2=可选；3=不必具有',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=573 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标分类和功能关系';

/*Data for the table `functiongroupref_copy` */

insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (312,3,79,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (313,3,85,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (314,3,87,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (315,3,93,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (316,3,94,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (317,3,80,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (318,3,100,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (319,3,101,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (320,3,102,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (321,3,103,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (322,3,104,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (323,3,109,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (324,3,110,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (325,3,111,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (326,3,112,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (327,3,113,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (328,3,114,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (329,3,115,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (330,3,116,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (331,3,117,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (332,3,118,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (333,3,119,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (334,3,105,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (335,3,120,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (336,3,121,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (337,3,122,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (338,3,106,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (339,3,107,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (340,3,123,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (341,3,124,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (419,6,79,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (420,6,87,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (421,6,94,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (422,6,104,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (423,6,110,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (424,6,111,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (425,6,112,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (426,6,113,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (427,6,114,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (428,6,118,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (429,6,119,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (430,6,234,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (431,6,235,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (432,6,105,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (433,6,121,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (434,6,122,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (435,6,106,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (436,6,129,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (437,6,131,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (438,6,132,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (439,6,133,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (440,6,107,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (441,6,135,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (442,6,123,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (443,6,125,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (444,6,126,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (445,6,127,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (446,6,175,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (447,6,186,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (448,6,187,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (449,6,188,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (450,6,189,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (451,6,190,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (452,6,191,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (453,6,192,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (454,6,176,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (455,6,193,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (456,6,194,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (457,6,195,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (458,6,177,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (459,6,196,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (460,6,197,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (461,6,198,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (462,6,199,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (463,6,200,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (464,6,201,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (465,6,202,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (466,6,231,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (467,6,178,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (468,6,203,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (469,6,204,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (470,6,179,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (471,6,205,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (472,6,180,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (473,6,206,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (474,6,207,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (475,6,208,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (476,6,209,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (477,6,210,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (478,6,211,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (479,6,212,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (480,6,213,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (481,6,181,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (482,6,214,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (483,6,216,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (484,6,217,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (485,6,182,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (486,6,218,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (487,6,220,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (488,6,221,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (489,6,222,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (490,6,223,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (491,6,183,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (492,6,224,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (493,6,225,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (494,6,232,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (495,6,233,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (496,5,79,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (497,5,87,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (498,5,94,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (499,5,104,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (500,5,110,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (501,5,111,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (502,5,112,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (503,5,113,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (504,5,114,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (505,5,118,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (506,5,119,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (507,5,234,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (508,5,235,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (509,5,105,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (510,5,121,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (511,5,122,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (512,5,106,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (513,5,129,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (514,5,131,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (515,5,132,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (516,5,133,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (517,5,107,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (518,5,135,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (519,5,123,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (520,5,125,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (521,5,126,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (522,5,127,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (523,5,175,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (524,5,186,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (525,5,187,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (526,5,188,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (527,5,189,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (528,5,190,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (529,5,191,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (530,5,192,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (531,5,176,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (532,5,193,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (533,5,194,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (534,5,195,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (535,5,177,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (536,5,196,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (537,5,197,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (538,5,198,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (539,5,199,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (540,5,200,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (541,5,201,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (542,5,202,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (543,5,231,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (544,5,178,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (545,5,203,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (546,5,204,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (547,5,179,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (548,5,205,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (549,5,180,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (550,5,206,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (551,5,207,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (552,5,208,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (553,5,209,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (554,5,210,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (555,5,211,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (556,5,212,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (557,5,213,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (558,5,181,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (559,5,214,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (560,5,216,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (561,5,217,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (562,5,182,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (563,5,218,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (564,5,220,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (565,5,221,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (566,5,222,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (567,5,223,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (568,5,183,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (569,5,224,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (570,5,225,0,1);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (571,5,232,0,0);
insert  into `functiongroupref_copy`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (572,5,233,0,1);

/*Table structure for table `functionlist` */

DROP TABLE IF EXISTS `functionlist`;

CREATE TABLE `functionlist` (
  `FunctionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '功能名称',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2322 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能信息';

/*Data for the table `functionlist` */

insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (79,'','4.5.1 链路管理业务类',1,0,0,'链路管理业务类',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (81,'0x1001','4.5.1.1 主链路登录请求消息',2,79,79,'主链路登录请求',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (82,'0x9001','4.5.1.9 从链路连接请求消息',2,79,79,'从链路连接请求消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (87,'0x1005','4.5.1.5 主链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (94,'0x9005','4.5.1.13 从链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (101,'','4.5.3.1 主链路动态信息交换消息',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (104,'','4.5.3.2 从链路车辆动态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (105,'','4.5.4.2 从链路平台间信息交互业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (106,'','4.5.5.1 主链路报警信息交互消息',1,0,0,'主链路报警信息交互消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (110,'0x9205','4.5.3.2.5 启动车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (111,'0x9204','4.5.3.2.4 交换车辆静态信息消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (112,'0x9202','4.5.3.2.2 交换车辆定位信息消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (113,'0x9203','4.5.3.2.3 车辆定位信息交换补发消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (114,'0x9206','4.5.3.2.6 结束车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (118,'0x920A','4.5.3.2.10 上报车辆驾驶员身份识别信息请求',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (119,'0x920B','4.5.3.2.11 上报车辆电子运单请求',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (121,'0x9301','4.5.4.2.2 平台查岗请求消息',2,105,105,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (122,'0x9302','4.5.4.2.3 下发平台间报文请求消息',2,105,105,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (123,'','4.5.5.2 从链路车辆报警信息业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (125,'0x9401','4.5.5.2.2 报警督办请求消息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (126,'0x9402','4.5.5.2.3 报警预警消息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (127,'0x9403','4.5.5.2.4 实时交换报警信息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (129,'0x9501','4.5.6.2.2 车辆单向监听请求消息',2,1061,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (131,'0x9503','4.5.6.2.4 下发车辆报文请求',2,1061,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (132,'0x9504','4.5.6.2.5 上报车辆行驶记录请求',2,1061,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (133,'0x9505','4.5.6.2.6 车辆应急接入监管平台请求消息',2,1061,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (135,'0x9601','4.5.7.2.2 补报车辆静态信息请求',2,1071,107,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (167,'','AS2',2,153,153,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (175,'','1.终端管理',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (176,'','2.位置报警',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (177,'','3.信息',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (178,'','4.电话',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (179,'','5.车辆控制',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (180,'','6.车辆管理',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (181,'','7.信息采集',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (182,'','8.多媒体',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (183,'','9.通用数据传输',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (186,'0x0100','1.1终端注册',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (187,'0x0003','1.2终端注销',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (188,'0x0102','1.3终端鉴权',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (189,'0x0002','1.4终端心跳',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (190,'0x8103','1.5设置终端参数',2,175,175,NULL,2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (191,'0x8104','1.6查询终端参数',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (192,'0x8105','1.7终端控制',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (193,'0x0200','2.1位置及报警',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (194,'0x8201','2.2位置信息查询',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (195,'0x8202','2.3临时位置跟踪控制',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (196,'0x8300','3.1文本信息下发',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (197,'0x8301','3.2事件设置',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (198,'0x0301','3.3事件报告',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (199,'0x8302','3.4提问下发',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (200,'0x8303','3.5信息点播菜单设置',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (201,'0x0303','3.6信息点播/取消',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (202,'0x8304','3.7信息服务',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (203,'0x8400','4.1电话回拨',2,178,178,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (204,'0x8401','4.2设置电话本',2,178,178,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (205,'0x8500','5.1车辆控制',2,179,179,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (206,'0x8600','6.1设置圆形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (207,'0x8601','6.2删除圆形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (208,'0x8602','6.3设置矩形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (209,'0x8603','6.4删除矩形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (210,'0x8604','6.5设置多边形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (211,'0x8605','6.6删除多边形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (212,'0x8606','6.7设置路线',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (213,'0x8607','6.8删除路线',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (214,'0x8700','7.1行驶记录仪数据采集命令',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (216,'0x0701','7.2电子运单上报',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (217,'0x0702','7.3驾驶员身份信息采集上报',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (218,'0x0800','8.1多媒体事件信息上传',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (220,'0x8801','8.2摄像头立即拍摄命令',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (221,'0x8802','8.3存储多媒体数据检索',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (222,'0x8803','8.4存储多媒体数据上传',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (223,'0x8804','8.5录音开始命令',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (224,'0x8900','9.1数据下行透传',2,183,183,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (225,'0x0900','9.2数据上行透传',2,183,183,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (231,'0x0302','3.8提问应答',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (233,'0x1003','4.5.1.3 注销主链路消息',2,2321,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (242,'0x9502','4.5.6.2.3 车辆拍照请求消息',2,1061,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (244,'0x1201','4.5.3.1.2 上传车辆注册信息消息',2,101,101,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (245,'0x1202','4.5.3.1.3 实时上传车辆定位信息',2,101,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (247,'0x1203','4.5.3.1.4 车辆定位信息自动补报',2,101,237,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (248,'0x1207','4.5.3.1.7 申请交换指定车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (249,'0x1209','4.5.3.1.9 补发车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (250,'0x1208','4.5.3.1.8 取消交换指定车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (251,'0x120C','4.5.3.1.12 主动上报驾驶员身份信息消息',2,101,237,'4.5.3.1.12 主动上报驾驶员身份信息消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (252,'0x120D','4.5.3.1.13 主动上报车辆电子运单信息消息',2,101,237,'主动上报车辆电子运单信息消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (256,'0x1402','4.5.5.1.3 上报报警信息消息',2,106,254,'上报报警信息消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (258,'0x1403','4.5.5.1.4 主动上报报警处理结果信息消息',2,106,254,'主动上报报警处理结果信息消息',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (259,'0x9209','4.5.3.2.9 补发车辆定位信息应答消息',2,104,104,'补发车辆定位信息应答消息',5,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (260,'0x9003','4.5.1.11 从链路注销',2,2321,2321,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (1061,'','4.5.6.2 从链路车辆监管业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (1071,'','4.5.7.2 从链路车辆静态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (2321,'','4.5.1 链路注销',1,0,0,'',1,0);

/*Table structure for table `functionlist_copy` */

DROP TABLE IF EXISTS `functionlist_copy`;

CREATE TABLE `functionlist_copy` (
  `FunctionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '功能名称',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能信息';

/*Data for the table `functionlist_copy` */

insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (79,'','4.5.1 链路管理业务类',1,0,0,'链路管理业务类',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (87,'0x1006','4.5.1.6 主链路连接保持应答消息',2,79,79,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (94,'0x9005','4.5.1.13 从链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (104,'','4.5.3.2 从链路车辆动态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (105,'','4.5.4.2 从链路平台间信息交互业务',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (106,'','4.5.6.2 从链路车辆监管业务',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (107,'','4.5.7.2 从链路车辆静态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (110,'0x9202','4.5.3.2.2 交换车辆定位信息消息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (111,'0x9203','4.5.3.2.3 车辆定位信息交换补发消息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (112,'0x9204','4.5.3.2.4 交换车辆静态信息消息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (113,'0x9205','4.5.3.2.5 启动车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (114,'0x9206','4.5.3.2.6 结束车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (118,'0x920A','4.5.3.2.10 上报车辆驾驶员身份识别信息请求',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (119,'0x920B','4.5.3.2.11 上报车辆电子运单请求',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (121,'0x9301','4.5.4.2.2 平台查岗请求消息',2,105,105,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (122,'0x9302','4.5.4.2.3 下发平台间报文请求消息',2,105,105,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (123,'','4.5.5.2 从链路车辆报警信息业务',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (125,'0x9401','4.5.5.2.2 报警督办请求消息',2,123,123,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (126,'0x9402','4.5.5.2.3 报警预警消息',2,123,123,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (127,'0x9403','4.5.5.2.4 实时交换报警信息',2,123,123,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (129,'0x9501','4.5.6.2.2 车辆单向监听请求消息',2,106,106,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (131,'0x9503','4.5.6.2.4 下发车辆报文请求',2,106,106,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (132,'0x9504','4.5.6.2.5 上报车辆行驶记录请求',2,106,106,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (133,'0x9505','4.5.6.2.6 车辆应急接入监管平台请求消息',2,106,106,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (135,'0x9601','4.5.7.2.2 补报车辆静态信息请求',2,107,107,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (167,'','AS2',2,153,153,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (175,'','1.终端管理',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (176,'','2.位置报警',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (177,'','3.信息',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (178,'','4.电话',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (179,'','5.车辆控制',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (180,'','6.车辆管理',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (181,'','7.信息采集',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (182,'','8.多媒体',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (183,'','9.通用数据传输',1,0,0,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (186,'0x0100','1.1终端注册',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (187,'0x0003','1.2终端注销',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (188,'0x0102','1.3终端鉴权',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (189,'0x0002','1.4终端心跳',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (190,'0x8103','1.5设置终端参数',2,175,175,NULL,2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (191,'0x8104','1.6查询终端参数',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (192,'0x8105','1.7终端控制',2,175,175,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (193,'0x0200','2.1位置及报警',2,176,176,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (194,'0x8201','2.2位置信息查询',2,176,176,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (195,'0x8202','2.3临时位置跟踪控制',2,176,176,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (196,'0x8300','3.1文本信息下发',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (197,'0x8301','3.2事件设置',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (198,'0x0301','3.3事件报告',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (199,'0x8302','3.4提问下发',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (200,'0x8303','3.5信息点播菜单设置',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (201,'0x0303','3.6信息点播/取消',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (202,'0x8304','3.7信息服务',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (203,'0x8400','4.1电话回拨',2,178,178,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (204,'0x8401','4.2设置电话本',2,178,178,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (205,'0x8500','5.1车辆控制',2,179,179,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (206,'0x8600','6.1设置圆形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (207,'0x8601','6.2删除圆形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (208,'0x8602','6.3设置矩形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (209,'0x8603','6.4删除矩形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (210,'0x8604','6.5设置多边形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (211,'0x8605','6.6删除多边形区域',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (212,'0x8606','6.7设置路线',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (213,'0x8607','6.8删除路线',2,180,180,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (214,'0x8700','7.1行驶记录仪数据采集命令',2,181,181,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (216,'0x0701','7.2电子运单上报',2,181,181,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (217,'0x0702','7.3驾驶员身份信息采集上报',2,181,181,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (218,'0x0800','8.1多媒体事件信息上传',2,182,182,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (220,'0x8801','8.2摄像头立即拍摄命令',2,182,182,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (221,'0x8802','8.3存储多媒体数据检索',2,182,182,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (222,'0x8803','8.4存储多媒体数据上传',2,182,182,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (223,'0x8804','8.5录音开始命令',2,182,182,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (224,'0x8900','9.1数据下行透传',2,183,183,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (225,'0x0900','9.2数据上行透传',2,183,183,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (231,'0x0302','3.8提问应答',2,177,177,'',2,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (232,'','4.5.1 链路注销',1,0,0,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (233,'0x1003','注销主从链路',2,232,232,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (234,'0x9202','4.5.3.1.3 实时上传车辆定位信息',2,104,104,'',1,0);
insert  into `functionlist_copy`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (235,'0x9203','4.5.3.1.4 车辆定位信息自动补报',2,104,104,'',1,0);

/*Table structure for table `functionlist_copy1` */

DROP TABLE IF EXISTS `functionlist_copy1`;

CREATE TABLE `functionlist_copy1` (
  `FunctionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '功能名称',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2322 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能信息';

/*Data for the table `functionlist_copy1` */

insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (79,'','4.5.1 链路管理业务类',1,0,0,'链路管理业务类',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (81,'0x1001','4.5.1.1 主链路登录请求消息',2,79,79,'主链路登录请求',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (82,'0x9001','4.5.1.9 从链路连接请求消息',2,79,79,'从链路连接请求消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (87,'0x1005','4.5.1.5 主链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (94,'0x9005','4.5.1.13 从链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (101,'','4.5.3.1 主链路动态信息交换消息',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (104,'','4.5.3.2 从链路车辆动态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (105,'','4.5.4.2 从链路平台间信息交互业务',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (106,'','4.5.5.1 主链路报警信息交互消息',1,0,0,'主链路报警信息交互消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (110,'0x9205','4.5.3.2.5 启动车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (111,'0x9204','4.5.3.2.4 交换车辆静态信息消息',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (112,'0x9202','4.5.3.2.2 交换车辆定位信息消息',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (113,'0x9203','4.5.3.2.3 车辆定位信息交换补发消息',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (114,'0x9206','4.5.3.2.6 结束车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (118,'0x920A','4.5.3.2.10 上报车辆驾驶员身份识别信息请求',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (119,'0x920B','4.5.3.2.11 上报车辆电子运单请求',2,104,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (121,'0x9301','4.5.4.2.2 平台查岗请求消息',2,105,105,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (122,'0x9302','4.5.4.2.3 下发平台间报文请求消息',2,105,105,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (123,'','4.5.5.2 从链路车辆报警信息业务',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (125,'0x9401','4.5.5.2.2 报警督办请求消息',2,123,123,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (126,'0x9402','4.5.5.2.3 报警预警消息',2,123,123,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (127,'0x9403','4.5.5.2.4 实时交换报警信息',2,123,123,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (129,'0x9501','4.5.6.2.2 车辆单向监听请求消息',2,1061,106,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (131,'0x9503','4.5.6.2.4 下发车辆报文请求',2,1061,106,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (132,'0x9504','4.5.6.2.5 上报车辆行驶记录请求',2,1061,106,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (133,'0x9505','4.5.6.2.6 车辆应急接入监管平台请求消息',2,1061,106,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (135,'0x9601','4.5.7.2.2 补报车辆静态信息请求',2,1071,107,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (167,'','AS2',2,153,153,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (175,'','1.终端管理',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (176,'','2.位置报警',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (177,'','3.信息',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (178,'','4.电话',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (179,'','5.车辆控制',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (180,'','6.车辆管理',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (181,'','7.信息采集',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (182,'','8.多媒体',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (183,'','9.通用数据传输',1,0,0,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (186,'0x0100','1.1终端注册',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (187,'0x0003','1.2终端注销',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (188,'0x0102','1.3终端鉴权',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (189,'0x0002','1.4终端心跳',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (190,'0x8103','1.5设置终端参数',2,175,175,NULL,2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (191,'0x8104','1.6查询终端参数',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (192,'0x8105','1.7终端控制',2,175,175,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (193,'0x0200','2.1位置及报警',2,176,176,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (194,'0x8201','2.2位置信息查询',2,176,176,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (195,'0x8202','2.3临时位置跟踪控制',2,176,176,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (196,'0x8300','3.1文本信息下发',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (197,'0x8301','3.2事件设置',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (198,'0x0301','3.3事件报告',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (199,'0x8302','3.4提问下发',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (200,'0x8303','3.5信息点播菜单设置',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (201,'0x0303','3.6信息点播/取消',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (202,'0x8304','3.7信息服务',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (203,'0x8400','4.1电话回拨',2,178,178,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (204,'0x8401','4.2设置电话本',2,178,178,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (205,'0x8500','5.1车辆控制',2,179,179,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (206,'0x8600','6.1设置圆形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (207,'0x8601','6.2删除圆形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (208,'0x8602','6.3设置矩形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (209,'0x8603','6.4删除矩形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (210,'0x8604','6.5设置多边形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (211,'0x8605','6.6删除多边形区域',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (212,'0x8606','6.7设置路线',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (213,'0x8607','6.8删除路线',2,180,180,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (214,'0x8700','7.1行驶记录仪数据采集命令',2,181,181,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (216,'0x0701','7.2电子运单上报',2,181,181,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (217,'0x0702','7.3驾驶员身份信息采集上报',2,181,181,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (218,'0x0800','8.1多媒体事件信息上传',2,182,182,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (220,'0x8801','8.2摄像头立即拍摄命令',2,182,182,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (221,'0x8802','8.3存储多媒体数据检索',2,182,182,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (222,'0x8803','8.4存储多媒体数据上传',2,182,182,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (223,'0x8804','8.5录音开始命令',2,182,182,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (224,'0x8900','9.1数据下行透传',2,183,183,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (225,'0x0900','9.2数据上行透传',2,183,183,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (231,'0x0302','3.8提问应答',2,177,177,'',2,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (233,'0x1003','注销主从链路',2,2321,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (242,'0x9502','4.5.6.2.3 车辆拍照请求消息',2,1061,106,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (244,'0x1201','4.5.3.1.2 上传车辆注册信息消息',2,101,101,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (245,'0x1202','4.5.3.1.3 实时上传车辆定位信息',2,101,104,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (247,'0x1203','4.5.3.1.4 车辆定位信息自动补报',2,101,237,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (248,'0x1207','4.5.3.1.7 申请交换指定车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (249,'0x1209','4.5.3.1.9 补发车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (250,'0x1208','4.5.3.1.8 取消交换指定车辆定位信息请求消息',2,101,237,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (251,'0x120C','4.5.3.1.12 主动上报驾驶员身份信息消息',2,101,237,'4.5.3.1.12 主动上报驾驶员身份信息消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (252,'0x120D','4.5.3.1.13 主动上报车辆电子运单信息消息',2,101,237,'主动上报车辆电子运单信息消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (256,'0x1402','4.5.5.1.3 上报报警信息消息',2,106,254,'上报报警信息消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (258,'0x1403','4.5.5.1.4 主动上报报警处理结果信息消息',2,106,254,'主动上报报警处理结果信息消息',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (1061,'','4.5.6.2 从链路车辆监管业务',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (1071,'','4.5.7.2 从链路车辆静态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist_copy1`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (2321,'','4.5.1 链路注销',1,0,0,'',1,0);

/*Table structure for table `functionlistbak` */

DROP TABLE IF EXISTS `functionlistbak`;

CREATE TABLE `functionlistbak` (
  `FunctionID` int(11) NOT NULL COMMENT '功能项ID',
  `MessageID` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '功能项名称',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能编码',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能编号',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能项管理';

/*Data for the table `functionlistbak` */

/*Table structure for table `gbversion` */

DROP TABLE IF EXISTS `gbversion`;

CREATE TABLE `gbversion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Name` varchar(64) COLLATE gbk_bin NOT NULL COMMENT '版本名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='国标版本信息';

/*Data for the table `gbversion` */

insert  into `gbversion`(`ID`,`Name`) values (1,'809-2011');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `LoginID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `LoginName` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '登录账号',
  `Name` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '姓名',
  `PassWord` varchar(50) COLLATE gbk_bin NOT NULL COMMENT '密码（MD5加密）',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效1=有效。0=无效',
  `IfAdmin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`LoginID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='登录用户';

/*Data for the table `login` */

insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (2,'Admin','超级管理员','21232F297A57A5A743894A0E4A801FC3',1,1,'超级管理员');
insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (3,'yuguibin','余贵滨','18A2FEEBCDBFCD8BBB415E00C1EEB167',1,0,'');

/*Table structure for table `messagecode` */

DROP TABLE IF EXISTS `messagecode`;

CREATE TABLE `messagecode` (
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `MsgName` varchar(200) COLLATE gbk_bin NOT NULL COMMENT '消息描叙',
  `MessageType` int(11) NOT NULL COMMENT '1=终端消息 2=平台消息',
  `FunctionID` int(11) DEFAULT NULL COMMENT '消息分类(上级消息)',
  `BusinessNote` varchar(50) COLLATE gbk_bin DEFAULT NULL COMMENT '业务类型标识',
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='消息信息';

/*Data for the table `messagecode` */

insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0002','终端心跳',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0003','终端注销',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0100','终端注册',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0102','终端鉴权',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0200','位置信息汇报',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0301','事件报告',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0302','提问应答',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0303','信息点播/取消',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0701','电子运单上报',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0702','驾驶员身份信息采集上报',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0800','多媒体事件信息上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0801','多媒体数据上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0900','数据上行透传',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0901','数据压缩上报',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0A00','终端RSA公钥',2,184,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1001','主链路登录请求消息',0,2,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1002','4.5.1.9 从链路连接请求',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1003','主链路注销请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1005','主链路连接保持请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1006','主链路连接保持应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1007','主链路断开通知消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1008','下级平台主动关闭主从链路通知消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1201','上传车辆注册信息消息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1202','实时上传车辆定位信息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1203','车辆定位信息自动补报请求消息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1205','启动车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1206','结束车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1207','申请交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1208','取消交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1209','补发车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120A','上报驾驶员身份识别信息应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120B','上报车辆电子运单应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1300','数据体描述',2,145,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1301','平台查岗应答',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1302','下发平台间报文应答',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1400','数据体',2,148,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1401','报警督办应答',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1402','上报报警信息',2,148,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1403','4.5.6.1.5',2,254,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1500','数据体',2,151,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1501','车辆单向监听应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1502','车辆拍照应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1503','下发车辆报文应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1504','上报车辆行驶记录应答消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1505','车辆应急接入监管平台应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1600','数据体',2,153,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x402','上报报警信息消息',2,254,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8103','查询终端参数',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8104','查询终端参数',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8105','终端控制',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8201','位置信息查询',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8202','临时位置跟踪控制',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8300','文本信息下发',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8301','事件设置',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8302','提问下发',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8303','信息点播菜单设置',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8304','信息服务',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8400','电话回拨',2,178,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8401','设置电话本',2,178,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8500','车辆控制',2,179,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8600','设置圆形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8601','删除圆形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8602','设置矩形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8603','删除矩形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8604','设置多边形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8605','删除多边形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8606','设置路线',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8607','删除路线',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8700','行驶记录仪数据采集命令',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8701','行驶记录仪参数下传命令',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8801','摄像头立即拍摄命令',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8802','存储多媒体数据检索',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8803','存储多媒体数据上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8804','录音开始命令',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8900','数据下行透传',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8A00','平台RSA公钥',2,184,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9003','4.5.1.11 从链路注销',2,2321,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9004','从链路注销应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9005','从链路连接保持请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9006','从链路连接保持应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9204','交换车辆静态信息消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9205','启动车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9206','结束车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9209','补发车辆定位信息应答消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920A','上报车辆驾驶员身份识别信息请求',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920B','上报车辆电子运单请求',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9301','平台查岗请求消息',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9302','下发平台间报文请求消息',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9401','报警督办请求消息',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9403','实时交换报警信息',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9501','车辆单向监听请求消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9502','4.5.6.2.3 车辆拍照请',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9503','下发车辆报文请求',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9504','上报车辆行驶记录请求',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9505','车辆应急接入监管平台请求消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9601','补报车辆静态信息请求',2,107,NULL);

/*Table structure for table `messagecode_copy` */

DROP TABLE IF EXISTS `messagecode_copy`;

CREATE TABLE `messagecode_copy` (
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `MsgName` varchar(200) COLLATE gbk_bin NOT NULL COMMENT '消息描叙',
  `MessageType` int(11) NOT NULL COMMENT '1=终端消息 2=平台消息',
  `FunctionID` int(11) DEFAULT NULL COMMENT '消息分类(上级消息)',
  `BusinessNote` varchar(50) COLLATE gbk_bin DEFAULT NULL COMMENT '业务类型标识',
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='消息信息';

/*Data for the table `messagecode_copy` */

insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0002','终端心跳',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0003','终端注销',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0100','终端注册',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0102','终端鉴权',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0200','位置信息汇报',2,176,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0301','事件报告',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0302','提问应答',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0303','信息点播/取消',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0701','电子运单上报',2,181,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0702','驾驶员身份信息采集上报',2,181,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0800','多媒体事件信息上传',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0801','多媒体数据上传',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0900','数据上行透传',2,183,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0901','数据压缩上报',2,183,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0A00','终端RSA公钥',2,184,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1001','主链路登录请求消息',0,2,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1002','4.5.1.9 从链路连接请求',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1003','主链路注销请求消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1005','主链路连接保持请求消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1006','主链路连接保持应答消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1007','主链路断开通知消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1008','下级平台主动关闭主从链路通知消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1201','上传车辆注册信息消息',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1202','实时上传车辆定位信息',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1203','车辆定位信息自动补报请求消息',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1205','启动车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1206','结束车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1207','申请交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1208','取消交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1209','补发车辆定位信息请求',2,136,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120A','上报驾驶员身份识别信息应答',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120B','上报车辆电子运单应答',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1300','数据体描述',2,145,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1301','平台查岗应答',2,105,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1302','下发平台间报文应答',2,105,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1400','数据体',2,148,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1401','报警督办应答',2,123,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1402','上报报警信息',2,148,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1403','4.5.6.1.5',2,254,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1500','数据体',2,151,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1501','车辆单向监听应答',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1502','车辆拍照应答',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1503','下发车辆报文应答',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1504','上报车辆行驶记录应答消息',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1505','车辆应急接入监管平台应答',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1600','数据体',2,153,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x402','上报报警信息消息',2,254,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8103','查询终端参数',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8104','查询终端参数',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8105','终端控制',2,175,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8201','位置信息查询',2,176,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8202','临时位置跟踪控制',2,176,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8300','文本信息下发',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8301','事件设置',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8302','提问下发',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8303','信息点播菜单设置',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8304','信息服务',2,177,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8400','电话回拨',2,178,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8401','设置电话本',2,178,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8500','车辆控制',2,179,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8600','设置圆形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8601','删除圆形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8602','设置矩形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8603','删除矩形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8604','设置多边形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8605','删除多边形区域',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8606','设置路线',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8607','删除路线',2,180,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8700','行驶记录仪数据采集命令',2,181,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8701','行驶记录仪参数下传命令',2,181,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8801','摄像头立即拍摄命令',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8802','存储多媒体数据检索',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8803','存储多媒体数据上传',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8804','录音开始命令',2,182,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8900','数据下行透传',2,183,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8A00','平台RSA公钥',2,184,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9004','从链路注销应答消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9005','从链路连接保持请求消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9006','从链路连接保持应答消息',2,79,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9204','交换车辆静态信息消息',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9205','启动车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9206','结束车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920A','上报车辆驾驶员身份识别信息请求',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920B','上报车辆电子运单请求',2,104,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9301','平台查岗请求消息',2,105,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9302','下发平台间报文请求消息',2,105,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9401','报警督办请求消息',2,123,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9403','实时交换报警信息',2,123,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9501','车辆单向监听请求消息',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9502','4.5.6.2.3 车辆拍照请',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9503','下发车辆报文请求',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9504','上报车辆行驶记录请求',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9505','车辆应急接入监管平台请求消息',2,106,NULL);
insert  into `messagecode_copy`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9601','补报车辆静态信息请求',2,107,NULL);

/*Table structure for table `operatelog` */

DROP TABLE IF EXISTS `operatelog`;

CREATE TABLE `operatelog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号：自增',
  `Operator` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '操作人员',
  `operateDate` datetime NOT NULL COMMENT '发生时间',
  `Flag` int(11) NOT NULL COMMENT '1=增加数据、2=删除数据3=编辑数据',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '操作说明',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='操作日志';

/*Data for the table `operatelog` */

insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (1,'test','2011-11-11 09:51:48',1,'增加厂商信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (2,'test','2011-11-11 10:15:59',1,'添加批次和目标信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (3,'test','2011-11-11 16:45:51',1,'增加厂商信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (4,'test','2011-11-11 16:51:35',1,'添加批次和目标信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (5,'test','2014-04-30 15:56:37',1,'增加厂商信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (6,'test','2014-04-30 15:57:18',1,'添加批次和目标信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (7,'test','2014-04-30 15:58:00',1,'添加批次和目标信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (8,'test','2014-04-30 15:58:06',1,'添加批次和目标信息');
insert  into `operatelog`(`ID`,`Operator`,`operateDate`,`Flag`,`Note`) values (9,'test','2014-04-30 15:59:38',1,'添加批次和目标信息');

/*Table structure for table `processdefin` */

DROP TABLE IF EXISTS `processdefin`;

CREATE TABLE `processdefin` (
  `progressid` varchar(32) NOT NULL,
  `progressname` varchar(50) DEFAULT NULL,
  `ordernum` int(11) DEFAULT NULL,
  `note` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`progressid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `processdefin` */

insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('239465c4-e9b3-4f7a-b9cf-f2878852','平台之间查岗业务',6,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('24aaa2ea-5959-483e-9413-a391ebed','车辆跨域交换业务（企业平台可选）',5,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('36028b7e-c0ba-44d1-bf03-fdb08579','上报车辆电子运单',7,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('514a4453-3dca-44b5-85e4-03d7feae','补发车辆定位信息（企业平台可选）',2,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('5285b4ce-63b2-46f4-b956-195aba13','车辆动态信息上传',3,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('70ef576c-9108-46cb-8d29-74373cce','交换指定车辆定位信息（企业平台可选）',9,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('b683ba76-f8d5-4cbe-9a46-16a71b23','下发平台间报文业务',10,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('ddf9ec3a-cfb5-4d8a-8bae-ddd32f3b','车辆监管相关业务',4,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('e271ea6a-03c4-4b7a-9c7f-e76975b2','平台链路管理业务',1,'');
insert  into `processdefin`(`progressid`,`progressname`,`ordernum`,`note`) values ('e67af467-538a-4a7a-9189-0b62ce11','上报驾驶员身份信息',8,'');

/*Table structure for table `progresscase` */

DROP TABLE IF EXISTS `progresscase`;

CREATE TABLE `progresscase` (
  `caseid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL,
  `casename` varchar(200) DEFAULT NULL,
  `groupnum` int(11) DEFAULT NULL COMMENT '界面开发时，用下拉框输入分组数，默认为0，若一个流程对应的用例有多个分组，抽取用例时，每个组抽取一个执行，执行先后顺序按照分组编号为准',
  `note` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`caseid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='流程检测用例表  流程检测用包含检测步骤';

/*Data for the table `progresscase` */

insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('1eff41f3-0618-46d1-850a-0417cd5d','514a4453-3dca-44b5-85e4-03d7feae','补发车辆定位信息同意补发用例',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('2ba477ae-d385-41e0-ad16-83f1db49','b683ba76-f8d5-4cbe-9a46-16a71b23','下发平台间报文正常用例',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('42800c1a-5f41-42a8-90e3-9051d52a','5285b4ce-63b2-46f4-b956-195aba13','车辆动态信息上传正常用例',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('4885cc61-44da-4aad-9481-13a661ae','36028b7e-c0ba-44d1-bf03-fdb08579','上报车辆电子运单正常用例',7,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('66ca2df5-b4ff-4339-a6e8-b866b10a','239465c4-e9b3-4f7a-b9cf-f2878852','查岗正常流程',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('698c9498-0323-4e96-813d-1924df97','70ef576c-9108-46cb-8d29-74373cce','交换车辆定位信息正常用例',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('b3ffc723-adfd-44a3-8369-7d55549b','e271ea6a-03c4-4b7a-9c7f-e76975b2','链路管理正常流程',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('dc466d36-e3e7-4794-b2dc-4a83d870','24aaa2ea-5959-483e-9413-a391ebed','车辆跨域交换业务正常流程',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('df52b775-b10c-4cc5-933a-7d72b95d','ddf9ec3a-cfb5-4d8a-8bae-ddd32f3b','车辆监管相关业务正常用例',0,'');
insert  into `progresscase`(`caseid`,`progressid`,`casename`,`groupnum`,`note`) values ('fdb1ecbf-7c13-4921-a7c5-146da81e','e67af467-538a-4a7a-9189-0b62ce11','上报驾驶员身份信息用例',0,'');

/*Table structure for table `progresstescasedetail` */

DROP TABLE IF EXISTS `progresstescasedetail`;

CREATE TABLE `progresstescasedetail` (
  `casedetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL COMMENT '流程id（不做关联 只记录一下 方便查询）',
  `progressdetailid` varchar(32) DEFAULT NULL,
  `caseid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`casedetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstescasedetail` */

/*Table structure for table `progresstesdetail` */

DROP TABLE IF EXISTS `progresstesdetail`;

CREATE TABLE `progresstesdetail` (
  `progressdetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`progressdetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstesdetail` */

/*Table structure for table `progresstestlog` */

DROP TABLE IF EXISTS `progresstestlog`;

CREATE TABLE `progresstestlog` (
  `id` varchar(32) NOT NULL,
  `targetid` varchar(30) DEFAULT NULL COMMENT '测试目标编号',
  `reportid` varchar(32) DEFAULT NULL,
  `flag` int(11) NOT NULL COMMENT '0=系统日志 1=上行 2=下行',
  `messageid` varchar(20) DEFAULT NULL,
  `eventdate` datetime NOT NULL COMMENT '日期',
  `serialnumber` int(11) NOT NULL COMMENT '消息流水号',
  `resultflag` int(11) DEFAULT NULL COMMENT '0=正常 1=错误',
  `oraginmsg` longblob COMMENT '解析前的原始数据',
  `outmsg` varchar(4000) DEFAULT NULL COMMENT '解析后数据',
  `mediatype` int(11) DEFAULT NULL COMMENT '0=无附加数据 1=图片 2=多媒体',
  `mediadata` longblob COMMENT '存储多媒体图片数据',
  `note` varchar(2000) DEFAULT NULL COMMENT '备注',
  `isalarm` int(11) NOT NULL DEFAULT '0' COMMENT '0=无报警 1=有报警',
  `alarmdescription` varchar(2000) DEFAULT NULL COMMENT '报警描叙',
  `xmlmessage` varchar(4000) DEFAULT NULL,
  `stepid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='流程检测日志';

/*Data for the table `progresstestlog` */

/*Table structure for table `progresstestreport` */

DROP TABLE IF EXISTS `progresstestreport`;

CREATE TABLE `progresstestreport` (
  `reportid` varchar(32) NOT NULL,
  `targetid` varchar(30) DEFAULT NULL COMMENT '测试目标编号',
  `environmentid` int(11) DEFAULT NULL,
  `testcount` int(11) NOT NULL COMMENT '测试次数',
  `timeout` int(11) NOT NULL DEFAULT '0' COMMENT '测试超时自动判断测试不通过',
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testprocess` varchar(200) NOT NULL COMMENT '测试过程描叙',
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstestreport` */

/*Table structure for table `progressteststepdetail` */

DROP TABLE IF EXISTS `progressteststepdetail`;

CREATE TABLE `progressteststepdetail` (
  `stepdetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL COMMENT '流程id（不做关联 只记录一下 方便查询）',
  `caseid` varchar(32) DEFAULT NULL COMMENT '用例id（不做关联 只记录一下 方便查询）',
  `casedetailid` varchar(32) DEFAULT NULL,
  `stepid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `testdate` datetime DEFAULT NULL,
  `resultid` int(11) DEFAULT NULL COMMENT '检测状态',
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `tester` varchar(16) DEFAULT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`stepdetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='用例步骤执行明细';

/*Data for the table `progressteststepdetail` */

/*Table structure for table `resultinfor` */

DROP TABLE IF EXISTS `resultinfor`;

CREATE TABLE `resultinfor` (
  `ResultID` int(11) NOT NULL COMMENT 'ID',
  `Name` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '状态描叙',
  PRIMARY KEY (`ResultID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标状态';

/*Data for the table `resultinfor` */

insert  into `resultinfor`(`ResultID`,`Name`) values (0,'未知');
insert  into `resultinfor`(`ResultID`,`Name`) values (1,'待测');
insert  into `resultinfor`(`ResultID`,`Name`) values (2,'测试中');
insert  into `resultinfor`(`ResultID`,`Name`) values (3,'待审核');
insert  into `resultinfor`(`ResultID`,`Name`) values (4,'测试通过');
insert  into `resultinfor`(`ResultID`,`Name`) values (5,'测试不通过');

/*Table structure for table `serialnumber` */

DROP TABLE IF EXISTS `serialnumber`;

CREATE TABLE `serialnumber` (
  `SheetType` int(11) NOT NULL COMMENT '序号类型',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '名称描叙',
  `PreFix` char(2) COLLATE gbk_bin DEFAULT NULL COMMENT '前缀',
  `ResetFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=按天重置 2=按月重置',
  `ResetDate` date DEFAULT NULL,
  `Serialnumber` int(11) NOT NULL DEFAULT '0' COMMENT '序列号',
  `serialcount` int(11) NOT NULL COMMENT '长度4:则输出格式:0001',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`SheetType`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='获取批次号使用';

/*Data for the table `serialnumber` */

insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (1,'检测目标批次号（终端）','',1,'2011-06-23',4,4,NULL);
insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (2,'检测目标批次号（企业平台）','',1,'2014-04-30',2,4,NULL);
insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (3,'检测目标批次号（政府平台）','',1,'2011-06-14',0,4,NULL);

/*Table structure for table `steplib` */

DROP TABLE IF EXISTS `steplib`;

CREATE TABLE `steplib` (
  `stepid` varchar(32) NOT NULL,
  `typeid` int(11) DEFAULT NULL,
  `caseid` varchar(32) DEFAULT NULL,
  `stepname` varchar(200) DEFAULT NULL,
  `ordernum` int(11) DEFAULT NULL,
  `stepparam` varchar(2000) DEFAULT NULL,
  `resultmsg` varchar(2000) DEFAULT NULL,
  `procedureparam` varchar(2000) DEFAULT NULL,
  `autojudge` int(11) DEFAULT NULL COMMENT '0 自动判定\r\n            1 手动判定\r\n            2 自动+手动',
  `note` varchar(2000) DEFAULT NULL,
  `overtime` int(11) DEFAULT NULL,
  `messageid` varchar(100) DEFAULT NULL,
  `sendmsg` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`stepid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='步骤库';

/*Data for the table `steplib` */

insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('03c2fd81-a1a6-4b11-b46f-1e359e5b',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收到结束监管消息应答',14,'','','pro_progress4_case1_check6_gcy_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('03f1b477-e4c8-4bf5-a4c5-c5aef2a4',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收到下发报文指令应答',7,'','','pro_progress4_case1_check3_gcy_cmp',0,'',180,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZUDAAAD8gEAAAAMz8K3orGozsTQxc+i');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('045af75c-1aa5-4e73-9d6c-537b78c1',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第5条车辆报警信息',21,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]23\r\n[BYTE|1]01\r\n[WORD]1\r\n[TIME_T|8]2011-12-12 12:12:12\r\n[DWORD]8\r\n[STRING|8]超速报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('081308fb-51e1-4d41-bc79-367c88a4',5,'df52b775-b10c-4cc5-933a-7d72b95d','是否收到至少1条多媒体数据上传消息',4,'','','',4,'',1,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('0862ee74-8a9d-4679-8a84-8b20e842',4,'b3ffc723-adfd-44a3-8369-7d55549b','判断下级平台主从链路是否正常注销',6,'','','pro_progress0_case1_check3_zb_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('08f082dd-e173-44bb-9757-21f62420',1,'42800c1a-5f41-42a8-90e3-9051d52a','下发补报车辆静态信息消息',5,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38401\r\n[DWORD]0','','',4,'',0,'0x9601','suJBMTIzNDUAAAAAAAAAAAAAAAAAApYBAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('0f7809dd-5640-4ed2-8205-1afa47f2',1,'df52b775-b10c-4cc5-933a-7d72b95d','下发车辆拍照请求',3,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]1\r\n[BYTE|1]1','','',4,'1.协议中没有\r\n2.测试时需要修改部分参数\r\n[STRING|21]京B12345\r\n[BYTE|1]1\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]实际镜头ID\r\n[BYTE|1]照片的SIZE',0,'0x9502','suJBMTIzNDUAAAAAAAAAAAAAAAAAApUCAAAAAgEB');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('1170f8a8-3f80-43b8-87e4-f466daa5',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送结束信息交换消息',23,'[STRING|21]京B12345\r\n[BYTE|1]1\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]00','','',4,'',0,'0x9206','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZIGAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('1201190e-7774-4633-939e-e23a573a',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔9',20,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('136be4d2-9a7b-4dfb-98b1-4b770ef0',3,'b3ffc723-adfd-44a3-8369-7d55549b','提示下级平台建立主链路',1,'请下级平台建立主链路','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('158a64f8-bb64-4eab-b860-35dfd98a',1,'df52b775-b10c-4cc5-933a-7d72b95d','下发紧急监管消息',10,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38149\r\n[DWORD]145\r\n[BYTE|10]30313233343536373839\r\n[STRING|20]CNNET\r\n[STRING|49]13888888888\r\n[STRING|22]123456789\r\n[STRING|32]192.168.1.114\r\n[WORD]8080\r\n[WORD]8081\r\n[TIME_T|8]2012-12-12 12:12:12','','',4,'实际测试过程中需要修改参数的值\r\n[BYTE|10]车载终端连接监管平台权鉴码\r\n[STRING|20]拨号点名称\r\n[STRING|49]拨号用户名\r\n[STRING|22]拨号密码\r\n[STRING|32]服务器IP\r\n[WORD]TCP端口\r\n[WORD]UDP端口\r\n[TIME_T|8]结束时间',0,'0x9505','suJBMTIzNDUAAAAAAAAAAAAAAAAAApUFAAAAkTAxMjM0NTY3ODlDTk5FVAAAAAAAAAAAAAAAAAAAADEzODg4ODg4ODg4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxMjM0NTY3ODkAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjExNAAAAAAAAAAAAAAAAAAAAAAAAAAfkB+RAAAAAFDIBBw=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('18b7f7b5-4c1a-43f8-b626-1d6d63c8',3,'42800c1a-5f41-42a8-90e3-9051d52a','提示被测平台发送车辆定位消息',3,'请下级平台发送车辆定位消息。','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('18bef000-2b8f-4eba-b41a-0e39ff5f',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔10',22,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('1c2e2b17-fdd1-4aff-8164-07db06a2',5,'698c9498-0323-4e96-813d-1924df97','时间间隔2',6,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('1dc55b26-6b8a-4892-8fc1-06c3eaf1',1,'df52b775-b10c-4cc5-933a-7d72b95d','下发行驶记录提取指令',8,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38148\r\n[DWORD]1\r\n[BYTE|1]1','','',4,'[STRING|21]京B12345\r\n[BYTE|1]1\r\n[WORD]38148\r\n[BYTE|1]1\r\n[BYTE|1]命令字测试时需要修改',0,'0x9504','suJBMTIzNDUAAAAAAAAAAAAAAAAAApUEAAAAAQE=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('218dd49d-7666-4efa-b1af-c419b45e',4,'b3ffc723-adfd-44a3-8369-7d55549b','判断下级平台主从链路是否正常登录',2,'','','pro_progress0_case1_check1_zb_cmp',0,'',5,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('2514e50b-e678-46c8-86de-1c005300',4,'42800c1a-5f41-42a8-90e3-9051d52a','判断是否收到下级平台发送的连接请求',10,'','','pro_progress3_case1_check4_zw_cmp',0,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIJAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('25b0192a-075a-4c92-ac1d-7d21f45b',5,'1eff41f3-0618-46d1-850a-0417cd5d','发送时间间隔3',9,'','','',4,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIDAAAAtQUBGAoUCwocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCwscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABGAoUCwwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('267642b9-aa45-424e-8a0c-1f3b685e',3,'4885cc61-44da-4aad-9481-13a661ae','提示下级平台上报电子运单',1,'请下级平台上报车辆电子运单。','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('279f3161-dc14-49d8-8750-9a36da1c',3,'1eff41f3-0618-46d1-850a-0417cd5d','提示被测下级平台发送补发车辆定位信息请求消息',1,'请下级平台发送补发车辆定位信息请求消息。','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('2c8491f7-1fa9-4ea1-b105-65800025',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔2',6,'','','',4,'',5,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('2dbc9d26-0c9d-41ef-b718-2ccfd369',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送第1条信息交换补发消息',4,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('2e0732ac-6091-4973-8ac7-ff29e4ae',5,'1eff41f3-0618-46d1-850a-0417cd5d','发送时间间隔2',7,'','','',4,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIDAAAAtQUBGAoUCwocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCwscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABGAoUCwwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('3421cafa-fdf7-4019-89cc-52f4bdef',1,'42800c1a-5f41-42a8-90e3-9051d52a','下发报警督办请求',15,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37889\r\n[DWORD]92\r\n[BYTE|1]2\r\n[WORD]8\r\n[TIME_T|8]2011-11-16 13:50:32\r\n[DWORD]32456\r\n[TIME_T|8]2011-11-17 12:00:00\r\n[BYTE|1]1\r\n[STRING|16]张王\r\n[STRING|20]15100120013\r\n[STRING|32]zhangwang@gmail.com','','',4,'',0,'0x9401','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQBAAAAXAIACAAAAABOw08oAAB+yAAAAABOxIbAAdXFzfUAAAAAAAAAAAAAAAAxNTEwMDEyMDAxMwAAAAAAAAAAAHpoYW5nd2FuZ0BnbWFpbC5jb20AAAAAAAAAAAAAAAAA');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('3589532d-851d-455c-8387-9c537f40',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔5',12,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('39571287-c61e-43e8-a3cc-c05b1bdd',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔1',4,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('3a2b61f0-478e-45c8-a2a6-7a7ee6db',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔3',8,'','','',4,'',5,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('3d303cba-80ac-43c4-98c5-cf6c4ec6',5,'42800c1a-5f41-42a8-90e3-9051d52a','等待5分钟',8,'','','',4,'',300,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIJAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('3f006cbe-d9be-46aa-9c5f-c1dd9757',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收到车辆应急监管应答',11,'','','pro_progress4_case1_check5_gcy_cmp',0,'',30,'','vqlKQjEyNDU2AAAAAAAAAAAAAAAAAZUFAAAAkTAxMjM0NTY3ODlDTk5FVAAAAAAAAAAAAAAAAAAAADEzODg4ODg4ODg4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxMjM0NTY3ODkAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjExNAAAAAAAAAAAAAAAAAAAAAAAAAAfkB+RAAAAAFDIBBw=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('458a0819-c52b-4a93-8c6d-8746b7b5',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第2次车辆定位信息',5,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|1]1\r\n[BYTE|4]0C1007DB\r\n[BYTE|3]091E10\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]10\r\n[WORD]10\r\n[DWORD]1000\r\n[WORD]120\r\n[WORD]100\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9202','suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('45ece765-ba8b-4d22-8521-2bf81edd',3,'698c9498-0323-4e96-813d-1924df97','提示下级平台发送取消指定车辆定位信息请求',15,'请下级平台发送取消指定车辆定位信息','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('45f80362-7a36-4504-93b7-5c857464',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔4',10,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('4a30de7f-2864-4efd-9924-19bcca71',1,'66ca2df5-b4ff-4339-a6e8-b866b10a','发送查岗请求',1,'[WORD]37633\r\n[DWORD]34\r\n[BYTE|1]3\r\n[BYTE|12]2\r\n[DWORD]123\r\n[DWORD]13\r\n[STRING|13]平台查岗请求','','',4,'',0,'0x9301','kwEAAAAiAwAAAAAAAAAAAAAAAgAAAHsAAAANxr3MqLLpuNrH68fzAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('507a7d62-12b5-4936-a3e5-011c532b',5,'698c9498-0323-4e96-813d-1924df97','时间间隔1',4,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('52ae2d9e-3ddb-4e9a-a6ee-a4f7a6fa',1,'698c9498-0323-4e96-813d-1924df97','发送第四条车辆动态消息',11,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('579953c6-cf9f-4637-8a12-50a5d955',3,'fdb1ecbf-7c13-4921-a7c5-146da81e','提示下级平台发送驾驶员身份信息',1,'请下级平台发送驾驶员身份信息','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('58ebc8ae-6c09-40bb-bf00-024e221b',4,'698c9498-0323-4e96-813d-1924df97','判断是否收到下级平台发送的取消车辆定位信息交换请求',16,'','','pro_progress9_case1_check2_zb_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('59796fae-1677-42c8-9da8-39ebee65',4,'66ca2df5-b4ff-4339-a6e8-b866b10a','判断是否有查岗应答',2,'','','pro_progress6_case1_check1_zw_cmp',0,'',180,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('5d0bbbd9-8734-4cb6-a993-ab0c9260',1,'698c9498-0323-4e96-813d-1924df97','发送第三条车辆动态消息',9,'[STRING|21]京B12345\r\n[STRING|1]1\r\n[WORD]37378\r\n[DWORD]36\r\n[DWORD]0\r\n[BYTE|4]150A07DB\r\n[BYTE|3]0E0F00\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]80\r\n[WORD]80\r\n[DWORD]200\r\n[WORD]0\r\n[WORD]10\r\n[DWORD]4294967295\r\n[DWORD]0','','',4,'',0,'0x9202','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('631c61d7-fd02-4a23-b1ad-1f78920f',5,'698c9498-0323-4e96-813d-1924df97','时间间隔5',12,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('64b95a76-7739-4c70-ba6a-d19073eb',4,'42800c1a-5f41-42a8-90e3-9051d52a','判定是否收到被测下级平台发送的车辆注册消息',2,'','','pro_progress3_case1_check1_zw_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('64f714a7-5f6e-4c1c-bc17-7749e99c',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送给下级平台同意补发应答',3,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37385\r\n[DWORD]1\r\n[BYTE|1]0','','',4,'',0,'0x9209','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIJAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('670edbcf-3c18-4d0b-81b5-2d967a4b',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送第2条信息交换补发消息',6,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('690b7c8a-6fd8-4979-beac-b8cfbd54',4,'dc466d36-e3e7-4794-b2dc-4a83d870','判断是否收到启动信息交换应答',2,'','','pro_progress5_case1_check1_gcy_cmp',0,'',30,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAQAAAAEA');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('6cde04ad-a7a9-4c4f-9aeb-b3c78eb4',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第4条车辆报警信息',19,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]23\r\n[BYTE|1]01\r\n[WORD]1\r\n[TIME_T|8]2011-12-12 12:12:12\r\n[DWORD]8\r\n[STRING|8]超速报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('71cf0586-320d-4e79-988c-37f060c8',4,'1eff41f3-0618-46d1-850a-0417cd5d','判定是否收到下级平台发送的补发车辆定位信息请求消息',2,'','','pro_progress2_case1_check1_zw_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('73d3f772-c79e-47d3-a222-257ae6cd',1,'df52b775-b10c-4cc5-933a-7d72b95d','下发报文指令',6,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38147\r\n[DWORD]21\r\n[DWORD]1010\r\n[BYTE|1]1\r\n[DWORD]12\r\n[STRING|12]下发报文信息','','',4,'消息ID序号\r\n[DWORD]消息ID序号',0,'0x9503','suJBMTIzNDUAAAAAAAAAAAAAAAAAApUDAAAAFQAAA/IBAAAADM/Ct6KxqM7E0MXPog==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('7738f198-dcb4-4574-b3a4-0af6a4b0',4,'4885cc61-44da-4aad-9481-13a661ae','判断是否收到下级平台发送的电子运单消息',2,'','','pro_progress7_case1_check1_zw_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('79dda06b-adab-4039-9dbb-4985912e',5,'698c9498-0323-4e96-813d-1924df97','时间间隔4',10,'','','',4,'',5,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('7da7a36c-e802-4acc-9f4b-0f34202d',4,'42800c1a-5f41-42a8-90e3-9051d52a','判定至少收到3条下级平台发送的车辆定位消息',4,'','','pro_progress3_case1_check2_zw_cmp',0,'',210,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8048ed38-94c0-40fd-8ef8-92d3d445',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第4次车辆定位信息',9,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|1]1\r\n[BYTE|4]0C1007DB\r\n[BYTE|3]091E10\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]10\r\n[WORD]10\r\n[DWORD]1000\r\n[WORD]120\r\n[WORD]100\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9202','suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('82452617-e24f-425e-a515-6f9952f9',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送第5条信息交换补发消息',12,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8530d4d0-079d-43a8-961e-cc6966f2',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第3条车辆报警信息',17,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]23\r\n[BYTE|1]01\r\n[WORD]1\r\n[TIME_T|8]2011-12-12 12:12:12\r\n[DWORD]8\r\n[STRING|8]超速报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8ba3330b-bd23-4176-8adc-c39c001a',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第1条车辆报警信息',13,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]23\r\n[BYTE|1]01\r\n[WORD]1\r\n[TIME_T|8]2011-12-12 12:12:12\r\n[DWORD]8\r\n[STRING|8]超速报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8bbf86d1-1b3b-4cf9-9a45-f77c45cf',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送启动信息交换消息',1,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[DWORD]1\r\n[BYTE|1]00','','',4,'',0,'0x9205','suJBMTIzNDUAAAAAAAAAAAAAAAAAAgAAAAEA');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8d32587b-13c2-489f-8e09-15a704ed',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送第4条信息交换补发消息',10,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8dcc9f88-55b2-419a-a936-8cefd572',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第2条车辆报警信息',15,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]23\r\n[BYTE|1]01\r\n[WORD]1\r\n[TIME_T|8]2011-12-12 12:12:12\r\n[DWORD]8\r\n[STRING|8]超速报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8e537f3e-cfe8-4753-9c57-6189043e',4,'b3ffc723-adfd-44a3-8369-7d55549b','判断是否收到上级平台发送的主链路心跳包及下级平台发送的从链路心跳包',4,'','','pro_progress0_case1_check2_zb_cmp',0,'',180,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('8f94cccd-537a-4885-8593-c4ca9f2e',3,'698c9498-0323-4e96-813d-1924df97','提示下级平台发送交换消息',1,'请下级平台发送交换消息','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('91538b25-bb65-4256-adf2-d01a627f',4,'dc466d36-e3e7-4794-b2dc-4a83d870','判断是否收到结束信息交换应答',24,'','','pro_progress5_case1_check2_gcy_cmp',0,'',30,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZIGAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('94dfb582-6b22-4de2-8c15-ae2be279',1,'698c9498-0323-4e96-813d-1924df97','发送第一条车辆动态消息',5,'[STRING|21]京B12345\r\n[STRING|1]1\r\n[WORD]37378\r\n[DWORD]36\r\n[DWORD]0\r\n[BYTE|4]150A07DB\r\n[BYTE|3]0E0F00\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]80\r\n[WORD]80\r\n[DWORD]200\r\n[WORD]0\r\n[WORD]10\r\n[DWORD]4294967295\r\n[DWORD]0','','',4,'',0,'0x9202','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('9a1756f1-221f-46ef-a4f4-ea8ffc3a',4,'2ba477ae-d385-41e0-ad16-83f1db49','判断是否收到报文应答',2,'','','pro_progress11_case1_check1_gcy_cmp',0,'',30,'','kwIAAAAKMDEyMzQ1Njc4OQ==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('9a2d861b-20b0-4a38-bb18-f21d0624',1,'42800c1a-5f41-42a8-90e3-9051d52a','下发预警消息',14,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]32\r\n[BYTE|1]1\r\n[WORD]5\r\n[TIME_T|8]2011-11-15 11:30:30\r\n[DWORD]17\r\n[STRING|17]离开指定区域报警','','',4,'',0,'0x9402','suJBMTIzNDUAAAAAAAAAAAAAAAAAApQCAAAAIAEABQAAAABOwdzWAAAAEcDrv6rWuLaox/jT8rGovq8A');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('9e087ecf-770c-48a5-ab2c-889d61ae',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第1次车辆定位信息',3,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|1]1\r\n[BYTE|4]0C1007DB\r\n[BYTE|3]091E10\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]10\r\n[WORD]10\r\n[DWORD]1000\r\n[WORD]120\r\n[WORD]100\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9202','suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('a506fa86-f14f-4410-8fd7-b63dd74b',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔8',18,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('a62f967d-35ad-47eb-8c32-8df9ac27',4,'698c9498-0323-4e96-813d-1924df97','判断是否收到5条车辆动态消息',14,'','','',1,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('a7cb9d39-4872-438b-99e3-1032a1b9',4,'42800c1a-5f41-42a8-90e3-9051d52a','判断是否收到下级平台补发的车辆定位信息至少5条',11,'','','pro_progress3_case1_check5_zw_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('aaad5674-81af-4ba8-9d77-b18fbe6a',1,'df52b775-b10c-4cc5-933a-7d72b95d','发送终端控制',15,'[BYTE|1]2\r\n[STRING|1]1','','',4,'消息ID0x8105',0,'','AjE=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('ab2664b7-4fc1-4f84-b839-3e1949db',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第5次车辆定位信息',11,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|1]1\r\n[BYTE|4]0C1007DB\r\n[BYTE|3]091E10\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]10\r\n[WORD]10\r\n[DWORD]1000\r\n[WORD]120\r\n[WORD]100\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9202','suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('aef0cee3-21a6-4706-8042-a801f577',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收到下发拍照请求应答',5,'','','pro_progress4_case1_check2_gcy_cmp',0,'',180,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZUCAAE=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('b231df34-e913-4e85-80be-d894e129',3,'42800c1a-5f41-42a8-90e3-9051d52a','提示被测下级平台发送车辆注册消息',1,'请下级平台发送车辆注册消息。','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('b9cb864a-5854-4c8a-83c9-a23187dc',1,'df52b775-b10c-4cc5-933a-7d72b95d','发送结束车辆定位信息交换请求消息',13,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]0','','',4,'',0,'0x9206','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIGAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('bcf16177-d1b2-428b-ba27-87dd9fe1',5,'df52b775-b10c-4cc5-933a-7d72b95d','等待3分钟',12,'','','',4,'',180,'','vqlKQjEyNDU2AAAAAAAAAAAAAAAAAZUFAAAAkTAxMjM0NTY3ODlDTk5FVAAAAAAAAAAAAAAAAAAAADEzODg4ODg4ODg4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAxMjM0NTY3ODkAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjExNAAAAAAAAAAAAAAAAAAAAAAAAAAfkB+RAAAAAFDIBBw=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('beb865b7-053b-4a89-a376-bb7beed2',4,'42800c1a-5f41-42a8-90e3-9051d52a','判定是否收到下级平台发送的车辆静态消息',6,'','','pro_progress3_case1_check3_zw_cmp',0,'',30,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('c1b43bf8-65f4-4c20-9d6f-a58cb1bc',1,'df52b775-b10c-4cc5-933a-7d72b95d','下发车辆监听请求',1,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]38145\r\n[DWORD]20\r\n[STRING|20]10000000000','','',4,'测试时需要修改[STRING|20]电话号码',0,'0x9501','suJBMTIzNDUAAAAAAAAAAAAAAAAAApUBAAAAFDEwMDAwMDAwMDAwAAAAAAAAAAAA');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('c5606bed-e03d-4e4d-a049-7835bf65',3,'42800c1a-5f41-42a8-90e3-9051d52a','提示下级平台注销主链路',7,'请下级平台注销主链路！','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('c931e29b-8385-420a-9d03-4325e2a2',4,'fdb1ecbf-7c13-4921-a7c5-146da81e','判断是否收到驾驶员身份信息上传消息',2,'','','pro_progress8_case1_check1_zb_cmp',0,'',30,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('ca52e64b-6597-47da-89ba-f9835305',1,'2ba477ae-d385-41e0-ad16-83f1db49','下发报文请求',1,'[WORD]37634\r\n[DWORD]31\r\n[BYTE|1]0\r\n[BYTE|12]000000000000000000000000\r\n[DWORD]1000\r\n[DWORD]10\r\n[STRING|10]0123456789','','',4,'',0,'0x9302','kwIAAAAfAAAAAAAAAAAAAAAAAAAAA+gAAAAKMDEyMzQ1Njc4OQ==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('cae1c009-a221-4f87-9e79-dfd9814c',3,'42800c1a-5f41-42a8-90e3-9051d52a','提示下级平台建立主链路连接',9,'请下级平台建立主链路连接！','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('ce23db8a-46fe-450e-b1c8-c4fad7ba',1,'dc466d36-e3e7-4794-b2dc-4a83d870','发送第3次车辆定位信息',7,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|1]1\r\n[BYTE|4]0C1007DB\r\n[BYTE|3]091E10\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]10\r\n[WORD]10\r\n[DWORD]1000\r\n[WORD]120\r\n[WORD]100\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9202','suJBMTIzNDUAAAAAAAAAAAAAAAAAApICAAAAJAEMEAfbCR4QBxs0RgGTfygACgAKAAAD6AB4AGQAABAjAAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('ce91cd61-e02d-4b4b-9fcc-7b02e585',1,'698c9498-0323-4e96-813d-1924df97','发送第二条车辆动态消息',7,'[STRING|21]京B12345\r\n[STRING|1]1\r\n[WORD]37378\r\n[DWORD]36\r\n[DWORD]0\r\n[BYTE|4]150A07DB\r\n[BYTE|3]0E0F00\r\n[DWORD]119223366\r\n[DWORD]26443560\r\n[WORD]80\r\n[WORD]80\r\n[DWORD]200\r\n[WORD]0\r\n[WORD]10\r\n[DWORD]4294967295\r\n[DWORD]0','','',4,'',0,'0x9202','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('cffcd89f-be23-4b31-bf5c-3a87680e',4,'42800c1a-5f41-42a8-90e3-9051d52a','判断是否收到下级平台发送的含有超速报警的定位信息',13,'','','pro_progress3_case1_check6_zw_cmp',0,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIJAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('d413d373-3148-48e1-a47c-a1cfdb54',1,'1eff41f3-0618-46d1-850a-0417cd5d','发送第3条信息交换补发消息',8,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('d6a1c5fb-f0bc-4471-ab0b-d6938bbb',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔7',16,'','','',4,'',5,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('d836db6f-5105-45ca-8ab9-55d99532',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收行驶记录提取应答消息',9,'','','pro_progress4_case1_check4_gcy_cmp',0,'',180,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZUEAQE=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('d90728e7-04ef-4717-8ee9-dae09cd5',3,'b3ffc723-adfd-44a3-8369-7d55549b','提示下级平台发送主链路注销指令',5,'请下级平台发送主链路注销请求','','',4,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('dbaf7e93-042e-4a2d-9b8e-eb55dab5',4,'1eff41f3-0618-46d1-850a-0417cd5d','判定下级平台是否收到补发的定位信息',13,'','','',1,'',0,'','');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('dc841c98-53ae-464a-8c62-6800b33c',4,'42800c1a-5f41-42a8-90e3-9051d52a','判断是否收到督办请求的应答',16,'','','pro_progress3_case1_check7_zw_cmp',0,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIJAAAAAQA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('e18af983-6ec9-4b46-bd43-86c62a11',5,'dc466d36-e3e7-4794-b2dc-4a83d870','发送时间间隔6',14,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZQCAAAAFwEAAQAAAABO5X8cAAAACLOsy9mxqL6v');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('e24d746b-86aa-4890-a184-b6a09b93',4,'df52b775-b10c-4cc5-933a-7d72b95d','判断是否收到车辆监听请求应答',2,'','','pro_progress4_case1_check1_gcy_cmp',0,'',60,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZUBAAAAFDEwMDAwMDAwMDAwAAAAAAAAAAAA');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('edaee5f2-2e36-4e84-8561-9e8f8143',4,'698c9498-0323-4e96-813d-1924df97','判断是否收到下级平台发送的申请消息',2,'','','pro_progress9_case1_check1_zb_cmp',0,'',30,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('edc79876-8baf-4b96-870d-88584a05',5,'698c9498-0323-4e96-813d-1924df97','时间间隔3',8,'','','',4,'',5,'','vqlCMTIzNDUAAAAAAAAAAAAAAAAAMZICAAAAJAAAAAAVCgfbDg8ABxs0RgGTfygAUABQAAAAyAAAAAr/////AAAAAA==');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('f268b844-840e-4c17-8dca-b769a5b9',5,'1eff41f3-0618-46d1-850a-0417cd5d','发送时间间隔4',11,'','','',4,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIDAAAAtQUBGAoUCwocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCwscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABGAoUCwwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('f4f6783d-cfdc-4b5c-82ad-4cd394f8',1,'698c9498-0323-4e96-813d-1924df97','发送车辆静态消息',3,'[STRING|21]京B12345\r\n[BYTE|1]1\r\n[WORD]37380\r\n[DWORD]511\r\n[STRING|511]TRANS_TYPE:=030;VIN:=浙A25307;TRACTION:=30;TRAILER_VIN:=浙A21107挂;VEHICLE_NATIONALIT:=330108;VEHICLE_TYPE:=40;RTPN:=330101212280;OWERS_NAME:=杭州货运代理公司;OWERS_ORIG_ID:=1000;OWERS_TEL:=13516814499;RTOLN:=330101200006;VEHICLE_MODE:=解放5163;VEHICLE_COLOR:=1;VEHICLE_ORIG_ID:=12345;DRIVER_INFO:=阮孟禄|3301011060008040000|13854389438;GUARDS_INFO:=刘二|33010110508030000|13717660901;APPROVED_TONNAGE:=5;DG_TYPE:=03115;CARGO_TONNAGE:=3;TRANSPORT_ORIGIN:=萧山区;TRANSPORT_DES:=长宁区;TSSL:=1261486591|1261488899','','',4,'',0,'0x9204','vqlCMTIzNDUAAAAAAAAAAAAAAAAAAZIEAAAB/1RSQU5TX1RZUEU6PTAzMDtWSU46PdXjQTI1MzA3O1RSQUNUSU9OOj0zMDtUUkFJTEVSX1ZJTjo91eNBMjExMDe50jtWRUhJQ0xFX05BVElPTkFMSVQ6PTMzMDEwODtWRUhJQ0xFX1RZUEU6PTQwO1JUUE46PTMzMDEwMTIxMjI4MDtPV0VSU19OQU1FOj26vNbdu/XUy7T6wO25q8u+O09XRVJTX09SSUdfSUQ6PTEwMDA7T1dFUlNfVEVMOj0xMzUxNjgxNDQ5OTtSVE9MTjo9MzMwMTAxMjAwMDA2O1ZFSElDTEVfTU9ERTo9veK3xTUxNjM7VkVISUNMRV9DT0xPUjo9MTtWRUhJQ0xFX09SSUdfSUQ6PTEyMzQ1O0RSSVZFUl9JTkZPOj3I7sPPwrt8MzMwMTAxMTA2MDAwODA0MDAwMHwxMzg1NDM4OTQzODtHVUFSRFNfSU5GTzo9wfW2/nwzMzAxMDExMDUwODAzMDAwMHwxMzcxNzY2MDkwMTtBUFBST1ZFRF9UT05OQUdFOj01O0RHX1RZUEU6PTAzMTE1O0NBUkdPX1RPTk5BR0U6PTM7VFJBTlNQT1JUX09SSUdJTjo9z/TJvcf4O1RSQU5TUE9SVF9ERVM6PbOkxP7H+DtUU1NMOj0xMjYxNDg2NTkxfDEyNjE0ODg4OTk=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('f9d34024-b276-422b-b844-0a93a60f',5,'1eff41f3-0618-46d1-850a-0417cd5d','发送时间间隔1',5,'','','',4,'',30,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIDAAAAtQUBGAoUCwocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCwscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABGAoUCwwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABGAoUCw4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('fa02b043-d505-4ebf-b427-e2c68c58',1,'698c9498-0323-4e96-813d-1924df97','发送第5条车辆动态信息',13,'[STRING|21]测A12345\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0A1C1C\r\n[DWORD]116175284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0B1C1C\r\n[DWORD]116181379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0C1C1C\r\n[DWORD]116195284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0D1C1C\r\n[DWORD]116215284\r\n[DWORD]39575284\r\n[WORD]20\r\n[WORD]20\r\n[DWORD]4252\r\n[WORD]180\r\n[WORD]60\r\n[DWORD]4131\r\n[DWORD]0\r\n[BYTE|1]1\r\n[BYTE|4]0B0B07DB\r\n[BYTE|3]0E1C1C\r\n[DWORD]116231379\r\n[DWORD]39512037\r\n[WORD]120\r\n[WORD]120\r\n[DWORD]4452\r\n[WORD]45\r\n[WORD]70\r\n[DWORD]4131\r\n[DWORD]0','','',4,'',0,'0x9203','suJBMTIzNDUAAAAAAAAAAAAAAAAAApIDAAAAtQUBCwsH2wocHAbssbQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2wscHAbsyYMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAABCwsH2wwcHAbs/9QCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w0cHAbtTfQCW970ABQAFAAAEJwAtAA8AAAQIwAAAAABCwsH2w4cHAbtjNMCWuflAHgAeAAAEWQALQBGAAAQIwAAAAA=');
insert  into `steplib`(`stepid`,`typeid`,`caseid`,`stepname`,`ordernum`,`stepparam`,`resultmsg`,`procedureparam`,`autojudge`,`note`,`overtime`,`messageid`,`sendmsg`) values ('fe03b50e-3e63-4c26-bfed-c8b91f92',3,'42800c1a-5f41-42a8-90e3-9051d52a','提示下级平台发送超速报警定位信息',12,'请下级平台发送超速报警定位信息。','','',4,'',0,'','vqlBMTIzNDUAAAAAAAAAAAAAAAAAAZIJAAAAAQA=');

/*Table structure for table `steptype` */

DROP TABLE IF EXISTS `steptype`;

CREATE TABLE `steptype` (
  `typeid` varchar(32) NOT NULL,
  `typename` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='步骤类型定义\r\n1发送\r\n2接收\r\n3提示\r\n4判定\r\n5等待';

/*Data for the table `steptype` */

insert  into `steptype`(`typeid`,`typename`) values ('1','发送');
insert  into `steptype`(`typeid`,`typename`) values ('2','接收');
insert  into `steptype`(`typeid`,`typename`) values ('3','提示');
insert  into `steptype`(`typeid`,`typename`) values ('4','判定');
insert  into `steptype`(`typeid`,`typename`) values ('5','等待');

/*Table structure for table `target` */

DROP TABLE IF EXISTS `target`;

CREATE TABLE `target` (
  `TargetbatchID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '目标批次号',
  `Name` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '目标名称描叙',
  `groupid` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '目标分类',
  `FactoryID` int(11) NOT NULL COMMENT '厂商编号',
  `TargetModel` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '目标型号',
  `TargetVersion` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '目标版本',
  `ReceiveDate` datetime NOT NULL COMMENT '送检日期',
  `Reporter` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '测试结果审核人',
  `ReportDate` datetime DEFAULT NULL COMMENT '测试结果确认日期',
  `ResultID` int(11) NOT NULL DEFAULT '1' COMMENT '检测状态',
  `ResultNote` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '检测说明',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `Positionmode` int(11) NOT NULL DEFAULT '0' COMMENT '定位模式',
  PRIMARY KEY (`TargetbatchID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标批次信息';

/*Data for the table `target` */

insert  into `target`(`TargetbatchID`,`Name`,`groupid`,`FactoryID`,`TargetModel`,`TargetVersion`,`ReceiveDate`,`Reporter`,`ReportDate`,`ResultID`,`ResultNote`,`Note`,`Positionmode`) values ('bjhsjc2201111110001','1','5',1,'','V1.0','2011-11-11 10:15:58',NULL,NULL,2,NULL,'',0);
insert  into `target`(`TargetbatchID`,`Name`,`groupid`,`FactoryID`,`TargetModel`,`TargetVersion`,`ReceiveDate`,`Reporter`,`ReportDate`,`ResultID`,`ResultNote`,`Note`,`Positionmode`) values ('bjhsjc2201111110003','01','5',2,'','0.1','2011-11-11 16:51:34',NULL,NULL,2,NULL,'dd',0);
insert  into `target`(`TargetbatchID`,`Name`,`groupid`,`FactoryID`,`TargetModel`,`TargetVersion`,`ReceiveDate`,`Reporter`,`ReportDate`,`ResultID`,`ResultNote`,`Note`,`Positionmode`) values ('bjhsjc2201404300001','1.0','5',3,'','2.0','2014-04-30 15:57:15',NULL,NULL,1,NULL,'12345678',0);
insert  into `target`(`TargetbatchID`,`Name`,`groupid`,`FactoryID`,`TargetModel`,`TargetVersion`,`ReceiveDate`,`Reporter`,`ReportDate`,`ResultID`,`ResultNote`,`Note`,`Positionmode`) values ('bjhsjc2201404300002','2.0','5',3,'','2.0','2014-04-30 15:59:37',NULL,NULL,2,NULL,'2.0',0);

/*Table structure for table `targetdetail` */

DROP TABLE IF EXISTS `targetdetail`;

CREATE TABLE `targetdetail` (
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标编号',
  `TargetbatchID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标批次号',
  `TargetName` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '测试目标名称',
  `AccessNO` varchar(40) COLLATE gbk_bin DEFAULT NULL COMMENT '下级平台接入码',
  `ProtocolVersion` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '协议版本号',
  `SimNo` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '0' COMMENT '通讯号码(手机号码)',
  `GovServerIP` varchar(16) COLLATE gbk_bin DEFAULT NULL,
  `GovServerPort` varchar(11) COLLATE gbk_bin DEFAULT NULL,
  `Reporter` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '测试结果确认人',
  `ReportDate` datetime DEFAULT NULL COMMENT '结果确认时间',
  `resultid` int(11) NOT NULL DEFAULT '1',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `m1` int(11) DEFAULT NULL,
  `a1` int(11) DEFAULT NULL,
  `c1` int(11) DEFAULT NULL,
  `CompanyIP` varchar(16) COLLATE gbk_bin DEFAULT NULL,
  `CompanyPort` int(11) DEFAULT NULL,
  `UserName` varchar(32) COLLATE gbk_bin DEFAULT NULL,
  `UsePswd` varchar(32) COLLATE gbk_bin DEFAULT NULL,
  `SerialNumber` int(11) NOT NULL COMMENT '目标序号',
  `object_id1` varchar(12) COLLATE gbk_bin DEFAULT NULL,
  `object_id2` varchar(12) COLLATE gbk_bin DEFAULT NULL,
  PRIMARY KEY (`TargetID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试目标明细表';

/*Data for the table `targetdetail` */

insert  into `targetdetail`(`TargetID`,`TargetbatchID`,`TargetName`,`AccessNO`,`ProtocolVersion`,`SimNo`,`GovServerIP`,`GovServerPort`,`Reporter`,`ReportDate`,`resultid`,`Note`,`m1`,`a1`,`c1`,`CompanyIP`,`CompanyPort`,`UserName`,`UsePswd`,`SerialNumber`,`object_id1`,`object_id2`) values ('bjhsjc220111111000101','bjhsjc2201111110001','中交测试平台','62000007','809-2011','','','0',NULL,NULL,1,NULL,NULL,NULL,NULL,'10.1.88.182',9010,'77777777','77777777',1,'7','1');
insert  into `targetdetail`(`TargetID`,`TargetbatchID`,`TargetName`,`AccessNO`,`ProtocolVersion`,`SimNo`,`GovServerIP`,`GovServerPort`,`Reporter`,`ReportDate`,`resultid`,`Note`,`m1`,`a1`,`c1`,`CompanyIP`,`CompanyPort`,`UserName`,`UsePswd`,`SerialNumber`,`object_id1`,`object_id2`) values ('bjhsjc220111111000301','bjhsjc2201111110003','车务通','62000009','809-2011','','','0',NULL,NULL,1,NULL,NULL,NULL,NULL,'211.137.45.81',9045,'99999999','99999999',1,'2011-001','0001');
insert  into `targetdetail`(`TargetID`,`TargetbatchID`,`TargetName`,`AccessNO`,`ProtocolVersion`,`SimNo`,`GovServerIP`,`GovServerPort`,`Reporter`,`ReportDate`,`resultid`,`Note`,`m1`,`a1`,`c1`,`CompanyIP`,`CompanyPort`,`UserName`,`UsePswd`,`SerialNumber`,`object_id1`,`object_id2`) values ('bjhsjc220140430000201','bjhsjc2201404300002','物流平台','12345678','809-2011','','','',NULL,NULL,2,NULL,NULL,NULL,NULL,'127.0.0.1',3523,'12345678','12345678',1,'12345678','12345678');

/*Table structure for table `targetgroup` */

DROP TABLE IF EXISTS `targetgroup`;

CREATE TABLE `targetgroup` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT COMMENT '目标分类编号',
  `TargetType` int(11) NOT NULL DEFAULT '1' COMMENT '1=终端2=企业平台=3政府平台',
  `GroupName` varchar(32) COLLATE gbk_bin NOT NULL COMMENT '分组名称',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`GroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标分类';

/*Data for the table `targetgroup` */

insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (1,1,'客运车辆终端','客运车辆终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (2,1,'出租车终端','出租车终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (3,1,'危险品运输车辆终端','危险品运输车辆终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (4,1,'货运车辆','货运车辆');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (5,2,'运营平台','运营平台');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (6,2,'非运营平台','非运营平台');

/*Table structure for table `targettest` */

DROP TABLE IF EXISTS `targettest`;

CREATE TABLE `targettest` (
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试序号',
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '目标编号',
  `TestCount` int(11) NOT NULL COMMENT '测试次数',
  `TestDate` datetime NOT NULL COMMENT '测试时间',
  `Tester` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '测试人',
  `EnvironmentID` int(11) NOT NULL DEFAULT '-1' COMMENT '测试环境编号',
  `ExampleID` int(11) NOT NULL COMMENT '用例编号',
  `resultid` int(11) NOT NULL DEFAULT '1',
  `timeout` int(11) NOT NULL DEFAULT '0' COMMENT '测试超时自动判断测试不通过',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `checker` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核日期',
  PRIMARY KEY (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='检测目标测试明细';

/*Data for the table `targettest` */

insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('01','',1,'2014-05-12 19:55:22','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('02','',1,'2014-05-12 19:55:22','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100010101','bjhsjc220111111000101',1,'2011-11-11 10:28:15','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100010102','bjhsjc220111111000101',2,'2011-11-11 10:36:24','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100010103','bjhsjc220111111000101',1,'2011-11-11 11:16:24','test',3,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030101','bjhsjc220111111000301',1,'2011-11-11 17:03:56','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030102','bjhsjc220111111000301',2,'2011-11-11 17:07:47','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030103','bjhsjc220111111000301',3,'2011-11-11 17:08:12','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030104','bjhsjc220111111000301',4,'2011-11-11 17:42:02','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030105','bjhsjc220111111000301',5,'2011-11-11 17:45:56','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22011111100030106','bjhsjc220111111000301',6,'2011-11-11 18:39:32','超级管理员',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020101','bjhsjc220140430000201',1,'2014-04-30 15:59:45','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020102','bjhsjc220140430000201',2,'2014-04-30 16:00:03','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020103','bjhsjc220140430000201',3,'2014-04-30 17:51:19','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020104','bjhsjc220140430000201',1,'2014-05-04 09:45:01','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020105','bjhsjc220140430000201',2,'2014-05-04 09:45:02','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020106','bjhsjc220140430000201',3,'2014-05-04 09:50:28','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020107','bjhsjc220140430000201',4,'2014-05-04 09:50:29','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020108','bjhsjc220140430000201',1,'2014-05-04 09:57:42','test',3,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020109','bjhsjc220140430000201',4,'2014-05-06 11:59:44','test',1,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020110','bjhsjc220140430000201',5,'2014-05-05 09:04:26','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020111','bjhsjc220140430000201',6,'2014-05-05 09:04:27','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020112','bjhsjc220140430000201',7,'2014-05-05 09:27:39','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020113','bjhsjc220140430000201',8,'2014-05-05 09:27:39','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020114','bjhsjc220140430000201',9,'2014-05-05 09:39:10','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020115','bjhsjc220140430000201',10,'2014-05-05 09:39:10','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020116','bjhsjc220140430000201',11,'2014-05-06 09:30:01','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020117','bjhsjc220140430000201',12,'2014-05-06 09:30:01','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020118','bjhsjc220140430000201',13,'2014-05-06 11:31:59','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020119','bjhsjc220140430000201',14,'2014-05-06 11:32:19','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020120','bjhsjc220140430000201',15,'2014-05-06 11:35:59','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020121','bjhsjc220140430000201',16,'2014-05-06 11:35:59','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020122','bjhsjc220140430000201',17,'2014-05-06 14:32:34','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020123','bjhsjc220140430000201',18,'2014-05-06 14:32:34','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020124','bjhsjc220140430000201',19,'2014-05-06 14:34:04','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020125','bjhsjc220140430000201',20,'2014-05-06 14:34:04','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020126','bjhsjc220140430000201',21,'2014-05-07 08:54:36','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020127','bjhsjc220140430000201',22,'2014-05-07 08:54:36','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020128','bjhsjc220140430000201',23,'2014-05-07 08:59:50','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020129','bjhsjc220140430000201',24,'2014-05-07 08:59:51','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020130','bjhsjc220140430000201',25,'2014-05-07 14:47:10','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020131','bjhsjc220140430000201',26,'2014-05-07 14:47:10','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020132','bjhsjc220140430000201',27,'2014-05-08 08:48:34','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020133','bjhsjc220140430000201',28,'2014-05-08 08:48:34','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020134','bjhsjc220140430000201',29,'2014-05-08 09:03:05','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020135','bjhsjc220140430000201',30,'2014-05-08 09:03:05','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020136','bjhsjc220140430000201',31,'2014-05-08 09:03:24','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020137','bjhsjc220140430000201',32,'2014-05-08 09:03:24','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020138','bjhsjc220140430000201',33,'2014-05-09 11:01:39','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020139','bjhsjc220140430000201',34,'2014-05-09 11:01:40','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020140','bjhsjc220140430000201',35,'2014-05-09 17:38:24','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020141','bjhsjc220140430000201',36,'2014-05-09 17:38:24','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020142','bjhsjc220140430000201',37,'2014-05-11 16:34:27','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020143','bjhsjc220140430000201',38,'2014-05-11 16:34:27','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020144','bjhsjc220140430000201',39,'2014-05-12 09:20:41','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020145','bjhsjc220140430000201',40,'2014-05-12 09:20:41','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020146','bjhsjc220140430000201',41,'2014-05-12 13:45:11','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020147','bjhsjc220140430000201',42,'2014-05-12 13:45:11','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020148','bjhsjc220140430000201',43,'2014-05-12 19:08:42','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020149','bjhsjc220140430000201',44,'2014-05-12 19:08:42','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020150','bjhsjc220140430000201',45,'2014-05-12 19:52:18','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020151','bjhsjc220140430000201',46,'2014-05-12 19:52:18','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020152','bjhsjc220140430000201',47,'2014-05-12 19:54:23','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020153','bjhsjc220140430000201',48,'2014-05-12 19:54:23','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020154','bjhsjc220140430000201',49,'2014-05-12 20:06:31','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020155','bjhsjc220140430000201',50,'2014-05-12 20:06:31','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020156','bjhsjc220140430000201',51,'2014-05-12 20:08:09','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020157','bjhsjc220140430000201',52,'2014-05-12 20:08:09','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020158','bjhsjc220140430000201',53,'2014-05-12 20:08:34','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020159','bjhsjc220140430000201',54,'2014-05-12 20:08:34','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020160','bjhsjc220140430000201',55,'2014-05-12 20:09:44','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020161','bjhsjc220140430000201',56,'2014-05-12 20:09:44','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020162','bjhsjc220140430000201',57,'2014-05-13 10:05:37','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020163','bjhsjc220140430000201',58,'2014-05-13 10:05:37','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020164','bjhsjc220140430000201',59,'2014-05-13 11:50:57','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020165','bjhsjc220140430000201',60,'2014-05-13 11:50:57','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020166','bjhsjc220140430000201',61,'2014-05-13 13:54:18','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020167','bjhsjc220140430000201',62,'2014-05-13 13:54:18','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020168','bjhsjc220140430000201',63,'2014-05-13 14:43:27','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020169','bjhsjc220140430000201',64,'2014-05-13 14:43:27','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020170','bjhsjc220140430000201',65,'2014-05-13 14:50:37','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020171','bjhsjc220140430000201',66,'2014-05-13 14:50:37','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020172','bjhsjc220140430000201',67,'2014-05-13 15:03:10','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020173','bjhsjc220140430000201',68,'2014-05-13 15:03:10','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020174','bjhsjc220140430000201',69,'2014-05-13 15:08:33','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020175','bjhsjc220140430000201',70,'2014-05-13 15:08:33','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020176','bjhsjc220140430000201',71,'2014-05-13 15:28:49','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020177','bjhsjc220140430000201',72,'2014-05-13 15:28:49','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020178','bjhsjc220140430000201',73,'2014-05-13 15:50:13','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020179','bjhsjc220140430000201',74,'2014-05-13 15:50:13','admin',2,-1,3,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020180','bjhsjc220140430000201',75,'2014-05-13 17:04:41','admin',2,-1,2,0,NULL,NULL,NULL);
insert  into `targettest`(`TestID`,`TargetID`,`TestCount`,`TestDate`,`Tester`,`EnvironmentID`,`ExampleID`,`resultid`,`timeout`,`Note`,`checker`,`checkdate`) values ('bjhsjc22014043000020181','bjhsjc220140430000201',76,'2014-05-13 17:04:41','admin',2,-1,2,0,NULL,NULL,NULL);

/*Table structure for table `testreport` */

DROP TABLE IF EXISTS `testreport`;

CREATE TABLE `testreport` (
  `ReportID` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试报告ID',
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '检测记录ID',
  `FunctionID` int(11) NOT NULL COMMENT '功能项ID',
  `ExampleID` int(11) NOT NULL COMMENT '测试用例',
  `EnvironmentID` int(11) NOT NULL COMMENT '测试环境编号',
  `TestProcess` varchar(200) COLLATE gbk_bin NOT NULL COMMENT '测试过程描叙',
  `Reporter` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '测试人员',
  `ReportDate` datetime NOT NULL COMMENT '测试日期',
  `TemplateResult` int(11) NOT NULL COMMENT '测试正常时返回的结果',
  `ReportResult` int(11) NOT NULL COMMENT '判定结果',
  `Reason` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '判定说明',
  PRIMARY KEY (`ReportID`),
  KEY `Index1_TestReport` (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试报告表';

/*Data for the table `testreport` */

/* Function  structure for function  `GetDescribe` */

/*!50003 DROP FUNCTION IF EXISTS `GetDescribe` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `GetDescribe`(iTargetbatchID varchar(30)) RETURNS varchar(200) CHARSET gbk
begin
     declare iGroupID        varchar(20);
     declare igroupid1       varchar(20);
     declare oDescribeStr    varchar(200);
     declare igroupname      varchar(32);
     declare stop int default 0;   
     
     select trim(groupid) into iGroupID from target where TargetbatchID=iTargetbatchID;
     if iGroupID='' then
           return '';
     end if;
     
     set oDescribeStr='';
     
     while length(iGroupID)>0 do
          if INSTR(iGroupID,',')>0 then
               set igroupid1=substring(iGroupID,1,INSTR(iGroupID,',')-1);
               set iGroupID=trim(substring(iGroupID,INSTR(iGroupID,',')+1));
          else
               set igroupid1=iGroupID;
               set iGroupID='';
          end if;
          
          select groupname into igroupname from targetgroup where groupid=igroupid1;
          if trim(igroupname)<>'' then
                if oDescribeStr='' then
                    set oDescribeStr=trim(igroupname);
                else
                    set igroupname=concat('|',trim(igroupname));
                    set oDescribeStr=concat(oDescribeStr,igroupname);
                end if;
          end if;
     end while;
     return oDescribeStr;
end */$$
DELIMITER ;

/* Function  structure for function  `GetSerialID` */

/*!50003 DROP FUNCTION IF EXISTS `GetSerialID` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `GetSerialID`(iString varchar(200),iflag int) RETURNS int(11)
begin
     declare ilength int;
     declare ordstr  varchar(30);
     declare serialid int;
     declare outserial int;
     
     set outserial=0;
     set ordstr='';
     set iString=trim(iString);
     set ilength=length(iString);
     set serialid=1;
        
     while serialid<=ilength do
             if substring(iString,serialid,1) in('1','2','3','4','5','6','7','8','9','0','.',' ')  then
                     if substring(iString,serialid,1) in('1','2','3','4','5','6','7','8','9','0','.') then
                            set ordstr=concat(ordstr,substring(iString,serialid,1));
                     end if;
             else
                    set serialid=ilength+1;
             end if;
             set serialid=serialid+1;
     end while;
     
     set ordstr=replace(trim(ordstr),' ','');
     
     if iflag=0 then
           set ordstr=replace(ordstr,'.','');  
     end if;
     
     while INSTR(ordstr,'.')>0 do
          set ordstr=substring(ordstr,INSTR(ordstr,'.')+1);
     end while;
     
     if ordstr<>'' then
        set outserial=ordstr;
     end if;
      
     return  outserial;
end */$$
DELIMITER ;

/* Function  structure for function  `InttoBin` */

/*!50003 DROP FUNCTION IF EXISTS `InttoBin` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `InttoBin`(iDec int,iflag int) RETURNS varchar(50) CHARSET gbk
BEGIN
    declare  iBinStr  VARCHAR(50);     
    declare iDiv2     INT;             
    declare iMod2    INT; 
    declare igpsnote  varchar(50);  
    declare oflag     int;
    declare idesstr1  varchar(20);
    declare idesstr2  varchar(20);
    declare idesstr4  varchar(20);
    declare idesstr8  varchar(20);
             
    IF iDec <= 0 then
        RETURN '0000';  
    end if;   
    
    set idesstr1='GPS';
    set idesstr2='北斗';
    set idesstr4='GLONASS';
    set idesstr8='伽利略';
                 
    SET iDiv2 = iDec;   
    SET iBinStr = '';
    set igpsnote='';
    
    while  (iDiv2>0) do
        SET iMod2 = iDiv2 % 2;
        SET iDiv2 = floor(iDiv2 / 2);
        SET iBinStr = trim(concat(iMod2,iBinStr));
    END while;
    if length(iBinStr)<4 then
       set iBinStr=lpad(iBinStr,4,'0');
    end if;
    
    if iflag=0 then
        RETURN iBinStr;
    end if;
    
    if substring(iBinStr,4,1)='1' then
         set igpsnote=idesstr1;
    end if;
    
    if substring(iBinStr,3,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr2;
         else
             set idesstr2=concat('|',idesstr2);
             set igpsnote=concat(igpsnote,idesstr2);
         end if;
    end if;
    
    if substring(iBinStr,2,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr4;
         else
             set idesstr4=concat('|',idesstr4);
             set igpsnote=concat(igpsnote,idesstr4);
         end if;
    end if;
    
    if substring(iBinStr,1,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr8;
         else
             set idesstr8=concat('|',idesstr8);
             set igpsnote=concat(igpsnote,idesstr8);
         end if;
    end if;
    
    RETURN igpsnote;
END */$$
DELIMITER ;

/* Procedure structure for procedure `AddEventLog` */

/*!50003 DROP PROCEDURE IF EXISTS  `AddEventLog` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `AddEventLog`(iFlag  int,iTestID varchar(30),iExampleID int,iMessageID varchar(20),iSerialNumber int,iResultFlag int,
                             iOraginMsg varchar(32767),iOutMsg varchar(2000),iMediaType int,iMediaData varchar(32767),iNote varchar(200))
begin
        declare  itargetid   varchar(30);
        declare  ilogid      int;
        
        set ilogid=0;
        select targetid into itargetid from targettest where testid=iTestID;
        if itargetid is null then
             set itargetid=substring(iTestID,1,length(iTestID)-2);
        end if;
        
        insert into EventLog(flag,targetid,testid,eventdate,exampleid,messageid,serialnumber,
                             resultflag,oraginmsg,outmsg,MediaType,MediaData,note)
                    values(iFlag,itargetid,iTestID,current_timestamp,iExampleID,iMessageID,iSerialNumber,
                            iResultFlag,iOraginMsg,iOutMsg,iMediaType,iMediaData,iNote);
                    
        set ilogid=@@identity;
        select  ilogid as logid;
end */$$
DELIMITER ;

/* Procedure structure for procedure `AddTargetTest` */

/*!50003 DROP PROCEDURE IF EXISTS  `AddTargetTest` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `AddTargetTest`(iTargetID varchar(30),iTester varchar(16),iEnvironmentID int,iExampleID int)
begin
     declare iTestCount int;
     declare icount     int;
     declare iteststr   char(2);
     declare iTestID    varchar(30);
     declare iTargetbatchID varchar(30);
     
     declare exit handler for sqlexception rollback;
     select  TargetbatchID into iTargetbatchID from TargetDetail where targetid=iTargetID;
     if iTargetbatchID is null then
            set iTargetbatchID=substring(iTargetID,1,length(iTargetID)-2);
     end if;
     
     select count(TestCount) into  icount from TargetTest where EnvironmentID=iEnvironmentID and
     targetid in(select targetid from TargetDetail where TargetbatchID=iTargetbatchID);
     
     select count(TestCount) into  iTestCount from TargetTest where targetid=iTargetID;
     
     set icount=icount+1;
     set iTestCount=iTestCount+1;
     set iteststr=lpad(iTestCount,2,'0');
     set iTargetID=trim(iTargetID);
     set iTestID=concat(iTargetID,iteststr);
     
     insert into TargetTest(testid,targetid,testcount,testdate,tester,EnvironmentID,ExampleID,resultid,timeout)
                  values(iTestID,iTargetID,icount,CURRENT_TIMESTAMP,iTester,iEnvironmentID,iExampleID,1,0);
                  
     select iTestID as TestID;
     
end */$$
DELIMITER ;

/* Procedure structure for procedure `GetAuthentication` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetAuthentication` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `GetAuthentication`(isimno varchar(20))
begin
      declare iAuthentication varchar(40);
      declare icount          int;
      
      set iAuthentication='';
      select count(*) into icount from TargetDetail where SimNo=isimno;
      if icount>0 then
          select trim(configvalue) into iAuthentication from config where configname='终端鉴权码';
      end if;
      
      select iAuthentication as Authentication;
end */$$
DELIMITER ;

/* Procedure structure for procedure `GetTargetInfo` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetTargetInfo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `GetTargetInfo`(iTestID varchar(30))
begin
      declare iAccessNO                 varchar(40);
      declare iProtocolVersion          varchar(30);
      declare iTargetID                 varchar(30);
      
      declare configversion             varchar(30);
      declare configaccessno            varchar(40);
      
      select a.AccessNO,a.ProtocolVersion,a.TargetID into iAccessNO,iProtocolVersion,iTargetID
             from TargetDetail a,TargetTest b where a.targetid=b.targetid and b.testid=iTestID;
             
      select configvalue into configversion from config where configname='协议版本号';
      if configversion is not null then
            set iProtocolVersion=configversion;
      end if;
      
      select iTargetID as targetid,iAccessNO as accessno,iProtocolVersion as ProtocolVersion;
end */$$
DELIMITER ;

/* Procedure structure for procedure `pro_check_messages_num` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_check_messages_num` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_check_messages_num`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32), IN messageID1 VARCHAR(6), IN messageID2 

VARCHAR(6), IN checkNum int)
BEGIN
	
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);
	DECLARE checkNum1 int default 0;
	DECLARE checkNum2 int default 0;
	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  

and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  

and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur; 
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml; 
			if iMessageId = messageID1 THEN 
					BEGIN
							set checkNum1 = checkNum1 + 1;
							IF iResultFlag = 1 THEN set checkNum1 = -100;
							end if;
					END;
			end if;	
			if iMessageId = messageID2 THEN 
					BEGIN
							set checkNum2 = checkNum2 + 1;
							IF iResultFlag = 1 THEN set checkNum2 =  -100;
							end if;
					END;
			end if;	
			set c = c+1;
  end while; 

  close cur; 
	
	IF checkNum1 >= checkNum && checkNum2 >= checkNum then set rMsg = 1; 
	end if;
	SELECT rMsg,c,checkNum1,checkNum2;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_check_message_and_xml` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_check_message_and_xml` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_check_message_and_xml`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32) , IN messageID VARCHAR(6) , IN xPath VARCHAR(100) , IN xmlCheckValue VARCHAR(10))
BEGIN
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);

	DECLARE resultValue varchar(10) default '';
	DECLARE checkData int default 0;

	DECLARE rMsg int default 0;

	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	OPEN cur; 
	WHILE stop<>1 && c<num do
			FETCH cur INTO iTime,iMessageId,iResultFlag,iXml; 
			IF checkData = 0 THEN
					IF iMessageId = messageID  THEN 
							BEGIN
									SET checkData = 1;
									IF iResultFlag = 1 THEN set checkData = 0;
									END IF;
									IF checkData = 1 THEN
											BEGIN
												SELECT EXTRACTVALUE (iXml, Xpath) INTO resultValue; 
												IF resultValue <> xmlCheckValue  THEN set checkData = 0;
												END IF;
											END;
									END IF;
							END;
					end if;	
			END IF;
			SET c = c+1;
  END WHILE; 

  CLOSE cur; 
	
	IF checkData = 1 THEN SET rMsg = 1; 
	END IF;
	SELECT rMsg,c,checkData,resultValue;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_check_message_and_xml_unequal` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_check_message_and_xml_unequal` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_check_message_and_xml_unequal`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32) , IN messageID VARCHAR(6) , IN xPath VARCHAR(100) , IN xmlCheckValue VARCHAR(10))
BEGIN
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);

	DECLARE resultValue varchar(10) default '';
	DECLARE checkData int default 0;

	DECLARE rMsg int default 0;

	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	OPEN cur; 
	WHILE stop<>1 && c<num do
			FETCH cur INTO iTime,iMessageId,iResultFlag,iXml; 
			IF checkData = 0 THEN
					IF iMessageId = messageID  THEN 
							BEGIN
									SET checkData = 1;
									IF iResultFlag = 1 THEN set checkData = 0;
									END IF;
									IF checkData = 1 THEN
											BEGIN
												SELECT EXTRACTVALUE (iXml, Xpath) INTO resultValue; 
												IF resultValue = xmlCheckValue  THEN set checkData = 0;
												END IF;
											END;
									END IF;
							END;
					end if;	
			END IF;
			SET c = c+1;
  END WHILE; 

  CLOSE cur; 
	
	IF checkData = 1 THEN SET rMsg = 1; 
	END IF;
	SELECT rMsg,c,checkData,resultValue;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_check_message_exist` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_check_message_exist` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_check_message_exist`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32),IN messageID VARCHAR(6))
BEGIN

	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);

	DECLARE checkData int default 0;

	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;

	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;

	SELECT count(*) into num FROM progresstestlog a
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur;
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml;
			if iMessageId = messageID  THEN
					BEGIN
							set checkData = 1;
							IF iResultFlag = 1 THEN set checkData = 0;
							end if;
					END;
			end if;
			set c = c+1;
  end while;

  close cur;

	IF checkData = 1 then set rMsg = 1;
	end if;
	SELECT rMsg,c,checkData;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_check_message_num` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_check_message_num` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_check_message_num`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32),IN messageID VARCHAR(6),IN checkNum INT)
BEGIN
		DECLARE iMessageId varchar(20);
		DECLARE iResultFlag int;
		DECLARE iXml varchar(4000);
		DECLARE iTime varchar(32);

		DECLARE rMsg int default 0;
		DECLARE checkData int default 1;

		DECLARE stop int DEFAULT 0;
		DECLARE c int default 0;
		DECLARE num int;

		DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
													 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
													 order by a.eventdate asc;
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  

		SELECT count(*) into num FROM progresstestlog a 
		where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
		order by a.eventdate asc;

		open cur; 
		
		while stop<>1 && c<num do
				fetch cur into iTime,iMessageId,iResultFlag,iXml; 
				if iMessageId = messageID  THEN 
							set checkData = checkData + 1;
							IF iResultFlag = 1 THEN set checkData = -100;
							end if;
				end if;	
				set c = c+1;
		end while; 

		close cur; 
		IF  checkData >= checkNum THEN set rMsg =1;
		end if;
		SELECT rMsg,checkData;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_progress_test_report` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_progress_test_report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_progress_test_report`(IN `reportId` varchar(32))
BEGIN
	#declare targetidstr varchar(32);
	#declare countint int;
	#declare environmentidint int;

	declare reportidstr varchar(32);
	set reportidstr = reportid;
	#set targetidstr = targetid;
	#set countint = testcount;
	#set environmentidint = environmentid;

	select c.ordernum as progressorder,c.progressname,d.groupnum as caseorder,
	d.casename,e.ordernum as setporder,e.stepname,
	b.testdate,b.autojudgeresult,b.judgeresult,b.judgeinfo 
	from progresstestreport a
	left join progressteststepdetail b on a.reportid = b.reportid
	left join processdefin c on b.progressid = c.progressid
	left join progresscase d on b.caseid = d.caseid
	left join steplib e on b.stepid = e.stepid
	where 1=1
	and e.typeid = '4'
	#and a.targetid = targetidstr
	#and a.environmentid = environmentidint
	#and a.testcount = countint
	and a.reportid = reportidstr
	order by c.ordernum,d.groupnum,e.ordernum;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_eventlog_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_eventlog_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_eventlog_syn_data`(IN testID varchar(30))
BEGIN
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
	SELECT CONCAT(test_org_id,a.ID) as ID,
				 a.TestID,a.TargetID,
				 a.EventDate,a.MessageID,a.ExampleID,
				 a.SerialNumber,a.ResultFlag,a.OraginMsg,
				 a.OutMsg,a.MediaType,a.MediaData,a.Note,
				 a.IsAlarm,a.AlarmDescription
  from eventlog a
	where a.TestID = testIdStr;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_factory_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_factory_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_factory_syn_data`(IN testID varchar(30))
BEGIN
	
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
  SELECT CONCAT(test_org_id,d.FactoryID) as FactoryID,
	a.FactoryName,a.Linker,a.Telephone,a.provinceID,a.Address,a.Note
	FROM factory a
  LEFT JOIN target b on a.FactoryID = b.FactoryID
	LEFT JOIN targetdetail c on b.TargetbatchID = c.TargetbatchID
	LEFT JOIN targettest d on c.TargetID = d.TargetID
	WHERE d.TargetID = testIdStr;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_testreport_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_testreport_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_testreport_syn_data`(IN testID varchar(30))
BEGIN
	
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
	SELECT CONCAT(test_org_id,a.ReportID) as ReportID,
				 a.TestID,
				 a.ExampleID,a.EnvironmentID,a.TestProcess,
				 a.Reporter,a.ReportDate,a.TemplateResult,
				 a.ReportResult,a.Reason,a.FunctionID
	from testreport a
	where a.TestID = testIdStr;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress0_case1_check1_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress0_case1_check1_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress0_case1_check1_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x1001 主链路登录消息
		#0x9002 从链路登录应答
		#判定是否收到被测下级平台发送的 主链路登录消息 从链路登录应答
		call pro_check_messages_num(searchTime ,reportID , '0x1001', '0x9002', 1);
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress0_case1_check2_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress0_case1_check2_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress0_case1_check2_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
		#0x1005 主链路登录消息
		#0x9006 从链路登录应答
		#判定是否收到被测下级平台发送的 主链路心跳 从链路心跳包应答 各两次
		call pro_check_messages_num(searchTime ,reportID , '0x1005', '0x9006', 2);
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress0_case1_check3_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress0_case1_check3_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress0_case1_check3_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
		#0x1001 注销主链路消息
		#0x9002 注销从链路应答
		#判定是否收到被测下级平台发送的 注销主链路消息 注销从链路应答
		call pro_check_messages_num(searchTime ,reportID , '0x1003', '0x9004', 1);
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress0_case2_check2_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress0_case2_check2_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress0_case2_check2_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN

    -- 9002 从链路连接应答
    -- 是否收到从链路连接应答失败消息
	  call pro_check_message_and_xml_unequal(searchTime,reportID,'0x9002','/id_9002/RESULT','0');

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress11_case1_check1_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress11_case1_check1_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress11_case1_check1_gcy_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#Routine body goes here...
  call pro_check_message_exist(searchTime ,reportID , '0x1302');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress2_case1_check1_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress2_case1_check1_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress2_case1_check1_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#0x1209 主链路补发车辆定位信息请求
	#判定是否收到下级平台发送的补发车辆定位信息请求消息
	call pro_check_message_exist(searchTime ,reportID , '0x1209');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check1_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check1_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check1_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x1201 车辆注册信息
		#判定是否收到被测下级平台发送的车辆注册消息
		call pro_check_message_exist(searchTime ,reportID , '0x1201');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check2_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check2_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check2_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x1202 下级平台时时上报车辆定位信息消息
		#判定至少收到3条下级平台发送的车辆定位消息
		call pro_check_message_num(searchTime ,reportID , '0x1202', 3);
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check3_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check3_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check3_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#0x1601 补发车辆静态消息应答
	#判定是否收到下级平台发送的车辆静态消息
	call pro_check_message_exist(searchTime ,reportID , '0x1601');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check4_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check4_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check4_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#判断是否收到下级平台发送的连接请求
	#0x1001 主链路连接申请
	call pro_check_message_exist(searchTime ,reportID , '0x1001');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check5_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check5_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check5_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#判断是否收到下级平台补发的车辆定位信息至少5条
	#0x1203 车辆定位信息补发请求
	call pro_check_message_num(searchTime ,reportID , '0x1203',5);
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check6_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check6_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check6_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#判断是否收到下级平台发送的报警信息
	#报警信息消息号 0x1402
	CALL pro_check_message_and_xml(searchTime ,reportID ,'0x1402', '/id_1402/WARN_TYPE' ,'1');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress3_case1_check7_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress3_case1_check7_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress3_case1_check7_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#判断是否收到督办请求的应答
	#0x1401 报警督办应答
	call pro_check_message_exist(searchTime ,reportID , '0x1401');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check1_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check1_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check1_gcy_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#
  call pro_check_message_and_xml(searchTime,reportID,'0x1501','/id_1501/RESULT','0');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check2_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check2_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check2_gcy_cmp`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32))
BEGIN
	#Routine body goes here...
  call pro_check_message_and_xml(searchTime,reportID,'0x1502','/id_1502/PHOTO_RSP_FLAG','1');

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check3_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check3_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check3_gcy_cmp`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32))
BEGIN
	#Routine body goes here...
  call pro_check_message_and_xml(searchTime,reportID,'0x1503','/id_1503/RESULT','0');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check4_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check4_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check4_gcy_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#Routine body goes here...
  call pro_check_message_exist(searchTime ,reportID , '0x1504');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check5_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check5_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check5_gcy_cmp`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32))
BEGIN
	#
  call pro_check_message_and_xml(searchTime,reportID,'0x1505','/id_1505/RESULT','0');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case1_check6_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case1_check6_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case1_check6_gcy_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#
  call pro_check_message_exist(searchTime ,reportID , '0x1206');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress4_case2_check1_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress4_case2_check1_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress4_case2_check1_gcy_cmp`(IN searchTime VARCHAR(32) , IN reportID  VARCHAR(32))
BEGIN
	#Routine body goes here...
  call pro_check_message_and_xml(searchTime,reportID,'0x1505','/id_1505/RESULT','1');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress5_case1_check1_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress5_case1_check1_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress5_case1_check1_gcy_cmp`(IN searchTime varchar(32),IN reportID varchar(32))
BEGIN
	#
  call pro_check_message_exist(searchTime ,reportID , '0x1205');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress5_case1_check2_gcy_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress5_case1_check2_gcy_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress5_case1_check2_gcy_cmp`(IN searchTime varchar(32),IN reportID varchar(32))
BEGIN
	#
  call pro_check_message_exist(searchTime ,reportID , '0x1206');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check1` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check1` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check1`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);
	DECLARE check1001 int default 0;
	DECLARE check9002 int default 0;
	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur; 
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml; 
			if iMessageId = '0x1001' THEN 
					BEGIN
							set check1001 = 1;
							IF iResultFlag = 1 THEN set check1001 = 0;
							end if;
					END;
			end if;	
			if iMessageId = '0x9002' THEN 
					BEGIN
							set check9002 = 1;
							IF iResultFlag = 1 THEN set check9002 = 0;
							end if;
					END;
			end if;	
			set c = c+1;
  end while; 

  close cur; 
	
	IF check1001 = 1 && check9002 = 1 then set rMsg = 1; 
	end if;
	SELECT rMsg,c,check1001,check9002;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check1_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check1_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress6_case1_check1_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#0x1301 平台查岗应答
	#判断是否收到平台查岗应答
	call pro_check_message_exist(searchTime ,reportID , '0x1301');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check2` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check2` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check2`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		DECLARE iMessageId varchar(20);
		DECLARE iResultFlag int;
		DECLARE iXml varchar(4000);
		DECLARE iTime varchar(32);

		DECLARE rMsg int default 0;
		DECLARE checkData int default 0;

		DECLARE stop int DEFAULT 0;
		DECLARE c int default 0;
		DECLARE num int;
		DECLARE TARGET_MESSAGE_ID VARCHAR(6) DEFAULT '0x1005';

		DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
													 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
													 order by a.eventdate asc;
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  

		SELECT count(*) into num FROM progresstestlog a 
		where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
		order by a.eventdate asc;

		open cur; 
		
		while stop<>1 && c<num do
				fetch cur into iTime,iMessageId,iResultFlag,iXml; 
				if iMessageId = TARGET_MESSAGE_ID THEN 
						set checkData = checkData + 1;
				end if;	
				set c = c+1;
		end while; 

		close cur; 
		IF  checkData >= 1 THEN set rMsg =1;
		end if;
		SELECT rMsg,checkData;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check3` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check3` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check3`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);
	DECLARE check1003 int default 0;
	DECLARE check9004 int default 0;
	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur; 
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml; 
			if iMessageId = '0x1003' THEN 
					BEGIN
							set check1003 = 1;
							IF iResultFlag = 1 THEN set check1003 = 0;
							end if;
					END;
			end if;	
			if iMessageId = '0x9004' THEN 
					BEGIN
							set check9004 = 1;
							IF iResultFlag = 1 THEN set check9004 = 0;
							end if;
					END;
			end if;	
			set c = c+1;
  end while; 

  close cur; 
	
	IF check1003 = 1 && check9004 = 1 then set rMsg = 1; 
	end if;
	SELECT rMsg,c,check1003,check9004;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress7_case1_check1_zw_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress7_case1_check1_zw_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress7_case1_check1_zw_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	#0x120D 电子运单消息
	#判断是否收到下级平台发送的电子运单
	call pro_check_message_exist(searchTime ,reportID , '0x120D');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress8_case1_check1_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress8_case1_check1_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress8_case1_check1_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x120C 驾驶员身份信息上传消息
		#判断是否收到驾驶员身份信息上传消息
		call pro_check_message_exist(searchTime ,reportID , '0x120C');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress9_case1_check1_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress9_case1_check1_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress9_case1_check1_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x1207 申请交换指定车辆定位信息请求
		#判断是否收到交换指定车辆定位信息请求
		call pro_check_message_exist(searchTime ,reportID , '0x1207');
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress9_case1_check2_zb_cmp` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress9_case1_check2_zb_cmp` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`gps`@`%` PROCEDURE `pro_progress9_case1_check2_zb_cmp`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		#0x120C 取消交换指定车辆定位信息请求
		#判断是否收到 取消交换指定车辆定位信息请求
		call pro_check_message_exist(searchTime ,reportID , '0x1208');
END */$$
DELIMITER ;

/* Procedure structure for procedure `queryTargetGroup` */

/*!50003 DROP PROCEDURE IF EXISTS  `queryTargetGroup` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `queryTargetGroup`(minid int,maxid int,itargettype int)
begin
     set @rownum=0;
     select @rownum:=@rownum+1 as rownum,t.* from TargetGroup t where targettype=itargettype and  (groupid>=minid or minid=-1) and (groupid<=maxid or maxid=-1);
end */$$
DELIMITER ;

/* Procedure structure for procedure `tl_getsheetid` */

/*!50003 DROP PROCEDURE IF EXISTS  `tl_getsheetid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `tl_getsheetid`(isheettype int,iaddstring varchar(30))
begin 
    declare iprefix       char(2);          
    declare iresetdate    date;             
    declare iResetFlag    int;              
    declare iserialnumber int;           
    declare iserialcount  int;             
    declare iserialstr    varchar(10);
    declare oSheetID      varchar(30);
    declare idatestr      int;
    
    declare exit handler for sqlexception rollback;
    set oSheetID='';
 
    select prefix,resetdate,resetflag,Serialnumber,serialcount into iprefix,iresetdate,iResetFlag,iserialnumber,iserialcount from 
           serialnumber where sheettype=isheettype;
  
    set iaddstring=trim(iaddstring);
    set iprefix=trim(iprefix);
    
    if iprefix!='' then
        set oSheetID=concat(oSheetID,iprefix);
    end if;
    
    if iaddstring!='' then
        set oSheetID=concat(oSheetID,iaddstring);
    end if;
 
    if iResetFlag=1 then
          if  iresetdate<current_date then
                 update serialnumber set resetdate=current_date,serialnumber=0 where sheettype=isheettype;
                 set iresetdate=current_date;
                 set iserialnumber=0;
          end if;
          set idatestr:=year(iresetdate)*10000+month(iresetdate)*100+day(iresetdate);
          set oSheetID=concat(oSheetID,idatestr);
    end if;
 
    if iResetFlag=2 then
         if (year(iresetdate)*100+month(iresetdate))<(year(current_date)*100+month(current_date)) then
                 update serialnumber set resetdate=current_date,serialnumber=0 where sheettype=isheettype;
                 set iresetdate=current_date;
                 set iserialnumber=0;
         end if;
         set idatestr:=year(iresetdate)*100+month(iresetdate);
         set oSheetID=concat(oSheetID,idatestr);
    end if;

    set iserialnumber=iserialnumber+1;
    update serialnumber set serialnumber=iserialnumber where sheettype=isheettype;
    
    set iserialstr=lpad(iserialnumber,iserialcount,'0');
    set oSheetID=concat(oSheetID,iserialstr);
    
    select oSheetID as outstr;
       
end */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
