/*
SQLyog Ultimate v11.3 (32 bit)
MySQL - 5.1.56-community : Database - testgpsterminal
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`testgpsterminal` /*!40100 DEFAULT CHARACTER SET gbk */;

USE `testgpsterminal`;

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `ConfigName` char(64) COLLATE gbk_bin NOT NULL COMMENT '置配名',
  `ConfigValue` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '置配值',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ConfigName`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='系统参数表';

/*Data for the table `config` */

insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('paramsA1Value','1','加密参数2');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('paramsC1Value','1','加密参数1');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('paramsM1Value','1','加密参数3');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsEnterCode','123456','企业平台接入码');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsEnterprisesIP','192.168.1.234','企业平台IP');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsEnterprisesPort','20000','企业平台端口号');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsProtocolVersion','1.0','协议版本号');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsPwd','12345678','密码');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('parmsUserName','123456','用户ID');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('检测机构代码','bjhsjc','检测机构代码/简称');
insert  into `config`(`ConfigName`,`ConfigValue`,`Note`) values ('终端鉴权码','jtpassword','默认的终端鉴权码,暂定测试系统所有终端都使用此鉴权码进行鉴权');

/*Table structure for table `environment` */

DROP TABLE IF EXISTS `environment`;

CREATE TABLE `environment` (
  `EnvironmentID` int(11) NOT NULL,
  `EnvironmentName` varchar(64) COLLATE gbk_bin NOT NULL,
  `EnvironmentDesc` varchar(500) COLLATE gbk_bin DEFAULT NULL,
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `TestFlag` int(11) DEFAULT '1',
  PRIMARY KEY (`EnvironmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试环境表';

/*Data for the table `environment` */

insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (1,'检测平台协议',NULL,1,1);
insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (2,'终端环境',NULL,2,1);
insert  into `environment`(`EnvironmentID`,`EnvironmentName`,`EnvironmentDesc`,`UserFlag`,`TestFlag`) values (3,'检测平台流程',NULL,1,1);

/*Table structure for table `eventlog` */

DROP TABLE IF EXISTS `eventlog`;

CREATE TABLE `eventlog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `Flag` int(11) NOT NULL COMMENT '0=系统错误日志 1=上行 2=下行',
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标',
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL DEFAULT '0' COMMENT '测试编号',
  `EventDate` datetime NOT NULL COMMENT '发生时间',
  `ExampleID` int(11) DEFAULT NULL COMMENT '用例编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `SerialNumber` int(11) NOT NULL COMMENT '消息流水号',
  `ResultFlag` int(11) DEFAULT NULL COMMENT '0=正常 1=错误 2=不能识别',
  `OraginMsg` longblob COMMENT '解析前的原始数据',
  `OutMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '日志描叙',
  `MediaType` int(11) DEFAULT NULL COMMENT '0=无附加数据 1=图片 2=多媒体（声音/视频）',
  `MediaData` longblob COMMENT '存储多媒体图片数据',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `IsAlarm` int(11) DEFAULT NULL,
  `AlarmDescription` varchar(200) COLLATE gbk_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Index1_EventLog` (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试日志表';

/*Data for the table `eventlog` */

/*Table structure for table `example` */

DROP TABLE IF EXISTS `example`;

CREATE TABLE `example` (
  `ExampleID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否只读，默认为1：只读',
  `FunctionID` int(11) NOT NULL COMMENT '功能ID',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定 ',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '答应结果',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='统系用例表';

/*Data for the table `example` */

insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (18,'从链路连接请求消息用例',1,93,0,0,10,'AAAABQ==','校验码，正常时与主链路登录应答消息中的校验码相同。','[DWORD]5\r\n','从链路返回连接结果为以下信息中的一个：\r\n成功；\r\n校验码错误；\r\n资源紧张，稍后再连接（已经占用）；\r\n其他。','0x9002','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (19,'从链路连接保持请求消息用例',1,94,1,0,120,'','空','','返回数据体为空，消息ID为0x9006。','0x9006','下发从链路连接保持请求消息。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (20,'从链路注销请求消息用例',1,100,0,0,5,'\0\0?','','[DWORD]4231\r\n','','0x9003','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (21,'主链路注销应答消息',1,101,0,1,2,'\0\0A','主链路注销','[DWORD]321\r\n','','主链路注销?','确定主链路注销','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (28,'主链路登录用例',1,85,0,1,30,'','下级平台主动上传链路登录请求','','下级平台上传以下信息：\r\n1.用户名；\r\n2.密码；\r\n3.对应从链路服务端IP地址；\r\n4.对应从链路服务端口号。','0x1001','请上传用户名和密码等登录信息。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (29,'主链路连接保持用例',1,87,1,1,120,'','主链路连接保持','','下级平台向上级平台发送主链路连接保持请求消息。','0x1005','执行此用例前请先提示下级平台登录上级平台，此用例检测过程中企业平台不作任何操作','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (30,'主链路动态信息交换数据体用例',1,137,1,1,20,'','','','','0x1200','请上传车辆动态信息交换业务数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (31,'上传车辆注册信息用例',1,138,1,1,20,'','上传车辆注册信息','','下级平台主动上传车辆注册信息：\r\n车牌号\r\n车牌颜色\r\n平台唯一编码\r\n车载终端厂商唯一编码\r\n车载终端型号\r\n车载终端编号\r\n车载终端SIM卡号','0x1201','请上传车辆注册信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (32,'实时上传车辆定位信息用例',1,155,1,1,60,'','实时上传车辆定位信息','','返回实时上传车辆定位信息请求指令\r\n包括车牌号、车牌颜色、定位数据','0x1202','请确保有送检平台有车辆在线，且有位置信息汇报','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (33,'车辆定位信息自动补报请求用例',1,139,1,1,120,'','','','返回车辆定位信息：\r\n车牌号\r\n车牌颜色\r\n定位数据（大于等于1条，小于等于5条）','0x1203','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络\r\n','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (34,'申请交换指定车辆定位信息请求',1,140,1,1,30,'','请上传申请交换指定车辆定位信息','','返回申请交换指定车辆定位信息指令，\r\n包括：车牌号、车牌颜色、开始时间、结束时间','0x1207','请上传申请交换指定车辆定位信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (35,'取消交换指定车辆定位信息请求用例',1,143,1,1,40,'','取消交换指定车辆定位信息请求','','返回取消交换指定车辆定位信息请求指令\r\n包括：车牌号、车牌颜色','0x1208','请取消之前申请监控的特殊车辆','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (36,'补发车辆定位信息请求',1,142,1,1,30,'','补发车辆定位信息','','返回补发车辆定位信息请求指令，\r\n包括车牌号、车牌颜色、开始时间、结束时间','0x1209','1.请断开送检平台与检测平台间的网络2分钟\r\n2.2分钟后请重新连上送检平台与检测平台间的网络','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (37,'从链路车辆动态信息数据体用例',1,109,0,2,2,'vqlRMTIzNDUAAAAAAAAAAAAAAAAAAZIBAAAABAAAEjQ=','车牌号=“京Q12345”；\r\n后续数据长度=4；\r\n数据=1234','[STRING|21]京Q12345\r\n[BYTE|1]1\r\n[WORD]37377\r\n[DWORD]0004\r\n[BYTE|4]1234\r\n','根据具体消息不同返回不同的内容','0x9200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (38,'交换车辆定位信息用例',1,110,0,1,10,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApICAAAAJAAHBwfZAAAAAAAAEAAAABAAEAAQABAAEAAQABAAAAAAAAAAAA==','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n车辆当前总里程数（4）\r\n方向（2）\r\n海拔高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37378\r\n[DWORD]36\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000000\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (39,'车辆定位信息交换补发消息用例',1,111,0,1,10,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIDAAAAtQUABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAEABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAIABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAMABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAQABwcH2QAAAAAAABAAAAAQABAAEAAQABAAEAAQAAAAAAAAAAU=','车辆定位信息\r\n车牌号\r\n车辆颜色\r\n加密标识（1）\r\n日期（4）\r\n时间（3）\r\n经度（4）\r\n纬度（4）\r\n速度（2）\r\n行驶记录速度（2）\r\n当前总里程数（4）\r\n方向（2）\r\n高度（2）\r\n车辆状态（4）\r\n报警状态（4）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37379\r\n[DWORD]181\r\n[BYTE|1]5\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000001\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000002\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000003\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000004\r\n[BYTE|36]00070707D900000000000010000000100010001000100010001000100000000000000005\r\n','','','本条消息无需下级平台应答，只是作为一般信息交互使用，直接判定为通过即可','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (40,'启动车辆定位信息交换请求用例',1,113,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIFAAAAAQA=','车牌号=测0002；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=00（车辆进入指定区域）','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37381\r\n[DWORD]1\r\n[BYTE|1]00\r\n','返回的消息业务子类型为1205','0x1200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (41,'结束车辆定位信息交换请求',1,114,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIGAAAAAQE=','车牌号=测0002；\r\n车牌颜色编码=2（黄色）；\r\n启动定位信息交换原因=01（人工停止交换）\r\n','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37382\r\n[DWORD]1\r\n[BYTE|1]01\r\n','返回的消息消息ID为1200，业务子类型为1206','0x1200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (42,'上报驾驶员身份识别信息请求用例',1,118,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApIKAAAAAA==','车牌号=测0002\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37386\r\n[DWORD]0\r\n','消息业务子类型为120A\r\n内容如下：\r\n车牌号\r\n车牌颜色\r\n驾驶员姓名\r\n身份证编号\r\n从业资格证号\r\n发证机构名称','0x1200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (43,'上报车辆电子运单请求用例',1,119,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApILAAAAAA==','车牌号=测0002\r\n车牌颜色编码=2（黄色）\r\n数据长度=0','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37387\r\n[DWORD]0\r\n','返回消息业务子类型为120B\r\n消息内容：\r\n车牌号\r\n车牌颜色\r\n电子运单数据体长度\r\n电子运单数据内容','0x1200','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (44,'主链路数据体用例',1,147,1,1,20,'','','','返回上传平台间交互信息数据体指令','0x1300','请上传平台间交互信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (46,'从链路平台信息交互数据用例',1,120,1,0,20,'?\0\0\0\0\0\054','子业务类型标识=0x9300\r\n数据=0','[WORD]37632\r\n[DWORD]5\r\n[BYTE|5]3534\r\n','','0x9300','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (59,'平台查岗请求用例',1,121,0,0,30,'kwEAAAAQAACTAQAAAAiy6bjaAAAAAA==','信息ID=0x9301\r\n信息长度=8\r\n信息内容=“查岗”','[WORD]37633\r\n[DWORD]16\r\n[DWORD]37633\r\n[DWORD]8\r\n[STRING|8]查岗\r\n','返回平台查岗应答，消息的业务子类型为1301','0x1300','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (61,'下发平台间报文请求用例',1,122,0,0,30,'kwIAAAAZAACTAgAAABHPwreixr3MqLzksajOxAAAAA==','子业务数据类型=0x9302\r\n信息ID=0x9302\r\n信息长度=17\r\n信息内容=“下发平台间报文”','[WORD]37634\r\n[DWORD]25\r\n[DWORD]37634\r\n[DWORD]17\r\n[STRING|17]下发平台间报文\r\n','返回下发平台间报文应答，业务子类型为1302','0x1300','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (62,'主链路车辆报警数据用例',1,149,1,1,20,'','主链路车辆报警数据','','返回主链路车辆报警信息业务数据体指令。','0x1400','请上传主链路车辆报警数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (91,'1.1.1终端注册成功',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=“10001”\r\n终端型号=“12345678”\r\n终端ID=“7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=京A12345','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答 注册成功；鉴权码=XXX','8100','1、确认企业平台中以下资料未被注册：\r\n终端ID=”7654321”\r\n车牌=”京A12345”\r\n2、在模拟终端发送终端注册指令\r\n3、观察模拟终端是否返回“成功；鉴权码=XXX”\r\n4、保存“鉴权码=XXX”的信息，以备“终端鉴权”功能测试。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (95,'上报报警信息用例',1,150,1,1,40,'','上报报警信息用例','','返回上报报警信息指令，\r\n包括车牌号、车牌颜色、报警信息来源、报警类型、报警时间、信息ID、报警信息内容','0x1402','请上传报警信息','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (98,'3.1.1文本信息下发',1,196,0,3,30,'Hb27zaiyv9Ct0unOxLG+0MXPos/Ct6LWuMHusuLK1KO6MTIzo6xBQkNEo6yjoUAjo6QlLi4uLi4=','1、从平台逐个下发所有类型的文本信息指令，下发内容譬如：“交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....”；\r\n2、观察模拟终端显示的标志和文本信息；\r\n3、观察平台接收到的终端应答指令。\r\n','[BYTE|1]1D\r\n[STRING|55]交通部协议文本信息下发指令测试：123，ABCD，！@#￥%.....\r\n','通用应答','0001','1、模拟终端显示的标志和文本信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (99,'1.1.2终端注册失败',1,186,0,3,30,'ABEAADEwMDAxMTIzNDU2Nzg3NjU0MzIxAb6pQTEyMzQ1','省域ID=11（北京）\r\n市县域ID=0000（北京）\r\n制造商ID=”10001”\r\n终端型号=”12345678”\r\n终端ID=”7654321”\r\n车牌颜色=1（蓝色）\r\n车牌=”京A12345”','[WORD]17\r\n[WORD]0\r\n[BYTE|5]3130303031\r\n[BYTE|8]3132333435363738\r\n[BYTE|7]37363534333231\r\n[BYTE|1]1\r\n[STRING|8]京A12345\r\n','终端注册应答：注册失败','8100','1、在模拟终端发送终端注册指令\r\n2、观察模拟终端是否返回“注册失败”\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (100,'从链路报警数据用例',1,124,1,0,20,'vqlRNTQzMjEAAAAAAAAAAAAAAAAAA5QAAAAACgAAAAAAAAADRlQ=','车牌号=“京Q54321”\r\n车牌颜色编码=3（黑色）\r\n子业务数据类型=0x9400\r\n数据=0x9400','[STRING|21]京Q54321\r\n[BYTE|1]3\r\n[WORD]37888\r\n[DWORD]10\r\n[BYTE|10]34654\r\n','','0x9400','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (104,'报警督办请求用例',1,125,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQBAAAAXAIACQAAAABOBVmyAAAOqgAAAABOpjCyAFN1cGVydmlzb3IAAAAAAAAwMTA4ODQ1Njc4OQAAAAAAAAAAAHN1cGVydmlzb3JfZUBzdXBlci5jb20AAAAAAAAAAAAA','车牌号=“测0002”\r\n车牌颜色编号=2（黄色）\r\n报警信息来源=0x02（企业监控平台）\r\n报警类型=0x0009（盗警）\r\n报警时间=2011-06-25 11:44:50\r\n报警督办ID=0x9401\r\n督办截止时间=2011-10-25 11:44:50\r\n督办级别=0x00（紧急）\r\n督办人=“Supervisor”\r\n督办人联系电话=“01088456789”\r\n督办联系电子邮件=“supervisor_e@super.com”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37889\r\n[DWORD]92\r\n[BYTE|1]02\r\n[WORD]9\r\n[TIME_T|8]2011-06-25 11:44:50\r\n[DWORD]003754\r\n[TIME_T|8]2011-10-25 11:44:50\r\n[BYTE|1]00\r\n[STRING|16]Supervisor\r\n[STRING|20]01088456789\r\n[STRING|32]supervisor_e@super.com\r\n','返回报警督办应答，业务子类型为1401\r\n\r\n返回消息参考如下：\r\n车牌号=“测0002”\r\n车牌颜色编号\r\n报警督办ID\r\n报警处理结果=0x00（处理中）\r\n/报警处理结果=0x01（已处理完毕）\r\n/报警处理结果=0x02（不作处理）\r\n/报警处理结果=0x02（将来处理）','0x1400','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (105,'1.2.1终端注销成功',1,187,0,3,30,'','终端注销','','通用应答','0001','1、在模拟终端发送终端注销指令\r\n2、观察平台是否下发通用应答：成功\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (110,'1.3.1终端鉴权成功',1,188,0,3,30,'MTIzNDU2Nzg5MEE=','1、前提：终端注册成功；\r\n2、使用注册时平台下发的鉴权码进行鉴权；\r\n鉴权码：\"1234567890A\"','[STRING|11]1234567890A\r\n','通用应答：鉴权成功','0001','1、在模拟终端发送终端鉴权指令\r\n2、观察平台是否下发通用应答：成功','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (112,'1.4.1终端心跳',1,189,0,3,30,'','1、前提：终端鉴权成功；\r\n2、终端开始按照默认时间间隔发送心跳，平台应答心跳；','','通用应答：平台的心跳应答','8001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (117,'3.2.1追加事件',1,197,0,3,30,'AgIBCLO1wb65ytXPAgSxqdPq','追加事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：暴雨','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]2\r\n[BYTE|1]4\r\n[STRING|4]暴雨\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (118,'3.2.2删除特定事件',1,197,0,3,30,'BAEBAA==','删除特定事件：\r\n设置总数：1\r\n事件项列表：\r\n事件ID：1\r\n事件内容长度：0','[BYTE|1]4\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台设置后结果相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (119,'3.3.1事件报告',1,198,0,3,30,'AQ==','事件报告：\r\n事件ID：1','[BYTE|1]1\r\n','平台通用应答','8001','模拟终端上传的事件报告，平台能正确显示。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (120,'6.1.1更新圆形区域',1,206,0,0,30,'AAEAAAABAD4CV6ugBu6Y4AAAA+gAZAo=','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n中心点纬度=39.3度\r\n中心点经度=116.3度\r\n半径=1000米\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]00\r\n[BYTE|1]01\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]39300000\r\n[DWORD]116300000\r\n[DWORD]1000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (121,'6.2.1删除圆形区域',1,207,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除圆形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (122,'1.5.1设置终端参数',1,190,0,3,30,'BgAAAAEEAAAAPAAAACAEAAAAAAAAAEALMDEwODc2NTQzMjEAAABBETEzNjAxMDEyMzQ1AAAARQQAAAAAAAAARgQAAAA8','平台下发如下参数\r\n心跳间隔=60s，\r\n位置汇报策略=位置汇报，\r\n监控平台电话号码=01087654321，\r\n复位电话号码=13601012345，\r\n终端电话接听策略=自动接听，\r\n每次最长通话时间=60s','[BYTE|1]6\r\n[DWORD]1\r\n[BYTE|1]4\r\n[DWORD]60\r\n[DWORD]32\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]64\r\n[BYTE|1]B\r\n[STRING|11]01087654321\r\n[DWORD]65\r\n[BYTE|1]11\r\n[STRING|11]13601012345\r\n[DWORD]69\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]70\r\n[BYTE|1]4\r\n[DWORD]60','通用应答：成功','8001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (123,'3.4.1提问下发',1,199,0,3,30,'GBS1scews7XBvsrHt/HT0LnK1c+jvwEAAsrHAgACt/E=','提问下发：\r\n标志：0x14：终端TTS播读，广告屏显示；\r\n问题内容长度：20\r\n问题：当前车辆是否有故障？\r\n候选答案列表：\r\n答案ID：1\r\n答案内容长度：2\r\n答案：是\r\n答案ID：2\r\n答案内容长度：2\r\n答案：否','[BYTE|1]18\r\n[BYTE|1]14\r\n[STRING|20]当前车辆是否有故障？\r\n[BYTE|1]1\r\n[WORD]2\r\n[STRING|2]是\r\n[BYTE|1]2\r\n[WORD]2\r\n[STRING|2]否\r\n','终端通用应答','0001','1、模拟终端显示的提问内容与平台下发相同；\r\n2、平台正确接收模拟终端的通用应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (124,'6.3.1更新矩形区域',1,208,0,0,30,'AAEAAAABAD4CYruoBurizAJb8WAG9Yo4AGQK','设置属性=0\r\n区域总数=1\r\n区域ID=1\r\n区域属性=0x003E\r\n左上点纬度=40.025度\r\n左上点经度=116.05678度\r\n右下点纬度=39.58度\r\n右下点经度=116.755度\r\n最高速度=100公里/小时\r\n超速持续时间=10秒','[BYTE|1]0\r\n[BYTE|1]1\r\n[DWORD]1\r\n[WORD]62\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n[WORD]100\r\n[BYTE|1]0A\r\n','成功','0001','1、平台下发设置矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (126,'3.8.1提问应答',1,231,0,3,30,'','提问应答：\r\n应答流水号：对应平台下发的提问的流水号；\r\n答案ID：1\r\n','','','','平台能解析出终端的的提问应答，答案ID是1；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (127,'从链路车辆静态信息交换业务',1,134,1,2,20,'y9VBMDAwMDEAAAAAAAAAAAAAAAAAMZYAAQ==','车牌号=“苏A00001”\r\n车牌颜色=1(蓝色)','[STRING|21]苏A00001\r\n[BYTE|1]31\r\n[WORD]38400\r\n[BYTE|1]1\r\n','','0x9600','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (128,'主链路车辆静态信息数据用例',1,154,1,1,20,'','主链路车辆静态信息数据','','返回主链路车辆静态信息数据指令','0x1600','请上传车辆静态信息数据','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (129,'主链路监管数据用例',1,152,1,1,20,'','主链路监管数据','','返回主链路监管业务数据指令','0x1500','请在送检企业平台向监管平台发送车辆监管业务','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (130,'补报车辆静态信息请求消息用例',1,135,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApYBAAAAAA==','车牌号=“测0002”\r\n车牌颜色=2(黄色)','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38401\r\n[DWORD]00000000\r\n\r\n','返回补报车辆静态信息应答，业务子类型为0x1601\r\n\r\n车辆静态信息：\r\n车牌号\r\n车牌颜色\r\n车辆类型\r\n运输行业编码\r\n车籍地\r\n业户ID\r\n业户名称\r\n业户联系电话','0x1600','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (131,'6.4.1删除矩形区域',1,209,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除矩形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (132,'3.5.1信息点播菜单追加',1,200,0,3,30,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻\r\n','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (133,'从链路监管数据用例',1,128,1,2,20,'tKhBNTQzMjEAAAAAAAAAAAAAAAAAOZUAAAAABQAAASNF','车牌号=“川A54321”\r\n车牌颜色编号=9（其他）\r\n监管数据=0x12345','[STRING|21]川A54321\r\n[BYTE|1]39\r\n[WORD]38144\r\n[DWORD]5\r\n[BYTE|5]12345\r\n','','0x9500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (134,'3.6.1信息点播',1,201,0,3,30,'','信息点播\r\n信息类型：1\r\n点播/取消标志：1，点播；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、平台能定期根据终端点播的信息，下发点播的相关信息。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (135,'车辆单向监听请求用例',1,129,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUBAAAAFDAxMDg4Nzg5NjUtNjc1NAAAAAAA','车牌号=“测0002”\r\n车牌颜色=2（黄色）\r\n回拨电话号码=“0108878965-6754”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38145\r\n[DWORD]20\r\n[STRING|20]0108878965-6754\r\n','返回车辆单向监听应答，业务子类型为1501\r\n返回消息内容参考如下：\r\n车牌号=“测0002”\r\n车牌颜色=4（黄色）\r\n应答结果=0x00（监听成功）\r\n/应答结果=0x01（监听失败）','0x1500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (136,'6.5.1更新多边形区域',1,210,0,0,60,'AAAAAQA+AGQKAAQCW/FgBurizAJiu6gG6uLMAmK7qAb1ijgCW/FgBvWKOA==','区域ID=1\r\n区域属性=0x003E\r\n最高速度=100公里/小时\r\n超速持续时间=10秒\r\n区域总顶点数=4\r\n顶点纬度=39.58度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.05678度\r\n顶点纬度=40.025度\r\n顶点经度=116.755度\r\n顶点纬度=39.58度\r\n顶点经度=116.755度','[DWORD]1\r\n[WORD]62\r\n[WORD]100\r\n[BYTE|1]0A\r\n[WORD]4\r\n[DWORD]39580000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116056780\r\n[DWORD]40025000\r\n[DWORD]116755000\r\n[DWORD]39580000\r\n[DWORD]116755000\r\n','成功','0001','1、平台下发设置多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (137,'3.7.1信息服务',1,202,0,3,30,'AQAqz8LO57Gxvqm12Mf4vavT0MDX0+rM7Mb4o6zH68zhx7DX9rrD17yxuKGj','信息类型：1\r\n信息长度：42\r\n信息内容：下午北京地区将有雷雨天气，请提前做好准备。','[BYTE|1]1\r\n[WORD]42\r\n[STRING|42]下午北京地区将有雷雨天气，请提前做好准备。\r\n','终端通用应答','0001','1、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (139,'6.6.1删除多边形区域',1,211,0,0,30,'AQAAAAE=','区域数=1\r\n区域ID1=1','[BYTE|1]01\r\n[DWORD]1\r\n','成功','0001','1、平台下发删除多边形区域指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (140,'车辆拍照请求用例',1,130,0,0,60,'vqlBNTQzMjEAAAAAAAAAAAAAAAAAMZUCAAAAAgkI','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n镜头ID=0x09\r\n照片大小=0x08（704*576）','[STRING|21]京A54321\r\n[BYTE|1]31\r\n[WORD]38146\r\n[DWORD]2\r\n[BYTE|1]09\r\n[BYTE|1]08\r\n','车牌号=“京A54321”\r\n车牌颜色编码=4（其他）\r\n拍照应答标识=0x00（不支持拍照相）\r\n/拍照应答标识=0x01（完成拍照）\r\n/拍照应答标识=0x02（完成拍照、照片数据稍后传送）\r\n/拍照应答标识=0x03（未拍照不在线）\r\n/拍照应答标识=0x04（未拍照无法使用指定镜头）\r\n/拍照应答标识=0x05（未拍照其他原因）\r\n/拍照应答标识=0x09（车牌号码错误）\r\n拍照位置点\r\n镜头ID=0x09\r\n图片长度\r\n图片大小==0x08（704*576）\r\n图片格式=0x01（jpg）\r\n/图片格式=0x02（gif）\r\n/图片格式=0x03（tiff）\r\n/图片格式=0x04（png）\r\n图片内容','0x9502','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (141,'下发车辆报文请求用例',1,131,0,0,60,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUDAAAADwAAAAEAAAAABr30vLEAAA==','车牌号=“测0002”\r\n车牌颜色=2（黄色）\r\n报文优先级=0x00\r\n报文内容=6','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38147\r\n[DWORD]15\r\n[DWORD]1\r\n[BYTE|1]00\r\n[DWORD]6\r\n[STRING|6]紧急\r\n','返回下发车辆报文应答，业务子类型为0x1503\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n消息ID=1500\r\n应答结果=0x00（下发成功）\r\n/应答结果=0x01（下发失败）','0x1500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (142,'下发车辆行驶记录请求用例',1,132,0,0,30,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUEAAAAEAAAAABOBY/CAAAAAE/oFMI=','车牌号=“测0002”\r\n开始时间=2011-06-25 15:35:30\r\n结束时间=2012-06-25 15:35:30','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38148\r\n[DWORD]16\r\n[TIME_T|8]2011-06-25 15:35:30\r\n[TIME_T|8]2012-06-25 15:35:30\r\n','返回上报车辆行驶记录应答，业务子类型为1504\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n车辆行驶记录信息','0x1500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (143,'1.6.1查询终端参数',1,191,0,3,90,'','查询终端参数','','查询终端参数应答：59个参数\r\n参数总数=59\r\n参数ID=0x0001,参数长度=4,参数值=60;\r\n参数ID=0x0002,参数长度=4,参数值=20;\r\n参数ID=0x0003,参数长度=4,参数值=3;\r\n参数ID=0x0004,参数长度=4,参数值=25;\r\n参数ID=0x0005,参数长度=4,参数值=2;\r\n参数ID=0x0006,参数长度=4,参数值=86400;\r\n参数ID=0x0007,参数长度=4,参数值=1;\r\n参数ID=0x0010,参数长度=5,参数值=\"CMNET\";\r\n参数ID=0x0011,参数长度=4,参数值=\"card\";\r\n参数ID=0x0012,参数长度=4,参数值=\"card\";\r\n参数ID=0x0013,参数长度=9,参数值=\"127.0.0.1\";\r\n参数ID=0x0014,参数长度=7,参数值=\"CSYL.BJ\";\r\n参数ID=0x0015,参数长度=4,参数值=\"CSBJ\";\r\n参数ID=0x0016,参数长度=4,参数值=\"BJCS\";\r\n参数ID=0x0017,参数长度=13,参数值=\"192.168.1.106\";\r\n参数ID=0x0018,参数长度=4,参数值=6500;\r\n参数ID=0x0019,参数长度=4,参数值=6580;\r\n参数ID=0x0020,参数长度=4,参数值=0;\r\n参数ID=0x0021,参数长度=4,参数值=0;\r\n参数ID=0x0022,参数长度=4,参数值=300;\r\n参数ID=0x0027,参数长度=4,参数值=600;\r\n参数ID=0x0028,参数长度=4,参数值=10;\r\n参数ID=0x0029,参数长度=4,参数值=60;\r\n参数ID=0x002C,参数长度=4,参数值=1000;\r\n参数ID=0x002D,参数长度=4,参数值=2000;\r\n参数ID=0x002E,参数长度=4,参数值=500;\r\n参数ID=0x002F,参数长度=4,参数值=500;\r\n参数ID=0x0030,参数长度=4,参数值=130;\r\n参数ID=0x0040,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0041,参数长度=11,参数值=\"13601012345\";\r\n参数ID=0x0042,参数长度=11,参数值=\"18876543210\";\r\n参数ID=0x0043,参数长度=12,参数值=\"106590202345\";\r\n参数ID=0x0044,参数长度=11,参数值=\"13901011000\";\r\n参数ID=0x0045,参数长度=4,参数值=0;\r\n参数ID=0x0046,参数长度=4,参数值=60;\r\n参数ID=0x0047,参数长度=4,参数值=3600;\r\n参数ID=0x0048,参数长度=11,参数值=\"01087654321\";\r\n参数ID=0x0049,参数长度=12,参数值=\"106590202347\";\r\n参数ID=0x0050,参数长度=4,参数值=0;\r\n参数ID=0x0051,参数长度=4,参数值=0x0000000F;\r\n参数ID=0x0052,参数长度=4,参数值=0;\r\n参数ID=0x0053,参数长度=4,参数值=0;\r\n参数ID=0x0054,参数长度=4,参数值=0x000000FF;\r\n参数ID=0x0055,参数长度=4,参数值=120;\r\n参数ID=0x0056,参数长度=4,参数值=30;\r\n参数ID=0x0057,参数长度=4,参数值=14400;\r\n参数ID=0x0058,参数长度=4,参数值=36000;\r\n参数ID=0x0059,参数长度=4,参数值=900;\r\n参数ID=0x005A,参数长度=4,参数值=600;\r\n参数ID=0x0070,参数长度=4,参数值=6;\r\n参数ID=0x0071,参数长度=4,参数值=128;\r\n参数ID=0x0072,参数长度=4,参数值=70;\r\n参数ID=0x0073,参数长度=4,参数值=60;\r\n参数ID=0x0074,参数长度=4,参数值=255;\r\n参数ID=0x0080,参数长度=4,参数值=983490;\r\n参数ID=0x0081,参数长度=4,参数值=17;\r\n参数ID=0x0082,参数长度=4,参数值=0;\r\n参数ID=0x0083,参数长度=8,参数值=\"京A12345\";\r\n参数ID=0x0084,参数长度=4,参数值=1;','0104','平台对照“返回消息”逐一检查读取的59个参数是否与终端上报参数相同','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (144,'车辆应急接入监管平台请求用例',1,133,0,0,40,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApUFAAAAkQAAAAAAAACyEN+3/s7xxvdBUE4AAAAAAAAAAAAAAEFkbWluaVVzZXIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyM3JzZDMyNDUAAAAAAAAAAAAAAAAAMTkyLjE2OC4xLjE4NAAAAAAAAAAAAAAAAAAAAAAAAAAiYB+QAAAAAbG18DM=','车牌号=“测0002”\r\n鉴权码=“B210DF”\r\n拨号点名称=“服务器APN”\r\n拨号用户名=“AdminiUser”\r\n拨号密码=23rsd3245\r\n服务器IP=“192.168.1.184”\r\nTCP端口=“8800”\r\nUDP端口=“8080”\r\n结束时间=“2012-08-01 14:20:35”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]38149\r\n[DWORD]145\r\n[BYTE|10]B210DF\r\n[STRING|20]服务器APN\r\n[STRING|49]AdminiUser\r\n[STRING|22]23rsd3245\r\n[STRING|32]192.168.1.184\r\n[WORD]8800\r\n[WORD]8080\r\n[TIME_T|8]2200-08-01 14:20:35\r\n','返回车辆应急接入监管平台应答，消息业务子类型为0x1505\r\n\r\n返回信息参考如下：\r\n车牌号=“测0002”\r\n应答结果=0x00（车载终端成功收到该命令）\r\n/应答结果=0x01（无该车辆）\r\n/应答结果=0x02（其它原因失败）','0x1500','','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (146,'报警预警消息用例',1,126,0,1,15,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQCAAAAHgMACwAAAABOBZKzAAAAD8arwOvCt8/fsai+rwAAAA==','车牌号=“测0002”\r\n车牌颜色编码=4（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=11\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“偏离路线报警”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37890\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]11\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]偏离路线报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (147,'实时交换报警用例',1,127,0,1,15,'suIwMDAyAAAAAAAAAAAAAAAAAAAAApQDAAAAHgMACAAAAABOBZKzAAAAD9S9veexqL6vAAAAAAAAAA==','车牌号=“测0002”\r\n车牌颜色编码=2（黄色）\r\n报警信息来源=0x03（政府监管平台）\r\n报警类型=08\r\n报警时间=2011-06-25 15:48:03\r\n报警描述=“越界报警”','[STRING|21]测0002\r\n[BYTE|1]2\r\n[WORD]37891\r\n[DWORD]30\r\n[BYTE|1]03\r\n[WORD]08\r\n[TIME_T|8]2011-06-25 15:48:03\r\n[DWORD]15\r\n[STRING|15]越界报警\r\n','无','','本条消息无需应答，只是作为一般信息交互，直接判断结果为通过即可。','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (148,'4.1.1电话回拨',1,203,0,3,60,'ADAxMDg4ODg4ODg4','电话回拨(普通通话):\r\n标志 = 0 \r\n电话号码 = \"01088888888\"','[BYTE|1]0\r\n[STRING|11]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (149,'4.1.2电话回拨_监听',1,203,0,3,60,'ATAxMDg4ODg4ODg4AAAAAAAAAAAA','电话回拨(监听):\r\n标志 : 1\r\n电话号码 : 01088888888','[BYTE|1]1\r\n[STRING|20]01088888888\r\n','终端通用应答：成功','0001','平台下发电话回拨指令，观察模拟终端显示的信息内容是否与平台下发一致','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (150,'4.2.1设置电话本_删除',1,204,0,0,60,'AA==','删除终端上存储的所有联系人:\r\n设置类型 = 0','[BYTE|1]0','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (151,'1.7.1终端控制',1,192,0,3,90,'ATtDTU5FVDs7OzE5Mi4xNjguMS4xMDM7NjAwMDs7MTAwMDE7MS4wMzsxLjAwOzM2MDA=','无限升级指令：\r\nURL地址=;(空)\r\n拨号点名称=\"CMNET\"\r\n拨号用户名=;\r\n拨号密码=;\r\n地址=\"192.168.1.103\"\r\nTCP端口=6000\r\nUDP端口=;\r\n硬件版本=\"1.03\"\r\n固件版本=\"1.00\"\r\n连接到服务器的时限=3600','[BYTE|1]1\r\n[STRING|49];CMNET;;;192.168.1.103;6000;;10001;1.03;1.00;3600\r\n','通用应答：成功','0001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (152,'4.2.2设置电话本_更新电话本',1,204,0,3,60,'AQUBCzEzODEyMzQ1Njc4BNXFyP0CCzEzODEyMzQ1Njc5BMDuy8QDCzEzODEyMzQ1NjgwBM31zuUDDDA3NTUxMjM0NTY3OAbVxcj9y8QDDDA3NjkzMzMzNTU1NQbVxcj9zuU=','更新电话本(删除终端中已有全部联系人并追加消息中的联系人):\r\n设置类型 = 1\r\n联系人总数= 5\r\n联系人项 :\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 11\r\n电话号码 = \"13812345679\"\r\n联系人长度 = 4\r\n联系人 = \"李四\"\r\n\r\n标志 = 3\r\n号码长度 = 11\r\n电话号码 = \"13812345680\"\r\n联系人长度 = 4\r\n联系人 = \"王五\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"张三四\"\r\n\r\n标志 = 3\r\n号码长度 = 12\r\n电话号码 = \"076933335555\"\r\n联系人长度 = 6\r\n联系人 = \"张三五\"\r\n\r\n','[BYTE|1]1\r\n[BYTE|1]5\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0B\r\n[STRING|11]13812345679\r\n[BYTE|1]4\r\n[STRING|4]李四\r\n[BYTE|1]3\r\n[BYTE|1]0B\r\n[STRING|11]13812345680\r\n[BYTE|1]4\r\n[STRING|4]王五\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]张三四\r\n[BYTE|1]3\r\n[BYTE|1]0C\r\n[STRING|12]076933335555\r\n[BYTE|1]6\r\n[STRING|6]张三五\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (153,'6.7.1更新路线',1,212,0,0,30,'AAAAAQA+AAYAAAABAAAAAQJgU0AG71wwZAIAZAoAAAACAAAAAgJgVmAG74NAZAMCWAAKAGQKAAAAAwAAAAICYF1oBu/RYGQDAlgACgBkCgAAAAQAAAACAmBiGAbwPsBkAwJYAAoAZAoAAAAFAAAAAgJgXWgG8E5gZAMCWAAKAGQKAAAABgAAAAICYFw8BvCUsGQDAlgACgBkCg==','路线ID=1\r\n路线属性=62\r\n路线总拐点数=6\r\n\r\n路段ID=1\r\n拐点ID=1，39.867200，116.350000，路宽=100米，属性=2，限速=100公里/小时，超速持续时间=10秒\r\n\r\n路段ID=2\r\n拐点ID=2，39.868000，116.360000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=3，39.869800，116.380000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=4，39.871000，116.408000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=5，39.869800，116.412000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒\r\n拐点ID=6，39.869500，116.430000，路宽=100米，属性=3，行驶过长阀值=600秒，行驶过短阀值=10秒，限速=100公里/小时，超速持续时间=10秒','[DWORD]1\r\n[WORD]62\r\n[WORD]6\r\n[DWORD]1\r\n[DWORD]1\r\n[DWORD]39867200\r\n[DWORD]116350000\r\n[BYTE|1]64\r\n[BYTE|1]2\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]2\r\n[DWORD]2\r\n[DWORD]39868000\r\n[DWORD]116360000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]3\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116380000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]4\r\n[DWORD]2\r\n[DWORD]39871000\r\n[DWORD]116408000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]5\r\n[DWORD]2\r\n[DWORD]39869800\r\n[DWORD]116412000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A\r\n[DWORD]6\r\n[DWORD]2\r\n[DWORD]39869500\r\n[DWORD]116430000\r\n[BYTE|1]64\r\n[BYTE|1]3\r\n[WORD]600\r\n[WORD]10\r\n[WORD]100\r\n[BYTE|1]0A','成功','0001','1、平台下发设置路线指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (154,'6.8.1删除所有路线',1,213,0,0,30,'AA==','路线数=0','[BYTE|1]0\r\n','成功','0001','1、平台下发删除线路指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (155,'4.2.3设置电话本_追加电话本',1,204,0,3,60,'AgIBCzEzODEyMzQ1Njc4BNXFyP0CDDA3NTUxMjM0NTY3OAbA7sj9y8Q=','设置电话本（追加电话本）\r\n设置类型 = 2\r\n联系人总数 = 2\r\n联系人项\r\n\r\n标志 = 1\r\n号码长度 = 11\r\n电话号码 = \"13812345678\"\r\n联系人长度 = 4\r\n联系人 = \"张三\"\r\n\r\n标志 = 2\r\n号码长度 = 12\r\n电话号码 = \"075512345678\"\r\n联系人长度 = 6\r\n联系人 = \"李三四\"\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n[BYTE|1]2\r\n[BYTE|1]0C\r\n[STRING|12]075512345678\r\n[BYTE|1]6\r\n[STRING|6]李三四\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (156,'4.2.4设置电话本_修改电话本',1,204,0,3,60,'13812345678张三','设置电话本（修改电话本，以联系人为索引）\r\n设置类型 = 3\r\n联系人总数 = 1\r\n联系人项\r\n','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0B\r\n[STRING|11]13812345678\r\n[BYTE|1]4\r\n[STRING|4]张三\r\n\r\n','终端通用应答','0001','1、平台下发设置电话本指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (157,'2.1.1手动报警',1,193,0,3,90,'','使用模拟终端中的“4、手动位置上报”,选中紧急告警项。然后点击执行命令按钮','','通用应答：成功','0001','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (158,'2.2.1位置信息查询',1,194,0,3,90,'','查询位置信息','','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。至少一种以上的附件信息测试。','0201','检测平台收到的信息和模拟终端发送的位置信息内容一致','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (159,'2.3.1临时位置跟踪控制',1,195,0,3,180,'ABQAAAB4','时间间隔=10秒\r\n位置跟踪有效期=120秒','[WORD]20\r\n[DWORD]120\r\n','使用模拟终端的“5、定时位置上报”，按照设置的时间间隔及持续时间上报位置信息12次','0001','1、平台下发临时位置跟踪控制指令；\r\n2、模拟终端按“时间间隔”和“跟踪有效期”定时上传位置信息；\r\n3、观察平台显示的信息内容是否与模拟终端上传一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (160,'5.1.1车辆控制_车门解锁',1,205,0,3,60,'AA==','控制标志=0（车门解锁）','[BYTE|1]0\r\n','模拟终端自动回应，平台上收到回应，显示“车门解锁”状态：\r\n应答流水号= \r\n位置信息  =\r\n','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门解锁”状态。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (161,'5.1.2车辆控制_车门加锁',1,205,0,3,60,'AQ==','控制标志=1（车门加锁）','[BYTE|1]1\r\n','模拟终端自动回应，平台上收到回应，显示“车门加锁”状态：\r\n应答流水号= \r\n位置信息  =','0050','1、平台下发车辆控制指令；\r\n2、观察模拟终端显示的信息内容是否与平台发送一致；\r\n3、模拟终端上传位置信息（含车辆控制后的车辆状态）；\r\n4、从平台观察车辆是否处于“车门加锁”状态。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (162,'9.2.1数据上行透传',1,225,0,3,30,'','使用模拟终端中手动用力中的“7、数据上行透传”，设置透传类型已经透传内容进行上传。','','平台通用应答','8001','1、观察平台收到的透传信息是否与模拟终端中输入的透传信息一致。\r\n2、观察平台是否正确返回应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (164,'3.5.2信息点播菜单修改',1,200,0,3,30,'AwIBAAjCt7/20MXPogIACLn6xNrQws7F','追加信息点播菜单\r\n设置类型：修改\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：路况信息\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国内新闻\r\n','[BYTE|1]3\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]路况信息\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国内新闻','终端通用应答','0001','1、平台下发信息点播菜单修改指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (165,'3.5.3信息点播菜单更新',1,200,0,3,30,'AQMBAAixsb6pzOzG+AIACLn6vMrQws7FAgAIwsPTzr3pydw=','追加信息点播菜单\r\n设置类型：更新\r\n信息项总数：3\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：北京天气\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：国际新闻\r\n信息类型：3\r\n信息名称长度：8\r\n信息名称：旅游介绍\r\n','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]北京天气\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]国际新闻\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]旅游介绍','终端通用应答','0001','1、平台下发信息点播菜单更新指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (166,'3.5.4信息点播菜单删除',1,200,0,3,30,'AA==','删除终端全部信息菜单','[BYTE|1]0\r\n','终端通用应答','0001','1、平台下发信息点播菜单删除指令；\r\n2、观察模拟终端是否全部删除信息点播菜单；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (167,'3.6.2信息取消',1,201,0,3,30,'','信息取消\r\n信息类型：1\r\n点播/取消标志：0，取消；','','平台通用应答','8001','1、平台正确解析并返回通用应答。\r\n2、终端不再接收到已取消的信息服务。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (168,'3.2.3更新事件',1,197,0,3,30,'AQMBCMDX0+rM7Mb4AgiztcG+ucrVzwMIwrfD5syuy/o=','更新事件\r\n设置总数：3\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：雷雨天气\r\n事件ID：2\r\n事件内容长度：8\r\n事件内容：车辆故障\r\n事件ID：3\r\n事件内容长度：8\r\n事件内容：路面坍塌','[BYTE|1]1\r\n[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]雷雨天气\r\n[BYTE|1]2\r\n[BYTE|1]8\r\n[STRING|8]车辆故障\r\n[BYTE|1]3\r\n[BYTE|1]8\r\n[STRING|8]路面坍塌','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (169,'3.2.4修改事件',1,197,0,3,30,'AwEBCLW9tO+3tMCh','修改事件：\r\n设置总数：1\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：到达反馈','[BYTE|1]3\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]8\r\n[STRING|8]到达反馈\r\n','终端通用应答','0001','1、模拟终端显示的事件信息与平台下发相同；\r\n2、平台正确接收并显示模拟终端应答。\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (170,'3.2.5删除全部事件',1,197,0,3,30,'AA==','删除终端现有所有事件','[BYTE|1]0\r\n','终端通用应答','0001','1、观察终端是否删除全部事件。\r\n2、平台是否正确接收终端应答。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (171,'8.1.1手动上发多媒体事件',1,218,0,1,30,'','','','多媒体事件上传通用应答','0x0800','请确认终端已发送多媒体事件信息上传报文','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (174,'7.2.1电子运单上报',1,216,0,3,90,'','使用模拟终端的“手工用例”中的“9、电子运单上报”发送数据\r\n运单数据“ID：20110628 货物名称：打印机20台 目的地：中国深圳”','','','8001','平台收到与终端发送信息一致的运单信息','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (175,'9.1.1数据下行透传',1,224,0,0,30,'123456789','透传消息类型=1\r\n透传消息内容=“123456789”','[BYTE|1]1\r\n[STRING|9]123456789\r\n','成功','0001','1、平台下发数据下行透传指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致\r\n','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (177,'8.2.1抓拍摄像头通道1立即上传',1,220,0,2,60,'\0\0\0\0','拍摄照片：1\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：640×480\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回图片应当为标注通道号码的粉红色测试图片','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (182,'7.3.1驾驶员身份信息采集上报',1,217,0,3,90,'','使用模拟终端的“手工用例”中的“6、驾驶员身份信息采集上报”上报信息\r\n1、驾驶员姓名：张老三\r\n2、驾驶员身份证：010456198009080234\r\n3、从业资格证编号：0106785869869807\r\n4、发证机构名称：北京市道路运输管理局\r\n','','','8001','平台收到与上报数据相同的驾驶员信息','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (186,'8.2.2录像通道1上传',1,220,0,2,300,'Af//AAAAAQEBAQEBAQ==','终端录像\r\n立即上传\r\n拍摄通道：1\r\n间隔：0\r\n分辨率：320×240\r\n图像质量：1\r\n亮度：127\r\n对比度：1\r\n饱和度：1\r\n色度：1','[BYTE|1]1\r\n[WORD]65535\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]127\r\n[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]1','1 抓拍命令通用应答\r\n2 立即开始上传多媒体数据','0x8801','抓拍后不仅需要等待抓拍命令通用应答，同时还应当在稍后收到终端上行的多媒体数据\r\n返回数据为AVI（RAFF）容器H264视频，内容为蓝色GPS图样文字与JT/T808-2011字样3维动画','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (187,'8.3.1检索图片列表',1,221,0,3,60,'AAAAEQYBAAABEQYwI1lZ','多媒体类型：图片\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]0\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959\r\n','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (188,'8.3.2检索音频列表',1,221,0,3,60,'AQAAEQYBAAABEQYwI1lZ','多媒体类型：音频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]1\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (189,'8.3.3检索视频列表',1,221,0,3,60,'AgAAEQYBAAABEQYwI1lZ','多媒体类型：视频\r\n通道：所有\r\n事件项：平台下发\r\n起始时间：2011年6月1日0时0分1秒\r\n结束时间：2011年6月30日23时59分59秒','[BYTE|1]2\r\n[BYTE|1]0\r\n[BYTE|1]0\r\n[BCD|6]110601000001\r\n[BCD|6]110630235959','返回多媒体检索应答和列表','0x0802','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (191,'8.4.1上传图像',1,222,0,2,60,'AAEAEQYlAAABEQYlI1lZAA==','上传存储图像\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]0\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0\r\n','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (192,'8.4.2上传音频',1,222,0,2,100,'AQEAEQYlAAABEQYlI1lZAA==','上传存储音频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]1\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (193,'8.4.3上传视频',1,222,0,2,300,'AgEAEQYlAAABEQYlI1lZAA==','上传存储视频\r\n通道：1\r\n事件项：平台下发\r\n起始时间：2011年6月25日0时0分1秒\r\n结束时间：2011年6月25日23时59分59秒\r\n不删除','[BYTE|1]2\r\n[BYTE|1]1\r\n[BYTE|1]0\r\n[BCD|6]110625000001\r\n[BCD|6]110625235959\r\n[BYTE|1]0','存储多媒体上传通用应答','0x8803','同时上传多媒体数据','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (194,'8.5.1录音32Kbps立即上传',1,223,0,2,100,'AQAAAAM=','开始录音\r\n持续录音\r\n实时上传\r\n采样率：32Kbps','[BYTE|1]1\r\n[WORD]0\r\n[BYTE|1]0\r\n[BYTE|1]3\r\n','开始录音通用应答','0x8804','同时上传多媒体数据\r\n音频内容为440Hz断续正弦波测试信号','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (195,'1.5.2超速设置',1,190,0,3,30,'','平台下发如下参数\r\n限速值=80公里/小时；\r\n持续时间=10秒','','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (196,'1.5.3疲劳驾驶设置',1,190,0,3,600,'AgAAAFcEAAACWAAAAFkEAAAAtA==','平台下发如下参数\r\n连续驾驶时间=600秒；\r\n最小休息时间=180秒。','[BYTE|1]2\r\n[DWORD]87\r\n[BYTE|1]4\r\n[DWORD]600\r\n[DWORD]89\r\n[BYTE|1]4\r\n[DWORD]180\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (197,'1.5.4超时停车',1,190,0,3,300,'AQAAAFoEAAABLA==','平台下发如下参数\r\n最长停车时间=300s','[BYTE|1]1\r\n[DWORD]90\r\n[BYTE|1]4\r\n[DWORD]300\r\n','终端通用应答','0001','1、平台下发设置终端参数指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (198,'2.1.2电瓶欠压',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“终端主电源欠压”项\r\n然后点击执行命令按钮','','通用应答成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (199,'注销主从链路消息',1,233,1,1,30,'','','','返回消息ID为1003','0x1003','通知被测下级平台发送主从链路断开消息（1003协议消息）','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (200,'2.1.3断电提醒',1,193,0,3,60,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须包括“电路断开”项\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (201,'2.1.4终端故障',1,193,0,3,30,'','使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。随便选择设备故障中的一个。\r\n然后点击执行命令按钮','','通用应答：成功','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (202,'2.1.5休眠',1,193,0,3,300,'AgAAACEEAAAAAAAAACcEAAABLA==','先通过平台进行参数设置：\r\n位置汇报方案=0（根据ACC状态）\r\n休眠时汇报时间间隔=300秒\r\n使用模拟终端中的“4、手动位置上报”位置基本信息和附加信息由界面动态显示和描述，其中报警标志和状态由检测人员任意勾选。必须选择ACC关。\r\n然后点击执行命令按钮','[BYTE|1]2\r\n[DWORD]33\r\n[BYTE|1]4\r\n[DWORD]0\r\n[DWORD]39\r\n[BYTE|1]4\r\n[DWORD]300\r\n','平台通用应答','0200','观察测试平台收到的位置信息及报警标志、状态、附加信息与上发信息是否一致。','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (203,'3.5.5信息点播菜单追加',1,200,1,0,10,'AgIBAAjM7Mb41KSxqAIACMO/yNXQws7F','追加信息点播菜单\r\n设置类型：追加\r\n信息项总数：2\r\n信息类型：1\r\n信息名称长度：8\r\n信息名称：天气预报\r\n信息类型：2\r\n信息名称长度：8\r\n信息名称：每日新闻\r\n','[BYTE|1]2\r\n[BYTE|1]2\r\n[BYTE|1]1\r\n[WORD]8\r\n[STRING|8]天气预报\r\n[BYTE|1]2\r\n[WORD]8\r\n[STRING|8]每日新闻','终端通用应答','0001','1、平台下发信息点播菜单追加指令；\r\n2、观察模拟终端显示的信息内容是否与平台下发一致；','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (206,'3.2.6追加新事件',1,197,1,3,30,'','追加新事件\r\n设置总数：2\r\n事件列表：\r\n事件ID：1\r\n事件内容长度：8\r\n事件内容：阴雨天气\r\n事件ID：2\r\n事件内容长度：4\r\n事件内容：路滑','','终端通用应答','0001','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (207,'7.1.1行车记录仪数据采集',1,214,0,1,30,'','下行行车记录仪设置指令','','返回行车记录仪数据上传，其中命令字与下发命令字相同','0700','','',2,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (208,'下级平台实时上传车辆定位信息',1,234,0,1,60,'','','','返回消息的业务类型为1200，业务子类型为1202','','请提示下级平台实时上传不少于3条车牌号为“川A12345”的定位信息(1202消息)','',1,0);
insert  into `example`(`ExampleID`,`Name`,`Flag`,`FunctionID`,`AutoResult`,`TipFlag`,`WaitTime`,`SendMsg`,`SendMsgNote`,`SendMsgDisplay`,`ResultMsgNote`,`ResultMsg`,`Note`,`ResultMsgDisplay`,`UserFlag`,`SerialID`) values (209,'下级平台车辆定位信息补报',1,235,0,1,60,'','','','最少收到3条定位信息补报，消息ID为1200，业务子类型为1203','','请提示下级平台补报不少于3条车牌号为“川A12345”自动补报信息（1203号协议）','',1,0);

/*Table structure for table `examplebak` */

DROP TABLE IF EXISTS `examplebak`;

CREATE TABLE `examplebak` (
  `ExampleID` int(11) NOT NULL COMMENT '用例ID',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '用例名称',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '功能ID 1=只读 0=可编辑',
  `FunctionID` int(11) NOT NULL COMMENT '功能编号',
  `AutoResult` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否自动判定',
  `TipFlag` int(11) NOT NULL DEFAULT '0' COMMENT '0=不提示 1=执行前提示 2=执行后提示 3=执行前/后都提示（提示内容为备注字段内容）',
  `WaitTime` int(11) NOT NULL COMMENT '等待时间（秒）',
  `SendMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令',
  `SendMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令描叙字符',
  `SendMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '发送指令显示',
  `ResultMsgNote` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果描叙字符',
  `ResultMsg` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果',
  `ResultMsgDisplay` varchar(2000) COLLATE gbk_bin DEFAULT NULL COMMENT '应答结果显示',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ExampleID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='用例管理';

/*Data for the table `examplebak` */

/*Table structure for table `factory` */

DROP TABLE IF EXISTS `factory`;

CREATE TABLE `factory` (
  `FactoryID` int(11) NOT NULL AUTO_INCREMENT COMMENT '商厂编号',
  `FactoryName` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '厂商名称',
  `Linker` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '联系人',
  `Telephone` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '联系电话',
  `Address` varchar(64) COLLATE gbk_bin DEFAULT NULL COMMENT '联系地址',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `provinceID` varchar(10) COLLATE gbk_bin NOT NULL COMMENT '省编号',
  PRIMARY KEY (`FactoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='厂商信息';

/*Data for the table `factory` */

/*Table structure for table `functiongroupref` */

DROP TABLE IF EXISTS `functiongroupref`;

CREATE TABLE `functiongroupref` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '号编',
  `GroupID` int(11) NOT NULL COMMENT '分类编号',
  `FunctionID` int(11) NOT NULL COMMENT '功能编号',
  `SerialNumber` int(11) NOT NULL DEFAULT '0' COMMENT '号序',
  `Flag` int(11) NOT NULL COMMENT '1=应具有；2=可选；3=不必具有',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=573 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标分类和功能关系';

/*Data for the table `functiongroupref` */

insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (312,3,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (313,3,85,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (314,3,87,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (315,3,93,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (316,3,94,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (317,3,80,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (318,3,100,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (319,3,101,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (320,3,102,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (321,3,103,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (322,3,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (323,3,109,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (324,3,110,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (325,3,111,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (326,3,112,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (327,3,113,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (328,3,114,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (329,3,115,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (330,3,116,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (331,3,117,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (332,3,118,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (333,3,119,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (334,3,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (335,3,120,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (336,3,121,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (337,3,122,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (338,3,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (339,3,107,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (340,3,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (341,3,124,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (419,6,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (420,6,87,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (421,6,94,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (422,6,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (423,6,110,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (424,6,111,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (425,6,112,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (426,6,113,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (427,6,114,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (428,6,118,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (429,6,119,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (430,6,234,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (431,6,235,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (432,6,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (433,6,121,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (434,6,122,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (435,6,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (436,6,129,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (437,6,131,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (438,6,132,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (439,6,133,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (440,6,107,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (441,6,135,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (442,6,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (443,6,125,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (444,6,126,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (445,6,127,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (446,6,175,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (447,6,186,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (448,6,187,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (449,6,188,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (450,6,189,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (451,6,190,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (452,6,191,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (453,6,192,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (454,6,176,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (455,6,193,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (456,6,194,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (457,6,195,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (458,6,177,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (459,6,196,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (460,6,197,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (461,6,198,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (462,6,199,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (463,6,200,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (464,6,201,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (465,6,202,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (466,6,231,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (467,6,178,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (468,6,203,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (469,6,204,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (470,6,179,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (471,6,205,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (472,6,180,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (473,6,206,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (474,6,207,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (475,6,208,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (476,6,209,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (477,6,210,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (478,6,211,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (479,6,212,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (480,6,213,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (481,6,181,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (482,6,214,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (483,6,216,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (484,6,217,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (485,6,182,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (486,6,218,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (487,6,220,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (488,6,221,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (489,6,222,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (490,6,223,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (491,6,183,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (492,6,224,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (493,6,225,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (494,6,232,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (495,6,233,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (496,5,79,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (497,5,87,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (498,5,94,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (499,5,104,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (500,5,110,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (501,5,111,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (502,5,112,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (503,5,113,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (504,5,114,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (505,5,118,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (506,5,119,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (507,5,234,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (508,5,235,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (509,5,105,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (510,5,121,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (511,5,122,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (512,5,106,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (513,5,129,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (514,5,131,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (515,5,132,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (516,5,133,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (517,5,107,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (518,5,135,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (519,5,123,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (520,5,125,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (521,5,126,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (522,5,127,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (523,5,175,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (524,5,186,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (525,5,187,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (526,5,188,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (527,5,189,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (528,5,190,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (529,5,191,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (530,5,192,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (531,5,176,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (532,5,193,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (533,5,194,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (534,5,195,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (535,5,177,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (536,5,196,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (537,5,197,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (538,5,198,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (539,5,199,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (540,5,200,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (541,5,201,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (542,5,202,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (543,5,231,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (544,5,178,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (545,5,203,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (546,5,204,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (547,5,179,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (548,5,205,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (549,5,180,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (550,5,206,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (551,5,207,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (552,5,208,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (553,5,209,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (554,5,210,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (555,5,211,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (556,5,212,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (557,5,213,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (558,5,181,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (559,5,214,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (560,5,216,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (561,5,217,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (562,5,182,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (563,5,218,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (564,5,220,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (565,5,221,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (566,5,222,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (567,5,223,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (568,5,183,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (569,5,224,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (570,5,225,0,1);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (571,5,232,0,0);
insert  into `functiongroupref`(`ID`,`GroupID`,`FunctionID`,`SerialNumber`,`Flag`) values (572,5,233,0,1);

/*Table structure for table `functionlist` */

DROP TABLE IF EXISTS `functionlist`;

CREATE TABLE `functionlist` (
  `FunctionID` int(11) NOT NULL AUTO_INCREMENT COMMENT '功能编号',
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '功能名称',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `UserFlag` int(11) NOT NULL DEFAULT '1',
  `SerialID` int(11) NOT NULL DEFAULT '0' COMMENT '排序使用',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能信息';

/*Data for the table `functionlist` */

insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (79,'','4.5.1 链路管理业务类',1,0,0,'链路管理业务类',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (87,'0x1006','4.5.1.6 主链路连接保持应答消息',2,79,79,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (94,'0x9005','4.5.1.13 从链路连接保持请求消息',2,79,79,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (104,'','4.5.3.2 从链路车辆动态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (105,'','4.5.4.2 从链路平台间信息交互业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (106,'','4.5.6.2 从链路车辆监管业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (107,'','4.5.7.2 从链路车辆静态信息交换业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (110,'0x9202','4.5.3.2.2 交换车辆定位信息消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (111,'0x9203','4.5.3.2.3 车辆定位信息交换补发消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (112,'0x9204','4.5.3.2.4 交换车辆静态信息消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (113,'0x9205','4.5.3.2.5 启动车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (114,'0x9206','4.5.3.2.6 结束车辆定位信息交换请求消息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (118,'0x920A','4.5.3.2.10 上报车辆驾驶员身份识别信息请求',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (119,'0x920B','4.5.3.2.11 上报车辆电子运单请求',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (121,'0x9301','4.5.4.2.2 平台查岗请求消息',2,105,105,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (122,'0x9302','4.5.4.2.3 下发平台间报文请求消息',2,105,105,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (123,'','4.5.5.2 从链路车辆报警信息业务',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (125,'0x9401','4.5.5.2.2 报警督办请求消息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (126,'0x9402','4.5.5.2.3 报警预警消息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (127,'0x9403','4.5.5.2.4 实时交换报警信息',2,123,123,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (129,'0x9501','4.5.6.2.2 车辆单向监听请求消息',2,106,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (131,'0x9503','4.5.6.2.4 下发车辆报文请求',2,106,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (132,'0x9504','4.5.6.2.5 上报车辆行驶记录请求',2,106,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (133,'0x9505','4.5.6.2.6 车辆应急接入监管平台请求消息',2,106,106,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (135,'0x9601','4.5.7.2.2 补报车辆静态信息请求',2,107,107,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (167,'','AS2',2,153,153,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (175,'','1.终端管理',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (176,'','2.位置报警',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (177,'','3.信息',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (178,'','4.电话',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (179,'','5.车辆控制',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (180,'','6.车辆管理',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (181,'','7.信息采集',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (182,'','8.多媒体',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (183,'','9.通用数据传输',1,0,0,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (186,'0x0100','1.1终端注册',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (187,'0x0003','1.2终端注销',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (188,'0x0102','1.3终端鉴权',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (189,'0x0002','1.4终端心跳',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (190,'0x8103','1.5设置终端参数',2,175,175,NULL,2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (191,'0x8104','1.6查询终端参数',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (192,'0x8105','1.7终端控制',2,175,175,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (193,'0x0200','2.1位置及报警',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (194,'0x8201','2.2位置信息查询',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (195,'0x8202','2.3临时位置跟踪控制',2,176,176,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (196,'0x8300','3.1文本信息下发',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (197,'0x8301','3.2事件设置',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (198,'0x0301','3.3事件报告',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (199,'0x8302','3.4提问下发',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (200,'0x8303','3.5信息点播菜单设置',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (201,'0x0303','3.6信息点播/取消',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (202,'0x8304','3.7信息服务',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (203,'0x8400','4.1电话回拨',2,178,178,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (204,'0x8401','4.2设置电话本',2,178,178,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (205,'0x8500','5.1车辆控制',2,179,179,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (206,'0x8600','6.1设置圆形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (207,'0x8601','6.2删除圆形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (208,'0x8602','6.3设置矩形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (209,'0x8603','6.4删除矩形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (210,'0x8604','6.5设置多边形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (211,'0x8605','6.6删除多边形区域',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (212,'0x8606','6.7设置路线',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (213,'0x8607','6.8删除路线',2,180,180,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (214,'0x8700','7.1行驶记录仪数据采集命令',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (216,'0x0701','7.2电子运单上报',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (217,'0x0702','7.3驾驶员身份信息采集上报',2,181,181,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (218,'0x0800','8.1多媒体事件信息上传',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (220,'0x8801','8.2摄像头立即拍摄命令',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (221,'0x8802','8.3存储多媒体数据检索',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (222,'0x8803','8.4存储多媒体数据上传',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (223,'0x8804','8.5录音开始命令',2,182,182,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (224,'0x8900','9.1数据下行透传',2,183,183,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (225,'0x0900','9.2数据上行透传',2,183,183,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (231,'0x0302','3.8提问应答',2,177,177,'',2,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (232,'','4.5.1 链路注销',1,0,0,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (233,'0x1003','注销主从链路',2,232,232,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (234,'0x9202','4.5.3.1.3 实时上传车辆定位信息',2,104,104,'',1,0);
insert  into `functionlist`(`FunctionID`,`MessageID`,`FunctionName`,`Level`,`ParentFunctionID`,`RootFunctionID`,`Note`,`UserFlag`,`SerialID`) values (235,'0x9203','4.5.3.1.4 车辆定位信息自动补报',2,104,104,'',1,0);

/*Table structure for table `functionlistbak` */

DROP TABLE IF EXISTS `functionlistbak`;

CREATE TABLE `functionlistbak` (
  `FunctionID` int(11) NOT NULL COMMENT '功能项ID',
  `MessageID` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '消息ID',
  `FunctionName` varchar(30) COLLATE gbk_bin NOT NULL DEFAULT '-1' COMMENT '功能项名称',
  `UserFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=平台使用 2=终端使用',
  `Level` int(11) NOT NULL COMMENT '级别',
  `ParentFunctionID` int(11) NOT NULL COMMENT '父功能编码',
  `RootFunctionID` int(11) NOT NULL COMMENT '根功能编号',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`FunctionID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='功能项管理';

/*Data for the table `functionlistbak` */

/*Table structure for table `gbversion` */

DROP TABLE IF EXISTS `gbversion`;

CREATE TABLE `gbversion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `Name` varchar(64) COLLATE gbk_bin NOT NULL COMMENT '版本名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='国标版本信息';

/*Data for the table `gbversion` */

insert  into `gbversion`(`ID`,`Name`) values (1,'809-2011');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `LoginID` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `LoginName` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '登录账号',
  `Name` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '姓名',
  `PassWord` varchar(50) COLLATE gbk_bin NOT NULL COMMENT '密码（MD5加密）',
  `Flag` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效1=有效。0=无效',
  `IfAdmin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否管理员',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`LoginID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='登录用户';

/*Data for the table `login` */

insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (2,'Admin','超级管理员','AA0FD9AFBB2564FCB70EB0E402C62E4F',1,1,'超级管理员');
insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (3,'chenlongfei','陈龙飞','783929FD2845763100FFC9438A62DBDD',1,0,'');
insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (4,'test','test','098F6BCD4621D373CADE4E832627B4F6',1,1,'');
insert  into `login`(`LoginID`,`LoginName`,`Name`,`PassWord`,`Flag`,`IfAdmin`,`Note`) values (5,'yuguibin','余贵滨','18A2FEEBCDBFCD8BBB415E00C1EEB167',1,0,'');

/*Table structure for table `messagecode` */

DROP TABLE IF EXISTS `messagecode`;

CREATE TABLE `messagecode` (
  `MessageID` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '消息ID',
  `MsgName` varchar(200) COLLATE gbk_bin NOT NULL COMMENT '消息描叙',
  `MessageType` int(11) NOT NULL COMMENT '1=终端消息 2=平台消息',
  `FunctionID` int(11) DEFAULT NULL COMMENT '消息分类(上级消息)',
  `BusinessNote` varchar(50) COLLATE gbk_bin DEFAULT NULL COMMENT '业务类型标识',
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='消息信息';

/*Data for the table `messagecode` */

insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0100','终端注册',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0002','终端心跳',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0003','终端注销',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0100','终端注册',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0102','终端鉴权',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0200','位置信息汇报',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0301','事件报告',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0302','提问应答',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0303','信息点播/取消',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0701','电子运单上报',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0702','驾驶员身份信息采集上报',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0800','多媒体事件信息上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0801','多媒体数据上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0900','数据上行透传',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0901','数据压缩上报',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x0A00','终端RSA公钥',2,184,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1001','主链路登录请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1003','主链路注销请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1005','主链路连接保持请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1006','主链路连接保持应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1007','主链路断开通知消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1008','下级平台主动关闭主从链路通知消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1201','上传车辆注册信息消息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1202','实时上传车辆定位信息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1203','车辆定位信息自动补报请求消息',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1205','启动车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1206','结束车辆定位信息交换应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1207','申请交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1208','取消交换指定车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1209','补发车辆定位信息请求',2,136,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120A','上报驾驶员身份识别信息应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x120B','上报车辆电子运单应答',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1300','数据体描述',2,145,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1301','平台查岗应答',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1302','下发平台间报文应答',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1400','数据体',2,148,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1401','报警督办应答',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1402','上报报警信息',2,148,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1500','数据体',2,151,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1501','车辆单向监听应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1502','车辆拍照应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1503','下发车辆报文应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1504','上报车辆行驶记录应答消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1505','车辆应急接入监管平台应答',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x1600','数据体',2,153,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8103','查询终端参数',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8104','查询终端参数',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8105','终端控制',2,175,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8201','位置信息查询',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8202','临时位置跟踪控制',2,176,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8300','文本信息下发',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8301','事件设置',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8302','提问下发',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8303','信息点播菜单设置',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8304','信息服务',2,177,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8400','电话回拨',2,178,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8401','设置电话本',2,178,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8500','车辆控制',2,179,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8600','设置圆形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8601','删除圆形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8602','设置矩形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8603','删除矩形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8604','设置多边形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8605','删除多边形区域',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8606','设置路线',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8607','删除路线',2,180,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8700','行驶记录仪数据采集命令',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8701','行驶记录仪参数下传命令',2,181,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8801','摄像头立即拍摄命令',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8802','存储多媒体数据检索',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8803','存储多媒体数据上传',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8804','录音开始命令',2,182,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8900','数据下行透传',2,183,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x8A00','平台RSA公钥',2,184,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9004','从链路注销应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9005','从链路连接保持请求消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9006','从链路连接保持应答消息',2,79,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9204','交换车辆静态信息消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9205','启动车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9206','结束车辆定位信息交换请求消息',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920A','上报车辆驾驶员身份识别信息请求',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x920B','上报车辆电子运单请求',2,104,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9301','平台查岗请求消息',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9302','下发平台间报文请求消息',2,105,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9401','报警督办请求消息',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9403','实时交换报警信息',2,123,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9501','车辆单向监听请求消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9503','下发车辆报文请求',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9504','上报车辆行驶记录请求',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9505','车辆应急接入监管平台请求消息',2,106,NULL);
insert  into `messagecode`(`MessageID`,`MsgName`,`MessageType`,`FunctionID`,`BusinessNote`) values ('0x9601','补报车辆静态信息请求',2,107,NULL);

/*Table structure for table `operatelog` */

DROP TABLE IF EXISTS `operatelog`;

CREATE TABLE `operatelog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号：自增',
  `Operator` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '操作人员',
  `operateDate` datetime NOT NULL COMMENT '发生时间',
  `Flag` int(11) NOT NULL COMMENT '1=增加数据、2=删除数据3=编辑数据',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '操作说明',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='操作日志';

/*Data for the table `operatelog` */

/*Table structure for table `processdefin` */

DROP TABLE IF EXISTS `processdefin`;

CREATE TABLE `processdefin` (
  `progressid` varchar(32) NOT NULL,
  `progressname` varchar(50) DEFAULT NULL,
  `ordernum` int(11) DEFAULT NULL,
  `note` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`progressid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `processdefin` */

/*Table structure for table `progresscase` */

DROP TABLE IF EXISTS `progresscase`;

CREATE TABLE `progresscase` (
  `caseid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL,
  `casename` varchar(200) DEFAULT NULL,
  `groupnum` int(11) DEFAULT NULL COMMENT '界面开发时，用下拉框输入分组数，默认为0，若一个流程对应的用例有多个分组，抽取用例时，每个组抽取一个执行，执行先后顺序按照分组编号为准',
  `note` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`caseid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='流程检测用例表  流程检测用包含检测步骤';

/*Data for the table `progresscase` */

/*Table structure for table `progresstescasedetail` */

DROP TABLE IF EXISTS `progresstescasedetail`;

CREATE TABLE `progresstescasedetail` (
  `casedetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL COMMENT '流程id（不做关联 只记录一下 方便查询）',
  `progressdetailid` varchar(32) DEFAULT NULL,
  `caseid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`casedetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstescasedetail` */

/*Table structure for table `progresstesdetail` */

DROP TABLE IF EXISTS `progresstesdetail`;

CREATE TABLE `progresstesdetail` (
  `progressdetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`progressdetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstesdetail` */

/*Table structure for table `progresstestlog` */

DROP TABLE IF EXISTS `progresstestlog`;

CREATE TABLE `progresstestlog` (
  `id` varchar(32) NOT NULL,
  `targetid` varchar(30) DEFAULT NULL COMMENT '测试目标编号',
  `reportid` varchar(32) DEFAULT NULL,
  `flag` int(11) NOT NULL COMMENT '0=系统日志 1=上行 2=下行',
  `messageid` varchar(20) DEFAULT NULL,
  `eventdate` datetime NOT NULL COMMENT '日期',
  `serialnumber` int(11) NOT NULL COMMENT '消息流水号',
  `resultflag` int(11) DEFAULT NULL COMMENT '0=正常 1=错误',
  `oraginmsg` longblob COMMENT '解析前的原始数据',
  `outmsg` varchar(4000) DEFAULT NULL COMMENT '解析后数据',
  `mediatype` int(11) DEFAULT NULL COMMENT '0=无附加数据 1=图片 2=多媒体',
  `mediadata` longblob COMMENT '存储多媒体图片数据',
  `note` varchar(2000) DEFAULT NULL COMMENT '备注',
  `isalarm` int(11) NOT NULL DEFAULT '0' COMMENT '0=无报警 1=有报警',
  `alarmdescription` varchar(2000) DEFAULT NULL COMMENT '报警描叙',
  `xmlmessage` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='流程检测日志';

/*Data for the table `progresstestlog` */

/*Table structure for table `progresstestreport` */

DROP TABLE IF EXISTS `progresstestreport`;

CREATE TABLE `progresstestreport` (
  `reportid` varchar(32) NOT NULL,
  `targetid` varchar(30) DEFAULT NULL COMMENT '测试目标编号',
  `environmentid` int(11) DEFAULT NULL,
  `testcount` int(11) NOT NULL COMMENT '测试次数',
  `timeout` int(11) NOT NULL DEFAULT '0' COMMENT '测试超时自动判断测试不通过',
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `testprocess` varchar(200) NOT NULL COMMENT '测试过程描叙',
  `testdate` datetime NOT NULL COMMENT '测试时间',
  `tester` varchar(16) NOT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `resultid` int(11) NOT NULL COMMENT '检测状态',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`reportid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

/*Data for the table `progresstestreport` */

/*Table structure for table `progressteststepdetail` */

DROP TABLE IF EXISTS `progressteststepdetail`;

CREATE TABLE `progressteststepdetail` (
  `stepdetailid` varchar(32) NOT NULL,
  `progressid` varchar(32) DEFAULT NULL COMMENT '流程id（不做关联 只记录一下 方便查询）',
  `caseid` varchar(32) DEFAULT NULL COMMENT '用例id（不做关联 只记录一下 方便查询）',
  `casedetailid` varchar(32) DEFAULT NULL,
  `stepid` varchar(32) DEFAULT NULL,
  `reportid` varchar(32) DEFAULT NULL,
  `testdate` datetime DEFAULT NULL,
  `resultid` int(11) DEFAULT NULL COMMENT '检测状态',
  `autojudgeresult` tinyint(1) DEFAULT NULL,
  `judgeresult` tinyint(1) DEFAULT NULL,
  `judgeinfo` varchar(2000) DEFAULT NULL,
  `tester` varchar(16) DEFAULT NULL COMMENT '测试人',
  `checker` varchar(16) DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核时间',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`stepdetailid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='用例步骤执行明细';

/*Data for the table `progressteststepdetail` */

/*Table structure for table `resultinfor` */

DROP TABLE IF EXISTS `resultinfor`;

CREATE TABLE `resultinfor` (
  `ResultID` int(11) NOT NULL COMMENT 'ID',
  `Name` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '状态描叙',
  PRIMARY KEY (`ResultID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标状态';

/*Data for the table `resultinfor` */

insert  into `resultinfor`(`ResultID`,`Name`) values (0,'未知');
insert  into `resultinfor`(`ResultID`,`Name`) values (1,'待测');
insert  into `resultinfor`(`ResultID`,`Name`) values (2,'测试中');
insert  into `resultinfor`(`ResultID`,`Name`) values (3,'待审核');
insert  into `resultinfor`(`ResultID`,`Name`) values (4,'测试通过');
insert  into `resultinfor`(`ResultID`,`Name`) values (5,'测试不通过');

/*Table structure for table `serialnumber` */

DROP TABLE IF EXISTS `serialnumber`;

CREATE TABLE `serialnumber` (
  `SheetType` int(11) NOT NULL COMMENT '序号类型',
  `Name` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '名称描叙',
  `PreFix` char(2) COLLATE gbk_bin DEFAULT NULL COMMENT '前缀',
  `ResetFlag` int(11) NOT NULL DEFAULT '1' COMMENT '1=按天重置 2=按月重置',
  `ResetDate` date DEFAULT NULL,
  `Serialnumber` int(11) NOT NULL DEFAULT '0' COMMENT '序列号',
  `serialcount` int(11) NOT NULL COMMENT '长度4:则输出格式:0001',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`SheetType`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='获取批次号使用';

/*Data for the table `serialnumber` */

insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (1,'检测目标批次号（终端）','',1,'2011-06-23',4,4,NULL);
insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (2,'检测目标批次号（企业平台）','',1,'2011-07-16',3,4,NULL);
insert  into `serialnumber`(`SheetType`,`Name`,`PreFix`,`ResetFlag`,`ResetDate`,`Serialnumber`,`serialcount`,`Note`) values (3,'检测目标批次号（政府平台）','',1,'2011-06-14',0,4,NULL);

/*Table structure for table `steplib` */

DROP TABLE IF EXISTS `steplib`;

CREATE TABLE `steplib` (
  `stepid` varchar(32) NOT NULL,
  `typeid` int(11) DEFAULT NULL,
  `caseid` varchar(32) DEFAULT NULL,
  `stepname` varchar(200) DEFAULT NULL,
  `ordernum` int(11) DEFAULT NULL,
  `stepparam` varchar(2000) DEFAULT NULL,
  `resultmsg` varchar(2000) DEFAULT NULL,
  `procedureparam` varchar(2000) DEFAULT NULL,
  `autojudge` int(11) DEFAULT NULL COMMENT '0 自动判定\r\n            1 手动判定\r\n            2 自动+手动',
  `note` varchar(2000) DEFAULT NULL,
  `overtime` int(11) DEFAULT NULL,
  `messageid` varchar(100) DEFAULT NULL,
  `sendmsg` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`stepid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='步骤库';

/*Data for the table `steplib` */

/*Table structure for table `steptype` */

DROP TABLE IF EXISTS `steptype`;

CREATE TABLE `steptype` (
  `typeid` varchar(32) NOT NULL,
  `typename` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COMMENT='步骤类型定义\r\n1发送\r\n2接收\r\n3提示\r\n4判定\r\n5等待';

/*Data for the table `steptype` */

insert  into `steptype`(`typeid`,`typename`) values ('1','发送');
insert  into `steptype`(`typeid`,`typename`) values ('2','接收');
insert  into `steptype`(`typeid`,`typename`) values ('3','提示');
insert  into `steptype`(`typeid`,`typename`) values ('4','判定');
insert  into `steptype`(`typeid`,`typename`) values ('5','等待');

/*Table structure for table `target` */

DROP TABLE IF EXISTS `target`;

CREATE TABLE `target` (
  `TargetbatchID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '目标批次号',
  `Name` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '目标名称描叙',
  `groupid` varchar(20) COLLATE gbk_bin DEFAULT NULL COMMENT '目标分类',
  `FactoryID` int(11) NOT NULL COMMENT '厂商编号',
  `TargetModel` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '目标型号',
  `TargetVersion` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '目标版本',
  `ReceiveDate` datetime NOT NULL COMMENT '送检日期',
  `Reporter` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '测试结果审核人',
  `ReportDate` datetime DEFAULT NULL COMMENT '测试结果确认日期',
  `ResultID` int(11) NOT NULL DEFAULT '1' COMMENT '检测状态',
  `ResultNote` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '检测说明',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `Positionmode` int(11) NOT NULL DEFAULT '0' COMMENT '定位模式',
  PRIMARY KEY (`TargetbatchID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标批次信息';

/*Data for the table `target` */

/*Table structure for table `targetdetail` */

DROP TABLE IF EXISTS `targetdetail`;

CREATE TABLE `targetdetail` (
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标编号',
  `TargetbatchID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试目标批次号',
  `TargetName` varchar(40) COLLATE gbk_bin NOT NULL COMMENT '测试目标名称',
  `AccessNO` varchar(40) COLLATE gbk_bin DEFAULT NULL COMMENT '下级平台接入码',
  `ProtocolVersion` varchar(30) COLLATE gbk_bin DEFAULT NULL COMMENT '协议版本号',
  `SimNo` varchar(20) COLLATE gbk_bin NOT NULL DEFAULT '0' COMMENT '通讯号码(手机号码)',
  `GovServerIP` varchar(16) COLLATE gbk_bin DEFAULT NULL,
  `GovServerPort` int(11) DEFAULT NULL,
  `Reporter` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '测试结果确认人',
  `ReportDate` datetime DEFAULT NULL COMMENT '结果确认时间',
  `resultid` int(11) NOT NULL DEFAULT '1',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `m1` int(11) DEFAULT NULL,
  `a1` int(11) DEFAULT NULL,
  `c1` int(11) DEFAULT NULL,
  `CompanyIP` varchar(16) COLLATE gbk_bin DEFAULT NULL,
  `CompanyPort` int(11) DEFAULT NULL,
  `UserName` varchar(32) COLLATE gbk_bin DEFAULT NULL,
  `UsePswd` varchar(32) COLLATE gbk_bin DEFAULT NULL,
  `SerialNumber` int(11) NOT NULL COMMENT '目标序号',
  PRIMARY KEY (`TargetID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试目标明细表';

/*Data for the table `targetdetail` */

/*Table structure for table `targetgroup` */

DROP TABLE IF EXISTS `targetgroup`;

CREATE TABLE `targetgroup` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT COMMENT '目标分类编号',
  `TargetType` int(11) NOT NULL DEFAULT '1' COMMENT '1=终端2=企业平台=3政府平台',
  `GroupName` varchar(32) COLLATE gbk_bin NOT NULL COMMENT '分组名称',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`GroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='目标分类';

/*Data for the table `targetgroup` */

insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (1,1,'客运车辆终端','客运车辆终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (2,1,'出租车终端','出租车终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (3,1,'危险品运输车辆终端','危险品运输车辆终端');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (4,1,'货运车辆','货运车辆');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (5,2,'运营平台','运营平台');
insert  into `targetgroup`(`GroupID`,`TargetType`,`GroupName`,`Note`) values (6,2,'非运营平台','非运营平台');

/*Table structure for table `targettest` */

DROP TABLE IF EXISTS `targettest`;

CREATE TABLE `targettest` (
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '测试序号',
  `TargetID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '目标编号',
  `TestCount` int(11) NOT NULL COMMENT '测试次数',
  `TestDate` datetime NOT NULL COMMENT '测试时间',
  `Tester` varchar(16) COLLATE gbk_bin NOT NULL COMMENT '测试人',
  `EnvironmentID` int(11) NOT NULL DEFAULT '-1' COMMENT '测试环境编号',
  `ExampleID` int(11) NOT NULL COMMENT '用例编号',
  `resultid` int(11) NOT NULL DEFAULT '1',
  `timeout` int(11) NOT NULL DEFAULT '0' COMMENT '测试超时自动判断测试不通过',
  `Note` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '备注',
  `checker` varchar(16) COLLATE gbk_bin DEFAULT NULL COMMENT '审核人',
  `checkdate` datetime DEFAULT NULL COMMENT '审核日期',
  PRIMARY KEY (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='检测目标测试明细';

/*Data for the table `targettest` */

/*Table structure for table `testreport` */

DROP TABLE IF EXISTS `testreport`;

CREATE TABLE `testreport` (
  `ReportID` int(11) NOT NULL AUTO_INCREMENT COMMENT '测试报告ID',
  `TestID` varchar(30) COLLATE gbk_bin NOT NULL COMMENT '检测记录ID',
  `FunctionID` int(11) NOT NULL COMMENT '功能项ID',
  `ExampleID` int(11) NOT NULL COMMENT '测试用例',
  `EnvironmentID` int(11) NOT NULL COMMENT '测试环境编号',
  `TestProcess` varchar(200) COLLATE gbk_bin NOT NULL COMMENT '测试过程描叙',
  `Reporter` varchar(20) COLLATE gbk_bin NOT NULL COMMENT '测试人员',
  `ReportDate` datetime NOT NULL COMMENT '测试日期',
  `TemplateResult` int(11) NOT NULL COMMENT '测试正常时返回的结果',
  `ReportResult` int(11) NOT NULL COMMENT '判定结果',
  `Reason` varchar(200) COLLATE gbk_bin DEFAULT NULL COMMENT '判定说明',
  PRIMARY KEY (`ReportID`),
  KEY `Index1_TestReport` (`TestID`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk COLLATE=gbk_bin COMMENT='测试报告表';

/*Data for the table `testreport` */

/* Function  structure for function  `GetDescribe` */

/*!50003 DROP FUNCTION IF EXISTS `GetDescribe` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `GetDescribe`(iTargetbatchID varchar(30)) RETURNS varchar(200) CHARSET gbk
begin
     declare iGroupID        varchar(20);
     declare igroupid1       varchar(20);
     declare oDescribeStr    varchar(200);
     declare igroupname      varchar(32);
     declare stop int default 0;   
     
     select trim(groupid) into iGroupID from target where TargetbatchID=iTargetbatchID;
     if iGroupID='' then
           return '';
     end if;
     
     set oDescribeStr='';
     
     while length(iGroupID)>0 do
          if INSTR(iGroupID,',')>0 then
               set igroupid1=substring(iGroupID,1,INSTR(iGroupID,',')-1);
               set iGroupID=trim(substring(iGroupID,INSTR(iGroupID,',')+1));
          else
               set igroupid1=iGroupID;
               set iGroupID='';
          end if;
          
          select groupname into igroupname from targetgroup where groupid=igroupid1;
          if trim(igroupname)<>'' then
                if oDescribeStr='' then
                    set oDescribeStr=trim(igroupname);
                else
                    set igroupname=concat('|',trim(igroupname));
                    set oDescribeStr=concat(oDescribeStr,igroupname);
                end if;
          end if;
     end while;
     return oDescribeStr;
end */$$
DELIMITER ;

/* Function  structure for function  `GetSerialID` */

/*!50003 DROP FUNCTION IF EXISTS `GetSerialID` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `GetSerialID`(iString varchar(200),iflag int) RETURNS int(11)
begin
     declare ilength int;
     declare ordstr  varchar(30);
     declare serialid int;
     declare outserial int;
     
     set outserial=0;
     set ordstr='';
     set iString=trim(iString);
     set ilength=length(iString);
     set serialid=1;
        
     while serialid<=ilength do
             if substring(iString,serialid,1) in('1','2','3','4','5','6','7','8','9','0','.',' ')  then
                     if substring(iString,serialid,1) in('1','2','3','4','5','6','7','8','9','0','.') then
                            set ordstr=concat(ordstr,substring(iString,serialid,1));
                     end if;
             else
                    set serialid=ilength+1;
             end if;
             set serialid=serialid+1;
     end while;
     
     set ordstr=replace(trim(ordstr),' ','');
     
     if iflag=0 then
           set ordstr=replace(ordstr,'.','');  
     end if;
     
     while INSTR(ordstr,'.')>0 do
          set ordstr=substring(ordstr,INSTR(ordstr,'.')+1);
     end while;
     
     if ordstr<>'' then
        set outserial=ordstr;
     end if;
      
     return  outserial;
end */$$
DELIMITER ;

/* Function  structure for function  `InttoBin` */

/*!50003 DROP FUNCTION IF EXISTS `InttoBin` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` FUNCTION `InttoBin`(iDec int,iflag int) RETURNS varchar(50) CHARSET gbk
BEGIN
    declare  iBinStr  VARCHAR(50);     
    declare iDiv2     INT;             
    declare iMod2    INT; 
    declare igpsnote  varchar(50);  
    declare oflag     int;
    declare idesstr1  varchar(20);
    declare idesstr2  varchar(20);
    declare idesstr4  varchar(20);
    declare idesstr8  varchar(20);
             
    IF iDec <= 0 then
        RETURN '0000';  
    end if;   
    
    set idesstr1='GPS';
    set idesstr2='北斗';
    set idesstr4='GLONASS';
    set idesstr8='伽利略';
                 
    SET iDiv2 = iDec;   
    SET iBinStr = '';
    set igpsnote='';
    
    while  (iDiv2>0) do
        SET iMod2 = iDiv2 % 2;
        SET iDiv2 = floor(iDiv2 / 2);
        SET iBinStr = trim(concat(iMod2,iBinStr));
    END while;
    if length(iBinStr)<4 then
       set iBinStr=lpad(iBinStr,4,'0');
    end if;
    
    if iflag=0 then
        RETURN iBinStr;
    end if;
    
    if substring(iBinStr,4,1)='1' then
         set igpsnote=idesstr1;
    end if;
    
    if substring(iBinStr,3,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr2;
         else
             set idesstr2=concat('|',idesstr2);
             set igpsnote=concat(igpsnote,idesstr2);
         end if;
    end if;
    
    if substring(iBinStr,2,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr4;
         else
             set idesstr4=concat('|',idesstr4);
             set igpsnote=concat(igpsnote,idesstr4);
         end if;
    end if;
    
    if substring(iBinStr,1,1)='1' then
         if igpsnote='' then
             set igpsnote=idesstr8;
         else
             set idesstr8=concat('|',idesstr8);
             set igpsnote=concat(igpsnote,idesstr8);
         end if;
    end if;
    
    RETURN igpsnote;
END */$$
DELIMITER ;

/* Procedure structure for procedure `AddEventLog` */

/*!50003 DROP PROCEDURE IF EXISTS  `AddEventLog` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `AddEventLog`(iFlag  int,iTestID varchar(30),iExampleID int,iMessageID varchar(20),iSerialNumber int,iResultFlag int,
                             iOraginMsg varchar(32767),iOutMsg varchar(2000),iMediaType int,iMediaData varchar(32767),iNote varchar(200))
begin
        declare  itargetid   varchar(30);
        declare  ilogid      int;
        
        set ilogid=0;
        select targetid into itargetid from targettest where testid=iTestID;
        if itargetid is null then
             set itargetid=substring(iTestID,1,length(iTestID)-2);
        end if;
        
        insert into EventLog(flag,targetid,testid,eventdate,exampleid,messageid,serialnumber,
                             resultflag,oraginmsg,outmsg,MediaType,MediaData,note)
                    values(iFlag,itargetid,iTestID,current_timestamp,iExampleID,iMessageID,iSerialNumber,
                            iResultFlag,iOraginMsg,iOutMsg,iMediaType,iMediaData,iNote);
                    
        set ilogid=@@identity;
        select  ilogid as logid;
end */$$
DELIMITER ;

/* Procedure structure for procedure `AddTargetTest` */

/*!50003 DROP PROCEDURE IF EXISTS  `AddTargetTest` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `AddTargetTest`(iTargetID varchar(30),iTester varchar(16),iEnvironmentID int,iExampleID int)
begin
     declare iTestCount int;
     declare icount     int;
     declare iteststr   char(2);
     declare iTestID    varchar(30);
     declare iTargetbatchID varchar(30);
     
     declare exit handler for sqlexception rollback;
     select  TargetbatchID into iTargetbatchID from TargetDetail where targetid=iTargetID;
     if iTargetbatchID is null then
            set iTargetbatchID=substring(iTargetID,1,length(iTargetID)-2);
     end if;
     
     select count(TestCount) into  icount from TargetTest where EnvironmentID=iEnvironmentID and
     targetid in(select targetid from TargetDetail where TargetbatchID=iTargetbatchID);
     
     select count(TestCount) into  iTestCount from TargetTest where targetid=iTargetID;
     
     set icount=icount+1;
     set iTestCount=iTestCount+1;
     set iteststr=lpad(iTestCount,2,'0');
     set iTargetID=trim(iTargetID);
     set iTestID=concat(iTargetID,iteststr);
     
     insert into TargetTest(testid,targetid,testcount,testdate,tester,EnvironmentID,ExampleID,resultid,timeout)
                  values(iTestID,iTargetID,icount,CURRENT_TIMESTAMP,iTester,iEnvironmentID,iExampleID,1,0);
                  
     select iTestID as TestID;
     
end */$$
DELIMITER ;

/* Procedure structure for procedure `GetAuthentication` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetAuthentication` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `GetAuthentication`(isimno varchar(20))
begin
      declare iAuthentication varchar(40);
      declare icount          int;
      
      set iAuthentication='';
      select count(*) into icount from TargetDetail where SimNo=isimno;
      if icount>0 then
          select trim(configvalue) into iAuthentication from config where configname='终端鉴权码';
      end if;
      
      select iAuthentication as Authentication;
end */$$
DELIMITER ;

/* Procedure structure for procedure `GetTargetInfo` */

/*!50003 DROP PROCEDURE IF EXISTS  `GetTargetInfo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `GetTargetInfo`(iTestID varchar(30))
begin
      declare iAccessNO                 varchar(40);
      declare iProtocolVersion          varchar(30);
      declare iTargetID                 varchar(30);
      
      declare configversion             varchar(30);
      declare configaccessno            varchar(40);
      
      select a.AccessNO,a.ProtocolVersion,a.TargetID into iAccessNO,iProtocolVersion,iTargetID
             from TargetDetail a,TargetTest b where a.targetid=b.targetid and b.testid=iTestID;
             
      select configvalue into configversion from config where configname='协议版本号';
      if configversion is not null then
            set iProtocolVersion=configversion;
      end if;
      
      select iTargetID as targetid,iAccessNO as accessno,iProtocolVersion as ProtocolVersion;
end */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_progress_test_report` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_progress_test_report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_progress_test_report`(IN `reportId` varchar(32))
BEGIN
	#declare targetidstr varchar(32);
	#declare countint int;
	#declare environmentidint int;

	declare reportidstr varchar(32);
	set reportidstr = reportid;
	#set targetidstr = targetid;
	#set countint = testcount;
	#set environmentidint = environmentid;

	select c.ordernum as progressorder,c.progressname,d.groupnum as caseorder,
	d.casename,e.ordernum as setporder,e.stepname,
	b.testdate,b.autojudgeresult,b.judgeresult,b.judgeinfo 
	from progresstestreport a
	left join progressteststepdetail b on a.reportid = b.reportid
	left join processdefin c on b.progressid = c.progressid
	left join progresscase d on b.caseid = d.caseid
	left join steplib e on b.stepid = e.stepid
	where 1=1
	and e.typeid = '4'
	#and a.targetid = targetidstr
	#and a.environmentid = environmentidint
	#and a.testcount = countint
	and a.reportid = reportidstr
	order by c.ordernum,d.groupnum,e.ordernum;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_eventlog_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_eventlog_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_eventlog_syn_data`(IN testID varchar(30))
BEGIN
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
	SELECT CONCAT(test_org_id,a.ID) as ID,
				 a.TestID,a.TargetID,
				 a.EventDate,a.MessageID,a.ExampleID,
				 a.SerialNumber,a.ResultFlag,a.OraginMsg,
				 a.OutMsg,a.MediaType,a.MediaData,a.Note,
				 a.IsAlarm,a.AlarmDescription
  from eventlog a
	where a.TestID = testIdStr;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_factory_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_factory_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_factory_syn_data`(IN testID varchar(30))
BEGIN
	
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
  SELECT CONCAT(test_org_id,d.FactoryID) as FactoryID,
	a.FactoryName,a.Linker,a.Telephone,a.provinceID,a.Address,a.Note
	FROM factory a
  LEFT JOIN target b on a.FactoryID = b.FactoryID
	LEFT JOIN targetdetail c on b.TargetbatchID = c.TargetbatchID
	LEFT JOIN targettest d on c.TargetID = d.TargetID
	WHERE d.TargetID = testIdStr;

END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_get_table_testreport_syn_data` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_get_table_testreport_syn_data` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_get_table_testreport_syn_data`(IN testID varchar(30))
BEGIN
	
	#获取参数
	DECLARE testIdStr varchar(30) ;
	#同步前缀  默认是UID
	DECLARE test_org_id VARCHAR(32) DEFAULT '';
	set testIdStr = testID;
	#同步前缀赋值为检测机构代码
	select config.ConfigValue INTO test_org_id from config where config.ConfigName = '检测机构代码'; 
	
	SELECT CONCAT(test_org_id,a.ReportID) as ReportID,
				 a.TestID,
				 a.ExampleID,a.EnvironmentID,a.TestProcess,
				 a.Reporter,a.ReportDate,a.TemplateResult,
				 a.ReportResult,a.Reason,a.FunctionID
	from testreport a
	where a.TestID = testIdStr;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check1` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check1` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check1`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);
	DECLARE check1001 int default 0;
	DECLARE check9002 int default 0;
	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur; 
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml; 
			if iMessageId = '0x1001' THEN 
					BEGIN
							set check1001 = 1;
							IF iResultFlag = 1 THEN set check1001 = 0;
							end if;
					END;
			end if;	
			if iMessageId = '0x9002' THEN 
					BEGIN
							set check9002 = 1;
							IF iResultFlag = 1 THEN set check9002 = 0;
							end if;
					END;
			end if;	
			set c = c+1;
  end while; 

  close cur; 
	
	IF check1001 = 1 && check9002 = 1 then set rMsg = 1; 
	end if;
	SELECT rMsg,c,check1001,check9002;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check2` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check2` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check2`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
		DECLARE iMessageId varchar(20);
		DECLARE iResultFlag int;
		DECLARE iXml varchar(4000);
		DECLARE iTime varchar(32);
		DECLARE rMsg int default 0;
		DECLARE check1005 int default 0;
		DECLARE stop int DEFAULT 0;
		DECLARE c int default 0;
		DECLARE num int;
		DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
													 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
													 order by a.eventdate asc;
		DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  

		SELECT count(*) into num FROM progresstestlog a 
		where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
		order by a.eventdate asc;

		open cur; 
		
		while stop<>1 && c<num do
				fetch cur into iTime,iMessageId,iResultFlag,iXml; 
				if iMessageId = '0x1005' THEN 
						set check1005 = check1005 + 1;
				end if;	
				set c = c+1;
		end while; 

		close cur; 
		IF  check1005 >= 1 THEN set rMsg =1;
		end if;
		SELECT rMsg,check1005;
END */$$
DELIMITER ;

/* Procedure structure for procedure `pro_progress6_case1_check3` */

/*!50003 DROP PROCEDURE IF EXISTS  `pro_progress6_case1_check3` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`db`@`%` PROCEDURE `pro_progress6_case1_check3`(IN searchTime VARCHAR(32), IN reportID VARCHAR(32))
BEGIN
	
	DECLARE iMessageId varchar(20);
	DECLARE iResultFlag int;
	DECLARE iXml varchar(4000);
	DECLARE iTime varchar(32);
	DECLARE check1003 int default 0;
	DECLARE check9004 int default 0;
	DECLARE rMsg int default 0;
	DECLARE stop int DEFAULT 0;
	DECLARE c int default 0;
	DECLARE num int;
	DECLARE cur CURSOR FOR SELECT a.eventdate,a.messageid,a.resultflag,a.xmlmessage FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION set stop = 1;  
 
	SELECT count(*) into num FROM progresstestlog a 
												 where a.reportid = reportID and a.eventdate >= searchTime  and a.eventdate <= NOW()
												 order by a.eventdate asc;
	open cur; 
	while stop<>1 && c<num do
			fetch cur into iTime,iMessageId,iResultFlag,iXml; 
			if iMessageId = '0x1003' THEN 
					BEGIN
							set check1003 = 1;
							IF iResultFlag = 1 THEN set check1003 = 0;
							end if;
					END;
			end if;	
			if iMessageId = '0x9004' THEN 
					BEGIN
							set check9004 = 1;
							IF iResultFlag = 1 THEN set check9004 = 0;
							end if;
					END;
			end if;	
			set c = c+1;
  end while; 

  close cur; 
	
	IF check1003 = 1 && check9004 = 1 then set rMsg = 1; 
	end if;
	SELECT rMsg,c,check1003,check9004;
END */$$
DELIMITER ;

/* Procedure structure for procedure `queryTargetGroup` */

/*!50003 DROP PROCEDURE IF EXISTS  `queryTargetGroup` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `queryTargetGroup`(minid int,maxid int,itargettype int)
begin
     set @rownum=0;
     select @rownum:=@rownum+1 as rownum,t.* from TargetGroup t where targettype=itargettype and  (groupid>=minid or minid=-1) and (groupid<=maxid or maxid=-1);
end */$$
DELIMITER ;

/* Procedure structure for procedure `tl_getsheetid` */

/*!50003 DROP PROCEDURE IF EXISTS  `tl_getsheetid` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`admin`@`%` PROCEDURE `tl_getsheetid`(isheettype int,iaddstring varchar(30))
begin 
    declare iprefix       char(2);          
    declare iresetdate    date;             
    declare iResetFlag    int;              
    declare iserialnumber int;           
    declare iserialcount  int;             
    declare iserialstr    varchar(10);
    declare oSheetID      varchar(30);
    declare idatestr      int;
    
    declare exit handler for sqlexception rollback;
    set oSheetID='';
 
    select prefix,resetdate,resetflag,Serialnumber,serialcount into iprefix,iresetdate,iResetFlag,iserialnumber,iserialcount from 
           serialnumber where sheettype=isheettype;
  
    set iaddstring=trim(iaddstring);
    set iprefix=trim(iprefix);
    
    if iprefix!='' then
        set oSheetID=concat(oSheetID,iprefix);
    end if;
    
    if iaddstring!='' then
        set oSheetID=concat(oSheetID,iaddstring);
    end if;
 
    if iResetFlag=1 then
          if  iresetdate<current_date then
                 update serialnumber set resetdate=current_date,serialnumber=0 where sheettype=isheettype;
                 set iresetdate=current_date;
                 set iserialnumber=0;
          end if;
          set idatestr:=year(iresetdate)*10000+month(iresetdate)*100+day(iresetdate);
          set oSheetID=concat(oSheetID,idatestr);
    end if;
 
    if iResetFlag=2 then
         if (year(iresetdate)*100+month(iresetdate))<(year(current_date)*100+month(current_date)) then
                 update serialnumber set resetdate=current_date,serialnumber=0 where sheettype=isheettype;
                 set iresetdate=current_date;
                 set iserialnumber=0;
         end if;
         set idatestr:=year(iresetdate)*100+month(iresetdate);
         set oSheetID=concat(oSheetID,idatestr);
    end if;

    set iserialnumber=iserialnumber+1;
    update serialnumber set serialnumber=iserialnumber where sheettype=isheettype;
    
    set iserialstr=lpad(iserialnumber,iserialcount,'0');
    set oSheetID=concat(oSheetID,iserialstr);
    
    select oSheetID as outstr;
       
end */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
